```sh
git checkout .
git pull origin development
docker compose exec -uroot php-fpm chown -R www-data:www-data /app
docker compose exec php-fpm php artisan down
docker compose exec php-fpm composer install
rm src/bootstrap/cache/*.php
docker compose exec php-fpm php artisan optimize
docker compose exec worker supervisorctl restart laravel-worker:*
docker compose exec worker supervisorctl restart laravel-scheduler:*
docker compose exec php-fpm php artisan up
```