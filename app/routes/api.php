<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\UOMController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



/*
* AUTHENTICATION
*/
Route::group(['prefix' => 'auth',], function ($router) {
	Route::post('login', [AuthController::class, 'login']);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});


Route::group(['prefix' => 'auth', 'middleware' => 'auth:api'], function ($router) {
	Route::post('logout', [AuthController::class, 'logout']);
	Route::post('me', [AuthController::class, 'me']);
	Route::post('refresh', [AuthController::class, 'refresh']);
});

Route::group(['middleware' => ['auth:api']], function ($router) {

	// TAX
	Route::post('importtax', 'TaxController@importTax')->name('api.tax.import');
	Route::post('tax/deletebyselection', 'TaxController@deleteBySelection');
	Route::get('tax/lims_tax_search', 'TaxController@limsTaxSearch')->name('api.tax.search');
	Route::resource('tax', '\App\Http\Controllers\API\TaxController', ['as' => 'api']);


	// UNIT
	Route::post('importunit', '\App\Http\Controllers\API\UOMController@importUnit')->name('api.unit.import');
	Route::post('uom/deletebyselection', '\App\Http\Controllers\API\UOMController@deleteBySelection');
	Route::get('uom/lims_unit_search', '\App\Http\Controllers\API\UOMController@limsUnitSearch')->name('api.unit.search');
	Route::resource('uom', '\App\Http\Controllers\API\UOMController');

	// ROLES
	Route::get('role/permission/{id}', 'RoleController@permission')->name('api.role.permission');
	Route::post('role/set_permission', 'RoleController@setPermission')->name('api.role.setPermission');
	Route::resource('role', '\App\Http\Controllers\API\RoleController', ['as' => 'api']);


	// CATEGORIES
	Route::post('category/import', 'CategoryController@import')->name('api.category.import');
	Route::post('category/deletebyselection', 'CategoryController@deleteBySelection');
	Route::post('category/category-data', 'CategoryController@categoryData');
	Route::resource('category', '\App\Http\Controllers\API\CategoryController', ['as' => 'api']);


	// BRAND
	Route::post('importbrand', 'BrandController@importBrand')->name('api.brand.import');
	Route::post('brand/deletebyselection', 'BrandController@deleteBySelection');
	Route::get('brand/lims_brand_search', 'BrandController@limsBrandSearch')->name('api.brand.search');
	Route::resource('brand', '\App\Http\Controllers\API\BrandController', ['as' => 'api']);


	// SUPPLIER
	Route::post('importsupplier', 'SupplierController@importSupplier')->name('api.supplier.import');
	Route::post('supplier/deletebyselection', 'SupplierController@deleteBySelection');
	Route::post('suppliers/clear-due', 'SupplierController@clearDue')->name('api.supplier.clearDue');
	Route::resource('supplier', '\App\Http\Controllers\API\SupplierController', ['as' => 'api']);



	/*
	* Inventory System
	*/
	Route::group(['prefix' => 'products'], function ($router) {
		// Route::get('/', ['App\Http\Controllers\']);
		Route::post('product-data', 'ProductController@productData');
		Route::get('gencode', 'ProductController@generateCode');
		Route::get('search', 'ProductController@search');
		Route::get('saleunit/{id}', 'ProductController@saleUnit');
		Route::get('getdata/{id}/{variant_id}', 'ProductController@getData');
		Route::get('product_warehouse/{id}', 'ProductController@productWarehouseData');
		Route::post('importproduct', 'ProductController@importProduct')->name('api.product.import');
		Route::post('exportproduct', 'ProductController@exportProduct')->name('api.product.export');
		Route::get('print_barcode', 'ProductController@printBarcode')->name('api.product.printBarcode');
		Route::get('lims_product_search', 'ProductController@limsProductSearch')->name('api.product.search');
		Route::post('deletebyselection', 'ProductController@deleteBySelection');
		Route::post('update', 'ProductController@updateProduct');
		Route::get('variant-data/{id}', 'ProductController@variantData');
		Route::get('history', 'ProductController@history')->name('api.products.history');
		Route::post('sale-history-data', 'ProductController@saleHistoryData');
		Route::post('purchase-history-data', 'ProductController@purchaseHistoryData');
		Route::post('sale-return-history-data', 'ProductController@saleReturnHistoryData');
		Route::post('purchase-return-history-data', 'ProductController@purchaseReturnHistoryData');
		Route::resource('', 'ProductController');
	});


	/*
	* Warehouse
	*/
	Route::post('importwarehouse', 'WarehouseController@importWarehouse')->name('api.warehouse.import');
	Route::post('warehouse/deletebyselection', 'WarehouseController@deleteBySelection');
	Route::get('warehouse/lims_warehouse_search', 'WarehouseController@limsWarehouseSearch')->name('api.warehouse.search');
	Route::resource('warehouse', 'WarehouseController', ['as' => 'api']);
});


/*
    * Inventory Congfiguration Substystem
    * - UOM
    */







/*
* Stock Management Actions
*/



// });

/*
* Purchase Order
*/



/*
* Sales Order
*/




/*
* Users
*/






/*
* System: Tax
*/


/*
* System: Payment Options
*/


/*
* System: Roles/pernissions
*/


/*
* Reports
*/
