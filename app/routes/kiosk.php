<?php

use Illuminate\Support\Facades\Route;

Route::get('/', '\App\Http\Controllers\V1\KioskController@index')->name('kiosk.search');
Route::get('search/products', '\App\Http\Controllers\V1\KioskController@product')->name('kiosk.search.products');
