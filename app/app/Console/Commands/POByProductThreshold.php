<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Purchase\PurchaseRepository;

class POByProductThreshold extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'po:threshold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate PO due to product threshold per warehouse and supplier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PurchaseRepository::createPurchaseOrderPerWarehouseAndSupplier();
    }
}
