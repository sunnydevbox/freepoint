<?php

namespace App\Console\Commands\Fixer;

use App\Enums\SaleStatusEnum;
use App\Models\Product;
use App\Models\Product_Sale;
use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Services\ProductService;
use App\Services\PurchaseOrderService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConvertPoItemsToStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'po:convert-po-items-to-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'FIxer:';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Clear Product_Warehouse
        Product_Warehouse::truncate();

        ProductPurchase::where('product_id', '>', 100)
            ->with(['purchase'])
            ->where('recieved', '>', 0)
            ->each(function ($productPurchase) {
                Product_Warehouse::firstOrCreate(
                    [
                        'product_id' => $productPurchase->product_id,
                        'warehouse_bin_id' => $productPurchase->warehouse_bin_id,
                        'warehouse_id' => $productPurchase->purchase->warehouse_id,
                        'po_item_id' => $productPurchase->id,
                    ],
                    [
                        'price' => $productPurchase->net_unit_cost,
                        'qty' => DB::raw("qty + {$productPurchase->qty}")
                    ]
                );
            });

        Product_Sale::where('product_id', '>', 100)
            ->with(['sale'])
            ->whereHas('sale', function ($query) {
                $query->whereSaleStatus(SaleStatusEnum::completed()->value);
            })
            ->where('qty', '>', 0)
            ->each(function ($productSale) {
                Product_Warehouse::firstOrCreate(
                    [
                        'product_id' => $productSale->product_id,
                        'warehouse_bin_id' => $productSale->warehouse_bin_id,
                        'warehouse_id' => $productSale->sale->warehouse_id,
                        'sale_item_id' => $productSale->id,
                    ],
                    [
                        'price' => $productSale->net_unit_price,
                        'qty' => DB::raw("qty - {$productSale->qty}")
                    ]
                );
            });


        Product::active()
            ->whereHas('inWarehouses')
            ->get()
            ->each(function ($product) {
                (new ProductService)->recalculateProductStock($product->id);
            });

        $this->info('Done');
    }
}
