<?php

namespace App\Console\Commands\Fixer;

use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Services\PurchaseOrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AssignPoItemIdToStockEntry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'po:assign-item-ids-to-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'FIxer:';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $p = ProductPurchase::where('product_id', '>', 100)
            ->each(function ($productPurchase) {
                $stock = Product_Warehouse::whereProductId($productPurchase->product_id)
                    ->whereNull('po_item_id')
                    ->whereWarehouseBinId($productPurchase->warehouse_bin_id)
                    ->whereQty($productPurchase->qty)
                    ->whereBetween('created_at', [$productPurchase->created_at->startOf('month'), $productPurchase->created_at->endOf('month')])
                    ->get();

                // Exact match
                if ($stock->count() == 1) {
                    $stock = $stock->first();
                    var_dump($stock->id, '--');
                }
                // If multiple match, get 
                else if ($stock->count() > 1) {
                    var_dump('mul: ' . $productPurchase->id);

                    $stock = Product_Warehouse::whereProductId($productPurchase->product_id)
                        ->whereWarehouseBinId($productPurchase->warehouse_bin_id)
                        ->whereQty($productPurchase->qty)
                        ->get();

                    // If exact match
                    if ($stock->count() == 1) {
                        var_dump($stock->id);
                        $stock = $stock->first();
                    } else {
                        // dd($stock);
                    }
                    // dd($stock);

                    var_dump('2nd');
                } else {
                    // dd($productPurchase);
                    var_dump('else');
                }

                // dd()

                if (!empty($stock->id)) {
                    $stock->po_item_id = $productPurchase->id;
                    // dd($stock);
                    $stock->save();
                    // var_dump($productPurchase->id, $stock->id);
                }
                // (new PurchaseOrderService)->received($productPurchase, $productPurchase->qty);

            });


        return 0;
    }
}
