<?php

namespace App\Console\Commands\Fixer;

use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Services\PurchaseOrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class POReceiveAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'po:receive-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixer:';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $p = Purchase::with(['items'])
            ->whereHas('items', function($query) {
                $query->where('product_id', '>', 100)
                    ->where('recieved', '!=', 'qty');
            })
            ->get();

            $p = ProductPurchase::where('product_id', '>', 100)
                    ->whereRaw(DB::raw('recieved != qty'))
                    // ->whereRaw(DB::raw('recieved != qty'))
                    ->where('recieved', 0)
                    ->whereNotNull('warehouse_bin_id')
                    ;

            // dd($p->toSql());

                    $p = $p->get();
        //dd($p->count());
    
        $p->each(function($productPurchase) {
            // (new PurchaseOrderService)->received($productPurchase, $productPurchase->qty);

        });
        

        return 0;
    }
}
