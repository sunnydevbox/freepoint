<?php

namespace App\Console\Commands\Fixer;

use App\Enums\SaleStatusEnum;
use App\Models\Sale;
use App\Services\SaleOrderService;
use Illuminate\Console\Command;

class SaleCompleteAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sale:set-to-complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixer: Re-setting sale to complete to trigger the updating of stock count';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sales =Sale::whereSaleStatus(SaleStatusEnum::completed()->value)
            ->whereHas('items.product', function($query) {
                $query->whereIsActive(1);
            })
            ->each(function($sale) {
                // dd($sale->id);
                // dd($sale);
                (new SaleOrderService)->update($sale, []);
                // echo $sale->id;
                // $sale->update([
                //     'sale_status' => SaleStatusEnum::completed()->value
                // ]);
            })
            ;
        // dd($sales->count());
        return 0 ;
    }
}
