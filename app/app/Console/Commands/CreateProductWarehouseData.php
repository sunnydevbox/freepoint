<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\ProductPurchase;
use App\Models\Product_Warehouse;
use DB;

class CreateProductWarehouseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product-warehouse:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate Product Warehouse table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $product_data = Product::where('is_active', true)
                        ->with([
                            'inWarehouses',
                            'inPurchases.purchase'
                        ])
                        ->doesntHave('inWarehouses')
                        ->whereHas('inPurchases.purchase', function($query){
                            $query->where('status', 1);
                        })
                        ->get();
        
        foreach($product_data as $product) {
            foreach($product->inPurchases as $purchaseOrder) {

                $qty = $purchaseOrder->qty;

                $PW = Product_Warehouse::updateOrCreate(
                    [
                        'product_id'        => $purchaseOrder->product_id,
                        'warehouse_id'      => $purchaseOrder->purchase->warehouse_id,
    
                    ],
                [
                    'qty'               => $qty,
                    'warehouse_bin_id'  => $purchaseOrder->warehouse_bin_id
                ]);

                $PW->qty = $PW->qty + $purchaseOrder->qty;
                $PW->save();
            }
        }
       
    }
}
