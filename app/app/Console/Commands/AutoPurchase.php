<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Purchase\PurchaseRepository;

class AutoPurchase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic purchase if the qty exeeds alert qty';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PurchaseRepository::autoPurchase();
    }
}
