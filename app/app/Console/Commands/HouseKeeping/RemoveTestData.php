<?php

namespace App\Console\Commands\HouseKeeping;

use App\Enums\SaleStatusEnum;
use App\Models\Sale;
use App\Models\User;
use App\Services\SaleOrderService;
use Illuminate\Console\Command;

class RemoveTestData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'housekeeping:remove-test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove test data when the app was initially deployed';

    /**
     * Create a new command instance.
     *sd
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Users
        User::whereBetween('id', 2, 51)->delete();
        
        return 0 ;
    }
}
