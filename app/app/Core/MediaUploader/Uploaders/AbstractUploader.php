<?php
namespace App\Core\MediaUploader\Uploaders;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractUploader
{
    protected self $model;
    protected self $file;
    protected self $config;
    
    public function __construct(
        Model $model, 
        $file, 
        array $config = []
    ) {

    }
}