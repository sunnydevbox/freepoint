<?php
namespace App\Core\MediaUploader\Uploaders;

class BnbAttachmentUploader extends AbstractUploader
{
    public function upload($model, $file, $config)
    {
        $config = [];

        if (!empty(self::$mediaUploader::getGroupName())) {
            $config['group'] = self::$mediaUploader::getGroupName();
        }

        if (!empty(self::$mediaUploader::getKeyName())) {
            $config['key'] = self::$mediaUploader::getKeyName();
        }

        return $model->attach($file, $config);
    }
}