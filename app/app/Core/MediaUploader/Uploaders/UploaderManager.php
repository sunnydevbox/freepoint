<?php
namespace App\Core\MediaUploader\Uploaders;

use Exception;
use Illuminate\Database\Eloquent\Model;

class UploaderManager
{
 
    public function __construct(string $type, Model $model, $file, array $config = [])
    {
        switch($type) {
            case 'bnbattachment':
                    return new BnbAttachmentUploader($model, $file, $config);
                break;

            default:
                throw new Exception('invalid uploader selected');
                break;
        }
    }
}