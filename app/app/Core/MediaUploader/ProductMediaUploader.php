<?php
namespace App\Core\MediaUploader;

use Illuminate\Database\Eloquent\Model;
use App\Core\MediaUploader\MediaUploaderInterface;

class ProductMediaUploader extends AbstractMediaUploader implements MediaUploaderInterface
{
    public static $model = null;
    public static $file = null;

    public static function getGroupName(): string
    {
        return 'product';
    }

    public static function getKeyName(): string
    {
        return "product-" . self::$model->id;
    }

    public function upload($file)
    {
        $config = [];

        if (!empty(self::getGroupName())) {
            $config['group'] = self::getGroupName();
        } 

        if (!empty(self::getKeyName())) {
            $config['key'] = self::getKeyName();
        } 

        return self::$model->attach($file, $config);
    }

    public function __construct(Model $model)
    {
        self::$model = $model;
    }
}