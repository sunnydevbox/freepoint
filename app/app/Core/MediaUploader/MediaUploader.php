<?php

namespace App\Core\MediaUploader;

use App\Core\MediaUploader\Uploaders\UplodaerManager;

class MediaUploader
{
    protected static MediaUploaderInterface $mediaUploader;
    protected static $file;
    protected static string $uploaderType;

    public function upload()
    {
        $uploader = new UplodaerManager(
                    self::$uploaderType,
                    self::$mediaUploader::$model,
                    self::$file
        );
        return $uploader->upload();
    }

    public function uploader($uploaderType)
    {
        self::$uploaderType = $uploaderType;
        return $this;
    }

    public function __construct(MediaUploaderInterface $mediaUploader, $file)
    {
        self::$mediaUploader = $mediaUploader;
        self::$file = $file;
    }
}
