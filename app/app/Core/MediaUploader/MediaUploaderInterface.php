<?php
namespace App\Core\MediaUploader;

interface MediaUploaderInterface
{
    public static function getGroupName(): string;

    public static function getKeyName(): string;
}