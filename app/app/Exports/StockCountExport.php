<?php

namespace App\Exports;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class StockCountExport implements FromCollection, WithStrictNullComparison, WithMapping, WithHeadings
{
    public function __construct(public $warehouseId, public $binId = null)
    {
        // dd($warehouseId, $binId);
    }

    public function map($product): array
    {
        // dd($product);
        return [
            'Product Name' => $product->product_name,
            'Product Code' => $product->product_code,
            'Product Model' => $product->product_model,
            'Warehouse' => $product->warehouse_name,
            'Bin' => $product->bin_name,
            'Stock Count' => $product->sum_qty,
        ];
    }

    public function headings(): array
    {
        return ['Product Name', 'Product Code', 'Model', 'Warehouse', 'Bin', 'Stock Count'];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $q = Product::query()
            ->select([
                'products.id',
                'product_warehouse.warehouse_bin_id',
                'products.name AS product_name',
                'products.code AS product_code',
                'products.model AS product_model',
                DB::raw('SUM(product_warehouse.qty) AS sum_qty'),
                DB::raw('warehouses.name AS warehouse_name'),
                DB::raw('warehouse_bins.name AS bin_name')
            ])
            ->join('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
            ->join('warehouses', 'warehouses.id', '=', 'product_warehouse.warehouse_id')
            ->join('warehouse_bins', 'warehouse_bins.id', '=', 'product_warehouse.warehouse_bin_id')
            ->where('products.is_active', 1)
            ->groupBy(['products.id', 'product_warehouse.warehouse_bin_id', 'product_warehouse.warehouse_id'])
            ->orderBy('products.name')
            ->when($this->warehouseId, fn($query) => $query->where('product_warehouse.warehouse_id', $this->warehouseId))
            ->when($this->binId, fn($query) => $query->where('product_warehouse.warehouse_bin_id', $this->binId))
        ;

        return $q->get();
    }
}
