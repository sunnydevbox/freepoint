<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self admin()
 */
final class RoleEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'admin' => 1,
        ];
    }

    protected static function labels(): array
    {
        return [
            'admin' => 'Admin',
        ];
    }
}
