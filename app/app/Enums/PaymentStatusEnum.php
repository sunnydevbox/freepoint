<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self paid()
 * @method static self due()
 * @method static self pending()
 * @method static self partial()
 */
final class PaymentStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'pending' => 1,
            'due' => 2,
            'partial' => 3,
            'paid'  => 4,
        ];
    }

    protected static function labels(): array
    {
        return [
            'paid' => trans('file.Paid'),
            'due' => trans('file.Due'),
            'pending' => trans('file.Pending'),
            'partial' => trans('file.Partial'),
        ];
    }
}
