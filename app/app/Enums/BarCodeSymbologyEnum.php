<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self C128()
 * @method static self C39()
 * @method static self UPCA()
 * @method static self UPCE()
 * @method static self EAN8()
 * @method static self EAN13()
 */
final class BarCodeSymbologyEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'C128' => 'C128',
            'C39' => 'C39',
            'UPCA' => 'UPCA',
            'UPCE' => 'UPCE',
            'EAN8' => 'EAN8',
            'EAN13' => 'EAN13',
        ];
    }

    protected static function labels(): array
    {
        return [
            'C128' => 'Code 128',
            'C39' => 'Code 39',
            'UPCA' => 'UPC-A',
            'UPCE' => 'UPC-E',
            'EAN8' => 'EAN-8',
            'EAN13' => 'EAN-13',
        ];
    }
}
