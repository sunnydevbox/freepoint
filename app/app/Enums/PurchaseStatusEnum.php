<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self received()
 * @method static self partial()
 * @method static self pending()
 * @method static self ordered()
 */
final class PurchaseStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'received' => 1, // completed
            'partial' => 2, // some items have been delivered
            'pending' => 3,
            'ordered' => 4,
        ];
    }

    protected static function labels(): array
    {
        return [
            'received' => trans('file.Recieved'),
            'partial' => trans('file.Partial'),
            'pending' => trans('file.Pending'),
            'ordered' => trans('file.Ordered'),
        ];
    }
}
