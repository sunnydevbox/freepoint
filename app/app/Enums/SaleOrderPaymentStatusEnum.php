<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self pending()
 * @method static self due()
 * @method static self partial()
 * @method static self paid()
 */
final class SaleOrderPaymentStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'pending' => 1,
            'due' => 2,
            'partial' => 3,
            'paid' => 4
        ];
    }

    protected static function labels(): array
    {
        return [
            'pending' => trans('file.Pending'),
            'due' => trans('file.Due'),
            'parital' => trans('file.Partial'),
            'paid' => trans('file.Paid'),
        ];
    }
}