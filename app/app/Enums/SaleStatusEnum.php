<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self completed()
 * @method static self pending()
 * @method static self canceled()
 */
final class SaleStatusEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'completed' => 1, // completed
            'pending' => 2, // some items have been delivered
            'canceled' => 3,
        ];
    }

    protected static function labels(): array
    {
        return [
            'completed' => trans('file.Completed'),
            'pending' => trans('file.Pending'),
            'canceled' => trans('file.Cancelled'),
        ];
    }
}