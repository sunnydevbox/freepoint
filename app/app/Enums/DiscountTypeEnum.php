<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self percentage()
 * @method static self flat()
 */
class DiscountTypeEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'percentage' => 'percentage',
            'flat' => 'flat',
        ];
    }

    protected static function labels(): array
    {
        return [
            'percentage' => 'Percentage',
            'flat' => 'Flat',
        ];
    }
}