<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self cash()
 * @method static selasdf gift_card()
 * @method static selasdf card()
 * @method static self cheque()
 * @method static selasdf paypal()
 * @method static selasdf deposit()
 * @method static selasdf points()
 */
final class PaymentMethodEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'cash' => 1,
            'gift_card' => 2,
            'card' => 3,
            'cheque' => 4,
            'paypal' => 5,
            'deposit' => 6,
            'points' => 7,
        ];
    }

    protected static function labels(): array
    {
        return [
            'cash' => 'Cash',
            'gift_card' => 'Gift Card',
            'card' => 'Credit Card',
            'cheque' => 'Cheque',
            'paypal' => 'Paypal',
            'deposit' => 'Deposit',
            'points' => 'Points',
        ];
    }
}
