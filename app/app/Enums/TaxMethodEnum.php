<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self inclusive()
 * @method static self exclusive()
 */
final class TaxMethodEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'exclusive' => 1,
            'inclusive' => 2,
        ];
    }

    protected static function labels(): array
    {
        return [
            'exclusive' => 'Exclusive',
            'inclusive' => 'Inclusive',
        ];
    }
}
