<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self standard()
 * @method static self combo()
 * @method static self digital()
 * @method static self service()
 */
class ProductTypeEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'standard' => 'standard',
            'combo' => 'combo',
            'digital' => 'digital',
            'service' => 'sercvice',
        ];
    }

    protected static function labels(): array
    {
        return [
            'standard' => 'Standard',
            'combo' => 'Combo',
            'digital' => 'Digital',
            'service' => 'Sercvice',
        ];
    }
}
