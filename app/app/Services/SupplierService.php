<?php

namespace App\Services;

use App\Repositories\Supplier\SupplierRepository;

class SupplierService extends AbstractBREADService
{
    public function repository()
    {
        return app(SupplierRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'name' => 'required|min:1|max:100',
                'image' => 'image|mimes:jpg,jpeg,png,gif',
                'company_name' => 'required|min:1|:max:100',
                'vat_number' => '',
                'email' => 'required|email',
                'phone_number' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => '',
                'postal_code' => '',
                'country' => '',
            ],
            'UPDATE_RECORD' => [
                'name' => 'min:1|max:100',
                'image' => 'image|mimes:jpg,jpeg,png,gif',
                'company_name' => 'min:1|:max:100',
                'vat_number' => '',
                'email' => 'email',
                'phone_number' => '',
                'address' => '',
                'city' => '',
                'state' => '',
                'postal_code' => '',
                'country' => '',
            ]
        ]);
    }
}
