<?php

namespace App\Services;

use App\Enums\PaymentStatusEnum;
use App\Enums\PurchaseStatusEnum;
use App\Http\Livewire\Product\Form\Product;
use App\Models\Payment;
use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Models\WarehouseBin;
use Exception;
use Illuminate\Support\Facades\DB;

use App\Services\Traits\PurchaseOrderCalculationTrait;
use App\Services\Traits\PurchaseOrderItemTrait;
use Illuminate\Validation\ValidationException;

class PurchaseOrderService
{
    use PurchaseOrderCalculationTrait, PurchaseOrderItemTrait;
    

    public function setStatus(Purchase $purchase)
    {
        // Order
        /*
         'received' => 1, // completed
            'partial' => 2, // some items have been delivered
            'pending' => 3,
            'ordered' => 4,
        */
        // 3, 4, 2, 1

        dd(PurchaseStatusEnum::toArray());

    }


    public function update(Purchase $purchase, $data)
    {
        try {
            DB::beginTransaction();
            $purchase->update($data);

            $purchase = $purchase->fresh();

            $this->recalculatePurchase($purchase);

            DB::commit();
            return $purchase->fresh();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function addPayment(Purchase $purchase, $data)
    {
        try {
            // Validate
            // 1) amount must not exceeed the remaining balance


            if (abs($purchase->balance / $purchase->balance) < abs($data['paid_amount'] / $data['paid_amount'])) {
                throw new ValidationException('Paid amount must not exceed', 400);
            }


            // dd($purchase, $purchase->balance, $data);
            $purchase->paid_amount += $data['paid_amount'];
            $balance = $purchase->grand_total - $purchase->paid_amount;

            DB::beginTransaction();

            $purchase->payment_status = ($balance == 0)
                ? PaymentStatusEnum::paid()->value
                : PaymentStatusEnum::due()->value;

            if ($data['paid_by_id'] == 1)
                $paying_method = 'Cash';
            elseif ($data['paid_by_id'] == 2)
                $paying_method = 'Gift Card';
            elseif ($data['paid_by_id'] == 3)
                $paying_method = 'Credit Card';
            else
                $paying_method = 'Cheque';

            $payment = new Payment();
            $payment->user_id = auth()->user()->id;
            $payment->purchase_id = $purchase->id;
            $payment->account_id = $data['account_id'];
            $payment->payment_reference = 'ppr-' . date("Ymd") . '-' . date("his");
            $payment->amount = $data['paid_amount'];
            $payment->change = $data['paying_amount'] - $data['paid_amount'];
            $payment->paying_method = $paying_method;
            $payment->payment_note = $data['payment_note'];

            $payment->save();

            $purchase->save();
            DB::commit();
            return $purchase->fresh();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete(Purchase $purchase)
    {
        try {
            DB::beginTransaction();

            // (new PurchaseOrderItemSer)
            // 1. Subtract the amount of the P.O. items from product warehouse
            $purchase->items->each(function($item) {
                
                if (!empty($item->received)) {
                    // dd($item->received, !empty($item->received));
                    $this->takeItems($item, $item->received);
                }
                
            });

            $purchase->delete();

            //

            // finally, delete the p.o,.


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
