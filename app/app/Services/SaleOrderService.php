<?php

namespace App\Services;

use App\Enums\PaymentStatusEnum;
use App\Enums\SaleStatusEnum;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Product_Sale;
use App\Models\Product_Warehouse;
use App\Models\Sale;
use App\Services\Traits\SaleOrderItemTrait;
use App\Services\Traits\SaleOrderPaymentTriat;
use Exception;
use Illuminate\Support\Facades\DB;

use App\Services\Traits\SaleOrderCalculationTrait;
use Illuminate\Validation\ValidationException;

class SaleOrderService
{
    use SaleOrderCalculationTrait;
    use SaleOrderItemTrait;
    use SaleOrderPaymentTriat;

    public function update(Sale $sale, $data)
    {
        try {
            DB::beginTransaction();
            $sale->update($data);

            $sale = $sale->fresh();

            $this->recalculateSale($sale);

            $this->processCompletedStatus($sale);

            DB::commit();
            return $sale->fresh();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function processCompletedStatus(Sale $sale)
    {
        if ($sale->sale_status == SaleStatusEnum::tryFrom('completed')->value) {
            // start subtracting from stock
            $sale->items->each(function ($saleOrderItem) use ($sale) {
                $newQty = (0 - $saleOrderItem->qty);
                (new ProductWarehouseService)->addItem(
                    [
                        'warehouse_id' => $sale->warehouse_id,
                        'warehouse_bin_id' => $saleOrderItem->warehouse_bin_id,
                        'sale_item_id' => $saleOrderItem->id,
                        'product_id' => $saleOrderItem->product_id,
                    ],
                    $newQty
                );



                // // Move this to it's trait
                // $p = Product_Warehouse::firstOrCreate(
                //     [
                //         'warehouse_id' => $sale->warehouse_id,
                //         'sale_item_id' => $saleOrderItem->id,
                //         'product_id' => $saleOrderItem->product_id,
                //     ],
                //     [
                //         'qty' => (0 - $saleOrderItem->qty)
                //     ]
                // );

                // (new ProductWarehouseService)->recalculateProductQuantity($saleOrderItem->product_id);
            });
        }
    }

    // public function removeItem($saleItems)
    // {
    //     try {
    //         DB::beginTransaction();

    //         if (is_array($saleItems)) {
    //             $sale = Product_Sale::with('sale')->find($saleItems[0])->sale;
    //             Product_Sale::whereIn('id', $saleItems)->delete();
    //         } else if (is_numeric($saleItems)) {
    //             $sale = Product_Sale::with('sale')->find($saleItems)->sale;
    //             Product_Sale::where('id', $saleItems)->delete();
    //         } else if ($saleItems instanceof Product_Sale) {
    //             $sale = $saleItems->sale;
    //             $saleItems->delete();
    //         }

    //         $this->recalculateSale($sale);

    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         throw $e;
    //     }
    // }

    // TODO this should be in a helper file
    public function generateReferenceCode()
    {
        return 'sr-' . date("Ymd") . '-' . date("his");
    }

    // TODO this should be in a helper file
    public function generatePaymentReferenceCode()
    {
        return 'spr-' . date("Ymd") . '-' . date("his");
    }

    public function create($data)
    {
        try {
            DB::beginTransaction();

            if (empty($data['user_id'])) {
                $data['user_id'] = auth()->user()->id;
            }

            if (empty($data['payment_status'])) {
                $data['payment_status'] = PaymentStatusEnum::due()->value;
            }

            if (empty($data['reference_no'])) {
                $data['reference_no'] = $this->generateReferenceCode();
            }

            $saleOrder = Sale::create($data);

            DB::commit();

            return $saleOrder;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function updateSale(Sale $sale, $data)
    {
        $this->update($sale, $data);
    }

    public function destroy(Sale|int $sale)
    {
        try {
            if (is_numeric($sale)) {
                $sale = Sale::with(['items.product'])->findOrFail($sale);
            }


            DB::beginTransaction();


            // Delete Sale, Sale Item, Product warehouse - sale item
            $saleItemIds = [];
            $productIds = [];

            $sale->items->each(function ($row) use (&$productIds, &$saleItemIds) {
                $saleItemIds[] = $row->id;
                $productIds[] = $row->product_id;

                $row->stocks->each(fn($stock) => $stock->delete());
                $row->delete();
            });

            $sale->delete();

            collect($productIds)->each(fn($productId) => (new ProductWarehouseService)->recalculateProductStockCount($productId));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
