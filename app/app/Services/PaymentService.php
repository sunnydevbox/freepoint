<?php

namespace App\Services;

use App\Enums\PaymentMethodEnum;
use App\Enums\PaymentStatusEnum;
use App\Events\Payment\PaymentAdded;
use App\Models\Payment;
use App\Models\PaymentWithCheque;
use App\Models\Purchase;
use App\Models\Sale;
use Exception;
use Illuminate\Support\Facades\DB;

class PaymentService
{
    public function addPayment_(PaymentMethodEnum $paymentMethod, Sale|Purchase $model, array $data = [])
    {
        dd($paymentMethod, $model, $data);
        $data = $request->all();
        if (!$data['amount'])
            $data['amount'] = 0.00;

        $lims_sale_data = Sale::find($data['sale_id']);
        $lims_customer_data = Customer::find($lims_sale_data->customer_id);
        $lims_sale_data->paid_amount += $data['amount'];
        $balance = $lims_sale_data->grand_total - $lims_sale_data->paid_amount;
        if ($balance > 0 || $balance < 0)
            $lims_sale_data->payment_status = 2;
        elseif ($balance == 0)
            $lims_sale_data->payment_status = 4;

        if ($data['paid_by_id'] == 1)
            $paying_method = 'Cash';
        elseif ($data['paid_by_id'] == 2)
            $paying_method = 'Gift Card';
        elseif ($data['paid_by_id'] == 3)
            $paying_method = 'Credit Card';
        elseif ($data['paid_by_id'] == 4)
            $paying_method = 'Cheque';
        elseif ($data['paid_by_id'] == 5)
            $paying_method = 'Paypal';
        elseif ($data['paid_by_id'] == 6)
            $paying_method = 'Deposit';
        elseif ($data['paid_by_id'] == 7)
            $paying_method = 'Points';


        $cash_register_data = CashRegister::where([
            ['user_id', Auth::id()],
            ['warehouse_id', $lims_sale_data->warehouse_id],
            ['status', true],
        ])->first();

        $lims_payment_data = new Payment();
        $lims_payment_data->user_id = Auth::id();
        $lims_payment_data->sale_id = $lims_sale_data->id;
        if ($cash_register_data)
            $lims_payment_data->cash_register_id = $cash_register_data->id;
        $lims_payment_data->account_id = $data['account_id'];
        $data['payment_reference'] = 'spr-' . date("Ymd") . '-' . date("his");
        $lims_payment_data->payment_reference = $data['payment_reference'];
        $lims_payment_data->amount = $data['amount'];
        $lims_payment_data->change = $data['paying_amount'] - $data['amount'];
        $lims_payment_data->paying_method = $paying_method;
        $lims_payment_data->payment_note = $data['payment_note'];
        $lims_payment_data->save();
        $lims_sale_data->save();
    }

    public function addPayment(PaymentMethodEnum $paymentMethod, Sale|Purchase $model, array $data = [])
    // (Sale $sale, $data)
    {
        try {
            // Validate
            // 1) amount must not exceeed the remaining balance


            // if (abs($model->balance/$model->balance) < abs($data['paid_amount']/$data['paid_amount'])) {
            //     throw new ValidationException('Paid amount must not exceed', 400);
            // }
            DB::beginTransaction();



            if (empty($data['payment_reference'])) {
                $data['payment_reference'] = $this->generatePaymentReferenceCode($model);
            }

            if (empty($data['user_id'])) {
                $data['user_id'] = auth()->user()->id;
            }

            $data['change'] = $data['paying_amount'] - $data['paid_amount'];
            $data['paying_method'] = $paymentMethod->label; // @deprecated in favor of payment_method_id
            $data['payment_method_id'] = PaymentMethodEnum::tryFrom($paymentMethod->value);
            $data['amount'] = $data['paid_amount'];

            $payment = $model->payments()->create($data);

            event(new PaymentAdded($payment));

            $this->paymentModePostProcess($payment, $data);
            DB::commit();
            return $model->fresh();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function paymentModePostProcess(Payment $payment, array $data = [])
    {
        try {
            switch ($payment->payment_method->value) {
                case PaymentMethodEnum::cheque()->value:
                    $data = collect($data)->only('cheque_no')->toArray();

                    if (empty($data)) {
                        throw new Exception('Additional data missing for payment mode processing', 400);
                    }

                    PaymentWithCheque::updateOrCreate([
                        'payment_id' => $payment->id,
                        'cheque_no' => $data['cheque_no'],
                    ]);
                    break;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function generatePaymentReferenceCode(Sale|Purchase $modelType)
    {
        $prefix = 'payment';
        if ($modelType instanceof Sale) {
            $prefix = 'spr';
        } else if ($modelType instanceof Purchase) {
            $prefix = 'ppr';
        }

        return "{$prefix}-" . date("Ymd") . '-' . date("his");
    }
}
