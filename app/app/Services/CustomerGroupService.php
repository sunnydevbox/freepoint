<?php

namespace App\Services;

use App\Models\CustomerGroup;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\CustomerGroup\CustomerGroupRepository;

class CustomerGroupService
{
    public static function getRepository(): CustomerGroupRepository
    {
        return new CustomerGroupRepository;
    }
}