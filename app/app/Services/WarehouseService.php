<?php
namespace App\Services;

use App\Repositories\Warehouse\WarehouseRepository;

class WarehouseService extends AbstractBREADService implements ServiceInterface
{
    public function repository()
    {
        return app(WarehouseRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'name' => 'required|min:3|max:100',
                'address' => 'required|min:3|max:100',
            ],
            'UPDATE_RECORD' => [
                'name' => 'min:3|max:100',
                'address' => 'min:3|max:100',
            ],
        ]);
    }
}