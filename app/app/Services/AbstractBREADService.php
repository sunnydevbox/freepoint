<?php

namespace App\Services;

use App\Traits\Services\BaseBreadValidationTrait;
use Exception;


abstract class AbstractBREADService implements ServiceInterface
{
    use BaseBreadValidationTrait;

    protected static $repository = null;

    protected $validationRules = [
        'CREATE_RECORD' => [],
        'UPDATE_RECORD' => [],
        'DELETE_RECORD' => [],
        'BROWSE_RECORDS' => [
            'limit' => 'numeric|max:300',
            'page' => 'required|numeric'
        ],
    ];

    abstract public function repository();
    abstract public function rules();

    public function browse($params)
    {
        $this->validate('BROWSE_RECORDS', $params);
        $result = self::$repository->paginate($params['page']);
        return $result;
    }

    public function read($id)
    {
        return self::$repository->find($id);
    }

    public function edit($id, $params)
    {
        try {
            $this->validate('UPDATE_RECORD', $params);
            return self::$repository->update($id, $params);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function add($params)
    {
        try {
            if ($this->validate('CREATE_RECORD', $params)) {
                return self::$repository->create($params);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    

    public function destroy($id)
    {
        dd(3);
        $this->validate('DELETE_RECORD', ['id' => $id]);
        return self::$repository->delete($id);
    }

    public function __construct()
    {
        self::$repository = $this->repository();
    }
}
