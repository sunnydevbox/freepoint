<?php

namespace App\Services\Traits;

use App\Enums\PaymentStatusEnum;
use App\Enums\TaxMethodEnum;
use App\Models\Product_Sale;
use App\Models\ProductPurchase;
use App\Models\Sale;
use Exception;
use Illuminate\Support\Facades\DB;

trait SaleOrderCalculationTrait
{
    public function computeItem(Product_Sale $saleItem)
    {
        $product = $saleItem->product;
        $productPrice = $saleItem->net_unit_price ?: $product->price;

        if (empty($product->tax_method) || $product->tax_method == TaxMethodEnum::exclusive()->value) {
            $netUnitCost = $productPrice - $saleItem->discount;
            // (363.64*2) * .10
            $tax = $netUnitCost * $saleItem->qty * ($saleItem->tax_rate / 100);
            $subTotal = ($netUnitCost * $saleItem->qty) + $tax;
        } else {
            $qty = (float) $saleItem->qty;
            $subTotalUnit = $productPrice - $saleItem->discount;
            $netUnitCost = (100 / (100 + $saleItem->tax_rate)) * $subTotalUnit;
            $tax = ($subTotalUnit - $netUnitCost) * $qty;

            $subTotal = $subTotalUnit * $qty;
        }


        $data = [
            'net_unit_price' => $productPrice,
            'tax' => $tax,
            'total' => $subTotal,
        ];

        return $data;
    }

    public function recalculateItem(Product_Sale $saleItem)
    {
        $saleItem->fill($this->computeItem($saleItem));
        $saleItem->save();
        return $saleItem;
    }

    public function recalculateSale(Sale $sale)
    {
        $saleItems = $sale->load('items')->items;

        $saleItems->each(fn($saleItem) => $this->recalculateItem($saleItem));

        $sale->item = $saleItems->count();
        $sale->total_tax = $saleItems->sum('tax');
        $sale->total_discount = $saleItems->sum('discount');
        $sale->total_qty = $saleItems->sum('qty');
        $sale->total_price = $saleItems->sum('total');

        $sale->order_tax = ($sale->total_price - $sale->order_discount) * ($sale->order_tax_rate / 100);
        $sale->grand_total = ($sale->total_price + $sale->order_tax + $sale->shipping_cost) - $sale->order_discount;

        // $lims_sale_data->order_tax = ($lims_sale_data->total_price - $lims_sale_data->order_discount) * ($data['order_tax_rate'] / 100);
        // $lims_sale_data->grand_total = ($lims_sale_data->total_price + $lims_sale_data->order_tax + $lims_sale_data->shipping_cost) - $lims_sale_data->order_discount;

        $sale->save();
    }

    public function calculatePayments(Sale $sale)
    {
        try {
            DB::beginTransaction();

            $totalAmount = $sale->payments->reduce(function ($total, $payment) {
                return $total + $payment->amount;
            }, 0);

            // Calculated Final Balanace
            $balance = $sale->grand_total - $totalAmount;

            $paymentStatus = ($balance == 0)
                ? PaymentStatusEnum::paid()
                : PaymentStatusEnum::due();

            $sale->update([
                'paid_amount' => $totalAmount,
                'payment_status' => $paymentStatus,
            ]);
            $sale->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }
}
