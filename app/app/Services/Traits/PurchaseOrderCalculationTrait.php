<?php
namespace App\Services\Traits;
use App\Enums\TaxMethodEnum;
use App\Models\ProductPurchase;
use App\Models\Purchase;

trait PurchaseOrderCalculationTrait
{
    public function computeItem(ProductPurchase $productPurchase)
    {
        $product = $productPurchase->product;
        
        // Product subtotal computation
        if ($product->tax_method == TaxMethodEnum::exclusive()->value) {
            $netUnitCost = $product->cost - $productPurchase->discount;
            $tax = $netUnitCost * $productPurchase->qty * ($productPurchase->tax_rate / 100);
            $subTotal = ($netUnitCost * $productPurchase->qty) + $tax;
        } else {
            $qty = (float) $productPurchase->qty;
            $subTotalUnit = $productPurchase->net_unit_cost - $productPurchase->discount;
            $netUnitCost = (100 / (100 + $productPurchase->tax_rate)) * $subTotalUnit;
            $tax = ($subTotalUnit - $netUnitCost) * $qty;
            $subTotal = $subTotalUnit * $qty;
        }

        return $subTotal;
    }

    public function recalculateItem(ProductPurchase $productPurchase)
    {        
        $productPurchase->total = $this->computeItem($productPurchase);
        $productPurchase->save();
    }

    public function recalculatePurchase(Purchase $purchase)
    {
        $items = $purchase->load('items')->items;

        $purchase->item = $items->count();
        $purchase->total_qty = $items->sum('qty');
        $purchase->total_cost = $items->sum('total');
        // $purchase->total_discount = $items->sum('discount');
        // $purchase->total_tax = $items->sum('tax');

        $purchase->order_tax = ($purchase->total_cost - $purchase->order_discount) * ($purchase->order_tax_rate / 100);
        $purchase->grand_total = ($purchase->total_cost + $purchase->order_tax + $purchase->shipping_cost) - $purchase->order_discount;
        // (999 - 8) * 5/100
        /*
        $qty = (float) $productPurchase->qty;
            $subTotalUnit = $productPurchase->net_unit_cost - $productPurchase->discount;
            $netUnitCost = (100 / (100 + $productPurchase->tax_rate)) * $subTotalUnit;
            $tax = ($subTotalUnit - $netUnitCost) * $qty;
            $subTotal = $subTotalUnit * $qty;
        */


        $purchase->save();
    }
}