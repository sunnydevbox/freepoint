<?php
namespace App\Services\Traits;

use App\Enums\PaymentStatusEnum;
use App\Models\Sale;
use Exception;
use Illuminate\Support\Facades\DB;

trait SaleOrderPaymentTriat {
    public function addPayment(Sale $sale, $data)
    {
        try {
            // Validate
            // 1) amount must not exceeed the remaining balance


            // if (abs($sale->balance/$sale->balance) < abs($data['paid_amount']/$data['paid_amount'])) {
            //     throw new ValidationException('Paid amount must not exceed', 400);
            // }


            // dd($sale, $sale->balance, $data);
            $sale->paid_amount += $data['paid_amount'];
            $balance = $sale->grand_total - $sale->paid_amount;

            DB::beginTransaction();

            $sale->payment_status = ($balance == 0)
                ? PaymentStatusEnum::paid()->value
                : PaymentStatusEnum::due()->value;

            if ($data['paid_by_id'] == 1)
                $paying_method = 'Cash';
            elseif ($data['paid_by_id'] == 2)
                $paying_method = 'Gift Card';
            elseif ($data['paid_by_id'] == 3)
                $paying_method = 'Credit Card';
            else
                $paying_method = 'Cheque';

            if (empty($data['payment_reference'])) {
                $data['payment_reference'] = $this->generatePaymentReferenceCode();
            }

            if (empty($data['user_id'])) {
                $data['user_id'] = auth()->user()->id;
            }

            $data['change'] = $data['paying_amount'] - $data['paid_amount'];
            $data['paying_method'] = $paying_method;
            $data['amount'] = $data['paid_amount'];

            $sale->payments()->create($data);

            $sale->save();
            DB::commit();
            return $sale->fresh();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function editPayment($id, $data)
    {}

}