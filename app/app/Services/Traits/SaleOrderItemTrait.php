<?php

namespace App\Services\Traits;

use App\Enums\SaleStatusEnum;
use App\Models\Product;
use App\Models\Product_Sale;
use App\Models\Product_Warehouse;
use App\Models\Sale;
use App\Services\ProductWarehouseService;
use Exception;
use Illuminate\Support\Facades\DB;

trait SaleOrderItemTrait
{
    public function addItem(Sale $sale, Product $product, $qty = 1, $parameters = [])
    {
        try {
            DB::beginTransaction();

            $discount = 0;
            $item = $sale->items()->firstOrCreate(
                array_merge(
                    [
                        'product_id' => $product->id,
                        'sale_id' => $sale->id,
                        'sale_unit_id' => $product->saleUnit->id,
                    ],
                    $parameters
                ),
                [
                    'qty' => $qty,
                    'discount' => $discount,
                    'tax_rate' => 0,
                    'tax' => 0,
                    'total' => 0,
                    'net_unit_price' => $product->price - $discount,
                ]

            );

            $item->save();
            $this->recalculateSale($sale);

            if ($sale->sale_status == SaleStatusEnum::completed()->value) {
                $item = $item->stocks()->updateOrCreate(
                    [
                        'warehouse_id' => $sale->warehouse_id,
                        'product_id' => $item->product_id,
                        'warehouse_bin_id' => $item->warehouse_bin_id,
                        'sale_item_id' => $item->id,

                    ],
                    [
                        'qty' => (0 - $item->qty),
                    ]
                );
            }
            DB::commit();
            return $item;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    // TODO:
    public function takeItem(Product_Sale $productSale)
    {
    }

    public function removeItem($saleItems)
    {
        try {
            DB::beginTransaction();

            if (is_array($saleItems)) {
                $items = Product_Sale::with(['sale'])->whereIn('id', $saleItems)->get();
                $sale = $items->first()->sale;
                $productIds = $items->mapWithKeys(fn ($item) => [$item->product_id])->toArray();
                $items->each(fn ($item) => $item->delete());
            } else if (is_numeric($saleItems)) {
                $item = Product_Sale::with(['sale'])->where('id', $saleItems)->get();
                $sale = $item->sale;
                $productIds = [$item->product_id];
                $item->delete();
            } else if ($saleItems instanceof Product_Sale) {
                $productIds = [$saleItems->product_id];
                $sale = $saleItems->sale;
                $saleItems->delete();
            }

            collect($productIds)->each(fn ($productId) => (new ProductWarehouseService)->recalculateProductStockCount($productId));

            // $this->recalculateSale($sale);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateQty(Product_Sale $saleItem, $qty, bool $toIncrement = false): Product_Sale
    {
        try {

            DB::beginTransaction();

            $newQty = $qty;
            if ($toIncrement) {
                $newQty = 0 - (abs($saleItem->qty) + $qty);
            }

            $saleItem->update([
                'qty' => $newQty
            ]);
          
            // Update product warehouse
            $stock = Product_Warehouse::whereSaleItemId($saleItem->id)->first();
            if ($stock) {
                $stock->update(['qty' => (0 - $newQty)]);
            }

            (new ProductWarehouseService)->recalculateProductStockCount($saleItem->product_id);

            $this->recalculateSale($saleItem->sale);

            DB::commit();
            return $saleItem;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateItem(Product_Sale $saleItem, array $data = [])
    {
        try {
            DB::beginTransaction();

            $saleItem->update($data);

            $this->recalculateSale($saleItem->sale);

            DB::commit();

            return $saleItem;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    // TODO:
    public function addToStock(Product_Sale $productSale)
    {
        try {
            DB::beginTransaction();

            Product_Warehouse::firstOrCreate(
                [
                    'product_id' => $productSale->product_id,
                    // 'warehouse_bin_id' => $productSale->warehouse_bin_id,
                    'warehouse_id' => $productSale->sale->warehouse_id,
                    'sale_item_id' => $productSale->id,
                ],
                [
                    'price' => $productSale->net_unit_price,
                    'qty' => DB::raw("qty - {$productSale->qty}")
                ]
            );

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function assignBin(Product_Sale $purchaseSaleItem, $binId)
    {
        // 1. Check  if there is PO item id assigned
        $stock = Product_Warehouse::whereSaleItemId($purchaseSaleItem->id)->first();

        if (empty($stock)) {
            // 2. check if there is a match using product_id, warehouse_id and qty received
            $stock = Product_Warehouse::whereProductId($purchaseSaleItem->product_id)
                ->whereWarehouseId($purchaseSaleItem->sale->warehouse_id)
                ->whereQty($purchaseSaleItem->recieved)
                ->get();

            // if there is only 1 match, then it's the exact match
            if ($stock->count() == 1) {
                $stock = $stock->first();
            } else if ($stock->count() == 0) {
                $stock = null;
            }

            // dd($purchaseSaleItem->recieved, $purchaseSaleItem->purchase->warehouse_id);
        }


        if ($stock) {
            $stock->warehouse_bin_id = $binId;
            $stock->save();
        }

        $purchaseSaleItem->update(['warehouse_bin_id' => $binId]);

        return $purchaseSaleItem;
    }
}
