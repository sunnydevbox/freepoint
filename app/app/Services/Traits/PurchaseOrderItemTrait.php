<?php

namespace App\Services\Traits;

use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Models\WarehouseBin;
use Exception;
use Illuminate\Support\Facades\DB;

trait PurchaseOrderItemTrait
{
    use StockTrait;

    public function addItem(Purchase $purchaseOrder, Product|int $product)
    {
        try {
            DB::beginTransaction();

            if (!$product instanceof Product) {
                $product = Product::with(
                    ['tax']
                )->find($product);
            }

            $warehouseBinId = $this->getLastBinLocation($product->id, $purchaseOrder->warehouse_id);

            $qty = 1;

            $purchaseOrder->items()->create([
                'product_id' => $product->id,
                'qty' => $qty,
                'recieved' => 0,
                'discount' => 0,

                'tax_rate' => !empty($product->tax) ? $product->tax->rate : 0,
                'tax' => 0,
                'total' => ($product->cost * $qty),

                'purchase_unit_id' => $product->purchase_unit_id,
                'net_unit_cost' => $product->cost,

                'warehouse_bin_id' => $warehouseBinId,
            ]);

            // CHeck if product is not yet in the warehosue
            if ($product->inWarehouses->isEmpty()) {
                Product_Warehouse::create([
                    'product_id'    => $product->id,
                    'warehouse_id'  => $purchaseOrder->warehouse_id,
                    'qty'           => 0
                ]);
            }

            DB::commit();

            $this->term = '';

            return redirect()->route('purchases.edit', ['purchase' => $purchaseOrder->id]);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getLastBinLocation(Product|int $product, $warehouseId = null)
    {
        try {
            $query = ProductPurchase::where('product_id', $product)
                ->select('warehouse_bin_id')
                ->orderBy('product_purchases.created_at', 'ASC')
                ->limit(1);

            if (!empty($warehouseId)) {
                $query = $query
                    ->leftJoin(
                        'warehouse_bins',
                        'warehouse_bins.id',
                        '=',
                        'product_purchases.warehouse_bin_id'
                    )
                    ->where('warehouse_bins.warehouse_id', $warehouseId);
            }

            return $query->first()?->warehouse_bin_id;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function received(ProductPurchase $purchaseOrderItem, $receivedQty = 0)
    {
        try {
            DB::beginTransaction();

            // $currentReceivedQty =  $purchaseOrderItem->recieved;
            // $currentOrderQty =  $purchaseOrderItem->qty;
            // $currentStatus = PurchaseStatusEnum::tryFrom($purchaseOrderItem->purchase->status);

            // VALIDATION: Warehouse BIN is REQUIRED!!
            if (empty($purchaseOrderItem->warehouse_bin_id) || !WarehouseBin::find($purchaseOrderItem->warehouse_bin_id)) {
                throw new Exception('Invalid warehouse bin', 400);
            }

            // VALIDATION: Received QTY must be numeric
            if (!is_numeric($receivedQty) || $receivedQty < 0) {
                throw new Exception('Invalid quantity type', 400);
            }

            // VALIDATION: total recevied qty must not be more that qty
            if ($purchaseOrderItem->remaining < $receivedQty) {
                throw new Exception('Invalid received quantity exceeds', 400);
            }


            $stock = Product_Warehouse::wherePoItemId($purchaseOrderItem->id)->first();

            if (empty($stock)) {
                $stock = Product_Warehouse::whereProductId($purchaseOrderItem->product_id)
                    ->whereNull('sale_item_id')
                    ->whereWarehouseId($purchaseOrderItem->purchase->warehouse_id)
                    ->get();

                // dd($stock);        
                // dd($stock->toSql(), $purchaseOrderItem->product_id, $purchaseOrderItem->purchase->warehouse_id, $purchaseOrderItem->warehouse_bin_id);
                if ($stock->count() == 1) {
                    $stock = $stock->first();
                } else if ($stock->count() > 1) {
                    $stock = Product_Warehouse::whereProductId($purchaseOrderItem->product_id)
                        ->whereNull('sale_item_id')
                        ->whereWarehouseId($purchaseOrderItem->purchase->warehouse_id)
                        ->whereWarehouseBinId($purchaseOrderItem->warehouse_bin_id)
                        ->get();
                    // dd($stock);
                    if ($stock->count() == 1) {
                        $stock = $stock->first();
                    } else if ($stock->count() > 1) {
                        // dd('moree 1', $stock->toArray());
                        // CREATE new;
                        $stock = null;
                    } else if (empty($stock->count())) {
                        // dd('asdasd');
                        $stock = null;
                    }
                } else {
                    $stock = null;
                }
            }

            if (!empty($stock->id)) {
                $stock->update([
                    'product_id' => $purchaseOrderItem->product->id,
                    'warehouse_id' => $purchaseOrderItem->purchase->warehouse_id,
                    'warehouse_bin_id' => $purchaseOrderItem->warehouse_bin_id,
                    'po_item_id' => $purchaseOrderItem->id,
                    'price' => $purchaseOrderItem->net_unit_cost,
                    'qty' => $stock->qty += $receivedQty,
                ]);
            } else {
                $stock = Product_Warehouse::create([
                    'product_id' => $purchaseOrderItem->product->id,
                    'warehouse_id' => $purchaseOrderItem->purchase->warehouse_id,
                    'warehouse_bin_id' => $purchaseOrderItem->warehouse_bin_id,
                    'po_item_id' => $purchaseOrderItem->id,
                    'price' => $purchaseOrderItem->net_unit_cost,
                    'qty' => $receivedQty,
                ]);
            }


            // // CHECK first if there is a possible match in product_warehouses
            // $productWarehouse = Product_Warehouse::whereQty(0)
            //     ->whereProductId($purchaseOrderItem->product_id)
            //     // ->
            //     ->get();

            // dd($productWarehouse);
            // // Get or create the product-warehouse data
            // $productWarehouse = Product_Warehouse::firstOrCreate([
            //     'product_id' => $purchaseOrderItem->product->id,
            //     'warehouse_id' => $purchaseOrderItem->purchase->warehouse_id,
            //     'warehouse_bin_id' => $purchaseOrderItem->warehouse_bin_id,
            // ], [
            //     'product_id' => $purchaseOrderItem->product->id,
            //     'warehouse_id' => $purchaseOrderItem->purchase->warehouse_id,
            //     'warehouse_bin_id' => $purchaseOrderItem->warehouse_bin_id,
            //     'po_item_id' => $purchaseOrderItem->id,
            //     'qty' => $productWarehouse->qty += $receivedQty,
            // ]);

            // Update po item record
            $purchaseOrderItem->recieved += $receivedQty;
            $purchaseOrderItem->save();

            $this->recalculateItem($purchaseOrderItem);


            // update product record
            $purchaseOrderItem->product->qty += $receivedQty;
            $purchaseOrderItem->product->save();

            $this->recalculateProductStock($purchaseOrderItem->product->id);

            DB::commit();

            return $purchaseOrderItem;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function takeItems(ProductPurchase $productPurchaseItem, $quantity = 0)
    {
        try {
            DB::beginTransaction();

            $stock = Product_Warehouse::wherePoItemId($productPurchaseItem)->first();

            if (empty($stock)) {
                $stock = Product_Warehouse::whereWarehouseBinId($productPurchaseItem->warehouse_bin_id)
                    ->whereProductId($productPurchaseItem->product_id)
                    ->first();
            }

            if ($stock->count()) {
                $stockQuantity = $stock->qty;

                // If there is enough qty in the stock
                if ($stockQuantity >= $quantity && $productPurchaseItem->qty >= $quantity) {
                    $stock->qty = $stockQuantity - $quantity;
                    $stock->save();


                    $productPurchaseItem->qty = $productPurchaseItem->qty - $quantity;
                    $productPurchaseItem->save();
                } else {
                    throw new Exception('Not enough stock', 400);
                }
            }

            // dd($stock->qty, $productPurchaseItem->qty);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function itemQuantityChange(ProductPurchase $purchaseOrderItem, $quantity)
    {
        try {
            DB::beginTransaction();

            if (!is_numeric($quantity)) {
                throw new Exception('Invalid quantity', 400);
            }
            $p = $purchaseOrderItem;

            // 1 update the item record
            //  -- update the qty 
            // -- recalculate total value (this is done via observer)
            // 

            $purchaseOrderItem->qty = $quantity;
            $purchaseOrderItem->save();

            // 3 update PO receord
            //  -- update total_qty
            // -- update item
            //  -- recalculate it grand total
            $this->recalculatePurchase($purchaseOrderItem->purchase);

            DB::commit();

            return $purchaseOrderItem;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function assignBin(ProductPurchase $purchaseOrderItem, $binId)
    {
        // 1. Check  if there is PO item id assigned
        $stock = Product_Warehouse::wherePoItemId($purchaseOrderItem->id)->first();

        if (empty($stock)) {
            // 2. check if there is a match using product_id, warehouse_id and qty received
            $stock = Product_Warehouse::whereProductId($purchaseOrderItem->product_id)
                ->whereWarehouseId($purchaseOrderItem->purchase->warehouse_id)
                ->whereQty($purchaseOrderItem->recieved)
                ->get();

            // if there is only 1 match, then it's the exact match
            if ($stock->count() == 1) {
                $stock = $stock->first();
            } else if ($stock->count() == 0) {
                $stock = null;
            }

            // dd($purchaseOrderItem->recieved, $purchaseOrderItem->purchase->warehouse_id);
        }


        if ($stock) {
            $stock->warehouse_bin_id = $binId;
            $stock->save();
        }

        $purchaseOrderItem->update(['warehouse_bin_id' => $binId]);

        return $purchaseOrderItem;

        // dd($purchaseOrderItem, $binId, empty($purchaseOrderItem->warehouse_bin_id), $stock);


        // if (empty($purchaseOrderItem->warehouse_bin_id)) {
        //     // 1. Check if PO item has been previously received but not yet assigned bin
        //     // this will be deprecated soon because this is just band-aid solution
        //     // Find a match in product warehouse
        //     if (!empty($purchaseOrderItem->recieved)) {
        //         $productWarehouse = Product_Warehouse::whereProductId($purchaseOrderItem->product_id)
        //             ->whereQty($purchaseOrderItem->recieved)
        //             ->whereNull('warehouse_bin_id')
        //             ->get();

        //         if ($productWarehouse->count() == 1) {
        //             $purchaseOrderItem->update(['warehouse_bin_id' => $binId]);

        //             $productWarehouse->first()
        //                 ->update(['warehouse_bin_id' => $binId]);
        //         }
        //         return $purchaseOrderItem;
        //     }

        //     if (empty($purchaseOrderItem->recieved)) {
        //         $purchaseOrderItem->update(['warehouse_bin_id' => $binId]);
        //         return $purchaseOrderItem;
        //     }
        // }
    }
}
