<?php

namespace App\Services\Traits;

use App\Models\Product;
use App\Models\Product_Warehouse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait StockTrait
{
    public function productStocks($productId)
    {
        $result = DB::table('product_warehouse')
            // ->leftJoin('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
            ->where('product_warehouse.product_id', $productId)
            ->get();

        return $result;
    }

    public function getProductStockLocations($productId)
    {
        return Product_Warehouse::with([
            'warehouse',
            'bin' => fn($query) => $query->groupBy('id')
        ])
            ->whereProductId($productId)
            ->where('qty', '>', 0)
            ->get();
    }


    public function stockCountPerWarehouse($productId): Collection
    {

        $result = DB::table('products')
            ->select([
                'warehouses.name',
                // 'warehouse_bins.name AS bin_name',
                DB::raw('SUM(product_warehouse.qty) AS qty')
            ])
            ->leftJoin('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
            ->leftJoin('warehouses', 'warehouses.id', '=', 'product_warehouse.warehouse_id')
            ->leftJoin('warehouse_bins', 'warehouse_bins.id', '=', 'product_warehouse.warehouse_bin_id')
            ->where('products.id', $productId)
            ->groupBy([
                'product_warehouse.warehouse_id',
                // 'product_warehouse.warehouse_bin_id'
            ])
            ->get();

        return $result;
        // dd($result);
    }

    public function recalculateProductStock($productId)
    {
        $product = Product::find($productId);
        $stockEntries = $this->productStocks($productId);

        $product->update([
            'qty' => $stockEntries->sum('qty')
        ]);
    }
}
