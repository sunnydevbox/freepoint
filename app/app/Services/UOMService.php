<?php
namespace App\Services;

use App\Models\Unit;
use App\Repositories\Unit\UnitRepository;

class UOMService extends AbstractBREADService implements ServiceInterface
{
    public function add($params)
    {
        $params['is_active'] = true;
        if(isset($params['base_uni']) && !$params['base_unit']){
            $params['operator'] = '*';
            $params['operation_value'] = 1;
        }

        return parent::add($params);
    }

    public function search(string $q)
    {
        // $lims_unit_all = Unit::where('unit_name', $q)->paginate(5);
        // $lims_unit_list = Unit::all();

        $lims_unit_all = $this->repository->where('unit_name', $q)->paginate(5);
        $lims_unit_list = $this->repository->all();

        return [
            'all' => $lims_unit_all,
            'list' => $lims_unit_list,
        ];
    }


    public function repository()
    {
        return app(UnitRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'unit_name' => 'required|min:3|max:50',
                'unit_code' => 'required|max:10',
            ],
            'UPDATE_RECORD' => [
                'unit_name' => 'min:3|max:50',
                'unit_code' => 'min:1|max:10',
            ],
        ]);
    }
}