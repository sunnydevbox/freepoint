<?php
namespace App\Services;

use App\Models\Unit;
use App\Repositories\Tax\TaxRepository;

class TaxService extends AbstractBREADService implements ServiceInterface
{
    public function repository()
    {
        return app(TaxRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'name' => 'required|min:3',
                'rate' => 'required|numeric',
            ],
            'UPDATE_RECORD' => [
                'name' => 'required|min:3',
                'rate' => 'required|numeric',
            ],
        ]);
    }

}