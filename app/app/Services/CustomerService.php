<?php

namespace App\Services;
use App\Repositories\Customer\CustomerRepository;
use App\Models\Customer;

class CustomerService 
{
    public static function getRepository(): CustomerRepository
    {
        return new CustomerRepository;
    }

}