<?php

namespace App\Services;

use App\Models\Product_Sale;
use Illuminate\Database\Eloquent\Collection;

class ProductSaleService
{
    public static function filter(array $filters = [], int $productId): Collection
    {
        $query = Product_Sale::with(['bin', 'sale', 'sale.customer'])
            ->whereProductId($productId)
            ->orderBy('created_at', 'DESC');

        if (!empty($filters['warehouse_id'])) {
            $query = $query->whereHas('purchase', function ($query) use ($filters) {
                $query->whereWarehouseId($filters['warehouse_id']);
            });
        }

        if (!empty($filters['start_at']) && !empty($filters['end_at'])) {
            $query = $query->whereHas('purchase', function ($query) use ($filters) {
                $query->whereBetween('created_at', [$filters['start_at'], $filters['end_at']]);
            });
        }

        return $query->get();
    }
}
