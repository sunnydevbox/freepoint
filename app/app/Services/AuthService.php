<?php

namespace App\Services;

class AuthService implements ServiceInterface
{
    public function login(
        string $id,
        string $password,
        string $fieldSource = 'name'
    ) {
        $credentials = [
            $fieldSource => $id,
            'password' => $password,
        ];

        // $valid = auth()->attempt(array($fieldType => $input['name'], 'password' => $input['password']));
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }


        return $token;
    }
}
