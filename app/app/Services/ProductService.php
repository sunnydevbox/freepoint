<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use App\Services\Traits\StockTrait;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use App\Traits\Services\ProductAdminTrait;

class ProductService extends AbstractBREADService implements ServiceInterface
{
    use StockTrait;
    use ProductAdminTrait;

    public function repository()
    {
        return app(ProductRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'name' => 'required|min:3',
                'rate' => 'required|numeric',
            ],
            'UPDATE_RECORD' => [
                'name' => 'required|min:3',
                'rate' => 'required|numeric',
            ],
        ]);
    }

    public function update($productId, $data)
    {
        if (!empty($data['image'])) {
            $this->attachImage($productId, $data['image']);
        }
    }

    public function store($data)
    {
        // dd($data,1);
        $product = Product::create($data);

        return $product;
    }

    public function delete(int|Product $product)
    {
        try {
            if (!$product instanceof Product) {
                $product = Product::findOrFail($product);
            }

            DB::beginTransaction();

            $product->update(['is_active' => false]);
            DB::commit();

            return $product;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }



    public function attachImage($productId, $image)
    {
        // dd(new ModelImageUploader('product'));
        // $product = $this->repository()->find($productId);

        // $pi = new MediaUploader(
        //     new ProductMediaUploader($product),
        //     $image
        // );
        // dd($pi, $pi->upload());

        // dd($pi);

        /**
         * Only one image per category for now
         */
        if ($image instanceof UploadedFile) {
        } else if (is_string($image)) {
        }
        $imageKey = "product-{$productId}";

        $category = $this->repository()->find($productId);
        if ($currentImage = $category->attachment($imageKey)) {
            $currentImage->delete();
        }

        return $category->attach($image, [
            'key' => $imageKey,
            'group' => 'product',
        ]);
    }

    public function EntitiesProductMediaUploaders($productId)
    {
        return $this->repository()->find($productId);
    }

    public function search(string $keyword)
    {
        $result = $this->repository()
            ->where('name', 'LIKE', "%{$keyword}%")
            ->orWhere('code', 'LIKE', "%{$keyword}%")
            ->limit(20);

        return $result;
    }

    public function searchProducts(string $keyword = '', array $filters = []): Builder
    {
        $query = DB::table('products')
            ->select([
                'products.id',
                'products.name',
                'products.code',
                'product_warehouse.warehouse_id',
                'warehouses.id AS warehouse_id',
                'warehouses.name AS warehouse_name',
                'warehouse_bins.id AS bin_id', // @deprecated
                'warehouse_bins.id AS warehouse_bin_id',
                'warehouse_bins.name AS bin_name',
                DB::raw('SUM(product_warehouse.qty) AS stock_qty'), // refactor this

            ])
            ->leftJoin('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
            ->leftJoin('warehouse_bins', 'warehouse_bins.id', '=', 'product_warehouse.warehouse_bin_id')
            ->leftJoin('warehouses', 'warehouses.id', '=', 'product_warehouse.warehouse_id')
            ->havingRaw('stock_qty > 0')
            ->where(function (Builder $query) use ($keyword) {
                // This prevents sql injection
                $query->orWhere('products.name', 'LIKE', "%{$keyword}%")
                    ->orWhere('products.code', 'LIKE', "%{$keyword}%");
            })
            ->where('products.is_active', 1)
            ->groupBy('product_warehouse.warehouse_bin_id', 'product_warehouse.warehouse_id',  'product_warehouse.product_id');


        if (!empty($filters['warehouse_id'])) {
            $query = $query->where('product_warehouse.warehouse_id', $filters['warehouse_id']);
        }

        return $query;
    }
}
