<?php

namespace App\Services;

use App\Repositories\Brand\BrandRepository;

class BrandService extends AbstractBREADService
{
    public function repository()
    {
        return app(BrandRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'title' => 'required|min:1|max:100|unique:brands',
                'image' => 'image|mimes:jpg,jpeg,png,gif',
            ],
            'DELETE_RECORD' => [
                'id'    => 'exists:brands,id'
            ]
        ]);
    }
}
