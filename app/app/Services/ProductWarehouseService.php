<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Product_Warehouse;

class ProductWarehouseService
{
    public function recalculateProductStockCount(int|Product $product)
    {
        if (!$product instanceof Product) {
            $product = Product::findOrFail($product);
        }

        $totalQty = Product_Warehouse::whereProductId($product->id)->sum('qty');

        $product->update([
            'qty' => $totalQty
        ]);
    }


    public function addItem(array $data, $qty)
    {
        Product_Warehouse::firstOrCreate(
            $data,
            [
                'qty' => $qty
            ]
        );

        $this->recalculateProductStockCount($data['product_id']);
    }
}
