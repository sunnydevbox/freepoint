<?php
namespace App\Services;

use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;

class CategoryService extends AbstractBREADService
{
    public function repository()
    {
        return app(CategoryRepository::class);
    }

    public function rules()
    {
        return array_merge($this->validationRules, [
            'CREATE_RECORD' => [
                'name' => 'required|min:1|max:100',
                'image' => 'image|mimes:jpg,jpeg,png,gif',
            ]
        ]);
    }

    public function update($categoryId, $data)
    {
        if (!empty($data['image'])) {
            $this->attachImage($categoryId, $data['image']);   
        }

        $result = $this->repository()
            ->update(
                $categoryId,
                collect($data)->only(['name', 'parent_id', 'is_active'])->toArray()
            );

        return $result;
    }

    public function attachImage($categoryId, $image)
    {
        /**
         * Only one image per category for now
         */
        if ($image instanceof UploadedFile) {
            
        } else if (is_string($image)) {

        }
        $imageKey = "category-{$categoryId}";

        $category = $this->repository()->find($categoryId);
        if ($currentImage = $category->attachment($imageKey)) {
            $currentImage->delete();
        }

        return $category->attach($image, [
            'key' => $imageKey,
            'group' => 'category',
        ]);
    }
}