<?php
namespace App\Repositories\Tax;

use App\Models\Tax;
use App\Repositories\BaseRepository;

class TaxRepository extends BaseRepository implements TaxRepositoryInterface
{
    public function model()
    {
        return Tax::class;
    }

    public function getActive()
    {
        return $this->model->active()
            ->orderBy('name')
            ->active()
            ->get();
    }
}
