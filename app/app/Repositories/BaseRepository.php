<?php

declare(strict_types=1);

namespace App\Repositories;

use Closure;
use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    use BaseCachedQueriesTrait;

    protected $model;
    protected $scopeQuery = null;

    abstract public function model();

    public function makeModel()
    {
        $model = app($this->model());

        if (!$model instanceof Model) {
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function resetModel()
    {
        return $this->makeModel();
    }

    public function find($id, $columns = ['*'])
    {
        $model = $this->model->findOrFail($id, $columns);
        $this->resetModel();

        return $model;
    }

    public function create($params)
    {
        $model = $this->model->create($params);
        $this->resetModel();

        return $model;
    }

    public function update($id, $params)
    {
        $model = $this->find($id);
        $model->update($params);
        $this->resetModel();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        $model->delete();
        $this->resetModel();

        return true;
    }

    public function where(string $column, string $value, string $operator = '=')
    {
        $model = $this->model->where($column, $operator, $value);

        return $model;
    }

    public function all()
    {
        $result = $this->model->all();
        $this->resetModel();

        return $result;
    }

    // ($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)

    public function paginate(
        $page = 1,
        $perPage = 15, 
        $columns = ['*'], 
        $pageName = 'page'
        // mixed $limit = null,
        // array $columns = ['*']
    ) {
        // $limit = is_null($limit) ? 10 : $limit;
        $results = $this->model->paginate(
            $perPage,
            $columns,
            $pageName,
            $page
        );
        $results->appends(app('request')->query());
        $this->resetModel();

        return $results;
    }

    public function scopeQuery(Closure $scope)
    {
        $this->scopeQuery = $scope;

        return $this;
    }

    protected function applyScope()
    {
        if (isset($this->scopeQuery) && is_callable($this->scopeQuery)) {
            $callback = $this->scopeQuery;
            $this->model = $callback($this->model);
        }

        return $this;
    }


    public function __construct()
    {
        $this->makeModel();
    }
}
