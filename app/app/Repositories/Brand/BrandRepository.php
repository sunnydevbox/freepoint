<?php

namespace App\Repositories\Brand;

use App\Models\Brand;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Cache;

class BrandRepository extends BaseRepository implements RepositoryInterface
{

    public function getAllActive()
    {
        return Cache::remember(
            'brand.get-all-active',
            3600,
            function () {
                return $this->model
                    ->active()
                    ->orderBy('title')
                    ->get();
            }
        );
    }

    public function model()
    {
        return Brand::class;
    }
}
