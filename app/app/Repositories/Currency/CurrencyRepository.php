<?php
namespace App\Repositories\Currency;

use App\Models\Currency;
use App\Repositories\BaseRepository;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryInterface
{
    public function model()
    {
        return Currency::class;
    }
}
