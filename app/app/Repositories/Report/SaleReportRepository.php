<?php

namespace App\Repositories\Report;

use Illuminate\Support\Facades\DB;
use App\Models\Product_Warehouse;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class SaleReportRepository
{
    public function querySaleReport(array $filter = []): Builder
    {
        $startDate = $filter['start_date'] ?? Carbon::now()->startOfMonth();
        $endDate = $filter['end_date'] ?? Carbon::now()->endOfDay();
        $warehouseId = $filter['warehouseId'] ?? null;
        $brandId = $filter['brandId'] ?? null;
        $searchTerm = $filter['searchTerm'] ?? null;

        // Main query builder
        $query = Product_Warehouse::whereHas('product', function ($query) use ($brandId) {
            $query->active()
                ->whereNull('is_variant')
                ->when(!empty($brandId), fn($query) => $query->where('brand_id', $brandId));
        })
            ->with(['product', 'saleItem.sale', 'warehouse', 'bin'])
            ->whereHas('saleItem.sale', function ($query) use ($startDate, $endDate) {
                // Only apply the date filter if both start and end dates are provided
                if ($startDate && $endDate) {
                    $query->whereBetween('created_at', [$startDate, $endDate]);
                }
            })
            ->when(!empty($warehouseId) && $warehouseId !== 'undefined', fn($query) => $query->whereWarehouseId($warehouseId))
            ->when(
                !empty($searchTerm),
                fn($query) =>
                $query->whereHas(
                    'product',
                    fn($query) => $query->whereRaw("name LIKE '%{$searchTerm}%'")
                        ->orWhereRaw("code LIKE '%{$searchTerm}%'")
                        ->orWhereRaw("model LIKE '%{$searchTerm}%'")
                )
            )
            ->select('*');

        $query = $query->addSelect([
            DB::raw("
                (
                    SELECT SUM(pw_warehouse_qty_count.qty)
                    FROM product_warehouse AS pw_warehouse_qty_count
                    WHERE pw_warehouse_qty_count.product_id = product_warehouse.product_id
                    " . ($warehouseId ? " AND pw_warehouse_qty_count.warehouse_id = {$warehouseId}" : "") . "
                ) AS warehouse_count
            "),

            DB::raw("
                (
                    SELECT SUM(pw_warehouse_bin_qty_count.qty)
                    FROM product_warehouse AS pw_warehouse_bin_qty_count
                    WHERE pw_warehouse_bin_qty_count.product_id = product_warehouse.product_id
                    AND pw_warehouse_bin_qty_count.warehouse_bin_id = product_warehouse.warehouse_bin_id
                    " . ($warehouseId ? " AND pw_warehouse_bin_qty_count.warehouse_id = {$warehouseId}" : "") . "
                ) AS warehouse_bin_count
            "),
        ]);

        return $query;
    }
}
