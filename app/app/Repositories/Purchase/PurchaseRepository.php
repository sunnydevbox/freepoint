<?php

namespace App\Repositories\Purchase;

use App\Models\Purchase;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\Product_Warehouse;
use App\Enums\PurchaseStatusEnum;
use App\Models\User;

class PurchaseRepository extends BaseRepository
{
    public function model()
    {
        return Purchase::class;
    }


    public function recalculate()
    {
    }

    /**
     * This function will create purchase order for a warehouse in pos setting.
     * @deprecated
     * This is the original code for re-order
     * Use this for reference only
     *
     * @return void
     */
    public static function autoPurchase()
    {
        $products = Product::where('is_active', true)
            ->whereColumn('alert_quantity', '>', 'qty')
            ->whereNull(['is_variant', 'is_batch'])
            ->get();

        if (count($products)) {

            $postSetting = DB::table('pos_setting')
                ->select('warehouse_id')
                ->latest()
                ->first();

            $userData = DB::table('users')
                ->select('id')
                ->where([
                    ['is_active', true],
                    ['role_id', 1]
                ])
                ->first();

            $data['reference_no'] = 'pr-' . date("Ymd") . '-' . date("his");
            $data['user_id'] = $userData->id;
            $data['warehouse_id'] = $postSetting->warehouse_id;
            $data['item'] = count($products);
            $data['total_qty'] = 10 * $data['item'];
            $data['total_discount'] = 0;
            $data['paid_amount'] = 0;
            $data['status'] = 1;
            $data['payment_status'] = 1;
            $data['total_tax'] = 0;
            $data['total_cost'] = 0;

            foreach ($products as $key => $product) {
                if ($product->tax_id) {
                    $tax_data = DB::table('taxes')->select('rate')->find($product->tax_id);
                    if ($product->tax_method == 1) {
                        $net_unit_cost = number_format($product->cost, 2, '.', '');
                        $tax = number_format($product->cost * 10 * ($tax_data->rate / 100), 2, '.', '');
                        $cost = number_format(($product->cost * 10) + $tax, 2, '.', '');
                    } else {
                        $net_unit_cost = number_format((100 / (100 + $tax_data->rate)) * $product->cost, 2, '.', '');
                        $tax = number_format(($product->cost - $net_unit_cost) * 10, 2, '.', '');
                        $cost = number_format($product->cost * 10, 2, '.', '');
                    }
                    $tax_rate = $tax_data->rate;
                    $data['total_tax'] += $tax;
                    $data['total_cost'] += $cost;
                } else {
                    $data['total_tax'] += 0.00;
                    $data['total_cost'] += number_format($product->cost * 10, 2, '.', '');
                    $net_unit_cost = number_format($product->cost, 2, '.', '');
                    $tax_rate = 0.00;
                    $tax = 0.00;
                    $cost = number_format($product->cost * 10, 2, '.', '');
                }

                $data['product_id'][$key] = $product->id;
                $data['unit_id'][$key] = $product->unit_id;
                $data['net_unit_cost'][$key] = $net_unit_cost;
                $data['tax_rate'][$key] = $tax_rate;
                $data['tax'][$key] = $tax;
                $data['total'][$key] = $cost;

                $product_warehouse_data = Product_Warehouse::select('id', 'qty')
                    ->where([
                        ['product_id', $product->id],
                        ['warehouse_id', $postSetting->warehouse_id]
                    ])->first();
                if ($product_warehouse_data) {
                    $product_warehouse_data->qty += 10;
                    $product_warehouse_data->save();
                }
                $product->qty += 10;
                $product->save();
            }
            $data['order_tax'] = 0;
            $data['grand_total'] = $data['total_cost'];
            $purchaseData = Purchase::create($data);
            foreach ($data['product_id'] as $key => $product_id) {
                ProductPurchase::create([
                    'purchase_id' => $purchaseData->id,
                    'product_id' => $product_id,
                    'qty' => 10,
                    'recieved' => 10,
                    'purchase_unit_id' => $data['unit_id'][$key],
                    'net_unit_cost' => $data['net_unit_cost'][$key],
                    'discount' => 0,
                    'tax_rate' => $data['tax_rate'][$key],
                    'tax' => $data['tax'][$key],
                    'total' => $data['total'][$key],
                ]);
            }
        }
    }

    /**
     * Will create a PO if product quantity is less than product threshold in each warehouse.
     *
     * @return void
     */
    public static function createPurchaseOrderPerWarehouseAndSupplier(): void
    {
        $productIdsQuery = DB::table('product_purchases')
            ->leftJoin('products', 'products.id', '=', 'product_purchases.product_id')
            ->select('product_id')
            ->where('products.alert_quantity', '>', 0)
            ->where('products.is_active', 1)
            ->whereNull('products.deleted_at')
            ->groupBy('product_purchases.product_id');

        $admin = User::find(1);

        DB::table('product_purchases')
            ->leftJoin('purchases', 'purchases.id', '=', 'product_purchases.purchase_id')
            ->leftJoin('products', 'products.id', '=', 'product_purchases.product_id')
            ->addSelect([
                DB::raw('product_purchases.id as product_purchases_id'),
                'product_purchases.product_id',
                'product_purchases.purchase_id',
                'products.unit_id',
                'purchases.warehouse_id',
                'purchases.supplier_id'
            ])
            ->whereIn('product_purchases.product_id', $productIdsQuery)
            ->orderBy('purchases.created_at', 'DESC')
            ->get()
            ->each(function ($product) use ($admin) {
                $purchaseOrder = Purchase::where('warehouse_id', '=', $product->warehouse_id)
                    ->where('supplier_id', '=', $product->supplier_id)
                    ->whereIsReorder(true)
                    ->whereStatus(PurchaseStatusEnum::pending()->value)
                    ->orderBy('id', 'DESC');

                if ($purchaseOrder->count() === 0) {
                    // create a new PO
                    $data = [
                        'reference_no' => sprintf(
                            'pr-%s-%s',
                            date("Ymd"),
                            date("his")
                        ),
                        'user_id' => $admin->id,
                        'warehouse_id' => $product->warehouse_id,
                        'supplier_id' => $product->supplier_id,
                        'item' => $product->product_purchases_id,
                        'total_qty' => 10,
                        'total_discount' => 0,
                        'paid_amount' => 0,
                        'status' => PurchaseStatusEnum::pending()->value,
                        'payment_status' => 1,
                        'total_tax' => 0,
                        'total_cost' => 0,
                        'grand_total' => 0,
                        'order_tax' => 0,
                        'is_reorder' => 1,
                    ];
                    $purchaseOrder = Purchase::create($data);
                } else {
                    $purchaseOrder = $purchaseOrder->latest()->first();
                }

                $productPurchase = ProductPurchase::where([
                    'product_id' => $product->product_id,
                    'purchase_id' => $purchaseOrder->id,
                ]);

                if ($productPurchase->count() === 0) {
                    // create product purchase record
                    $productPurchase = [
                        'purchase_id' => $purchaseOrder->id,
                        'product_id' => $product->product_id,
                        'qty' => 10,
                        'recieved' => 0,
                        'purchase_unit_id' => $product->unit_id,
                        'net_unit_cost' => 0,
                        'discount' => 0,
                        'tax_rate' => 0,
                        'tax' => 0,
                        'total' => 0
                    ];
                    $purchaseOrder->items()->create($productPurchase);
                }
            });
    }
}
