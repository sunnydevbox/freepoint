<?php

namespace App\Repositories\Customer;

use App\Models\Customer;
use App\Repositories\BaseRepository;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    public function model()
    {
        return Customer::class;
    }

    public function getActive()
    {
        return $this->model->active()
            ->orderBy('name')
            ->get();
    }
}
