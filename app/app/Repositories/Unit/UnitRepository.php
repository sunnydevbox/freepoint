<?php
namespace App\Repositories\Unit;

use App\Models\Unit;
use App\Repositories\BaseRepository;

class UnitRepository extends BaseRepository implements UnitRepositoryInterface
{
    public function model()
    {
        return Unit::class;
    }
}
