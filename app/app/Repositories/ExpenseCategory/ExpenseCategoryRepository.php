<?php
namespace App\Repositories\ExpenseCategory;

use App\Models\ExpenseCategory;
use App\Repositories\BaseRepository;

class ExpenseCategoryRepository extends BaseRepository implements ExpenseCategoryRepositoryInterface
{
    public function model()
    {
        return ExpenseCategory::class;
    }
}
