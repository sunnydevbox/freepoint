<?php

namespace App\Repositories\Warehouse;

use App\Enums\RoleEnum;
use App\Models\Warehouse;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class WarehouseRepository extends BaseRepository implements WarehouseRepositoryInterface
{
    public function model()
    {
        return Warehouse::class;
    }

    public function listForDropdown(bool $formatMap = true): Collection
    {
        $result = $this->model
            ->select('id', 'name')
            ->active()
            ->orderBy('name', 'ASC')
            ->get();


        if ($formatMap) {
            return $result->mapWithKeys(fn($warehouse) => [$warehouse->id => $warehouse->name]);
        }


        return $result;
    }

    public function listForDropdownByCurrentUser(bool $formatMap = true): Collection
    {
        return Cache::remember(
            sprintf('warehouse.list-dropdown-current-user_%d-format_%s', auth()->user()->id, $formatMap ? 'yes' : 'no'),
            3600,
            function () use ($formatMap) {
                $result = $this->warehousesByCurrentUser()
                    ->select('id', 'name')
                    ->orderBy('name', 'ASC')
                    ->get();

                return ($formatMap)
                    ? $result->mapWithKeys(fn($warehouse) => [$warehouse->id => $warehouse->name])
                    : $result;
            }
        );
    }

    public function getListByCurrentuser(): Collection
    {
        return Cache::remember(
            sprintf('warehouse.get-by-current-user_%d', auth()->user()->id),
            3600,
            fn() => $this->warehousesByCurrentUser()->get()
        );
    }

    public function warehousesByCurrentUser(): Builder
    {
        if (auth()->check()) {
            // has access to ALL warehouses 
            $query = (auth()->user()->hasRole(['admin', 'owner', 'business-manager']))
                ? $this->makeModel()
                : auth()->user()->warehouses()->getQuery();

            return $query->active();
        }

        return $this->makeModel();
    }

    public function getAllActive(): Collection
    {
        return Cache::remember(
            'warehouse.get-all-active',
            3600,
            function () {
                return $this->model
                    ->active()
                    ->orderBy('name')
                    ->get();
            }
        );
    }
}
