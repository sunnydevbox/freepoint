<?php

namespace App\Repositories\ProductPurchase;

use App\Enums\TaxMethodEnum;
use App\Models\ProductPurchase;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Log;

class ProductPurchaseRepository extends BaseRepository
{
    public function model()
    {
        return ProductPurchase::class;
    }

    public function recalculate($productPurchase)
    {
        $product = $productPurchase->product;

        // Product subtotal computation
        if ($product->tax_method == TaxMethodEnum::exclusive()->value) {
            $netUnitCost = $product->cost - $productPurchase->discount;
            $tax = $netUnitCost * $productPurchase->qty * ($productPurchase->tax_rate / 100);
            $subTotal = ($netUnitCost * $productPurchase->qty) + $tax;
        } else {
            $qty = (float) $productPurchase->qty;
            $subTotalUnit = $productPurchase->net_unit_cost - $productPurchase->discount;
            $netUnitCost = (100 / (100 + $productPurchase->tax_rate)) * $subTotalUnit;
            
            $tax = ($subTotalUnit - $netUnitCost) * $qty;
                        
            $subTotal = $subTotalUnit * $qty;
        }

        $productPurchase->total = $subTotal;
    }
}
