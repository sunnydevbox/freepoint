<?php
namespace App\Repositories\CustomerGroup;

use App\Models\CustomerGroup;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Collection;

class CustomerGroupRepository extends BaseRepository implements CustomerGroupRepositoryInterface
{
    public function model()
    {
        return CustomerGroup::class;
    }

    public function getActive(): Collection
    {
        return Cache::remember('customer_group_get_active', 1800, fn() => CustomerGroup::active()->orderBy('name')->get());
    }
}
