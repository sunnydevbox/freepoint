<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;

trait BaseCachedQueriesTrait
{
    /**
     * TODO: Temporary FIX
     */
    public function getCachedAll()
    {
        $result = Cache::remember($this->makeModel()->getTable(), 3600, function () {
            return $this->scopeQuery(function($query) {
                $query->statusActive();
            })
            ->all();
        });

        return $result;
    }
}
