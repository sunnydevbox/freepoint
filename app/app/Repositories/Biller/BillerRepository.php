<?php

namespace App\Repositories\Biller;

use App\Models\Biller;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class BillerRepository extends BaseRepository implements BillerRepositoryInterface
{
    public function model()
    {
        return Biller::class;
    }

    public function getListByCurrentuser()
    {
        if (auth()->check()) {
            return Biller::where([
                ['is_active', true],
                ['id', auth()->user()->biller_id]
            ])->get();
        }

        return collect([]);
    }

    public function getActive(array $with = []): Collection
    {
        return Cache::remember(
            sprintf('all-active-biller-%s', join('-', $with)),
            3600,
            fn() => $this->model
                ->active()
                ->with($with)
                ->orderBy('name')
                ->get()
        );
    }
}
