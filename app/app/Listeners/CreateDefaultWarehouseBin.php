<?php

namespace App\Listeners;

use App\Events\WarehouseCreated;
use App\Models\WarehouseBin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateDefaultWarehouseBin
{

    public function handle(WarehouseCreated $event)
    {
        WarehouseBin::create([
            'warehouse_id' => $event->warehouse->id,
            'name' => 'Receiving',
            'aisle' => '',
            'rack' => '',
            'shelf' => '',
            'bin' => '000',
            "is_default" => true,
        ]);
    }
}
