<?php

namespace App\Listeners\Payment;

use App\Models\CashRegister;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AssignCashRegisterToPayment
{
    public function __construct()
    {
    }

    public function handle($event)
    {
        $sale = $event->payment->sale;

        $cashRegister = CashRegister::where([
            ['user_id', auth()->user()?->id],
            ['warehouse_id', $sale->warehouse_id],
            ['status', true],
        ])->first();

        if ($cashRegister) {
            $event->payment->update(['cash_register_id' => $cashRegister->id]);
        }
    }
}
