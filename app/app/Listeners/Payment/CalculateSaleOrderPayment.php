<?php

namespace App\Listeners\Payment;

use App\Services\SaleOrderService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CalculateSaleOrderPayment
{
    public function __construct()
    {
    }

    public function handle($event)
    {
        $sale = $event->payment->load('sale')->sale;

        (new SaleOrderService)->calculatePayments($sale);
    }
}
