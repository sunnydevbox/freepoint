<?php
namespace App\Traits;

use Bnb\Laravel\Attachments\HasAttachment as OriginalHasAttachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Bnb\Laravel\Attachments\Attachment;
use Illuminate\Support\Arr;

trait HasAttachment
{
    // use OriginalHasAttachment {
    //     attach as attach;
    // }

    // public function attach($fileOrPath, $options = [])
    // {
    //     if ( ! is_array($options)) {
    //         throw new \Exception('Attachment options must be an array');
    //     }

    //     if(empty($fileOrPath)) {
    //         throw new \Exception('Attached file is required');
    //     }
    //     // dd($options);
    //     $options = Arr::only($options, ['title', 'description', 'key', 'disk']);

    //     if ( ! empty($options['key']) && $attachment = $this->attachment($options['key'])) {
    //         $attachment->delete();
    //     }

    //     /** @var Attachment $attachment */
    //     $attachment = new Attachment($options);

    //     if ($fileOrPath instanceof UploadedFile) {
    //         $attachment->fromPost($fileOrPath);
    //     } else {
    //         $attachment->fromFile($fileOrPath);
    //     }

    //     if ($attachment = $this->attachments()->save($attachment)) {
    //         return $attachment;
    //     }

    //     return null;
    // }
}