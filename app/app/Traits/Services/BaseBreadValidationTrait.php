<?php

namespace App\Traits\Services;

use Illuminate\Validation\ValidationException;
// use Rakit\Validation\Validator;
use Illuminate\Support\Facades\Validator;

trait BaseBreadValidationTrait
{
    private function validate(string $ruleKey, $values)
    {
        if (isset($this->rules()[$ruleKey])) {
            $validation = Validator::make($values, $this->rules()[$ruleKey]);
            $validation->validate();

            if ($validation->fails()) {
                $errors = ValidationException::withMessages($validation->errors()->toArray());
                throw $errors;
            }
        }

        return true;
    }
}