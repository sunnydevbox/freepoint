<?php

namespace App\Traits\Services;

use App\Models\Product;
use Exception;

trait ProductAdminTrait
{
    public function setPublishStatus(Product $product, int|bool $status)
    {
        try {
            $product->update(['is_active' => $status]);
            return $product;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
