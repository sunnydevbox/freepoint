<?php
namespace App\Traits;

use Spatie\Activitylog\Traits\LogsActivity;

trait ActivityLoggerTrait
{
    use LogsActivity;

    protected static $logFillable = true;
}