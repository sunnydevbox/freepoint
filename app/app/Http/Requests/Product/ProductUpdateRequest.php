<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $productId = $this->request->get('id');
        return [
            'name' => [
                'max:255',
                Rule::unique('products', 'name')
                    ->ignore($productId)
                    ->where(function ($query) {
                        return $query->where('is_active', 1);
                    }),
            ],

            'code' => [
                'max:255',
                Rule::unique('products', 'code')
                    ->ignore($productId)
                    ->where(function ($query) {
                        return $query->where('is_active', 1);
                    }),
            ],
            'model' => [
                'required'
            ]
        ];
    }
}
