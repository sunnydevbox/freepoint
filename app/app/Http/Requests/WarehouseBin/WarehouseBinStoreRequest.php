<?php

namespace App\Http\Requests\WarehouseBin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WarehouseBinStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:100',
                Rule::unique('warehouse_bins')->where(function ($query)  {
                    return $query 
                        ->when($this->request->get('warehouse_id', null), fn($query) => $query->where('warehouse_id', $this->request->get('warehouse_id')))
                        ->when($this->request->get('name', null), fn($query) => $query->where('name', $this->request->get('name')));
                })
            ],
            'aisle' => 'max:100',
            'rack' => 'max:100',
            'shelf' => 'max:100',
            'bin' => 'max:100',
            'warehouse_id' => [
                'required',
                'exists:warehouses,id',
                Rule::unique('warehouse_bins')->where(function ($query)  {
                    return $query 
                        ->when($this->request->get('warehouse_id', null), fn($query) => $query->where('warehouse_id', $this->request->get('warehouse_id')))
                        ->when($this->request->get('aisle', null), fn($query) => $query->where('aisle', $this->request->get('aisle')))
                        ->when($this->request->get('rack', null), fn($query) => $query->where('rack', $this->request->get('rack')))
                        ->when($this->request->get('shelf', null), fn($query) => $query->where('shelf', $this->request->get('shelf')))
                        ->when($this->request->get('bin', null), fn($query) => $query->where('bin', $this->request->get('bin')));
                }),
            ]
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'This name already exists in the location',
            'warehouse_id.unique' => 'This bin location already exists in the location',
        ];
    }
}
