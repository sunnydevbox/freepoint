<?php

namespace App\Http\Requests\Category;

use  Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request->all());
        return [
            'name' => [
                'max:255',
                Rule::unique('categories')
                    ->ignore($this->request->get('category_id'))
                    ->where(function ($query) {
                        return $query->where('is_active', 1);
                    }),
            ],
            'image' => 'image|mimes:jpg,jpeg,png,gif',
        ];
    }
}
