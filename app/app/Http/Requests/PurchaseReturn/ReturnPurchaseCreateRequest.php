<?php

namespace App\Http\Requests\PurchaseReturn;

use Illuminate\Foundation\Http\FormRequest;

class ReturnPurchaseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->hasPermissionTo('purchase-return-add');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'total_qty' => 'required|numeric|min:1',
        ];
    }

    public function messages()
    {
        return [
            // 'total_qty.min' => 'Quantity to return must be more than 0' ,
        ];
    }
}
