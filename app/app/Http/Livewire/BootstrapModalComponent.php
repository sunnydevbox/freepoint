<?php

namespace App\Http\Livewire;

use LivewireUI\Modal\ModalComponent;

class BootstrapModalComponent extends ModalComponent
{
    protected static array $maxWidths = [
        'sm' => 'modal-sm',
        'md' => '',
        'lg' => 'modal-lg',
        'xl' => 'modal-xl',
    ];


    public static function modalMaxWidth(): string
    {
        return 'xl';
    }

    public function render()
    {
        return view('livewire.bootstrap-modal-component');
    }
}