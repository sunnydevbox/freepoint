<?php

namespace App\Http\Livewire\Sales\Form;

use App\Enums\SaleStatusEnum;
use App\Models\Product_Sale;
use App\Models\Sale;
use App\Services\SaleOrderService;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Traits\WithReordering;
use Rappasoft\LaravelLivewireTables\Views\Column;
use WireUi\Traits\Actions;

class AttachedProducts extends DataTableComponent
{
    use WithReordering;
    use Actions;

    public Sale $saleOrder;

    public $qty = 0;

    public array $bulkActions = [
        'deleteSelected' => 'Delete',
    ];

    protected $listeners = ['refreshAttachedProducts' => '$refresh'];

    public function canEdit()
    {
        return true;
    }


    public function deleteSelected()
    {
        try {
            if (!empty($this->getSelected())) {
                (new SaleOrderService)->removeItem($this->getSelected());
                (new SaleOrderService)->recalculateSale($this->saleOrder);

                $message = 'Items were removed from the list';
                if (count($this->getSelected()) == 1) {
                    $message = 'Item was removed from the list';
                }

                $this->notification()->success(
                    'Success',
                    $message,
                );
                $this->clearSelected();
                $this->emit('refreshDatatable');
                $this->emit('refreshSummary');
            }
        } catch (Exception $e) {
            $this->notification()->warning(
                'Warning',
                'There was a problem removing one or more items',
            );
            dd($e);
            $this->emit('refreshDatatable');
        }
        ;
    }

    public function builder(): Builder
    {
        return Product_Sale::with([
            // 'product.inWarehouses.bin',
            // 'bin',
        ])
            ->where('sale_id', $this->saleOrder->id)
            ->select('product_sales.*')
            ->orderBy('id', 'DESC');
    }

    public function mount($saleOrder)
    {
        $this->saleOrder = $saleOrder;
    }


    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(false);
        $this->setPaginationDisabled();
        $this->setReorderMethod('changeOrder');

        $this->setBulkActionsDisabled();
        if ($this->saleOrder->items()->count() > 0) {
            $this->setBulkActionsEnabled();
        }
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->hideIf('local' != App::environment()),

            Column::make("Id", "sale_id")
                ->hideIf(true),

            Column::make("Name", "product.name"),

            Column::make("Part Number", "product.code"),

            Column::make("Bin Location", "bin.name")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.sales.form.inline-bin-location-wrapper',
                        [
                            'productSale' => $row,
                            'field' => 'warehouse_bin_id',
                            'value' => $row->warehouse_bin_id,
                            'canEdit' => $this->canEdit()
                        ]
                    );
                }),

            Column::make("Quantity", "qty")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.sales.form.inline-field-wrapper',
                        [
                            'saleItem' => $row,
                            'field' => 'qty',
                            'value' => $row->qty,
                            'type' => 'number',
                            'validation' => 'numeric|required|min:1',
                            'canEdit' => $this->canEdit()
                        ]
                    );
                })
                ->footer(function ($rows) {
                    $value = 0;
                    if ($rows->count() > 0) {
                        $value = $rows[0]->sale->total_qty;
                    }
                    return $value;
                }),

            Column::make("Unit Price", "net_unit_price")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.sales.form.inline-field-wrapper',
                        [
                            'saleItem' => $row,
                            'field' => 'net_unit_price',
                            'value' => $row->net_unit_price,
                            'type' => 'number',
                            'validation' => 'numeric|required|min:0',
                            'canEdit' => $this->canEdit()
                        ]
                    );
                })
                ->footer(function ($rows) {
                    $value = 0;
                    if ($rows->count() > 0) {
                        $value = $rows[0]->sale->total_price;
                    }
                    return formatMoney($value, true);
                }),

            Column::make("Discount", "discount")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.sales.form.inline-field-wrapper',
                        [
                            'saleItem' => $row,
                            'field' => 'discount',
                            'value' => $row->discount,
                            'type' => 'number',
                            'validation' => 'numeric|required|min:0',
                            'canEdit' => $this->canEdit()
                        ]
                    );
                })
                ->footer(function ($rows) {
                    $value = 0;
                    if ($rows->count() > 0) {
                        $value = $rows[0]->sale->total_discount;
                    }
                    return formatMoney($value, true, true);
                }),

            Column::make("Tax", "tax")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.sales.form.inline-field-tax-wrapper',
                        [
                            'saleItem' => $row,
                            'field' => 'tax',
                            'value' => $row->discount,
                            'type' => 'number',
                        ]
                    );
                })
                ->footer(function ($rows) {
                    $value = 0;
                    if ($rows->count() > 0) {
                        $value = $rows[0]->sale->total_tax;
                    }
                    return formatMoney($value, true);
                }),

            Column::make("Subtotal", "total")
                ->format(function ($value) {
                    return formatMoney($value, true);
                })
                ->footer(function ($rows) {
                    $value = 0;
                    if ($rows->count() > 0) {
                        $value = $rows[0]->sale->grand_total;
                    }
                    return formatMoney($value, true);
                }),
        ];
    }
}
