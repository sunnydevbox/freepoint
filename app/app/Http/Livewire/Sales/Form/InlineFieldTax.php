<?php

namespace App\Http\Livewire\Sales\Form;

use App\Models\Product_Sale;
use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Repositories\Tax\TaxRepository;
use App\Services\SaleOrderService;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use WireUi\Traits\Actions;

class InlineFieldTax extends Component
{
    use Actions;

    public Product_Sale $saleItem;

    public $field = null;

    public $value = null;

    public $type = null;

    public $tax_rate = 0;
    
    public $taxAmount;

    public function mount($saleItem, $field, $value, $type)
    {
        $this->saleItem = $saleItem;
        $this->field = $field;
        $this->value = $value;
        $this->type = $type;
        $this->tax_rate = $saleItem->tax_rate;
        $this->taxAmount = $saleItem->tax;
    }

    public function updatedTaxRate($rate)
    {
        (new SaleOrderService)->updateItem($this->saleItem, [
            'tax_rate' => $rate
        ]);
        
        $this->notification()->success('Tax updated successfully');
            
        $this->emit('refreshAttachedProducts');
        $this->emit('refreshSummary');
    }

    public function render()
    {
        $taxes = (new TaxRepository)->getActive();
        return view(
            'livewire.sales.form.inline-field-tax',
            compact('taxes')
        );
    }
}
