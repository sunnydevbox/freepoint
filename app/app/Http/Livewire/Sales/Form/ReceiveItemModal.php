<?php

namespace App\Http\Livewire\Sales\Form;

use LivewireUI\Modal\ModalComponent;

class ReceiveItemModal extends ModalComponent
{
    public function render()
    {
        return view('livewire.sales.form.receive-item-modal');
    }
}
