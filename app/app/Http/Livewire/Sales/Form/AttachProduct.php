<?php

namespace App\Http\Livewire\Sales\Form;

use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\Sale;
use App\Services\ProductService;
use App\Services\SaleOrderService;
use Exception;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use WireUi\Traits\Actions;

class AttachProduct extends Component
{
    use Actions;
    
    public Sale $saleOrder;

    public $term = '';

    public $results = [];

    protected $listeners = ['refreshAttachProducts' => '$refresh'];

    public function mount($saleOrder)
    {
        $this->saleOrder = $saleOrder;
        $this->_reset();
    }

    public function _reset()
    {
        $this->term = '';
        $this->results = [];
    }

    public function addProduct($productId, $warehouseBinId)
    {
        // Check if this item is already in the list
        try {
            $product = Product::with(
                ['tax']
            )->findOrFail($productId);

            $qty = 1;
            $parameters = ['warehouse_bin_id' => $warehouseBinId];

            (new SaleOrderService)->addItem($this->saleOrder, $product, $qty, $parameters);

            $this->notification()->success(
                'Success',
                'Item added to the list',
            );

            $this->term = '';
            $this->emit('refreshAttachedProducts');
            $this->emit('refreshSummary');
        } catch (Exception $e) {
            $this->notification()->warning(
                'Warning',
                'There was a problem adding the item to the list',
            );
        }
    }

    public function updatedTerm($value)
    {
        $this->term = trim($value);
        $this->doSearch();
    }


    public function doSearch()
    {
        if (strlen($this->term) < 3) {
            $this->results = collect([]);
        } else {
            $query = (new ProductService)->searchProducts($this->term, ['warehouse_id' => $this->saleOrder->warehouse_id])
                // This prevents re-adding already added items    
                ->whereNotIn('products.id', $this->saleOrder->items->pluck('product_id')->toArray());

            $this->results = $query->get();
        }
    }

    public function render()
    {
        return view('livewire.sales.form.attach-product');
    }
}
