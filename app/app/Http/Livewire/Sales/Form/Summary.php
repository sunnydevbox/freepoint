<?php

namespace App\Http\Livewire\Sales\Form;

use App\Models\GeneralSetting;
use Livewire\Component;

class Summary extends Component
{
    public $saleOrder;

    public $items = 0;

    public $total = 0;

    public $orderTax = 0;

    public $orderDiscount = 0;

    public $shippingCost = 0;

    public $grandTotal = 0;

    public $total_qty = 0;

    protected $listeners = ['refreshSummary' => 'refreshSummary'];
    

    public function refreshSummary()
    {
        $this->saleOrder = $this->saleOrder->fresh();
        $this->initiateProperties();
    }

    private function initiateProperties()
    {
        // dd($this->saleOrder);
        $this->items = $this->saleOrder->item;
        $this->total_qty = $this->saleOrder->total_qty;
        $this->total = $this->saleOrder->total_price;
        // $this->orderTax = $this->saleOrder->total_tax;
        $this->orderTax = $this->saleOrder->order_tax;
        $this->orderDiscount = $this->saleOrder->order_discount;
        $this->shippingCost = $this->saleOrder->shipping_cost;
        $this->grandTotal = $this->saleOrder->grand_total; 
    }

    public function mount($saleOrder)
    {   
        $this->saleOrder = $saleOrder;
        $this->initiateProperties();

        // ($saleOrder->total_cost + $saleOrder->total_tax + $saleOrder->total_tax) - $saleOrder->order_discount;
    }

    public function render()
    {
        return view('livewire.sales.form.summary');
    }
}
