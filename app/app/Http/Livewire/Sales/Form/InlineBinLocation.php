<?php

namespace App\Http\Livewire\Sales\Form;

use App\Models\Product_Sale;
use App\Services\ProductService;
use App\Services\SaleOrderService;
use Livewire\Component;
use WireUi\Traits\Actions;

class InlineBinLocation extends Component
{
    use Actions;

    public $productSaleId;
    public $field = null;
    public $bin = '';
    public $warehouseBins;
    public $productWarehouse = null;
    public $productSale = null;
    public bool $canEdit = false;


    public $isDisabled = false;

    protected $listeners = [
        '$refresh',
        // 'refreshInlineBin' => 
    ];

    public function mount($productSale, $field, $value, $canEdit = false)
    {
        $this->productSale = $productSale;
        $this->field = $field;
        $this->bin = $value;
        $this->canEdit = $canEdit;

        $this->warehouseBins = (new ProductService)
            ->getProductStockLocations(
                $this->productSale->product_id
            )
            ->mapWithKeys(fn($warehouse) => [$warehouse->bin->id => $warehouse->bin]);

        if (!empty($this->productSale->recieved)) {
            // TODO: Temporarily commented
            // $this->isDisabled = true;
        }

        // IF bin location is already assigned,
        // prevent re-assignment
        if (!empty($this->productSale->warehouse_bin_id)) {
            // $this->isDisabled = true;
        }
    }

    public function updatedBin()
    {
        if (!empty($this->bin) && !empty($this->productSale)) {
            $this->productSale = (new SaleOrderService)->assignBin($this->productSale, $this->bin);

            $this->notification()->success('Bin updated successfully');
        }

        $this->emit('refreshAttachedProducts');
    }

    public function render()
    {
        return view('livewire.sales.form.inline-bin-location');
    }
}
