<?php

namespace App\Http\Livewire\Sales\Form;

use App\Models\Product_Sale;
use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Services\SaleOrderService;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use WireUi\Traits\Actions;

class InlineField extends Component
{
    use Actions;

    public Product_Sale $saleItem;

    public $field = null;

    public $value = null;

    public $type = null;

    public string $validation = '';

    protected $listeners = ['$refresh'];

    public function mount($saleItem, $field, $value, $type, string $validation = '')
    {
        $this->saleItem = $saleItem;
        $this->field = $field;
        $this->value = $value;
        $this->type = $type;

        $this->{$field} = $value;

        $this->validation = $validation;
    }

    public function updated($property)
    {
        if (!empty($this->saleItem)) {
            if (!empty($this->validation)) {
                $this->validateOnly($this->field, [$this->field => $this->validation]);
            }

            if ('qty' == $property) {
                (new SaleOrderService)->updateQty($this->saleItem, $this->qty);
            } else {

                (new SaleOrderService)->updateItem($this->saleItem, [$this->field => $this->{$this->field}]);
            }

            $this->notification()->success(ucwords($this->field) . ' saved successfully');

            $this->emit('refreshAttachedProducts');
            $this->emit('refreshSummary');
        }
    }

    public function render()
    {
        return view('livewire.sales.form.inline-field');
    }
}
