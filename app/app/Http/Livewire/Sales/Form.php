<?php

namespace App\Http\Livewire\Sales;

use App\Models\Sale;
use Livewire\Component;

class Form extends BaseFormComponent
{

    public $generalSettings;

    public $reference_no;

    public $isNew;

    public function mount(Sale $saleOrder)
    {
        parent::mount($saleOrder);
    }

    public function render()
    {
        return view('livewire.sales.form');
    }
}
