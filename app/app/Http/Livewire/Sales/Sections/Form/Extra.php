<?php

namespace App\Http\Livewire\Sales\Sections\Form;

use App\Enums\DiscountTypeEnum;
use App\Repositories\Tax\TaxRepository;
use App\Services\SaleOrderService;
use Livewire\Component;

class Extra extends Component
{
    public $saleOrder;

    public $order_tax_rate;

    public $order_discount;


    public $shipping_cost;

    public $order_discount_type;

    public $note;

    public function mount(
        $saleOrder,
    ) {
        $this->saleOrder = $saleOrder;

        $this->note = $saleOrder->note;
        $this->order_tax_rate = $saleOrder->order_tax_rate;
        $this->order_discount = $saleOrder->order_discount;
        $this->order_discount_type = $saleOrder->order_discount_type;
        $this->shipping_cost = $saleOrder->shipping_cost;
    }

    public function rules()
    {
        return [
            'order_tax_rate' => 'required|numeric|min:0',
            'order_discount' => 'required|numeric|min:0',
            'shipping_cost' => 'required|numeric|min:0',
        ];
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName, $this->rules());
        dd($this->{$propertyName});

        $this->saleOrder = (new SaleOrderService)->update(
            $this->saleOrder,
            [
                "{$propertyName}" => $this->{$propertyName},
            ]
        );
        $this->emit('refreshSummary');
    }



    public function render()
    {
        $taxes = (new TaxRepository)->getActive();
        $discountTypes = DiscountTypeEnum::toArray();

        return view(
            'livewire.sales.sections.form.extra',
            compact(
                'taxes',
                'discountTypes'
            )
        );
    }
}
