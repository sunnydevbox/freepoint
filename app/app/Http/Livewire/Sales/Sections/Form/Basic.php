<?php

namespace App\Http\Livewire\Sales\Sections\Form;

use App\Enums\SaleStatusEnum;
use App\Http\Livewire\Sales\BaseFormComponent;
use App\Models\GeneralSetting;
use App\Models\Sale;
use App\Models\Warehouse;
use App\Repositories\Biller\BillerRepository;
use App\Repositories\Customer\CustomerRepository;
use App\Rules\UniqueOfficialReceiptPerWarehouse;
use App\Services\SaleOrderService;
use Carbon\Carbon;

class Basic extends BaseFormComponent
{
    public $saleOrder;

    public $generalSettings;

    public $reference_no;

    public $customer_id;

    public $created_at;

    public $warehouse_id;
    public $biller_id;

    public $sale_status;

    public $receipt_no;

    public bool $canChangeLocation = true;

    public $btnSaveLabel = 'Update';

    protected $listeners = ['refreshComponent' => '$refresh'];

    public function mount(Sale $saleOrder)
    {

        parent::mount($saleOrder);

        $this->saleOrder = $saleOrder;

        $this->created_at = Carbon::parse($saleOrder->created_at)->format(GeneralSetting::first()->date_format);
        $this->reference_no = $saleOrder->reference_no;
        $this->customer_id = $saleOrder->customer_id;
        $this->warehouse_id = $saleOrder->warehouse_id;
        $this->biller_id = $saleOrder->biller_id;

        $this->sale_status = $saleOrder->sale_status;
        $this->receipt_no = $saleOrder->receipt_no;

        if ($this->isNew) {
            $this->sale_status = SaleStatusEnum::pending()->value;
            $this->btnSaveLabel = 'Save';
        }


        // TODO:
        // Uncomment this when this permission is added to the list already
        // if (auth()->user()->hasPermissionTo('sale.update.warehouse')) {
        //     $this->canChangeLocation = true;
        // }
    }

    public function save()
    {
        $this->validate();

        $data = [
            'reference_no' => $this->reference_no,
            'customer_id' => $this->customer_id,
            'created_at' => $this->created_at,
            'warehouse_id' => $this->warehouse_id,
            'biller_id' => $this->biller_id,
            'sale_status' => $this->sale_status,
            'receipt_no' => $this->receipt_no,
        ];

        if ($this->isNew) {
            $saleOrder = (new SaleOrderService)->create($data);
            return redirect()->route('sales.edit', $saleOrder->id);
        } else {
            $saleOrder = (new SaleOrderService)->update($this->saleOrder, $data);
        }
    }

    public function rules()
    {
        $validators = [
            'customer_id' => 'required',
            'warehouse_id' => 'required',
            'biller_id' => 'required',
            'created_at' => 'required|date',
            'receipt_no' => [
                'required',
                new UniqueOfficialReceiptPerWarehouse($this->warehouse_id, $this->saleOrder?->id),
            ],
        ];

        if ($this->isNew) {
            $validators['reference_no'] = 'unique:sales';
        }

        return $validators;
    }

    public function updated($propertyName): void
    {
        if ($propertyName == 'receipt_no') {
            $this->receipt_no = trim($this->receipt_no);
        }

        if ($propertyName == 'reference_no') {
            $this->reference_no = trim($this->reference_no);
        }


        $this->validateOnly($propertyName);
        if ($this->saleOrder->id) {
            // $this->validateOnly($propertyName, $this->rules());

            $this->saleOrder = (new SaleOrderService)->update(
                $this->saleOrder,
                [
                    "{$propertyName}" => $this->{$propertyName},
                ]
            );

            (new SaleOrderService)->recalculateSale($this->saleOrder);
        }
    }

    public function render()
    {
        // customer, location, biller, official receipt
        $customers = (new CustomerRepository)->getActive();
        $billers = (new BillerRepository)->getActive();
        $saleStatuses = SaleStatusEnum::toArray();

        $warehouses = auth()->user()->warehouses;

        return view(
            'livewire.sales.sections.form.basic',
            compact(
                'customers',
                'warehouses',
                'billers',
                'saleStatuses',
            )
        );
    }
}
