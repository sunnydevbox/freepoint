<?php

namespace App\Http\Livewire\Sales\Sections\Form;

use App\Models\Sale;
use App\Services\SaleOrderService;
use Livewire\Component;

class Notes extends Component
{
    public $staff_note;

    public $sale_note;

    public $saleOrder;

    public function mount(Sale $saleOrder) 
    {
        $this->saleOrder  = $saleOrder;
        $this->staff_note = $saleOrder->staff_note;
        $this->sale_note = $saleOrder->sale_note;
    }

    public function updating($propertyName, $value)
    {
        (new SaleOrderService)->update($this->saleOrder, [
            $propertyName => $value
        ]);
    }

    public function render()
    {
        return view('livewire.sales.sections.form.notes');
    }
}
