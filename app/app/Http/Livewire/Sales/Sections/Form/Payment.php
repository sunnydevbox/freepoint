<?php

namespace App\Http\Livewire\Sales\Sections\Form;

use App\Enums\PaymentStatusEnum;
use App\Models\Sale;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use WireUi\Traits\Actions;

class Payment extends DataTableComponent
{
    use Actions;

    public $saleOrder;

    protected $model = Sale::class;

    protected $listeners = ['refreshPurchaseBrowse' => '$refresh'];

    public $paymentMethods = [];

    public $paymentStatuses = [];

    public $payment_status;

    public function mount(Sale $saleOrder)
    {
        $this->saleOrder = $saleOrder;
        $this->paymentStatuses = PaymentStatusEnum::toArray();

        $this->payment_status = (empty($saleOrder->payment_status))
            ? PaymentStatusEnum::pending()->value
            : $saleOrder->payment_status->value;
    }

    public function updatedPaymentStatus($value)
    {
        $this->saleOrder->update([
            'payment_status' => $this->payment_status,
        ]);

        $this->notification()->success('Payment status updated');
    }

    public function deletePayment($paymentId)
    {
        $payment = $this->saleOrder->payments()->findOrFail($paymentId);
        $payment->delete();

        $this->notification()->success('Payment deleted successfully');
        $this->emit('refreshPurchaseBrowse');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(false);

        $this->setConfigurableAreas([
            'toolbar-left-start' => 'livewire.sales.sections.form.payment-toolbar',
        ]);
    }

    public function builder(): Builder
    {
        return $this->saleOrder->payments()
            ->with(['account'])
            ->select('payments.*')
            ->orderBy('created_at', 'DESC')
            ->getQuery();
    }

    public function columns(): array
    {
        /*
            Date
            Reference
            Account
            Amount
            Paid By
            Action
        */
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),

            Column::make("Date", "created_at")
                ->format(fn($value) => $value->format('M. d, Y')),

            Column::make("Reference No.", "payment_reference"),

            Column::make("Account")
                ->label(function ($row) {
                    return "{$row->account->name} ({$row->account->account_no})";
                }),

            Column::make("Amount", "amount")
                ->label(function ($row) {
                    return formatMoney($row->amount, true);
                }),

            Column::make("Paid By", "paying_method"),

            Column::make('Action')
                ->label(function ($row) {
                    return view('livewire.sales.sections.form.delete-payment-action', ['row' => $row]);
                }),

        ];
    }
}
