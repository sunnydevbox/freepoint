<?php

namespace App\Http\Livewire\Sales;

use App\Enums\PaymentStatusEnum;
use App\Enums\SaleStatusEnum;
use App\Services\SaleOrderService;
use Livewire\WithPagination;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Sale;
use App\Repositories\Warehouse\WarehouseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Rappasoft\LaravelLivewireTables\Views\Filters\DateFilter;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;

class Browse extends DataTableComponent
{
    use WithPagination;

    protected $model = Sale::class;

    protected $listeners = ['refreshSaleBrowse' => '$refresh'];

    public Collection $assignedWarehouses;

    protected WarehouseRepository $warehouseRepository;

    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->warehouseRepository = app(WarehouseRepository::class);
    }

    public function mount()
    {
        $this->assignedWarehouses = $this->warehouseRepository->getListByCurrentuser();
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(true);
    }

    public function builder(): Builder
    {
        $user = auth()->user();
        $canAccessAllInThisBranch = $user->hasRole(['Area Manager', 'Branch Manager', 'business-manager', 'admin']);

        return Sale::query()
            ->orderBy('created_at', 'DESC')
            ->groupBy('id')
            ->with([
                'items',
            ])
            ->when(!$canAccessAllInThisBranch, fn($query) => $query->where('sales.user_id', $user->id))
            ->when(
                empty(data_get(request()->all(), 'table.filters.location')),
                fn($query) => $query
                    ->whereIn('sales.warehouse_id', $this->assignedWarehouses->pluck('id')->toArray())
            );
        ;
    }

    public function delete($id)
    {
        try {
            (new SaleOrderService)->destroy($id);

            // display notification
        } catch (Exception $e) {
            // display notification
            throw $e;
        }
    }

    public function filters(): array
    {
        return [

            DateFilter::make('Start Date')
                ->filter(function (Builder $builder, string $value) {
                    $startDate = Carbon::createFromFormat('Y-m-d', $value);
                    $builder->where('sales.created_at', '>=', $startDate->startOfDay());
                }),

            DateFilter::make('End Date')
                ->filter(function (Builder $builder, string $value) {
                    $endDate = Carbon::createFromFormat('Y-m-d', $value);
                    $builder->where('sales.created_at', '<=', $endDate->endOfDay());
                }),

            DateFilter::make('SO Date')
                ->filter(function (Builder $builder, string $value) {
                    $date = Carbon::createFromFormat('Y-m-d', $value);
                    $builder->whereBetween(
                        'sales.created_at',
                        [
                            $date->copy()->startOfDay(),
                            $date->copy()->endOfDay(),
                        ]
                    );
                }),

            SelectFilter::make('Location')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        $this->assignedWarehouses->pluck('name', 'id')->toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('sales.warehouse_id', $value);
                    }
                }),

            SelectFilter::make('SO Status')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        SaleStatusEnum::toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('sale_status', $value);
                    }
                }),

            SelectFilter::make('Payment Status')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        PaymentStatusEnum::toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('payment_status', $value);
                    }
                }),

        ];
    }

    public function columns(): array
    {
        /*
            Grand Total
            Returned Amount
            Paid
            Due
            Payment Status
            Action
        */
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),

            Column::make("reference_no", "reference_no")
                ->searchable()
                ->hideIf(true),

            Column::make("Date", "created_at")
                ->format(fn($value) => $value->format('M. d, Y'))
                ->sortable(),

            Column::make("Author", "author.name"),

            Column::make("Official Receipt", "receipt_no")
                ->searchable()
                ->sortable(),

            Column::make("Biller", "biller.name")
                ->searchable()
                ->sortable(),

            Column::make('Customer', 'customer.name')
                ->searchable()
                ->sortable(),

            Column::make("Sale Status", "sale_status")
                ->searchable()
                ->format(function ($value, $row) {
                    return SaleStatusEnum::tryFrom($value)->label;
                })
                ->sortable(),

            Column::make("Payment Status", "payment_status")
                ->format(function ($value, $row) {
                    $status = paymentStatusFormatter($value);
                    return '<div class="badge badge-' . $status['class_type'] . '">' . $status['label'] . '</div>';
                })
                ->html()
                ->sortable(),

            Column::make("Grant Total", "grand_total")
                ->format(function ($value, $row) {
                    return formatMoney($value, true);
                })
                ->sortable(),

            Column::make("Paid", "paid_amount")
                ->sortable()
                ->format(fn($value) => formatMoney($value))
                ->html(),

            Column::make("Last update", "updated_at")
                ->format(fn($value) => $value->format('M. d, Y'))
                ->sortable(),

            Column::make('Actions')
                ->label(
                    fn($row) => view(
                        'livewire.sales.actions',
                        [
                            'id' => $row->id,
                            'balance' => $row->balance,
                        ]
                    )
                ),
        ];
    }

    public function applySearch(): Builder
    {
        if ($this->searchIsEnabled() && $this->hasSearch()) {
            $searchableColumns = $this->getSearchableColumns();

            if ($searchableColumns->count()) {
                $this->setBuilder(
                    $this->getBuilder()->where(function ($query) use ($searchableColumns) {
                        foreach ($searchableColumns as $index => $column) {
                            if ($column->hasSearchCallback()) {
                                ($column->getSearchCallback())($query, $this->getSearch());
                            } else {
                                $query->{$index === 0 ? 'where' : 'orWhere'}($column->getColumn(), 'like', '%' . $this->getSearch() . '%');
                            }
                        }
                    })
                        ->leftJoin(
                            'product_sales',
                            'product_sales.sale_id',
                            '=',
                            'sales.id'
                        )
                        ->leftJoin(
                            'products',
                            'products.id',
                            '=',
                            'product_sales.product_id'
                        )
                        ->orWhere('products.name', 'like', '%' . $this->getSearch() . '%')
                        ->orWhere('products.model', 'like', '%' . $this->getSearch() . '%')
                        ->orWhere('products.code', 'like', '%' . $this->getSearch() . '%')
                );
            }
        }
        return $this->getBuilder();
    }

    public function getSearch(): ?string
    {
        $this->{$this->getTableName()}['search'] = trim($this->{$this->getTableName()}['search']);
        return $this->{$this->getTableName()}['search'] ?? null;
    }
}
