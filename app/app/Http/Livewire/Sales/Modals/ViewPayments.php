<?php

namespace App\Http\Livewire\Sales\Modals;

use App\Models\Sale;
use LivewireUI\Modal\ModalComponent;

class ViewPayments extends ModalComponent
{
    public $purchase_id;

    public function mount($purchase_id)
    {
        $this->purchase_id = $purchase_id;
    }
    public static function modalMaxWidth(): string
    {
        // dd(config('livewire-ui-modal.component_defaults.modal_max_width', '2xl'));
        return 'md';
    }

    public function render()
    {
        $purchase = Purchase::with('payments.user')->find($this->purchase_id);

        return view('livewire.sales.modals.view-payments', [
            'payments' => $purchase->payments
        ]);
    }
}
