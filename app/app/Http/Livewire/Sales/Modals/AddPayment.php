<?php

namespace App\Http\Livewire\Sales\Modals;

use App\Enums\PaymentMethodEnum;
use App\Enums\PaymentStatusEnum;
use App\Models\Account;
use App\Models\GeneralSetting;
use App\Models\Sale;
use App\Services\PaymentService;
use Exception;
use LivewireUI\Modal\ModalComponent;
use Stripe\PaymentMethod;
use WireUi\Traits\Actions;

class AddPayment extends ModalComponent
{
    use Actions;

    public $saleOrder;
    public $purchase_id;

    public $paid_by_id = 1;

    public $due = 0;

    public $paying_amount = 0.00;

    public $amount = 0;

    public float $change = 0;

    public $account_id;

    public $payment_note;

    public $cheque_no;
    public $showChequeField = false;

    public $showCreditCardField = false;

    public $disabled = true;

    public $selectedPaymentMethod;

    protected $messages = [
        'paying_amount.required' => 'The paying amount field is required.',
        'paying_amount.numeric' => 'The paying amount must be a numeric value.',
        'amount.required' => 'The amount field is required.',
        'amount.numeric' => 'The amount must be a numeric value.',
        'amount.lte' => 'The amount must be less than or equal to the paying amount.',
    ];

    public function mount(int|Sale $saleOrder)
    {
        if (is_numeric($saleOrder)) {
            $saleOrder = Sale::findOrFail($saleOrder);
        }

        $this->saleOrder = $saleOrder;
        $this->selectedPaymentMethod = PaymentMethodEnum::cash()->value;

        $this->due = $this->saleOrder->due;
        $this->paying_amount = $this->saleOrder->due;
        $this->amount = $this->paying_amount;
        $this->change = $this->paying_amount - $this->amount;
    }

    public function doAddPayment()
    {
        try {
            // throw new Exception('asdasd');
            $data = [
                'amount' => $this->paying_amount,
                'paid_amount' => $this->amount,
                'paid_by_id' => 1,
                'payment_note' => $this->payment_note,
                'account_id' => $this->account_id,
                'paying_amount' => $this->paying_amount,
                'cheque_no' => $this->cheque_no,
            ];

            (new PaymentService)->addPayment(PaymentMethodEnum::tryFrom($this->selectedPaymentMethod), $this->saleOrder, $data);
            $this->notification()->success('Payment added successfully');
            $this->emit('refreshPurchaseBrowse');
            $this->closeModal();
        } catch (Exception $e) {
            dd($e->getMessage());
            $this->notification()->warning('There was a problem processing your request');

            // throw $e;
        }
    }

    public function getPurchaseOrder($purchaseId = null)
    {
        return Sale::find($purchaseId ?? $this->purchase_id);
    }

    public function updatedPaidById($value)
    {
        $this->showChequeField = false;

        if ($value == 4) {
            $this->showChequeField = true;
            $this->showCreditCardField = false;
        }
    }

    public function updatedSelectedPaymentMethod($value)
    {
        $this->showChequeField = false;

        if ($value == PaymentMethodEnum::cheque()->value) {
            $this->showChequeField = true;
            $this->showCreditCardField = false;
        }
    }

    public function updatedPayingAmount($value)
    {
        $this->paying_amount = !is_numeric($value) ? 0 : $value;
        $this->calculateChange();
    }

    public function updatedAmount($value)
    {
        $this->amount = !is_numeric($value) ? 0 : $value;
        $this->calculateChange();
    }

    public function calculateChange()
    {
        $this->change = -0;

        $this->validate([
            'paying_amount' => 'required|numeric',
            'amount' => 'required|numeric|lte:paying_amount',
        ]);

        $this->change = $this->paying_amount - $this->amount;
    }

    public function doAddPayment_()
    {
        $data = [
            'paid_amount' => $this->amount,
            'paid_by_id' => 1,
            'payment_note' => $this->payment_note,
            'account_id' => $this->account_id,
            'paying_amount' => $this->paying_amount,

        ];

        (new PaymentService)->addPayment(
            PaymentMethodEnum::tryFrom($this->selectedPaymentMethod),
            $this->saleOrder,
            $data
        );
        $this->emit('refreshPurchaseBrowse');
        $this->closeModal();
    }

    public function render()
    {
        $accounts = Account::where('is_active', true)->orderBy('is_default', 'DESC')->orderBy('name')->get();
        $this->account_id = $accounts->where('is_default', true)->first()->id;

        return view(
            'livewire.sales.modals.add-payment',
            [
                'paymentMethods' => PaymentMethodEnum::toArray(),
                'purchaseOrder' => $this->getPurchaseOrder(),
                'accounts' => $accounts,
            ]
        );
    }
}
