<?php

namespace App\Http\Livewire\Sales\Modals;

use App\Models\Sale;
use LivewireUI\Modal\ModalComponent;

class ViewDetails extends ModalComponent
{
    public int $saleId;

    public $object = null;

    public function mount(int $saleId)
    {
        $this->saleId = $saleId;
    }


    public static function modalMaxWidth(): string
    {
        return 'sm';
    }

    public function render()
    {
        $sale = Sale::with([
            'author',
            'warehouse',
            'customer',
            'items',
        ])
        ->find($this->saleId);
        
        $sale->load('items.product');

        return view(
            'livewire.sales.modals.view-details',
            [
                'sale' => $sale
            ]
        );
    }
}
