<?php

namespace App\Http\Livewire\Sales\Modals;

use LivewireUI\Modal\ModalComponent;
use App\Services\CustomerGroupService;
use WireUi\Traits\Actions;
use App\Services\CustomerService;

class AddCustomer extends ModalComponent
{
    use Actions;

    public string $name = '';
    public string $email = '';
    public string $company_name = '';
    public string $phone_number = '';
    public string $tax_no = '';
    public string $city = '';
    public string $state = '';
    public string $address = '';
    public string $postal_code = '';
    public string $country = '';
    public int $customer_group_id = 1;

    public $customerGroup = null;
 
    protected $rules = [
        'name' => 'required',
        'customer_group_id' => 'required',
        'email' => 'required|email',
        'company_name' => 'required',
        'phone_number' => 'required',
        'tax_no' => 'nullable',
        'city' => 'required',
        'state' => 'nullable',
        'address' => 'required',
        'postal_code' => 'nullable',
        'country' => 'nullable',
    ];

    public function mount()
    {
        $this->customerGroup = CustomerGroupService::getRepository()->getActive();
    }

    public static function modalMaxWidth(): string
    {
        return 'sm';
    }

    public function store()
    {
        $validated = $this->validate();
        $validated['is_active'] = 1;
        CustomerService::getRepository()->create($validated);
        $this->emit('customerCreated');
        $this->notification()->success('Success!', 'Customer successfully added.');
        $this->closeModal();
    }

    public function render()
    {
        return view(
            'livewire.sales.modals.add-customer',
            [
                'customerGroup' => $this->customerGroup,
            ]
        );
    }
}
