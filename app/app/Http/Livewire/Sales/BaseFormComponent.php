<?php

namespace App\Http\Livewire\Sales;

use App\Models\Sale;
use Livewire\Component;

class BaseFormComponent extends Component
{
    public $saleOrder;

    public $generalSettings;

    public $reference_no;

    public $isNew;

    public function mount(
        Sale $saleOrder
    ) {
        $this->saleOrder = $saleOrder;

        $this->reference_no = $saleOrder->reference_no ? "#{$saleOrder->reference_no}" : '';

        $this->isNew = empty($this->saleOrder->id);
    }

    public function render()
    {
        return view('livewire.sales.edit');
    }
}
