<?php

namespace App\Http\Livewire\Purchase\Sections\Form;

use App\Enums\PurchaseStatusEnum;
use App\Models\Purchase;
use App\Services\PurchaseOrderService;
use Carbon\Carbon;
use Exception;
use Livewire\Component;

class Basic extends Component
{
    public Purchase $purchaseOrder;

    public $generalSettings;

    public $warehouses;

    public $suppliers;

    public $created_at;

    public $warehouse_id;

    public $supplier_id;

    public $status;
    public $invoice_number;

    protected $listeners = ['refreshComponent' => '$refresh'];

    public function mount(
        $purchaseOrder,
        $generalSettings,
        $warehouses,
        $suppliers
    ) {
        $this->purchaseOrder = $purchaseOrder;
        $this->generalSettings = $generalSettings;
        $this->warehouses = $warehouses;
        $this->suppliers = $suppliers;

        $this->created_at = Carbon::parse($purchaseOrder->created_at)->format($generalSettings->date_format);
        $this->warehouse_id = $purchaseOrder->warehouse->id;
        $this->supplier_id = $purchaseOrder->supplier?->id;
        $this->status = $purchaseOrder?->status ?: PurchaseStatusEnum::pending()->value;
        $this->invoice_number = $purchaseOrder->invoice_number;
    }

    public function canChangeStatus()
    {
        return canEditPurchaseOrderItem($this->purchaseOrder);
    }

    public function getStatusLabel()
    {
        return PurchaseStatusEnum::tryFrom($this->purchaseOrder?->status)?->label;
    }

    public function save()
    {
        (new PurchaseOrderService)->update(
            $this->purchaseOrder,
            [
                'created_at' => $this->created_at,
                'warehouse_id' => $this->warehouse_id,
                'supplier_id' => $this->supplier_id,
                'status' => $this->status,
                'invoice_number' => $this->invoice_number,
            ]
        );

        $this->emit('refreshComponent');
        $this->emit('refreshAttachedProducts');
    }

    public function updatedWarehouseId($id)
    {
        $this->emit('refreshAttachedProducts');
    }


    public function rules()
    {
        return [
            'supplier_id' => 'required',
            'warehouse_id' => 'required',
            'created_at' => 'required|date',
            'invoice_number' => 'required|unique:purchases,invoice_number,' . $this->purchaseOrder->id,
        ];
    }

    public function canEdit()
    {
        return canEditPurchaseOrderItem($this->purchaseOrder);
    }


    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName, $this->rules());

        $this->purchaseOrder = (new PurchaseOrderService)->update(
            $this->purchaseOrder,
            [
                "{$propertyName}" => $this->{$propertyName},
            ]
        );
        // if ($purchase->isDirty()) {
        // dd($this->purchaseOrder);
        (new PurchaseOrderService)->recalculatePurchase($this->purchaseOrder);
        // }
        $this->emit('refreshComponent');
    }

    public function render()
    {
        return view('livewire.purchase.sections.form.basic');
    }
}
