<?php

namespace App\Http\Livewire\Purchase\Sections\Form;

use App\Services\PurchaseOrderService;
use Livewire\Component;

class Extra extends Component
{
    public $purchaseOrder;

    public $taxes;

    public $order_tax_rate;

    public $order_discount;


    public $shipping_cost;

    public $note;

    public function mount(
        $purchaseOrder,
        $taxes
    ) {
        $this->purchaseOrder = $purchaseOrder;
        $this->taxes = $taxes;

        $this->note = $purchaseOrder->note;
        $this->order_tax_rate = $purchaseOrder->order_tax_rate;
        $this->order_discount = $purchaseOrder->order_discount;
        $this->shipping_cost = $purchaseOrder->shipping_cost;
    }

    public function rules()
    {
        return [
            'order_tax_rate' => 'numeric',
            'order_discount' => 'numeric',
            'shipping_cost' => 'numeric',
        ];
    }

    public function updated($propertyName): void
    {
        $this->validateOnly($propertyName, $this->rules());
        
        $this->purchaseOrder = (new PurchaseOrderService)->update(
            $this->purchaseOrder,
            [
                "{$propertyName}" => $this->{$propertyName},
            ]
        );
        $this->emit('refreshSummary');
    }


    
    public function render()
    {
        return view('livewire.purchase.sections.form.extra');
    }
}
