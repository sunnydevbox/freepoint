<?php

namespace App\Http\Livewire\Purchase\Sections;

use App\Models\GeneralSetting;
use Livewire\Component;

class POSummary extends Component
{
    public $purchaseOrder;

    public $decimal = 2;
    public $items = 0;

    public $total = 0;

    public $orderTax = 0;

    public $orderDiscount = 0;

    public $shippingCost = 0;

    public $grandTotal = 0;

    protected $listeners = ['refreshSummary' => 'refreshSummary'];


    public function refreshSummary()
    {
        $this->purchaseOrder = $this->purchaseOrder->fresh();
        $this->initiateProperties();
    }

    private function initiateProperties()
    {
        // dd($this->purchaseOrder);
        $this->items = $this->purchaseOrder->item;
        $this->total = $this->purchaseOrder->total_cost;
        // $this->orderTax = $this->purchaseOrder->total_tax;
        $this->orderTax = $this->purchaseOrder->order_tax;
        $this->orderDiscount = $this->purchaseOrder->order_discount;
        $this->shippingCost = $this->purchaseOrder->shipping_cost;
        $this->grandTotal = $this->purchaseOrder->grand_total;
    }

    public function mount($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
        $this->initiateProperties();

        // ($purchaseOrder->total_cost + $purchaseOrder->total_tax + $purchaseOrder->total_tax) - $purchaseOrder->order_discount;
    }

    public function render()
    {
        return view('livewire.purchase.sections.p-o-summary');
    }
}
