<?php

namespace App\Http\Livewire\Purchase\Modals;

use App\Models\Purchase;
use LivewireUI\Modal\ModalComponent;

class ViewDetails extends ModalComponent
{
    public int $purchaseId;

    public $object = null;

    public function mount(int $purchaseId)
    {
        $this->purchaseId = $purchaseId;
    }


    public static function modalMaxWidth(): string
    {
        return 'sm';
    }

    public function render()
    {
        $P = Purchase::with([
            'author',
            'warehouse',
            'supplier',    
        ])
        ->with([
            'items.product',
            'items.product.unit',
            'items.batch', 
            'items' => function($query) {
                $query->orderBy('order_position','ASC');
        }])
        ->find($this->purchaseId);


        return view(
            'livewire.purchase.modals.view-details',
            [
                'purchaseOrder' => $P,
            ]
        );
    }
}
