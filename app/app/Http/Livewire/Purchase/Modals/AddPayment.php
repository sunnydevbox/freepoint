<?php

namespace App\Http\Livewire\Purchase\Modals;

use App\Enums\PaymentMethodEnum;
use App\Models\Account;
use App\Models\Purchase;
use App\Services\PurchaseOrderService;
use LivewireUI\Modal\ModalComponent;

class AddPayment extends ModalComponent
{
    public $purchaseOrder;
    public $purchase_id;
    // 1 = admin
    public $paid_by_id = 1;

    public $due = 0;

    public float $paying_amount;

    public string $formattedPayingAmount;

    public float $amount;

    public string $formattedAmount;

    public float $change;

    public string $formattedChange;

    public $account_id;

    public $payment_note;

    public $showChequeField = false;

    public $showCreditCardField = false;

    public $disabled = true;

    protected $messages = [
        'paying_amount.required' => 'The paying amount field is required.',
        'paying_amount.numeric' => 'The paying amount must be a numeric value.',
        'amount.required' => 'The amount field is required.',
        'amount.numeric' => 'The amount must be a numeric value.',
        'amount.lte' => 'The amount must be less than or equal to the paying amount.',
    ];

    public function mount($purchase_id)
    {
        $this->purchase_id = $purchase_id;
        $this->purchaseOrder = $this->getPurchaseOrder();
        $this->due = $this->purchaseOrder->due;
        // By default we will set the amount to be paid 
        // equivalent to due amount
        $this->paying_amount = $this->purchaseOrder->due;
        $this->amount = $this->paying_amount;
        $this->change = $this->paying_amount - $this->amount;
        $this->formattedPayingAmount = formatMoney($this->purchaseOrder->due);
        $this->formattedAmount = $this->formattedPayingAmount;
    }

    public function getPurchaseOrder($purchaseId = null)
    {
        return Purchase::find($purchaseId ?? $this->purchase_id);
    }

    public function updatedPaidById($value)
    {
        $this->showChequeField = false;

        if ($value == 4) {
            $this->showChequeField = true;
            $this->showCreditCardField = false;
        }
    }

    public function updatedPayingAmount($value)
    {
        $this->calculateChange();
    }

    public function updatedAmount($value)
    {
        $this->calculateChange();
    }

    public function calculateChange()
    {
        $this->change = -0;

        $this->validate([
            'paying_amount' => 'required|numeric',
            'amount' => 'required|numeric|lte:paying_amount',
        ]);

        $this->change = $this->paying_amount - $this->amount;
    }

    /**
     * Sanitize the provided amount by removing any non-numeric characters
     * except for the decimal point, and round it to two decimal places.
     * 
     * Input: "1234.5678" → Output: "1234.57"
     * Input: "999.999" → Output: "1000.00"
     *
     * @param string $value The input string representing an amount that may contain
     *                      unwanted characters.
     * @return string
     */
    function sanitizeAmount(int|float|string $value): string
    {
        if (!is_numeric(str_replace(',', '', $value))) {
            throw new \Exception('Invalid Input');
        }

        // Return the rounded value as a string with two decimal places
        return formatMoney($value);
    }



    public function updatedFormattedPayingAmount($value)
    {
        $this->formattedPayingAmount = $this->sanitizeAmount($value);
        $this->calculateChange();
    }

    public function updatedFormattedAmount($value)
    {
        $this->formattedAmount = $this->sanitizeAmount($value);
        $this->calculateChange();
    }

    public function doAddPayment()
    {

        $this->paying_amount = $this->sanitizeAmount($this->formattedPayingAmount);
        $this->amount = $this->sanitizeAmount($this->formattedAmount);

        $this->purchaseOrder = (new PurchaseOrderService)->addPayment(
            $this->purchaseOrder,
            [
                'paid_amount' => $this->amount,
                'paid_by_id' => $this->paid_by_id,
                'payment_note' => $this->payment_note,
                'account_id' => $this->account_id,
                'paying_amount' => $this->paying_amount,
            ]
        );

        $this->emit('refreshPurchaseBrowse');
        $this->closeModal();
    }

    public function render()
    {
        $accounts = Account::where('is_active', true)->orderBy('is_default', 'DESC')->orderBy('name')->get();
        $this->account_id = $accounts->where('is_default', true)->first()->id;
        return view(
            'livewire.purchase.modals.add-payment',
            [
                'paymentMethods' => PaymentMethodEnum::toArray(),
                'purchaseOrder' => $this->getPurchaseOrder(),
                'accounts' => $accounts,
            ]
        );
    }
}
