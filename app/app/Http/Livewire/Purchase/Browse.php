<?php

namespace App\Http\Livewire\Purchase;

use App\Enums\PaymentStatusEnum;
use App\Enums\PurchaseStatusEnum;
use App\Services\PurchaseOrderService;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Purchase;
use App\Repositories\Warehouse\WarehouseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Rappasoft\LaravelLivewireTables\Views\Filters\DateFilter;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;
use WireUi\Traits\Actions;

class Browse extends DataTableComponent
{
    use Actions;

    protected $model = Purchase::class;

    protected $listeners = ['refreshPurchaseBrowse' => '$refresh'];

    public Collection $assignedWarehouses;

    protected WarehouseRepository $warehouseRepository;


    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->warehouseRepository = app(WarehouseRepository::class);
    }

    public function mount()
    {
        $this->assignedWarehouses = $this->warehouseRepository->getListByCurrentuser();
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(true);
    }

    public function builder(): Builder
    {
        $user = auth()->user();

        $canAccessAllInThisBranch = $user->hasRole(['Area Manager', 'Branch Manager', 'business-manager', 'admin']);

        return Purchase::query()
            ->orderBy('created_at', 'DESC')
            ->with([
                'supplier',
                'warehouse',
                'returns',
                'items.product',
            ])
            ->when(!$canAccessAllInThisBranch, fn($query) => $query->whereUserId($user->id))
            ->when(
                empty(data_get(request()->all(), 'table.filters.location')),
                fn($query) => $query
                    ->whereIn('purchases.warehouse_id', $this->assignedWarehouses->pluck('id')->toArray())
            );
    }

    public function applySearch(): Builder
    {
        if ($this->searchIsEnabled() && $this->hasSearch()) {
            $searchableColumns = $this->getSearchableColumns();

            if ($searchableColumns->count()) {
                $this->setBuilder($this->getBuilder()->where(function ($query) use ($searchableColumns) {
                    foreach ($searchableColumns as $index => $column) {
                        if ($column->hasSearchCallback()) {
                            ($column->getSearchCallback())($query, $this->getSearch());
                        } else {
                            $query->{$index === 0 ? 'where' : 'orWhere'}($column->getColumn(), 'like', '%' . $this->getSearch() . '%');
                        }
                    }
                }));
            }

            $this->setBuilder(
                $this->getBuilder()
                    ->leftJoin(
                        'product_purchases',
                        'product_purchases.purchase_id',
                        '=',
                        'purchases.id'
                    )
                    ->leftJoin(
                        'products',
                        'products.id',
                        '=',
                        'product_purchases.product_id'
                    )
                    ->orWhere('products.name', 'like', '%' . $this->getSearch() . '%')
                    ->orWhere('products.model', 'like', '%' . $this->getSearch() . '%')
                    ->orWhere('products.code', 'like', '%' . $this->getSearch() . '%')
                    ->distinct('products.id')
            );
        }

        return $this->getBuilder();
    }

    public function filters(): array
    {
        return [
            DateFilter::make('PO Date')
                ->filter(function (Builder $builder, string $value) {
                    $date = Carbon::createFromFormat('Y-m-d', $value);
                    $builder->whereBetween(
                        'purchases.created_at',
                        [
                            $date->copy()->startOfDay(),
                            $date->copy()->endOfDay()
                        ]
                    );
                }),

            SelectFilter::make('Location')
                ->options(
                    $this->warehouseRepository
                        ->listForDropdownByCurrentUser(true)
                        ->prepend('All', '')
                        ->toArray()
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('purchases.warehouse_id', $value);
                    }
                }),

            SelectFilter::make('Purchase Status')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        PurchaseStatusEnum::toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('status', $value);
                    }
                }),

            SelectFilter::make('Payment Status')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        PaymentStatusEnum::toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('payment_status', $value);
                    }
                }),

        ];
    }


    public function delete($purhcaseOrderId)
    {
        try {
            (new PurchaseOrderService)->delete(Purchase::find($purhcaseOrderId));
            $this->notification()->success(
                'Success',
                'The purchase order was successfully deleted'
            );
        } catch (Exception $e) {
            $this->notification()->error(
                'We could not process your request',
                $e->getMessage()
            );
        }
    }

    public function columns(): array
    {
        /*
            Grand Total
            Returned Amount
            Paid
            Due
            Payment Status
            Action
        */
        return [

            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),

            Column::make("Date", "created_at")
                ->format(fn($value) => $value->format('M. d, Y'))
                ->sortable(),

            Column::make("Author", "author.name"),

            Column::make("Reference #", "reference_no")
                ->searchable()
                ->sortable(),

            Column::make("Invoice #", "invoice_number")
                ->searchable()
                ->sortable(),

            Column::make('Location', 'warehouse.name')
                ->searchable()
                ->sortable(),

            Column::make("Supplier", "supplier.name")
                ->searchable()
                ->sortable(),

            Column::make("Grand total", "grand_total")
                ->format(function ($value, $row) {
                    return formatMoney($value, true);
                })
                ->sortable(),

            Column::make("Returned Amount", "grand_total")
                ->format(function ($value, $row) {
                    return formatMoney($row->returns->sum('grand_total'), true);
                })
                ->sortable(),

            Column::make("Paid amount", "paid_amount")
                ->sortable(),

            Column::make("Due", "balance")
                ->label(
                    function ($row, Column $column) {
                        return formatMoney($row->due, true);
                    }
                ),
            // ->sortable(),

            Column::make("Status", "status")
                ->sortable()
                ->format(function ($value) {
                    $status = purchaseStatusFormatter($value);
                    return '<div class="badge badge-' . $status['class_type'] . '">' . $status['label'] . '</div>';
                })
                ->html(),
            Column::make("Payment status", "payment_status")
                ->sortable()
                ->format(function ($value) {
                    $status = paymentStatusFormatter($value);
                    return '<div class="badge badge-' . $status['class_type'] . '">' . $status['label'] . '</div>';
                })
                ->html(),

            Column::make("Last update", "updated_at")
                ->format(fn($value) => $value->format('M. d, Y'))
                ->sortable(),

            Column::make('Actions')
                ->label(
                    function ($row) {
                        // dd($row, $column);
                        return view(
                            'livewire.purchase.actions',
                            [
                                'id' => $row->id,
                                'balance' => $row->balance,
                                'label' => $row->reference_no,
                            ]
                        );
                    }
                ),
        ];
    }
}
