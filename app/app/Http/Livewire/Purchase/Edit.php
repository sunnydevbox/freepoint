<?php

namespace App\Http\Livewire\Purchase;

use App\Models\Purchase;
use Livewire\Component;

class Edit extends Component
{
    public $purchaseOrder;

    public $generalSettings;

    public $warehouses;

    public $suppliers;

    public $taxes;

    public function mount(
        Purchase $purchaseOrder,
        $generalSettings,
        $warehouses,
        $suppliers,
        $taxes
    ) {
        $this->purchaseOrder = $purchaseOrder;
        $this->generalSettings = $generalSettings;
        $this->warehouses = $warehouses;
        $this->suppliers = $suppliers;
        $this->taxes = $taxes;
    }

    public function render()
    {
        return view('livewire.purchase.edit');
    }
}
