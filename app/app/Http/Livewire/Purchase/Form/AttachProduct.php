<?php

namespace App\Http\Livewire\Purchase\Form;

use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Services\PurchaseOrderService;
use Exception;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class AttachProduct extends Component
{
    public $purchaseOrder = null;

    public $term = '';

    public $results = [];

    protected $listeners = ['refreshAttachProducts' => '$refresh'];

    public $warehouseBins;

    public function mount($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
        $this->warehouseBins = $purchaseOrder->warehouse->bins->pluck('name', 'id')->toArray();
        $this->_reset();
    }

    public function _reset()
    {
        $this->term = '';
        $this->results = [];
    }

    public function addProduct($productId)
    {
        try {
            (new PurchaseOrderService)->addItem($this->purchaseOrder, $productId);

            $this->term = '';

            return redirect()->route('purchases.edit', ['purchase' => $this->purchaseOrder->id]);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    public function updatedTerm($value)
    {
        $this->doSearch();
    }


    public function doSearch()
    {
        if (strlen($this->term) < 3) {
            $this->results = collect([]);
        } else {
            $this->results = Product::where(function ($query) {
                $query->where('name', 'LIKE', "%{$this->term}%")
                    ->orWhere('code', 'LIKE', "%{$this->term}%");
            })
                ->orderBy('name', 'ASC')
                ->active()
                ->get();
        }
    }

    public function render()
    {
        return view('livewire.purchase.form.attach-product');
    }
}
