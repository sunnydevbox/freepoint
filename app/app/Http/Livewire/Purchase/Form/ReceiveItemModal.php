<?php

namespace App\Http\Livewire\Purchase\Form;

use LivewireUI\Modal\ModalComponent;

class ReceiveItemModal extends ModalComponent
{
    public function render()
    {
        return view('livewire.purchase.form.receive-item-modal');
    }
}
