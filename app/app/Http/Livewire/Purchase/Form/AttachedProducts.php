<?php

namespace App\Http\Livewire\Purchase\Form;

use App\Enums\PurchaseStatusEnum;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Traits\WithReordering;
use Rappasoft\LaravelLivewireTables\Views\Column;
use WireUi\Traits\Actions;

class AttachedProducts extends DataTableComponent
{
    use WithReordering;
    use Actions;

    public $purchaseOrderId = null;

    public $purchaseOrder = null;

    public $qty = 0;

    public array $bulkActions = [
        'deleteSelected' => 'Delete',
    ];

    protected $listeners = ['refreshAttachedProducts' => '$refresh'];

    public function deleteSelected()
    {
        try {
            if (!empty($this->getSelected())) {
                ProductPurchase::whereIn('id', $this->getSelected())->delete();
                $message = 'Items were removed from the list';
                if (count($this->getSelected()) == 1) {
                    $message = 'Item was removed from the list';
                }

                $this->notification()->success(
                    'Success',
                    $message,
                );

                $this->clearSelected();
                $this->emit('refreshDatatable');
                $this->emit('refreshSummary');
            }
        } catch (Exception $e) {
            $this->notification()->warning(
                'Warning',
                'There was a problem removing one or more items',
            );

            $this->emit('refreshDatatable');
        };
    }

    public function builder(): Builder
    {
        return ProductPurchase::with([
            'product',
            'bin'
        ])
            ->where('purchase_id', $this->purchaseOrderId)
            ->orderBy('order_position', 'ASC')
            ->limit(70)
        ;
    }

    public function changeOrder($items): void
    {
        foreach ($items as $item) {
            ProductPurchase::find($item['value'])->update(['order_position' => $item['order']]);
        }
    }

    public function mount($purchaseOrderId, Purchase $purchaseOrder)
    {
        $purchaseOrder->loadMissing('warehouse.bins');
        $this->purchaseOrderId = $purchaseOrderId;
        $this->purchaseOrder = $purchaseOrder;
    }

    public function getList()
    {
        return ProductPurchase::where('purchase_id', $this->purchaseOrderId)
            ->first();
    }

    public function canEdit()
    {
        return canEditPurchaseOrderItem($this->purchaseOrder);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(false);
        $this->setPaginationStatus(true);
        $this->setReorderEnabled();
        $this->setReorderMethod('changeOrder');
        $this->setDefaultReorderSort('order_position', 'asc');
        $this->setFooterDisabled();
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->hideIf(App::environment() != 'local'),

            Column::make("Id", "purchase_id")
                ->hideIf(true),
            Column::make("Id", "product_id")
                ->hideIf(true),
            Column::make("Id", "warehouse_bin_id")
                ->hideIf(true),

            Column::make("Name")
                ->label(function ($row) {
                    // Custom formatting logic
                    return $row->product->name;
                }),

            // Column::make("Name")
            //     ->format(function ($value, $row) {
            //         // dd(1, $row);
            //         return 'PJ';
            //     }),

            Column::make("Part Number", "product.code"),

            Column::make("Quantity", "qty")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.purchase.form.inline-field-wrapper',
                        [
                            'productPurchaseId' => $row->id,
                            'field' => 'qty',
                            'value' => $row->qty,
                            'type' => 'number',
                            'validation' => 'numeric|required|min:1',
                            'canEdit' => $this->canEdit()
                        ]
                    );
                })
                // ->disabled()
                ->footer(function ($rows) {
                    return $rows->sum('qty');
                }),

            Column::make("Received", "recieved")
                ->format(function ($value, $row) {
                    return view(
                        'livewire.purchase.form.inline-field-receive-wrapper',
                        [
                            'productPurchaseId' => $row->id,
                            'field' => 'recieved',
                            'value' => $row->recieved,
                            'type' => 'number',
                            'validation' => 'numeric|required|min:0',
                            'canEdit' => $this->canEdit()
                        ]
                    );
                }),

            Column::make("Bin Location")
                // ->format(function ($value, $row) {
                ->label(function ($row) {

                    return view(
                        'livewire.purchase.form.inline-bin-location-wrapper',
                        [
                            'productPurchaseId' => $row->id,
                            'productPurchase' => $row,
                            'bin' => $row->warehouse_bin_id,
                            'bins' => $this->purchaseOrder->warehouse->bins,
                            // 'warehouseBins' => $this->purchaseOrder->warehouse->bins->pluck('name', 'id')->toArray(),
                            'field' => 'warehouse_bin_id',
                            'value' => $row->warehouse_bin_id,
                            'canEdit' => $this->canEdit()
                        ]
                    );
                }),

            // Column::make("Order Position", "order_position")
            //     ->hideIf(true),

            // Column::make("Order Position", "order_position")
            //     ->hideIf(true),

            Column::make("Net Unit Cost", "net_unit_cost")
                ->format(fn($value) => formatMoney($value, true))
                ->footer(function ($rows) {
                    return formatMoney($rows->sum('net_unit_cost'), true);
                }),

            Column::make("Discount", "discount")
                ->format(fn($value) => formatMoney($value, true))
                ->footer(function ($rows) {
                    return formatMoney($rows->sum('discount'), true);
                }),

            Column::make("Tax Rate", "tax_rate"),

            Column::make("Subtotal", "total")
                ->format(function ($value) {
                    return formatMoney($value, true);
                })
                ->footer(function ($rows) {
                    return formatMoney($rows->sum('total'), true);
                }),
        ];
    }
}
