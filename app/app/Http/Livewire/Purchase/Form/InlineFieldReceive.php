<?php

namespace App\Http\Livewire\Purchase\Form;

use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Services\PurchaseOrderService;
use Exception;
use Livewire\Component;

class InlineFieldReceive extends Component
{
    public $productPurchaseId = null;

    public $value = 0;

    public $messages = [];

    public $remaining = 0;

    public $received = 0;

    public $minValue = 0;
    public $maxValue = 0;

    public string $validation = '';

    public ProductPurchase $purchaseOrderItem;

    public function mount($productPurchaseId, string $validation = '')
    {
        $this->purchaseOrderItem = ProductPurchase::find($productPurchaseId);
        $this->received = $this->purchaseOrderItem->recieved;
        $this->remaining = $this->maxValue = $this->purchaseOrderItem->remaining;
        $this->validation = $validation;
    }

    public function updatedValue()
    {
        $this->messages = [];
        try {
            if (!empty($this->validation)) {
                $this->validateOnly('value', ['value' => $this->validation]);
            }

            // dd($this->value);

            if ($this->value > 0) {

                $purchaseOrderItem = ProductPurchase::find($this->productPurchaseId);

                $received = $this->value;
                $service = (new PurchaseOrderService);
                $purchaseOrderItem = $service->received($purchaseOrderItem, $this->value);

                $this->remaining = $this->maxValue = $purchaseOrderItem->remaining;
                $this->received = $purchaseOrderItem->recieved;


                $this->emit('refreshAttachedProducts');
                $this->emit('refreshSummary');
            }
        } catch (Exception $e) {
            $this->messages[] = $e->getMessage();
        }

        $this->value = 0;
        // $this->emitTo('inline-field-receive', '$refresh');
        
    }

    public function render()
    {
        return view(
            'livewire.purchase.form.inline-field-receive',
            [
                'messages' => $this->messages,
            ]
        );
    }
}
