<?php

namespace App\Http\Livewire\Purchase\Form;

use App\Models\Product_Warehouse;
use App\Models\ProductPurchase;
use App\Services\PurchaseOrderService;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class InlineField extends Component
{
    public $productPurchaseId = null;

    public $field = null;

    public $value = null;

    public $type = null;

    public string $validation = '';

    public function mount($productPurchaseId, $field, $value, $type, string $validation = '')
    {
        $this->productPurchaseId = $productPurchaseId;
        $this->field = $field;
        $this->value = $value;
        $this->type = $type;
        $this->{$field} = $value;
        $this->validation = $validation;
    }

    public function updated()
    {
        if (!empty($this->productPurchaseId)) {

            if (!empty($this->validation)) {
                $this->validateOnly($this->field, [$this->field => $this->validation]);
            }


            $purchaseOrderItem = ProductPurchase::find($this->productPurchaseId);
            $service = (new PurchaseOrderService);
            $service->itemQuantityChange($purchaseOrderItem, $this->{$this->field});

            $this->emit('refreshAttachedProducts');
            $this->emit('refreshSummary');
        }
    }

    public function render()
    {
        return view('livewire.purchase.form.inline-field');
    }
}
