<?php

namespace App\Http\Livewire\Purchase\Form;

use App\Models\ProductPurchase;
use App\Services\PurchaseOrderService;
use Exception;
use Livewire\Component;

class InlineBinLocation extends Component
{
    public $productPurchaseId;
    public $field = null;
    public $bin = '';
    public $warehouseBins;
    public $productWarehouse = null;
    public $productPurchase = null;

    public $isDisabled = false;

    public bool $canEdit = false;
    protected $listeners = [
        '$refresh',
        // 'refreshInlineBin' =>
    ];

    public function mount($productPurchaseId, $productPurchase, $field, $value, $canEdit = false, $bins)
    {
        $this->productPurchaseId = $productPurchaseId;
        $this->field = $field;
        $this->bin = $value;
        $this->canEdit = $canEdit;
        // dd($productPurchase->purchase);
        $this->productPurchase = $productPurchase; // ProductPurchase::with(['purchase'])->findorFail($productPurchaseId);
        $this->warehouseBins = $bins->pluck('name', 'id')->toArray();

        // dd($this->warehouseBins); // $this->productPurchase->purchase->warehouse->bins;

        if (!empty($this->productPurchase->recieved)) {
            // TODO: Temporarily commented
            // $this->isDisabled = true;
        }

        // IF bin location is already assigned,
        // prevent re-assignment
        if (!empty($this->productPurchase->warehouse_bin_id)) {
            // $this->isDisabled = true;
        }
    }

    public function updatedBin()
    {
        if (!empty($this->bin) && !empty($this->productPurchase)) {
            $this->productPurchase = (new PurchaseOrderService)->assignBin($this->productPurchase, $this->bin);
        }

        $this->emit('refreshAttachedProducts');
    }

    public function render()
    {
        return view('livewire.purchase.form.inline-bin-location');
    }
}
