<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use App\Services\CustomerService;

class CustomerList extends Component
{
    public $customers = null;
    public int $customer_id;

    public $listeners = [
        'customerCreated',
    ];

    public function mount()
    {
        $this->getData();
    }

    public function getData()
    {
        $this->customers = CustomerService::getRepository()->getActive();
    }

    public function customerCreated()
    {
        $this->mount();
        $this->dispatchBrowserEvent('contentChanged');
    }

    public function render()
    {
        return view('livewire.customer.customer-list', [
            'customers' => $this->customers,
        ]);
    }
}