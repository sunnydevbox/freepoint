<?php

namespace App\Http\Livewire\Reports;

use App\Exports\StockCountExport;
use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\Warehouse;
use App\Models\WarehouseBin;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;
use WireUi\Traits\Actions;

class StockReport extends DataTableComponent
{
    use Actions, WithPagination;

    protected $model = Product_Warehouse::class;

    public array $perPageAccepted = [10, 25, 50, 500];


    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(false);
    }

    public function bulkActions(): array
    {
        return [
            'export' => 'Export',
        ];
    }

    public function export()
    {
        return Excel::download(
            new StockCountExport(
                $this->table['filters']['warehouse_id'] ?? null,
                $this->table['filters']['warehouse_bin_id'] ?? null
            ),
            'stock-count-' . now()->format('Ymdhis') . '.xlsx'
        );
    }

    public function filters(): array
    {
        $warehouses = Warehouse::active()
            ->whereHas('bins', fn($query) => $query->whereHas('stocks'))
            ->orderBy('name')
            ->pluck('name', 'id')
            ->toArray();

        $filters = [
            SelectFilter::make('Warehouse', 'warehouse_id')
                ->options([null => 'All'] + $warehouses)
                ->filter(
                    fn(Builder $builder, string $warehouseId) =>
                    $builder->where('product_warehouse.warehouse_id', $warehouseId)
                ),
        ];

        if (!empty($this->table['filters']['warehouse_id'])) {
            $bins = WarehouseBin::whereWarehouseId($this->table['filters']['warehouse_id'])
                ->orderBy('name')
                ->pluck('name', 'id')
                ->toArray();

            $filters[] = SelectFilter::make('Bin', 'warehouse_bin_id')
                ->options($bins)
                ->filter(
                    fn(Builder $builder, string $warehouseId) =>
                    $builder->where('product_warehouse.warehouse_bin_id', $warehouseId)
                );
        }

        return $filters;
    }

    public function builder(): Builder
    {
        return Product::query()
            ->select([
                'products.id',
                'products.name AS product_name',
                'product_warehouse.warehouse_bin_id',
                DB::raw('SUM(product_warehouse.qty) AS sum_qty'),
                'warehouses.name AS warehouse_name',
                'warehouse_bins.name AS bin_name',
            ])
            ->join('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
            ->join('warehouses', 'warehouses.id', '=', 'product_warehouse.warehouse_id')
            ->join('warehouse_bins', 'warehouse_bins.id', '=', 'product_warehouse.warehouse_bin_id')
            ->where('products.is_active', true)
            ->groupBy([
                'products.id',
                'product_warehouse.warehouse_bin_id',
                'products.name',
                'warehouses.name',
                'warehouse_bins.name',
            ])
            ->orderBy('products.name');
    }


    public function columns(): array
    {
        return [
            Column::make('Product Name', 'name'),
            Column::make('Code', 'code'),
            Column::make('Model', 'model'),
            Column::make('Warehouse', 'warehouse_name')
                ->label(fn($row) => $row->warehouse_name),
            Column::make('Bin', 'bin_name')
                ->label(fn($row) => $row->bin_name),
            Column::make('Stock Count   ', 'sum_qty')
                ->label(fn($row) => $row->sum_qty),
        ];
    }
}
