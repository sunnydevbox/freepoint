<?php

namespace App\Http\Livewire\Product\Form\Admin;

use App\Models\Product;
use App\Services\ProductService;
use Livewire\Component;
use WireUi\Traits\Actions;

class PublishStatus extends Component
{
    use Actions;

    public $product;

    public int $is_active = 0;

    public function mount($product = null)
    {
        $this->product = $product;

        if (!empty($product)) {
            $this->is_active = $product->is_active;
        }
    }

    public function updatedIsActive($value)
    {
        if (!empty($this->product)) {
            (new ProductService)->setPublishStatus($this->product, $value);
            $this->notification()->success(
                'Success',
                'Publish status successfully set'
            );
        }
        $this->emitUp('propertyUpdate', 'is_active', $value);
    }

    public function render()
    {
        return view('livewire.product.form.admin.publish-status');
    }
}
