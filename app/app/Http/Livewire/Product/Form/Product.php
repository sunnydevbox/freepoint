<?php

namespace App\Http\Livewire\Product\Form;

use App\Enums\BarCodeSymbologyEnum;
use App\Enums\ProductTypeEnum;
use App\Models\Product as ModelsProduct;
use App\Models\Unit;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Tax\TaxRepository;
use App\Repositories\Unit\UnitRepository;
use App\Services\ProductService;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Spatie\Enum\Laravel\Rules\EnumRule;
use Livewire\Component;

class Product extends Component
{
    /**
     * Product Types
     */
    public $types;

    public $selectedUnitId = null;

    /**
     * Sub-Units
     */
    public $saleUnits = [];

    /**
     * Sub-Units
     */
    public $purchaseUnits = [];


    public $barcodeSymbology = [];

    public $product;

    public $isEditing = false;

    public $name;

    public $code;

    public $type = 'standard';

    public $barcode_symbology = 'C128';

    public $brand_id;

    public $category_id;

    public $unit_id;

    public $purchase_unit_id;

    public $sale_unit_id;

    public $cost = 0;

    public $price = 0;

    public $qty = 0;

    public $alert_quantity;

    public $daily_sale_objective;

    public $promotion;

    public $promotion_price;

    public $starting_date;

    public $last_date;

    public $tax_id;

    public $tax_method;

    public $image;

    public $file;

    public $is_embeded;

    public $is_batch;

    public $is_variant;

    public $is_diffPrice;

    public $is_imei;

    public $featured;

    public $product_list;

    public $variant_list;

    public $qty_list;

    public $price_list;

    public $product_details;

    public $variant_option;

    public $variant_value;

    public $is_active = false;

    public $model;

    /** Additional fields */
    public $year;

    public $version;

    public $listeners = [
        'propertyUpdate' => 'propertyUpdate',
        'productRefresh' => '$refresh',
    ];


    public function propertyUpdate($property = null, $value = null)
    {
        if (isset($this->{$property})) {
            $this->{$property} = $value;
        }
    }

    public function rules()
    {
        $rules = [
            'type' => 'enum:' . ProductTypeEnum::class,
            'name' => 'required|min:3',
            'model' => 'required|min:2',
            'unit_id' => 'required',
            'sale_unit_id' => 'required',
            'purchase_unit_id' => 'required',
            'brand_id' => 'required',
            'code' => 'required|min:2|unique:products,code',
            'category_id' => 'required',
            'code' => 'required|min:2|unique:products,code',
        ];

        if ($this->isEditing) {
            $rules['code'] = [
                'required',
                'min:2',
                Rule::unique('products')
                    ->ignore($this->product)
                    ->where(function ($query) {
                        $query->where('is_active', true);
                    }),
            ];
        }

        return $rules;
    }

    public function mount($product)
    {
        $this->getCategoriesProperty();
        $this->getBrandsProperty();
        $this->isEditing = false;

        if ($product) {
            $this->isEditing = true;
            $this->updatedUnitId($product->unit_id);


            collect($product->toArray())
                ->each(function ($value, $key) {
                    if ($key == 'type') {
                        $this->{$key} = $value->value;
                    } else {
                        $this->{$key} = $value;
                    }
                });
        }

        $this->barcode_symbology = BarCodeSymbologyEnum::C128()->value;

        if (!empty($product)) {
            $this->product = $product;
            $this->isEditing = true;
        }

        $this->types = ProductTypeEnum::toArray();

        $this->barcodeSymbology = BarCodeSymbologyEnum::toArray();
    }

    public function updatedIsActive($v)
    {
        dd($v);
    }

    public function getTaxesProperty()
    {
        return app(TaxRepository::class)
            ->where('is_active', true)
            ->orderBy('name', 'ASC')
            ->get();
    }

    public function getUnitsProperty()
    {
        return app(UnitRepository::class)
            ->where('is_active', true)
            ->whereNull('base_unit')
            ->orderBy('unit_name', 'ASC')
            ->get();
    }

    public function getBrandsProperty()
    {
        return app(BrandRepository::class)
            ->where('is_active', true)
            ->orderBy('title', 'ASC')
            ->get();
    }

    public function getCategoriesProperty()
    {
        return app(CategoryRepository::class)
            ->where('is_active', true)
            ->orderBy('name', 'ASC')
            ->get();
    }

    public function updatedUnitId($id)
    {
        $this->sale_unit_id = null;
        $this->purchase_unit_id = null;
        $this->purchaseUnits = $this->saleUnits = [];

        if (!empty($id)) {
            $this->saleUnits = Unit::where('base_unit', $id)
                ->orWhere('id', $id)
                ->orderBy('unit_name')
                ->get();

            $this->purchaseUnits = $this->saleUnits;

            $this->sale_unit_id = $this->saleUnits->first()->id;
            $this->purchase_unit_id = $this->sale_unit_id;
        }
    }

    public function updatedSaleUnitId($value)
    {
        $this->purchase_unit_id = $value;
    }

    public function submit()
    {
        $this->validate();

        $data = collect((new ModelsProduct)->getFillable())
            ->mapWithKeys(function ($key) {
                return [$key => $this->{$key}];
            })->toArray();

        if ($this->isEditing) {
            $this->product->fill($data)->update();
        } else {

            $this->product = (new ModelsProduct)->create($data);
        }

        if ($this->product) {
            // Store the custome fields
            $this->product->customFields()->create([
                'title' => 'What is your name?',
                'type' => 'text',
            ]);

            Session::flash('create_message', 'Product created successfully');
            return redirect()->route('products.index');
        }

        Session::flash('message', 'There was a problem saving the product');
    }

    public function updatedCode()
    {
        $this->code = trim($this->code);
        $this->validate();
    }

    public function render()
    {
        return view(
            'livewire.product.form.product',
            [
                'product' => $this->product,
                'brands' => $this->brands,
                'categories' => $this->categories,
                'units' => $this->units,
                'taxes' => $this->taxes,
            ]
        );
    }
}
