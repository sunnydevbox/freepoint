<?php

namespace App\Http\Livewire\Product\Form;

use Livewire\Component;

class ExtraFields extends Component
{
    public $product;

    public $year;
    
    public $version;

    public function mount()
    {
        // $this->product = $product;
    }

    public function render()
    {
        return view('livewire.product.form.extra-fields');
    }
}
