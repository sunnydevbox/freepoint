<?php

namespace App\Http\Livewire\Product\History;

use App\Models\Product;
use Livewire\Component;

class Index extends Component
{
    public $productId;

    public $product;

    // TEMP
    public $product_data = [];
    
    public $name;

    public function mount($productId)
    {
        $this->productId = $productId;


        $this->product = Product::find($productId);
    }

    public function render()
    {
        return view(
            'livewire.product.history.index'
        );
    }
}
