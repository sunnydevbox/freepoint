<?php

namespace App\Http\Livewire\Product\History;

use App\Repositories\Warehouse\WarehouseRepository;
use Livewire\Component;

class Filter extends Component
{
    public $warehouseId;

    public $startingDate;

    public $endingDate;

    public $product;

    public function mount($product)
    {
        $this->product = $product;
    }

    public function filter()
    {
        $filter = [
            'warehouse_id' => $this->warehouseId,
            'start_at' => $this->startingDate,
            'end_at' => $this->endingDate
        ];

        $this->emit('applyeFilters', $filter);
        $this->emit('printProductPurchases', $filter);
    }

    public function updatedWarehouseId($value)
    {
        $this->filter();
    }

    public function updated()
    {
        $this->filter();
    }

    public function set()
    {
        dd('set');
    }

    public function render()
    {
        $warehouses = (new WarehouseRepository)->getAllActive();
        return view(
            'livewire.product.history.filter',

            compact('warehouses')
        );
    }
}
