<?php

namespace App\Http\Livewire\Product\History\Tabs;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\ProductPurchase;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class PurchaseOrder extends DataTableComponent
{
    protected $model = ProductPurchase::class;

    public $productId;

    public bool $searchStatus = false;

    public $listeners = [
        'applyeFilters'
    ];

    public function getTheme(): string
    {
        return 'bootstrap-4';
    }

    public function booted(): void
    {
        $this->setTheme();
        parent::booted();
    }

    public function mount($productId)
    {
        $this->productId  = $productId;
    }

    public function builder(): Builder
    {
        return ProductPurchase::with([
            'purchase.supplier',
        ])
            ->whereProductId($this->productId)
            ->orderBy('purchases.created_at', 'DESC');
    }

    public function applyeFilters(array $filters = [])
    {
        $builder = $this->getBuilder();

        if (!empty($filters['warehouse_id'])) {
            $builder = $builder->whereHas('purchase', function ($query) use ($filters) {
                $query->whereWarehouseId($filters['warehouse_id']);
            });
        }

        if (!empty($filters['start_at']) && !empty($filters['end_at'])) {
            $builder = $builder->whereHas('purchase', function ($query) use ($filters) {
                $query->whereBetween('created_at', [$filters['start_at'], $filters['end_at']]);
            });
        }

        $this->setBuilder($builder);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),
            Column::make("Author", "purchase.author.name")
                ->sortable(),

            Column::make("purchase id", "purchase.id")
                ->hideIf(true),

            Column::make("Date", "purchase.created_at")
                ->format(function ($value, $b) {
                    return Carbon::parse($value)->toFormattedDateString();
                }),


            Column::make("Reference no", "purchase.reference_no")
                ->format(function ($value, $row) {
                    return '<a href="' . route('purchases.edit', ['purchase' => $row->{'purchase.id'}]) . '" target="_new">' . $value . '</a>';
                })
                ->html()
                ->sortable(),

            Column::make("Invoice Number", "purchase.invoice_number")
                ->sortable(),

            Column::make("Location", "bin.name")
                ->hideIf(true),

            Column::make("Supplier", "purchase.supplier.name")
                ->sortable(),

            Column::make("Location", "purchase.warehouse.name")
                ->format(
                    fn($value, $row) =>
                    sprintf(
                        '%s: %s',
                        $value,
                        !empty($row->{'bin.name'}) ?  "<strong>{$row->{'bin.name'}}</strong>" : '<em class="text-warning">No BIN assigned</em>'
                    )
                )->html(),

            Column::make("Qty", "qty"),

            Column::make("On-hand", "recieved"),

            Column::make("Unit Price", "net_unit_cost")
                ->sortable(),

            Column::make("Subtotal", "total")
                ->format(function ($value) {
                    return formatMoney($value, true);
                }),
        ];
    }
}
