<?php

namespace App\Http\Livewire\Product\History\Tabs;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Sale as ModelSale;
use App\Models\Product_Sale;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Sale extends DataTableComponent
{
    protected $model = Product_Sale::class;

    public $productId;

    public bool $searchStatus = false;

    public $listeners = [
        'applyeFilters'
    ];

    public function getTheme(): string
    {
        return 'bootstrap-4';
    }

    public function setTableClass(): ?string
    {
        return 'table table-hover dataTable no-footer';
    }

    public function mount($productId)
    {
        $this->productId  = $productId;
    }

    public function builder(): Builder
    {
        return Product_Sale::with([
            'sale',
            'bin',
        ])
            ->whereProductId($this->productId)
            ->orderBy('sales.created_at', 'DESC');
    }

    public function applyeFilters(array $filters = [])
    {
        $builder = $this->getBuilder();

        if (!empty($filters['warehouse_id'])) {
            $builder = $builder->whereHas('sale', function ($query) use ($filters) {
                $query->whereWarehouseId($filters['warehouse_id']);
            });
        }

        if (!empty($filters['start_at']) && !empty($filters['end_at'])) {
            $builder = $builder->whereHas('sale', function ($query) use ($filters) {
                $query->whereBetween('created_at', [$filters['start_at'], $filters['end_at']]);
            });
        }

        $this->setBuilder($builder);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),

            Column::make("Author", "sale.author.name")
                ->sortable(),


            Column::make("sale id", "sale.id")
                ->hideIf(true),

            Column::make("Date", "sale.created_at")
                ->format(function ($value, $b) {
                    return Carbon::parse($value)->toFormattedDateString();
                }),

            Column::make("Reference no", "sale.reference_no")
                ->format(function ($value, $row) {
                    return '<a href="' . route('sales.edit', ['sale' => $row->{'sale.id'}]) . '" target="_new">' . $value . '</a>';
                })
                ->html(),

            Column::make("Receipt no", "sale.receipt_no"),

            Column::make("Location", "bin.name")
                ->hideIf(true),

            Column::make("Location", "sale.warehouse.name")
                ->format(function ($value, $row) {
                    $binName = !empty($row->{'bin.name'}) ?  "<strong>{$row->{'bin.name'}}</strong>" : '<em class="text-warning">No BIN assigned</em>';
                    return "{$value}: {$binName}";
                })
                ->html(),

            Column::make("Customer", "sale.customer.name"),

            Column::make("Qty", "qty"),

            Column::make("Unit Price", "net_unit_price"),

            Column::make("Subtotal", "total")
                ->format(function ($value) {
                    return formatMoney($value, true);
                }),
        ];
    }
}
