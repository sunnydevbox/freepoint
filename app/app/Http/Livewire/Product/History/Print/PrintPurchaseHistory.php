<?php

namespace App\Http\Livewire\Product\History\Print;

use Livewire\Component;
use App\Services\ProductPurchaseService;

class PrintPurchaseHistory extends Component
{
    public $data = null;
    public int $productId;

    public $listeners = ['printProductPurchases'];

    public function mount($productId)
    {
        $this->productId  = $productId;
        $this->printProductPurchases();
    }

    public function render()
    {
        return view('livewire.product.history.print-purchase-history');
    }

    public function printProductPurchases(array $filters = [])
    {
        $this->data = ProductPurchaseService::filter($filters, $this->productId);
    }
}
