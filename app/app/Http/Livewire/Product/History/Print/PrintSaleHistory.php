<?php

namespace App\Http\Livewire\Product\History\Print;

use Livewire\Component;
use App\Services\ProductSaleService;

class PrintSaleHistory extends Component
{
    public $data = null;
    public int $productId;

    public $listeners = ['printProductSales'];

    public function mount($productId)
    {
        $this->productId  = $productId;
        $this->printProductSales();
    }

    public function render()
    {
        return view('livewire.product.history.print-sale-history');
    }

    public function printProductSales(array $filters = [])
    {
        $this->data = ProductSaleService::filter($filters, $this->productId);
    }
}
