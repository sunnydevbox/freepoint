<?php

namespace App\Http\Livewire\Product;

use App\Services\ProductService;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\Supplier;
use App\Models\WarehouseBin;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;

class Browse extends DataTableComponent
{
    protected $model = Product::class;

    public $currentUser;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchDebounce(1000);

        $this->currentUser = auth()->user();
    }

    public function builder(): Builder
    {
        $query = Product::with(
            'inWarehouses.warehouse',
            'inWarehouses.product',
            'inWarehouses.bin.warehouse',
            'inPurchases.purchase.supplier',
            'inSales',
            'category',
            'unit',
        )
            // ->active()
            // ->groupBy('products.id')
            ->orderBy('name')
            // ->orderBy('products.created_at', 'DESC')
        ;

        return $query;
    }

    public function filters(): array
    {
        $warehouses = Product_Warehouse::groupBy('warehouse_id')
            ->leftJoin('warehouses', 'warehouses.id', '=', 'product_warehouse.warehouse_id')
            ->select('warehouse_id', 'warehouses.*')
            ->orderBy('warehouses.name')
            ->get();

        $bins = ['Select a warehouse'];

        if (!empty($this->table['filters']['warehouse'])) {
            $bins =  WarehouseBin::whereWarehouseId($this->table['filters']['warehouse'])
                ->orderBy('name')
                ->get()
                ->mapWithKeys(fn($bin) => [$bin->id => $bin->name])
                ->toArray();
        }
        $suppliers = Supplier::orderBy('name')
            ->where('is_active', 1)
            ->get()
            ->mapWithKeys(fn($supplier) => [$supplier->id => $supplier->name])
            ->toArray();

        return [
            SelectFilter::make('Supplier')
                ->options(array_replace(['' => 'Select Supplier'], $suppliers))
                ->filter(fn(Builder $builder, string $value) =>
                $builder->when(
                    !empty($value),
                    fn($query) => $query->whereHas('inPurchases.purchase.supplier', fn(Builder $query): Builder => $query->where('suppliers.id', $value))
                )),

            SelectFilter::make('Warehouse')
                ->options(
                    array_replace(
                        ['' => 'Select Warehouse'],
                        $warehouses->mapWithKeys(fn($warehouse) => [$warehouse->id => $warehouse->name])->toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder
                            ->leftJoin('product_warehouse', 'product_warehouse.product_id', '=', 'products.id')
                            ->where('product_warehouse.warehouse_id', $value);
                    }
                }),

            SelectFilter::make('BIN', 'bin')
                ->options($bins)
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        if (!hasJoinTable($builder, 'product_warehouse')) {
                            $builder = $builder
                                ->leftJoin('product_warehouse', 'product_warehouse.product_id', '=', 'products.id');
                        }
                        $builder  = $builder->where('product_warehouse.warehouse_id', $value);
                    }
                }),

            SelectFilter::make('Publish Satus', 'is_active')
                ->options([
                    null => 'All',
                    1 => 'Published',
                    0 => 'Unpublished'
                ])
                ->setFilterDefaultValue(1)
                ->filter(function (Builder $builder, string $value) {
                    if ($value !== null) {
                        $builder  = $builder->where('products.is_active', $value);
                    }
                }),
        ];
    }


    public function delete($id)
    {
        $product = (new ProductService)->delete($id);
    }


    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),
            Column::make("Name", "name")
                ->searchable()
                ->sortable(),

            Column::make("Part Number", "code")
                ->searchable(),

            Column::make("Model", "model")
                ->searchable(),

            Column::make("Category id", "category.name"),

            Column::make('Location')
                ->label(
                    function ($row) {
                        $d =
                            $row->inWarehouses
                            ->groupBy('warehouse_bin_id')
                            ->map(function ($ps) {
                                return [
                                    'warehouse_name' => $ps[0]->warehouse->name,
                                    'bin_name' => $ps[0]->bin ? $ps[0]->bin->name : null,
                                    'sum' => $ps->sum('qty')
                                ];
                            });
                        return view(
                            'livewire.product.browse-column-product-in-warehouse',
                            [
                                'productInWarehouse' => $d,
                            ]
                        );
                    }
                ),

            Column::make("Alert Qty", "alert_quantity")
                ->hideIf(true),

            Column::make("Quantity", "qty")
                ->format(function ($value, $row) {
                    return sprintf(
                        '<span class="%s font-weight-bold" data-toggle="tooltip" title="Threshold Status">%s</span>',
                        productThresholdClassHightlighter($value, $row->alert_quantity ?: 0),
                        $value
                    );
                })
                ->html(),

            Column::make("Unit ", "unit.unit_name"),

            Column::make("Price", "price")
                ->format(function ($value, $row) {
                    return formatMoney($value, true);
                })
                ->sortable(),

            BooleanColumn::make("Active?", "is_active"),

            Column::make('Actions')
                ->label(
                    function ($row) {
                        return view(
                            'livewire.product.browse-actions',
                            [
                                'id' => $row->id,
                                'name' => $row->name,
                                'code' => $row->code,
                                'product' => $row
                            ]
                        );
                    }
                ),
        ];
    }
}
