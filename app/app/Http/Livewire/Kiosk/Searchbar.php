<?php

namespace App\Http\Livewire\Kiosk;

use App\Models\Category;
use App\Models\Product;
use App\Models\Warehouse;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Action;
use Mediconesystems\LivewireDatatables\NumberColumn;
class Searchbar extends LivewireDatatable
{
    public function builder()
    {
        return Product::query()
                ;
    }

    public function columns()
    {
        return [
            Column::checkbox(),

            NumberColumn::name('id')
                ->hide(),

            // Column::callback(['id', 'name'], function ($id, $name) {
            Column::name('name')
                ->label('Name')
                ->sortable()
                ->searchable()
                ->defaultSort('asc')
                ->sortBy('products.name'),

            Column::name('code')
                ->label(trans('file.Code'))
                ->searchable()
                ->defaultSort('asc')
                ->sortBy('code'),



            Column::name('category.name')
                ->label(trans('file.category'))
                ->searchable()
                ->filterable($this->categories->pluck('name'))
                ->defaultSort('asc')
                ->sortBy('categories.name'),


            Column::name('warehouses.name')
                ->label('Warehouse')
                ->searchable()
                ->filterable($this->warehouses->pluck('name'))
                ->defaultSort('asc')
                ->sortBy('categories.name'),

            // NumberColumn::callback(['id'], function ($id) {
            //     return Product_Warehouse::where('product_id', $id)->sum('qty');
            // })
            //     ->label(trans('file.Quantity'))
            //     ->alignRight(),


            Column::name('unit.unit_name')
                ->label(trans('file.Unit'))
                ->unsortable(),

            Column::name('price')
                ->label(trans('file.Price'))
                ->unsortable(),

            // Column::callback(['id', 'name', 'code'], function ($id, $name, $code) {
            //     return view(
            //         'livewire.product.browse-actions',
            //         compact(
            //             'id',
            //             'name',
            //             'code'
            //         )
            //     );
            // })->unsortable(),
        ];
    }

    public function getWarehousesProperty()
    {
        return Warehouse::active()->orderBy('name', 'ASC')->get();
    }

    public function getCategoriesProperty()
    {
        return Category::active()->orderBy('name', 'ASC')->get();
    }
}