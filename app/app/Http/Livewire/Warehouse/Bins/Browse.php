<?php

namespace App\Http\Livewire\Warehouse\Bins;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\WarehouseBin;
use App\Repositories\Warehouse\WarehouseRepository;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;

class Browse extends DataTableComponent
{
    protected $model = WarehouseBin::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectStatus(false);
        $this->setSearchStatus(false);
    }

    public function builder(): Builder
    {
        return WarehouseBin::query()
            ->orderBy('warehouse_bins.name', 'ASC')
            ->with([
                'warehouse',
            ]);
    }



    public function filters(): array
    {
        return [
            SelectFilter::make('Location')
                ->options(
                    array_replace(
                        [
                            '' => 'All',
                        ],
                        (new WarehouseRepository)->listForDropdown()->toArray()
                    )
                )
                ->filter(function (Builder $builder, string $value) {
                    if (!empty($value)) {
                        $builder->where('warehouse_id', $value);
                    }
                }),

        ];
    }



    public function columns(): array
    {
        /*
            Grand Total
            Returned Amount
            Paid
            Due
            Payment Status
            Action
        */
        return [
            Column::make("Id", "id")
                ->hideIf(true)
                ->sortable(),

            Column::make("Bin Name", 'name'),

            Column::make("Location", 'warehouse.name'),

            Column::make("Aisle", 'aisle'),

            Column::make("rack", 'rack'),

            Column::make("shelf", 'shelf'),

            Column::make("Bin", 'bin'),

            Column::make('Actions')
                ->label(
                    function ($row, Column $column) {
                        // dd($row, $column);
                        return view(
                            'livewire.warehouse.browse-actions',
                            [
                                'id' => $row->id,
                            ]
                        );
                    }
                ),



           
        ];
    }
}
