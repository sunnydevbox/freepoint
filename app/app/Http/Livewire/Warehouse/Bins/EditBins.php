<?php

namespace App\Http\Livewire\Warehouse\Bins;

use App\Models\Warehouse;
use App\Models\WarehouseBin;
use Illuminate\Support\Collection;
use Livewire\Component;

class EditBins extends Component
{
    public $warehouseBinId;
    public $name;
    public $aisle;
    public $rack;
    public $shelf;
    public $bin;

    public ?int $warehouse = null; 

    public Collection $warehouses;

    public function mount(WarehouseBin $bins)
    {
        $this->warehouseBinId = $bins->id;
        $this->name = $bins->name;
        $this->aisle = $bins->aisle;
        $this->rack = $bins->rack;
        $this->shelf = $bins->shelf;
        $this->bin = $bins->bin;

        $this->warehouse = $bins->warehouse_id;
        $this->warehouses = Warehouse::where('is_active', 1)->get();

    }

    public function submit()
    {   
        // dd($this->warehouseBinId);
        WarehouseBin::whereId($this->warehouseBinId)->update([
            'warehouse_id'  =>  $this->warehouse,
            'name'          =>  $this->name,
            'aisle'         =>  $this->aisle,
            'rack'          =>  $this->rack,
            'shelf'         =>  $this->shelf,
            'bin'           =>  $this->bin
        ]);

        session()->flash('message', 'Bins successfully updated.');

        redirect()->route('warehouse.bins.index');
    }

    public function render()
    {
        return view('livewire.warehouse.edit-bins');
    }
}
