<?php

namespace App\Http\Livewire\Warehouse\Bins;

use App\Models\Purchase;
use App\Models\WarehouseBin;
use LivewireUI\Modal\ModalComponent;

class ViewBins extends ModalComponent
{
    public $warehouse_bin_id;

    public function mount($warehouse_bin_id)
    {
        $this->warehouse_bin_id = $warehouse_bin_id;
    }

    public function render()
    {
        $bins = WarehouseBin::with([
            'warehouse',   
        ])
        ->find($this->warehouse_bin_id);


        return view(
            'livewire.warehouse.view-bins',
            [
                'bins' => $bins,
            ]
        );
    }
}
