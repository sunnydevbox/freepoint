<?php

namespace App\Http\Middleware;

use App\Models\Currency;
use App\Models\DsoAlert;
use App\Models\GeneralSetting;
use App\Models\Product;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class Common
{
    public function handle(Request $request, Closure $next)
    {
        /*if( (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
            URL::forceScheme('https');
        }*/
        //get general setting value        
        $generalSettings = Cache::remember('general_settings', 3600, function () {
            return GeneralSetting::latest()->first();
        });


        $todayDate = date("Y-m-d");
        if ($generalSettings->expiry_date) {
            $expiry_date = date("Y-m-d", strtotime($generalSettings->expiry_date));
            if ($todayDate > $expiry_date) {
                auth()->logout();
                return redirect()->route('contactForRenewal');
            }
        }
        //setting language
        if (isset($_COOKIE['language'])) {
            \App::setLocale($_COOKIE['language']);
        } else {
            \App::setLocale('en');
        }
        //setting theme
        if (isset($_COOKIE['theme'])) {
            View::share('theme', $_COOKIE['theme']);
        } else {
            View::share('theme', 'light');
        }

        $currency = Cache::remember('general_settings', 3600, function () use($generalSettings) {
            return Currency::find($generalSettings->currency);
        });

        View::share('general_setting', $generalSettings);
        View::share('currency', $currency);
        config([
            'staff_access' => $generalSettings->staff_access,
            'date_format' => $generalSettings->date_format,
            'currency' => $currency->code,
            'currency_position' => $generalSettings->currency_position, 
            'decimal' => $generalSettings->decimal
        ]);

        $alertProduct = Cache::remember('alertProductCount',60 * 60, function() {
                return Product::where('is_active', true)
                                ->where('alert_quantity', '>', 'gty')
                                ->count();
        });

        $dsoAlertProduct = Cache::remember('dsoAlertProduct', 60 * 60, function() {
                            return DsoAlert::whereDate('created_at', date("Y-m-d"))
                                ->select('number_of_products')
                                ->first();
        });

        if ($dsoAlertProduct) {
            $dsoAlertProductCount = $dsoAlertProduct->number_of_products;
        } else {
            $dsoAlertProductCount = 0;
        }
        
        View::share(['alert_product' => $alertProduct, 'dso_alert_product_no' => $dsoAlertProductCount]);
        return $next($request);
    }
}
