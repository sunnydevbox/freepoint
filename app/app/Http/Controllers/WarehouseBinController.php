<?php

namespace App\Http\Controllers;

use App\Http\Requests\WarehouseBin\WarehouseBinStoreRequest;
use App\Models\WarehouseBin;
use App\Repositories\Warehouse\WarehouseRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseBinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.warehouse.bins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouses = (new WarehouseRepository)->listForDropdown();
        // dd($warehouses);

        return response()->view(
            'backend.warehouse.bins.create',
            compact('warehouses')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseBinStoreRequest $request)
    {
        try {
            // dd($request->all());
            DB::beginTransaction();

            
            $bin = WarehouseBin::create($request->only(
                'name',
                'warehouse_id',
                'aisle',
                'rack',
                'shelf',
                'bin'
            ));

            DB::commit();

            return redirect()->route('warehouse.bins.index');

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bins = WarehouseBin::find($id);
        return view('backend.warehouse.bins.edit', compact('bins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
