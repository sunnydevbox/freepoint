<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Models\Warehouse;
use App\Models\Product_Warehouse;
use DB;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Report\SaleReportRepository;
use App\Repositories\Warehouse\WarehouseRepository;
use Illuminate\Http\JsonResponse;

class ReportController extends \App\Http\Controllers\ReportController
{
    public function __construct(
        public SaleReportRepository $saleReportRepository,
        public BrandRepository $brandRepository,
        public WarehouseRepository $warehouseRepository
    ) {}

    public function purchaseReport(Request $request)
    {
        $page = $request->input('page', 10);
        $startDate = $request->get('start_date', null);
        $endDate = $request->get('end_date', null);
        $warehouseId = $request->get('warehouse_id', null);
        $binId = null;
        $product_id = [];
        $variant_id = [];
        $product_name = [];
        $product_qty = [];

        $warehouses = Warehouse::where('is_active', true)->get();

        $warehouseCountSubQuery = DB::table('product_warehouse AS pw_warehouse_qty_count')
            ->selectRaw('SUM(pw_warehouse_qty_count.qty)')
            ->whereRaw('pw_warehouse_qty_count.product_id=product_warehouse.product_id')
            ->when(!empty($warehouseId), fn($query) => $query->whereRaw('pw_warehouse_qty_count.warehouse_id=?', [$warehouseId]));

        $warehouseBinCountSubQuery = DB::table('product_warehouse AS pw_warehouse_bin_qty_count')
            ->selectRaw('SUM(pw_warehouse_bin_qty_count.qty)')
            ->whereRaw('pw_warehouse_bin_qty_count.product_id=product_warehouse.product_id')
            ->whereRaw('pw_warehouse_bin_qty_count.warehouse_bin_id=product_warehouse.warehouse_bin_id')
            ->when(!empty($warehouseId), fn($query) => $query->whereRaw('pw_warehouse_bin_qty_count.warehouse_id=?', [$warehouseId]));

        $warehouseCountSubQuerySql = str_replace(array('?'), array('\'%s\''), $warehouseCountSubQuery->toSql());
        $warehouseCountSubQuerySql = vsprintf($warehouseCountSubQuerySql, $warehouseCountSubQuery->getBindings());
        $warehouseBinCountSubQuerySql = str_replace(array('?'), array('\'%s\''), $warehouseBinCountSubQuery->toSql());
        $warehouseBinCountSubQuerySql = vsprintf($warehouseBinCountSubQuerySql, $warehouseBinCountSubQuery->getBindings());


        $products = Product_Warehouse::whereHas('product', fn($query) => $query->active()->whereNull('is_variant'))
            ->with(['product', 'purchaseItem.purchase', 'warehouse', 'bin'])
            ->whereHas('purchaseItem.purchase', fn($query) => $query->whereBetween('created_at', [$startDate, $endDate]))
            ->when($warehouseId && $warehouseId != 'undefined', fn($query) => $query->whereWarehouseId($warehouseId))
            ->when($binId && $binId != 'undefined', fn($query) => $query->whereWarehouseBinId($binId))
            ->addSelect([

                '*',
                // )

                // ('(
                //     SELECT SUM(pw_qty_count.qty)
                //     FROM product_warehouse pw_qty_count
                //     WHERE pw_qty_count.product_id=3072
                //     AND pw_qty_count.warehouse_id=' . $warehouseId . '
                // ) AS warehouse_count')
                DB::raw("({$warehouseCountSubQuerySql}) AS warehouse_count"),
                DB::raw("({$warehouseBinCountSubQuerySql}) AS warehouse_bin_count"),
                // DB::raw("({$warehouseBinCountSubQuery->toSql()}) AS warehouse_bin_count", $warehouseBinCountSubQuery->getBindings())

            ])
            // ->pagin  ate($page)
            // ->get()
        ;

        // dd($products->toSql(), $products->getBindings());

        $products = $products->get();

        return view(
            'backend.report.purchase_report',
            compact(
                'products',
                'product_id',
                'variant_id',
                'product_name',
                'product_qty',
                'startDate',
                'endDate',
                'warehouses',
                'warehouseId'
            )
        );
    }
    public function saleReport(Request $request)
    {
        $warehouses = $this->warehouseRepository->getAllActive();
        $brands = $this->brandRepository->getAllActive();
        $startDate = $request->get('start_date', dateFormat(now()->startOfMonth()->startOfDay(), 'Y-m-d H:i:s'));
        $endDate = $request->get('end_date', dateFormat(now()->endOfMonth()->endOfDay(), 'Y-m-d H:i:s'));
        $warehouseId = $request->get('warehouse_id', null);
        $brandId = $request->get('brand_id', null);
        return view('backend.report.sale_report', compact(
            'startDate',
            'endDate',
            'warehouses',
            'warehouseId',
            'brands',
            'brandId'
        ));
    }


    public function saleReportData(Request $request)
    {
        $limit = $request->input('length', 10);
        $page = ($request->input('start') / $limit) + 1;
        $searchTerm = $request->input('search.value', null);

        $filter = [
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'warehouseId' => $request->input('warehouse_id', null),
            'brandId' => $request->input('brand_id', null),
            'searchTerm' => $searchTerm,
        ];

        $saleReportsPaginated = $this->saleReportRepository->querySaleReport($filter)->paginate($limit, ['*'], 'page', $page);

        $totalData = $saleReportsPaginated->total();

        $saleReports = $saleReportsPaginated->map(function ($item) {
            $so = sprintf(
                "<strong>%s<br>%s</strong><br>%s",
                $item->saleItem?->sale->reference_no,
                $item->saleItem?->sale->id,
                $item->saleItem?->sale->created_at->format('M d, Y h:i A')
            );

            return [
                'SO' => $so,
                'productName' => $item->product->name,
                'warehouse' => $item->warehouse->name,
                'saleItem' => $item->product->name,
                'soldAmount' => formatMoney($item->saleItem?->net_unit_price * $item->saleItem?->qty),
                'soldQty' => $item->saleItem?->qty,
                'stockInBins' => 'Bin/Warehouse: ' . $item->warehouse_bin_count . '/' . $item->warehouse_count,
            ];
        });

        return new JsonResponse([
            "draw" => $request->input('draw'),
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $saleReports
        ]);
    }
}
