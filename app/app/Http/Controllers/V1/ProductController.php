<?php

namespace App\Http\Controllers\V1;

use App\Enums\ProductTypeEnum;
use App\Http\Controllers\ProductController as ControllersProductController;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Models\Adjustment;
use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\ProductVariant;
use App\Models\Tax;
use App\Models\Transfer;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Tax\TaxRepository;
use App\Repositories\Unit\UnitRepository;
use App\Repositories\Warehouse\WarehouseRepository;
use App\Services\ProductService;
use App\Services\ServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Collective\Html\FormFacade as Form;
use Exception;

class ProductController extends ControllersProductController
{
    public ServiceInterface $service;

    public function create()
    {
        ;
        
        if (auth()->check() && auth()->user()->can('products-add')) {
            $lims_product_list_without_variant = $this->productWithoutVariant();
            $lims_product_list_with_variant = $this->productWithVariant();
            $lims_brand_list = app(BrandRepository::class)->where('is_active', true)->get(); // ->getCachedAll();
            $lims_category_list = app(CategoryRepository::class)->where('is_active', true)->get();// ->getCachedAll();
            $lims_unit_list = app(UnitRepository::class)->where('is_active', true)->get(); //->getCachedAll();
            $lims_tax_list = app(TaxRepository::class)->where('is_active', true)->get(); // ->getCachedAll();
            $lims_warehouse_list = app(WarehouseRepository::class)->where('is_active', true)->get(); // ->getCachedAll();
            $lims_product_data = null;
            
            return view(
                'backend.product.create',
                compact(
                    'lims_product_list_without_variant',
                    'lims_product_list_with_variant',
                    'lims_brand_list',
                    'lims_category_list',
                    'lims_unit_list',
                    'lims_tax_list',
                    'lims_warehouse_list',
                    'lims_product_data'
                )
            );
        } else {
            return redirect()
                ->back()
                ->with(
                    'not_permitted',
                    'Sorry! You are not allowed to access this module'
                );
        }
    }

    public function edit($id)
    {
        if (auth()->check() && auth()->user()->hasPermissionTo('products-edit')) {
            $categoryRpo = app(CategoryRepository::class);
            $brandRpo = app(BrandRepository::class);
            $unitRpo = app(UnitRepository::class);
            $taxRpo = app(TaxRepository::class);
            $warehouseRpo = app(WarehouseRepository::class);

            $lims_product_list_without_variant = $this->productWithoutVariant();
            $lims_product_list_with_variant = $this->productWithVariant();
            $lims_brand_list = $brandRpo->getCachedAll();
            $lims_category_list = $categoryRpo->getCachedAll();
            $lims_unit_list = $unitRpo->getCachedAll();
            $lims_tax_list = $taxRpo->getCachedAll();
            $lims_product_data = Product::with(['customFields.responses',])->where('id', $id)->first();

            $customFields = $lims_product_data->customFields;

            if ($lims_product_data->variant_option) {
                $lims_product_data->variant_option = json_decode($lims_product_data->variant_option);
                $lims_product_data->variant_value = json_decode($lims_product_data->variant_value);
            }
            $lims_product_variant_data = $lims_product_data->variant()->orderBy('position')->get();
            $lims_warehouse_list = $warehouseRpo->getCachedAll();
            $noOfVariantValue = 0;

            return view(
                'backend.product.edit',
                compact(
                    'lims_product_list_without_variant',
                    'lims_product_list_with_variant',
                    'lims_brand_list',
                    'lims_category_list',
                    'lims_unit_list',
                    'lims_tax_list',
                    'lims_product_data',
                    'lims_product_variant_data',
                    'lims_warehouse_list',
                    'noOfVariantValue',
                    'customFields'
                )
            );
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function updateProduct(Request $request)
    {
        if (auth()->check() && !auth()->user()->hasPermissionTo('products-edit')) {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }

        $lims_product_data = Product::findOrFail($request->input('id'));
        $data = $request->except('image', 'file', 'prev_img');
        $data['name'] = htmlspecialchars(trim($data['name']));

        if ($data['type'] == 'combo') {
            $data['product_list'] = implode(",", $data['product_id']);
            $data['variant_list'] = implode(",", $data['variant_id']);
            $data['qty_list'] = implode(",", $data['product_qty']);
            $data['price_list'] = implode(",", $data['unit_price']);
            $data['cost'] = $data['unit_id'] = $data['purchase_unit_id'] = $data['sale_unit_id'] = 0;
        } elseif ($data['type'] == 'digital' || $data['type'] == 'service')
            $data['cost'] = $data['unit_id'] = $data['purchase_unit_id'] = $data['sale_unit_id'] = 0;

        if (!isset($data['featured']))
            $data['featured'] = 0;

        if (!isset($data['is_embeded']))
            $data['is_embeded'] = 0;

        if (!isset($data['promotion']))
            $data['promotion'] = null;

        if (!isset($data['is_batch']))
            $data['is_batch'] = null;

        if (!isset($data['is_imei']))
            $data['is_imei'] = null;

        $data['product_details'] = str_replace('"', '@', $data['product_details']);
        $data['product_details'] = $data['product_details'];
        if ($data['starting_date'])
            $data['starting_date'] = date('Y-m-d', strtotime($data['starting_date']));
        if ($data['last_date'])
            $data['last_date'] = date('Y-m-d', strtotime($data['last_date']));

        $previous_images = [];
        //dealing with previous images
        if ($request->prev_img) {
            foreach ($request->prev_img as $key => $prev_img) {
                if (!in_array($prev_img, $previous_images))
                    $previous_images[] = $prev_img;
            }
            $lims_product_data->image = implode(",", $previous_images);
            $lims_product_data->save();
        } else {
            $lims_product_data->image = null;
            $lims_product_data->save();
        }

        //dealing with new images
        if ($request->image) {
            $image = $this->service->attachImage($request->get('id'), $request->image[0]);

            // $this->service->

            // $images = $request->image;
            // $image_names = [];
            // $length = count(explode(",", $lims_product_data->image));
            // foreach ($images as $key => $image) {
            //     $ext = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
            //     /*$image = Image::make($image)->resize(512, 512);*/
            //     $imageName = date("Ymdhis") . ($length + $key+1) . '.' . $ext;
            //     $image->move('public/images/product', $imageName);
            //     $image_names[] = $imageName;
            // }
            // if($lims_product_data->image)
            //     $data['image'] = $lims_product_data->image. ',' . implode(",", $image_names);
            // else
            //     $data['image'] = implode(",", $image_names);


        } else
            $data['image'] = $lims_product_data->image;

        $file = $request->file;
        if ($file) {
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $fileName = strtotime(date('Y-m-d H:i:s'));
            $fileName = $fileName . '.' . $ext;
            $file->move('public/product/files', $fileName);
            $data['file'] = $fileName;
        }

        $old_product_variant_ids = ProductVariant::where('product_id', $request->input('id'))->pluck('id')->toArray();
        $new_product_variant_ids = [];
        //dealing with product variant
        if (isset($data['is_variant'])) {
            if (isset($data['variant_option']) && isset($data['variant_value'])) {
                $data['variant_option'] = json_encode($data['variant_option']);
                $data['variant_value'] = json_encode($data['variant_value']);
            }
            foreach ($data['variant_name'] as $key => $variant_name) {
                $lims_variant_data = Variant::firstOrCreate(['name' => $data['variant_name'][$key]]);
                $lims_product_variant_data = ProductVariant::where([
                    ['product_id', $lims_product_data->id],
                    ['variant_id', $lims_variant_data->id]
                ])->first();
                if ($lims_product_variant_data) {
                    $lims_product_variant_data->update([
                        'position' => $key + 1,
                        'item_code' => $data['item_code'][$key],
                        'additional_cost' => $data['additional_cost'][$key],
                        'additional_price' => $data['additional_price'][$key]
                    ]);
                } else {
                    $lims_product_variant_data = new ProductVariant();
                    $lims_product_variant_data->product_id = $lims_product_data->id;
                    $lims_product_variant_data->variant_id = $lims_variant_data->id;
                    $lims_product_variant_data->position = $key + 1;
                    $lims_product_variant_data->item_code = $data['item_code'][$key];
                    $lims_product_variant_data->additional_cost = $data['additional_cost'][$key];
                    $lims_product_variant_data->additional_price = $data['additional_price'][$key];
                    $lims_product_variant_data->qty = 0;
                    $lims_product_variant_data->save();
                }
                $new_product_variant_ids[] = $lims_product_variant_data->id;
            }
        } else {
            $data['is_variant'] = null;
            $data['variant_option'] = null;
            $data['variant_value'] = null;
        }
        //deleting old product variant if not exist
        foreach ($old_product_variant_ids as $key => $product_variant_id) {
            if (!in_array($product_variant_id, $new_product_variant_ids))
                ProductVariant::find($product_variant_id)->delete();
        }
        if (isset($data['is_diffPrice'])) {
            foreach ($data['diff_price'] as $key => $diff_price) {
                if ($diff_price) {
                    $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($lims_product_data->id, $data['warehouse_id'][$key])->first();
                    if ($lims_product_warehouse_data) {
                        $lims_product_warehouse_data->price = $diff_price;
                        $lims_product_warehouse_data->save();
                    } else {
                        Product_Warehouse::create([
                            "product_id" => $lims_product_data->id,
                            "warehouse_id" => $data["warehouse_id"][$key],
                            "qty" => 0,
                            "price" => $diff_price
                        ]);
                    }
                }
            }
        } else {
            $data['is_diffPrice'] = false;
            foreach ($data['warehouse_id'] as $key => $warehouse_id) {
                $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($lims_product_data->id, $warehouse_id)->first();
                if ($lims_product_warehouse_data) {
                    $lims_product_warehouse_data->price = null;
                    $lims_product_warehouse_data->save();
                }
            }
        }

        $lims_product_data->update($data);
        Session::flash('edit_message', 'Product updated successfully');
    }


    public function store_v2(ProductCreateRequest $request)
    {
        $data = $request->except('image', 'file');

        if (isset($data['is_variant'])) {
            $data['variant_option'] = json_encode($data['variant_option']);
            $data['variant_value'] = json_encode($data['variant_value']);
        } else {
            $data['variant_option'] = $data['variant_value'] = null;
        }
        
        $data['name'] = preg_replace('/[\n\r]/', "<br>", htmlspecialchars(trim($data['name'])));
       
        if ($data['type'] == 'combo') {
            $data['product_list'] = implode(",", $data['product_id']);
            $data['variant_list'] = implode(",", $data['variant_id']);
            $data['qty_list'] = implode(",", $data['product_qty']);
            $data['price_list'] = implode(",", $data['unit_price']);
            $data['cost'] = $data['unit_id'] = $data['purchase_unit_id'] = $data['sale_unit_id'] = 0;
        } elseif ($data['type'] == 'digital' || $data['type'] == 'service') {
            $data['cost'] = $data['unit_id'] = $data['purchase_unit_id'] = $data['sale_unit_id'] = 0;
        }

        $data['product_details'] = str_replace('"', '@', $data['product_details']);

        if ($data['starting_date']) {
            $data['starting_date'] = date('Y-m-d', strtotime($data['starting_date']));
        }
        
        if ($data['last_date']) {
            $data['last_date'] = date('Y-m-d', strtotime($data['last_date']));
        }

        $data['is_active'] = true;
        $images = $request->image;
        $image_names = [];
        if ($images) {
            foreach ($images as $key => $image) {
                $ext = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
                $imageName = date("Ymdhis") . ($key + 1);
                if (!config('database.connections.saleprosaas_landlord')) {
                    $imageName = $imageName . '.' . $ext;
                    $image->move('public/images/product', $imageName);
                } else {
                    $imageName = $this->getTenantId() . '_' . $imageName . '.' . $ext;
                    $image->move('public/images/product', $imageName);
                }
                $image_names[] = $imageName;
            }
            $data['image'] = implode(",", $image_names);
        } else {
            $data['image'] = 'zummXD2dvAtI.png';
        }
        $file = $request->file;
        if ($file) {
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $fileName = strtotime(date('Y-m-d H:i:s'));
            $fileName = $fileName . '.' . $ext;
            $file->move('public/product/files', $fileName);
            $data['file'] = $fileName;
        }
        dd($data);
        /**
         * STATIC VALUES
         */

        $data['type'] = 'standard';


        $lims_product_data = Product::create($data);

        /** 
         * TODO:
         * CUSTOM FIELDS */
        $lims_product_data
            ->customFields()
            ->create([
                'title' => 'Model',
                'type' => 'text'
            ]);

        $lims_product_data
            ->customFields()
            ->create([
                'title' => 'Model',
                'type' => 'text'
            ]);



        //dealing with product variant
        if (!isset($data['is_batch']))
            $data['is_batch'] = null;
        if (isset($data['is_variant'])) {
            foreach ($data['variant_name'] as $key => $variant_name) {
                $lims_variant_data = Variant::firstOrCreate(['name' => $data['variant_name'][$key]]);
                $lims_variant_data->name = $data['variant_name'][$key];
                $lims_variant_data->save();
                $lims_product_variant_data = new ProductVariant;
                $lims_product_variant_data->product_id = $lims_product_data->id;
                $lims_product_variant_data->variant_id = $lims_variant_data->id;
                $lims_product_variant_data->position = $key + 1;
                $lims_product_variant_data->item_code = $data['item_code'][$key];
                $lims_product_variant_data->additional_cost = $data['additional_cost'][$key];
                $lims_product_variant_data->additional_price = $data['additional_price'][$key];
                $lims_product_variant_data->qty = 0;
                $lims_product_variant_data->save();
            }
        }
        if (isset($data['is_diffPrice'])) {
            foreach ($data['diff_price'] as $key => $diff_price) {
                if ($diff_price) {
                    Product_Warehouse::create([
                        "product_id" => $lims_product_data->id,
                        "warehouse_id" => $data["warehouse_id"][$key],
                        "qty" => 0,
                        "price" => $diff_price
                    ]);
                }
            }
        }
        \Session::flash('create_message', 'Product created successfully');
    }


    public function history(Request $request)
    {
        if (auth()->check() && auth()->user()->hasPermissionTo('product_history')) {

            $warehouse_id = $request->get('warehouse_id', 0);

            $starting_date = date("Y-m-d", strtotime(date('Y-m-d', strtotime('-1 year', strtotime(date('Y-m-d'))))));
            $ending_date = date("Y-m-d");

            if ($request->input('starting_date')) {
                $starting_date = $request->input('starting_date');
                $ending_date = $request->input('ending_date');
            }

            $product_id = $request->input('product_id');
            $product_data = Product::select('name', 'code')->find($product_id);
            $lims_warehouse_list = Warehouse::where('is_active', true)->get();
            return view(
                'backend.product.history',
                compact(
                    'starting_date',
                    'ending_date',
                    'warehouse_id',
                    'product_id',
                    'product_data',
                    'lims_warehouse_list'
                )
            );
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function productWarehouseData($id)
    {
        $warehouse = [];
        $qty = [];
        $batch = [];
        $expired_date = [];
        $imei_number = [];
        $warehouse_name = [];
        $variant_name = [];
        $variant_qty = [];
        $product_warehouse = [];
        $product_variant_warehouse = [];
        $lims_product_data = Product::select('id', 'is_variant')->find($id);
        if ($lims_product_data->is_variant) {
            $lims_product_variant_warehouse_data = Product_Warehouse::where('product_id', $lims_product_data->id)->orderBy('warehouse_id')->get();
            $lims_product_warehouse_data = Product_Warehouse::select('warehouse_id', DB::raw('sum(qty) as qty'))->where('product_id', $id)->groupBy('warehouse_id')->get();
            foreach ($lims_product_variant_warehouse_data as $key => $product_variant_warehouse_data) {
                $lims_warehouse_data = Warehouse::find($product_variant_warehouse_data->warehouse_id);
                $lims_variant_data = Variant::find($product_variant_warehouse_data->variant_id);
                $warehouse_name[] = $lims_warehouse_data->name;
                $variant_name[] = $lims_variant_data->name;
                $variant_qty[] = $product_variant_warehouse_data->qty;
            }
        } else {
            $lims_product_warehouse_data = Product_Warehouse::where('product_id', $id)->orderBy('warehouse_id', 'asc')->get();
        }
        foreach ($lims_product_warehouse_data as $key => $product_warehouse_data) {
            $lims_warehouse_data = Warehouse::find($product_warehouse_data->warehouse_id);
            if ($product_warehouse_data->product_batch_id) {
                $product_batch_data = ProductBatch::select('batch_no', 'expired_date')->find($product_warehouse_data->product_batch_id);
                $batch_no = $product_batch_data->batch_no;
                $expiredDate = date(config('date_format'), strtotime($product_batch_data->expired_date));
            } else {
                $batch_no = 'N/A';
                $expiredDate = 'N/A';
            }
            $warehouse[] = $lims_warehouse_data->name;
            $batch[] = $batch_no;
            $expired_date[] = $expiredDate;
            $qty[] = $product_warehouse_data->qty;
            if ($product_warehouse_data->imei_number)
                $imei_number[] = $product_warehouse_data->imei_number;
            else
                $imei_number[] = 'N/A';
        }

        $product_warehouse = [$warehouse, $qty, $batch, $expired_date, $imei_number];
        $product_variant_warehouse = [$warehouse_name, $variant_name, $variant_qty];

        return ['product_warehouse' => $product_warehouse, 'product_variant_warehouse' => $product_variant_warehouse];
    }


    public function productData(Request $request)
    {
        $columns = array(
            2 => 'name',
            3 => 'code',
            4 => 'brand_id',
            5 => 'category_id',
            6 => 'qty',
            7 => 'unit_id',
            8 => 'price',
            9 => 'cost',
            10 => 'stock_worth'
        );

        $totalData = Product::where('is_active', true)->count();
        $totalFiltered = $totalData;

        if ($request->input('length') != -1)
            $limit = $request->input('length');
        else
            $limit = $totalData;
        $start = $request->input('start');
        $order = 'products.' . $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $products = Product::with('category', 'brand', 'unit')
                ->offset($start)
                ->where('is_active', true)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');
            $products =  Product::select('products.*')
                ->with('category', 'brand', 'unit')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->leftjoin('brands', 'products.brand_id', '=', 'brands.id')
                ->where([
                    ['products.name', 'LIKE', "%{$search}%"],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['products.code', 'LIKE', "%{$search}%"],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['categories.name', 'LIKE', "%{$search}%"],
                    ['categories.is_active', true],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['brands.title', 'LIKE', "%{$search}%"],
                    ['brands.is_active', true],
                    ['products.is_active', true]
                ])
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)->get();

            $totalFiltered = Product::join('categories', 'products.category_id', '=', 'categories.id')
                ->leftjoin('brands', 'products.brand_id', '=', 'brands.id')
                ->where([
                    ['products.name', 'LIKE', "%{$search}%"],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['products.code', 'LIKE', "%{$search}%"],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['categories.name', 'LIKE', "%{$search}%"],
                    ['categories.is_active', true],
                    ['products.is_active', true]
                ])
                ->orWhere([
                    ['brands.title', 'LIKE', "%{$search}%"],
                    ['brands.is_active', true],
                    ['products.is_active', true]
                ])
                ->count();
        }
        $data = [];
        if (!empty($products)) {
            foreach ($products as $key => $product) {
                $productImage = 'none';

                if ($product->images->first()) {
                    $productImage = $product->images->first()->url;
                }
                $nestedData['id'] = $product->id;
                $nestedData['key'] = $key;

                // $product_image = explode(",", $product->image);
                // $product_image = htmlspecialchars($product_image[0]);

                $nestedData['image'] = '<img src="' . $productImage . '" height="80" width="80">';
                $nestedData['name'] = $product->name;
                $nestedData['code'] = $product->code;
                if ($product->brand_id)
                    $nestedData['brand'] = $product->brand->title;
                else
                    $nestedData['brand'] = "N/A";
                $nestedData['category'] = $product->category->name;

                dd(Auth::user()->getWarehouseIds());
                if ($product->type == 'standard') {
                    $nestedData['qty'] = Product_Warehouse::where([
                        ['product_id', $product->id],
                        ['warehouse_id', Auth::user()->warehouse_id]
                    ])->sum('qty');
                } else
                    $nestedData['qty'] = $product->qty;

                if ($product->unit_id)
                    $nestedData['unit'] = $product->unit->unit_name;
                else
                    $nestedData['unit'] = 'N/A';

                $nestedData['price'] = $product->price;
                $nestedData['cost'] = $product->cost;

                if (config('currency_position') == 'prefix')
                    $nestedData['stock_worth'] = config('currency') . ' ' . ($nestedData['qty'] * $product->price) . ' / ' . config('currency') . ' ' . ($nestedData['qty'] * $product->cost);
                else
                    $nestedData['stock_worth'] = ($nestedData['qty'] * $product->price) . ' ' . config('currency') . ' / ' . ($nestedData['qty'] * $product->cost) . ' ' . config('currency');

                $nestedData['options'] = '<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . trans("file.action") . '
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu">
                            <li>
                                <button="type" class="btn btn-link view"><i class="fa fa-eye"></i> ' . trans('file.View') . '</button>
                            </li>';

                if (in_array("products-edit", $request['all_permission']))
                    $nestedData['options'] .= '<li>
                            <a href="' . route('products.edit', $product->id) . '" class="btn btn-link"><i class="fa fa-edit"></i> ' . trans('file.edit') . '</a>
                        </li>';
                if (in_array("product_history", $request['all_permission']))
                    $nestedData['options'] .=  Form::open(["route" => "products.history", "method" => "GET"]) . '
                            <li>
                                <input type="hidden" name="product_id" value="' . $product->id . '" />
                                <button type="submit" class="btn btn-link"><i class="dripicons-checklist"></i> ' . trans("file.Product History") . '</button> 
                            </li>' . Form::close();
                if (in_array("print_barcode", $request['all_permission'])) {
                    $product_info = $product->code . ' (' . $product->name . ')';
                    $nestedData['options'] .=  Form::open(["route" => "product.printBarcode", "method" => "GET"]) . '
                        <li>
                            <input type="hidden" name="data" value="' . $product_info . '" />
                            <button type="submit" class="btn btn-link"><i class="dripicons-print"></i> ' . trans("file.print_barcode") . '</button> 
                        </li>' . Form::close();
                }
                if (in_array("products-delete", $request['all_permission']))
                    $nestedData['options'] .=  Form::open(["route" => ["products.destroy", $product->id], "method" => "DELETE"]) . '
                            <li>
                              <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="fa fa-trash"></i> ' . trans("file.delete") . '</button> 
                            </li>' . Form::close() . '
                        </ul>
                    </div>';
                // data for product details by one click
                if ($product->tax_id)
                    $tax = Tax::find($product->tax_id)->name;
                else
                    $tax = "N/A";

                if ($product->tax_method == 1)
                    $tax_method = trans('file.Exclusive');
                else
                    $tax_method = trans('file.Inclusive');

                $nestedData['product'] = array(
                    '[ "' . $product->type . '"', ' "' . $product->name . '"', ' "' . $product->code . '"', ' "' . $nestedData['brand'] . '"', ' "' . $nestedData['category'] . '"', ' "' . $nestedData['unit'] . '"', ' "' . $product->cost . '"', ' "' . $product->price . '"', ' "' . $tax . '"', ' "' . $tax_method . '"', ' "' . $product->alert_quantity . '"', ' "' . preg_replace('/\s+/S', " ", $product->product_details) . '"', ' "' . $product->id . '"', ' "' . $product->product_list . '"', ' "' . $product->variant_list . '"', ' "' . $product->qty_list . '"', ' "' . $product->price_list . '"', ' "' . $nestedData['qty'] . '"', ' "' . $product->image . '"', ' "' . $product->is_variant . '"]'
                );
                //$nestedData['imagedata'] = DNS1D::getBarcodePNG($product->code, $product->barcode_symbology);
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function stockAdjustmentHistoryData(Request $request)
    {
        $columns = array(
            1 => 'created_at',
            2 => 'reference_no',
        );

        $warehouse_id = $request->input('warehouse_id');
        $product_id = $request->input('product_id');

        $q = Adjustment::with(['warehouse', 'items.product'])
            ->whereHas('items.product', function ($query) use ($product_id) {
                $query->where('product_id', $product_id);
            })
            ->whereDate('created_at', '>=', $request->input('starting_date'))
            ->whereDate('created_at', '<=', $request->input('ending_date'));
        if ($warehouse_id) {
            $q = $q->where('warehouse_id', $warehouse_id);
        }

        $totalData = $q->count();
        $totalFiltered = $totalData;

        $limit = $totalData;
        if ($request->input('length') != -1) {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = 'returns.' . $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $returnss = $q->get();

        $data = [];
        if (!empty($returnss)) {
            foreach ($returnss as $key => $returns) {
                $nestedData['id'] = $returns->id;
                $nestedData['key'] = $key;
                $nestedData['date'] = date(config('date_format'), strtotime($returns->created_at));
                $nestedData['reference_no'] = $returns->reference_no;
                $nestedData['warehouse'] = $returns->warehouse->name;
                $nestedData['qty'] = number_format($returns->total_qty, config('decimal'));
                // dd($returns);
                // if($returns->sale_unit_id) {
                //     $unit_data = DB::table('units')->select('unit_code')->find($returns->sale_unit_id);
                //     $nestedData['qty'] .= ' '.$unit_data->unit_code;
                // }
                // $nestedData['unit_price'] = number_format(($returns->total / $returns->qty), config('decimal'));
                // $nestedData['sub_total'] = number_format($returns->total, config('decimal'));
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }


    public function stockTransferHistoryData(Request $request)
    {
        $columns = array(
            1 => 'created_at',
            2 => 'reference_no',
        );

        $warehouse_id = $request->input('warehouse_id');
        $product_id = $request->input('product_id');

        $q = Transfer::with(['fromWarehouse', 'toWarehouse'])
            // join('product_returns', 'returns.id', '=', 'product_returns.return_id')
            // ->where('product_returns.product_id', $product_id)
            ->whereHas('items.product', function ($query) use ($product_id) {
                $query->where('product_id', $product_id);
            })
            ->whereDate('created_at', '>=', $request->input('starting_date'))
            ->whereDate('created_at', '<=', $request->input('ending_date'));
        if ($warehouse_id) {
            $q = $q->where(function ($query) use ($warehouse_id) {
                $query->where('from_warehouse_id', $warehouse_id)
                    ->orWhere('to_warehouse_id', $warehouse_id);
            });
        }

        $totalData = $q->count();
        $totalFiltered = $totalData;

        $limit = $totalData;
        if ($request->input('length') != -1) {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = 'returns.' . $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $returnss = $q->get();

        $data = [];
        if (!empty($returnss)) {
            foreach ($returnss as $key => $returns) {
                $nestedData['id'] = $returns->id;
                $nestedData['key'] = $key;
                $nestedData['date'] = date(config('date_format'), strtotime($returns->created_at));
                $nestedData['reference_no'] = $returns->reference_no;
                $nestedData['from_warehouse'] = $returns->fromWarehouse->name;
                $nestedData['to_warehouse'] = $returns->toWarehouse->name;
                $nestedData['qty'] = number_format($returns->total_qty, config('decimal'));
                // dd($returns);
                // if($returns->sale_unit_id) {
                //     $unit_data = DB::table('units')->select('unit_code')->find($returns->sale_unit_id);
                //     $nestedData['qty'] .= ' '.$unit_data->unit_code;
                // }
                // $nestedData['unit_price'] = number_format(($returns->total / $returns->qty), config('decimal'));
                // $nestedData['sub_total'] = number_format($returns->total, config('decimal'));
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function show(Request $request, $id)
    {
        try {
            $product = Product::with(['unit', 'warehouses'])->find($id);
            $stocks = (new \App\Services\ProductService)->stockCountPerWarehouse($product->id);
            
            return view(
                'backend.product.show',
                compact(
                    'product',
                    'stocks',
                )
            );


        } catch (Exception $e) {
            throw $e;
        }
    }


    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }
}
