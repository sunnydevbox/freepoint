<?php

namespace App\Http\Controllers\V1;

use App\Enums\ProductTypeEnum;
use App\Http\Controllers\PurchaseController as ControllersPurchaseController;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Models\Adjustment;
use App\Models\Payment;
use App\Models\PaymentWithCheque;
use App\Models\PaymentWithCreditCard;
use App\Models\PosSetting;
use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\ProductBatch;
use App\Models\ProductPurchase;
use App\Models\ProductVariant;
use App\Models\Purchase;
use App\Models\Tax;
use App\Models\Transfer;
use App\Models\Unit;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Tax\TaxRepository;
use App\Repositories\Unit\UnitRepository;
use App\Repositories\Warehouse\WarehouseRepository;
use App\Services\ProductService;
use App\Services\ServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Collective\Html\FormFacade as Form;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends ControllersPurchaseController
{
    public function show($id)
    {
        try {
            $purchase = Purchase::find($id);

            return view('backend.purchases.show', compact('purchase'));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function store(Request $request)
    {
        $data = $request->except('document');
        //return dd($data);
        $data['user_id'] = auth()->user()->id;
        $data['reference_no'] = 'pr-' . date("Ymd") . '-' . date("his");
        $document = $request->document;


        if (isset($data['created_at']))
            $data['created_at'] = date("Y-m-d H:i:s", strtotime($data['created_at']));
        else
            $data['created_at'] = date("Y-m-d H:i:s");
        //return dd($data);
        $data['total_qty'] = 0;
        $data['total_discount'] = 0;
        $data['total_tax'] = 0;
        $data['total_cost'] = 0;
        $data['grand_total'] = 0;

        $lims_purchase_data = Purchase::create($data);

        return redirect()
            ->route('purchases.edit', ['purchase' => $lims_purchase_data])
            ->with('message', 'Purchase created successfully');
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('document');

        //return dd($data);
        DB::beginTransaction();
        try {
            $balance = $data['grand_total'] - $data['paid_amount'];
            if ($balance < 0 || $balance > 0) {
                $data['payment_status'] = 1;
            } else {
                $data['payment_status'] = 2;
            }
            $lims_purchase_data = Purchase::find($id);

            $data['created_at'] = date("Y-m-d", strtotime(str_replace("/", "-", $data['created_at'])));

            foreach ($lims_purchase_data->items as $i => $purchase) {

                if ($data['status'] == '1') {
                    $lims_product_warehouse_data = Product_Warehouse::where([
                        ['product_id', $purchase->product_id],
                        ['warehouse_id', $data['warehouse_id']]
                    ])->first();

                    if ($lims_product_warehouse_data) {
                        $lims_product_warehouse_data->qty = $lims_product_warehouse_data->qty + $purchase->qty;
                        $lims_product_warehouse_data->save();
                    }
                }

                $lims_purchase_data->update($data);
            }

            DB::commit();
            return redirect('purchases')->with('message', 'Purchase updated successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function limsProductSearch(Request $request)
    {
        $product_code = $request['data'];

        $lims_product_data = Product::active()->find($product_code);

        $product[] = $lims_product_data->name;
        if ($lims_product_data->is_variant)
            $product[] = $lims_product_data->item_code;
        else
            $product[] = $lims_product_data->code;
        $product[] = $lims_product_data->cost;

        if ($lims_product_data->tax_id) {
            $lims_tax_data = Tax::find($lims_product_data->tax_id);
            $product[] = $lims_tax_data->rate;
            $product[] = $lims_tax_data->name;
        } else {
            $product[] = 0;
            $product[] = 'No Tax';
        }
        $product[] = $lims_product_data->tax_method;

        $units = Unit::where("base_unit", $lims_product_data->unit_id)
            ->orWhere('id', $lims_product_data->unit_id)
            ->get();
        $unit_name = array();
        $unit_operator = array();
        $unit_operation_value = array();
        foreach ($units as $unit) {
            if ($lims_product_data->purchase_unit_id == $unit->id) {
                array_unshift($unit_name, $unit->unit_name);
                array_unshift($unit_operator, $unit->operator);
                array_unshift($unit_operation_value, $unit->operation_value);
            } else {
                $unit_name[]  = $unit->unit_name;
                $unit_operator[] = $unit->operator;
                $unit_operation_value[] = $unit->operation_value;
            }
        }

        $product[] = implode(",", $unit_name) . ',';
        $product[] = implode(",", $unit_operator) . ',';
        $product[] = implode(",", $unit_operation_value) . ',';
        $product[] = $lims_product_data->id;
        $product[] = $lims_product_data->is_batch;
        $product[] = $lims_product_data->is_imei;

        return $product;
    }

    public function deleteBySelection(Request $request)
    {
        $purchase_id = $request['purchaseIdArray'];
        foreach ($purchase_id as $id) {
            $lims_purchase_data = Purchase::find($id);
            $lims_product_purchase_data = ProductPurchase::where('purchase_id', $id)->get();
            $lims_payment_data = Payment::where('purchase_id', $id)->get();
            foreach ($lims_product_purchase_data as $product_purchase_data) {
                $lims_purchase_unit_data = Unit::find($product_purchase_data->purchase_unit_id);
                if ($lims_purchase_unit_data->operator == '*')
                    $recieved_qty = $product_purchase_data->recieved * $lims_purchase_unit_data->operation_value;
                else
                    $recieved_qty = $product_purchase_data->recieved / $lims_purchase_unit_data->operation_value;

                $lims_product_data = Product::find($product_purchase_data->product_id);
                if ($product_purchase_data->variant_id) {
                    $lims_product_variant_data = ProductVariant::select('id', 'qty')->FindExactProduct($lims_product_data->id, $product_purchase_data->variant_id)->first();
                    $lims_product_warehouse_data = Product_Warehouse::FindProductWithVariant($product_purchase_data->product_id, $product_purchase_data->variant_id, $lims_purchase_data->warehouse_id)
                        ->first();
                    $lims_product_variant_data->qty -= $recieved_qty;
                    $lims_product_variant_data->save();
                } elseif ($product_purchase_data->product_batch_id) {
                    $lims_product_batch_data = ProductBatch::find($product_purchase_data->product_batch_id);
                    $lims_product_warehouse_data = Product_Warehouse::where([
                        ['product_batch_id', $product_purchase_data->product_batch_id],
                        ['warehouse_id', $lims_purchase_data->warehouse_id]
                    ])->first();

                    $lims_product_batch_data->qty -= $recieved_qty;
                    $lims_product_batch_data->save();
                } else {
                    $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($product_purchase_data->product_id, $lims_purchase_data->warehouse_id)
                        ->first();
                }

                if (!empty($lims_product_warehouse_data)) {
                    $lims_product_data->qty -= $recieved_qty;
                    $lims_product_warehouse_data->qty -= $recieved_qty;
                    $lims_product_warehouse_data->save();
                }

                $lims_product_data->save();
                $product_purchase_data->delete();
            }
            foreach ($lims_payment_data as $payment_data) {
                if ($payment_data->paying_method == "Cheque") {
                    $payment_with_cheque_data = PaymentWithCheque::where('payment_id', $payment_data->id)->first();
                    $payment_with_cheque_data->delete();
                } elseif ($payment_data->paying_method == "Credit Card") {
                    $payment_with_credit_card_data = PaymentWithCreditCard::where('payment_id', $payment_data->id)->first();
                    $lims_pos_setting_data = PosSetting::latest()->first();
                    \Stripe\Stripe::setApiKey($lims_pos_setting_data->stripe_secret_key);
                    \Stripe\Refund::create(array(
                        "charge" => $payment_with_credit_card_data->charge_id,
                    ));

                    $payment_with_credit_card_data->delete();
                }
                $payment_data->delete();
            }

            $lims_purchase_data->delete();
        }
        return 'Purchase deleted successfully!';
    }

    public function print($id)
    {
        $purchaseOrder = Purchase::with([
            'author',
            'warehouse',
            'supplier',
        ])
            ->with([
                'items.product',
                'items.product.unit',
                'items.batch',
                'items' => function ($query) {
                    $query->orderBy('order_position', 'ASC');
                }
            ])
            ->find($id);

        return view('backend.purchase.print', compact('purchaseOrder'));
    }
}
