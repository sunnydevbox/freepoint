<?php
namespace App\Http\Controllers\V1\API;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function search(Request $request)
    {
        $service = app(ProductService::class);
        $result = $service->search($request->all());

        return $result;
    }
}