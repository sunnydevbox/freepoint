<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\UserController as ControllersUserController;
use App\Mail\UserDetails;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Product_Warehouse;
use App\Models\ProductVariant;
use App\Models\Roles;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\User;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Repositories\Biller\BillerRepository;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\CustomerGroup\CustomerGroupRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Tax\TaxRepository;
use App\Repositories\Unit\UnitRepository;
use App\Repositories\Warehouse\WarehouseRepository;
use App\Services\ProductService;
use App\Services\ServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class UserController extends ControllersUserController
{
    public ServiceInterface $service;

    public function index()
    {
        $user = auth()->user()->load(['roles.permissions']);

        if ($user->can('users-index')) {
            $all_permission = $user->getPermissionsViaRoles()->map(fn ($permission) => $permission->name)->toArray();
            $lims_user_list = User::where('is_deleted', false)->get();
            return view(
                'backend.user.index',
                compact(
                    'lims_user_list',
                    'all_permission'
                )
            );
        } else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }

    public function create()
    {
        if(auth()->user()->can('users-add')){
            $lims_role_list = app(RoleRepository::class)->getCachedAll();
            $lims_biller_list = app(BillerRepository::class)->getCachedAll();
            $lims_warehouse_list = app(WarehouseRepository::class)->where('is_active', true)->get();// getCachedAll();
            $lims_customer_group_list = app(CustomerGroupRepository::class)->getCachedAll();
            return view(
                'backend.user.create', 
                compact(
                    'lims_role_list', 
                    'lims_biller_list', 
                    'lims_warehouse_list', 
                    'lims_customer_group_list'
                )
            );
        }
        else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'max:255',
                    Rule::unique('users')->where(function ($query) {
                    return $query->where('is_deleted', false);
                }),
            ],
            'email' => [
                'email',
                'max:255',
                    Rule::unique('users')->where(function ($query) {
                    return $query->where('is_deleted', false);
                }),
            ],
        ]);
        
        if($request->role_id == 5) {
            $this->validate($request, [
                'phone_number' => [
                    'max:255',
                        Rule::unique('customers')->where(function ($query) {
                        return $query->where('is_active', 1);
                    }),
                ],
            ]);
        }

        $data = $request->all();
        $message = 'User created successfully';
        try {
            Mail::to($data['email'])->send(new UserDetails($data));
        }
        catch(\Exception $e){
            $message = 'User created successfully. Please setup your <a href="setting/mail_setting">mail setting</a> to send mail.';
        }
        if(!isset($data['is_active'])) {
            $data['is_active'] = false;
        }

        $data['is_deleted'] = false;
        $data['password'] = bcrypt($data['password']);
        $data['phone'] = $data['phone_number'];

        $user = User::create($data);


        // TODO
        // Update role. ONLY ONE ROLE PER 
        // - remove all rolese
        // - replace with new one
        $user->syncRoles([]);
        $user->assignRole($data['role_id']);

        
        if($data['role_id'] == 5) {
            $data['name'] = $data['customer_name'];
            $data['phone_number'] = $data['phone'];
            $data['is_active'] = true;
            Customer::create($data);
        }
        
        return redirect('user')->with('message1', $message); 
    }

    public function edit($id)
    {
        if(auth()->user()->can('users-edit')){
            $lims_user_data = User::with('warehouses')->find($id);
            $lims_role_list = app(RoleRepository::class)->getCachedAll();
            $lims_biller_list = app(BillerRepository::class)->getCachedAll();
            $lims_warehouse_list = app(WarehouseRepository::class)->listForDropdown();
            return view(
                'backend.user.edit', 
                compact(
                    'lims_user_data', 
                    'lims_role_list', 
                    'lims_biller_list', 
                    'lims_warehouse_list'
                )
            );
        }
        else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }


    public function update(Request $request, $id)
    {
        if (false /*!env('USER_VERIFIED')*/)
            return redirect()->back()->with('not_permitted', 'This feature is disable for demo!');

        $this->validate($request, [
            'name' => [
                'max:255',
                Rule::unique('users')->ignore($id)->where(function ($query) {
                    return $query->where('is_deleted', false);
                }),
            ],
            'email' => [
                'email',
                'max:255',
                Rule::unique('users')->ignore($id)->where(function ($query) {
                    return $query->where('is_deleted', false);
                }),
            ],
        ]);

        $input = $request->except('password');
        $assignedWarehouses = $request->get('assigned_warehouse', []);
        // dd($request->all(), $input, $assignedWarehouses, 2);
        
        if (!isset($input['is_active'])) {
            $input['is_active'] = false;
        }
        
        if (!empty($request['password'])) {
            $input['password'] = bcrypt($request['password']);
        }
        
        $lims_user_data = User::find($id);
        $lims_user_data->update($input);

        $lims_user_data->warehouses()->sync($assignedWarehouses);

        // TODO
        // Update role. ONLY ONE ROLE PER 
        // - remove all rolese
        // - replace with new one
        $lims_user_data->syncRoles([]);
        $lims_user_data->assignRole($input['role_id']);

        return redirect('user')->with('message2', 'Data updated successfullly');
    }

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }
}
