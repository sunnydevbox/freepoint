<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Search\ProductCollection;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function product(Request $request)
    {
        try {

            $supplierId = $request->get('supplier_id', null);
            $term = $request->get('term', null);

            if (!empty($term)) {
                $query = Product::active()
                    ->select(['id', 'name', 'code'])
                    ->where(function ($query) use ($term) {
                        $query->where('name', 'LIKE', "%{$term}%")
                            ->orWhere('code', 'LIKE', "%{$term}%")
                            ->orWhere('model', 'LIKE', "%{$term}%");
                    })
                    ->orderBy('name', 'ASC');

                if (!empty($supplierId)) {
                    // $query = $query->whereHas();
                }

                
                $products = $query->get();
                return $products;
                return new ProductCollection($products);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
