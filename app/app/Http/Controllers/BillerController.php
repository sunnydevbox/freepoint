<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biller;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use App\Mail\BillerCreate;
use App\Models\Warehouse;
use App\Repositories\Biller\BillerRepository;
use App\Repositories\Warehouse\WarehouseRepository;
use Mail;
use App\Traits\TenantInfo;

class BillerController extends Controller
{
    use TenantInfo;

    public function __construct(
        public WarehouseRepository $warehouseRepository,
        public BillerRepository $billerRepository
    ) {}

    public function index()
    {
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('billers-index')) {
            $permissions = Role::findByName($role->name)->permissions;
            foreach ($permissions as $permission) {
                $all_permission[] = $permission->name;
            }
            if (empty($all_permission)) {
                $all_permission[] = '';
            }
            $lims_biller_all = $this->billerRepository->getActive(with: ['warehouse']);
            return view('backend.biller.index', compact('lims_biller_all', 'all_permission'));
        } else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }

    public function create()
    {
        $warehouses = $this->warehouseRepository->getAllActive();
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('billers-add')) {
            return view('backend.biller.create', compact('warehouses'));
        } else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => [
                'max:255',
                Rule::unique('billers')->where(fn($query) => $query->where('is_active', 1))

            ],
            'email' => [
                'nullable',
                'email',
                'max:255',
                Rule::unique('billers')->where(fn($query) => $query->where('is_active', 1))
            ],
            'warehouse_id' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:10000',
        ]);

        $lims_biller_data = $request->except('image');
        $lims_biller_data['is_active'] = true;
        $lims_biller_data['warehouse_id'] = $request->warehouse_id;
        $image = $request->image;
        if ($image) {
            $ext = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
            $imageName = date("Ymdhis");

            if (!config('database.connections.saleprosaas_landlord')) {
                $formattedImageName = sprintf('%s.%s', $imageName, $ext);
                $image->move('public/images/biller', $formattedImageName);
            } else {
                $tenantId = $this->getTenantId();
                $formattedImageName = sprintf('%s_%s.%s', $tenantId, $imageName, $ext);
                $image->move('public/images/biller', $formattedImageName);
            }
            $lims_biller_data['image'] = $imageName;
        }
        Biller::create($lims_biller_data);
        $message = 'Data inserted successfully';
        try {
            Mail::to($lims_biller_data['email'])->send(new BillerCreate($lims_biller_data));
        } catch (\Exception $e) {
            $message = 'Data inserted successfully. Please setup your <a href="setting/mail_setting">mail setting</a> to send mail.';
        }

        return redirect('biller')->with('message', $message);
    }

    public function edit($id)
    {
        $warehouses = $this->warehouseRepository->getAllActive();
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('billers-edit')) {
            $lims_biller_data = Biller::find($id);
            return view('backend.biller.edit', compact('lims_biller_data', 'warehouses'));
        } else {
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => [
                'max:255',
                Rule::unique('billers')->ignore($id)->where(fn($query) => $query->where('is_active', 1)),
            ],
            'email' => [
                'nullable',
                'email',
                'max:255',
                Rule::unique('billers')->ignore($id)->where(fn($query) => $query->where('is_active', 1)),
            ],
            'warehouse_id' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:100000',
        ]);
        $input['warehouse_id'] = $request->warehouse_id;
        $input = $request->except('image');
        $image = $request->image;

        if ($image) {
            $ext = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
            $imageName = date("Ymdhis");

            if (!config('database.connections.saleprosaas_landlord')) {
                $formattedImageName = sprintf('%s.%s', $imageName, $ext);
                $image->move('public/images/biller', $formattedImageName);
            } else {
                $tenantId = $this->getTenantId();
                $formattedImageName = sprintf('%s_%s.%s', $tenantId, $imageName, $ext);
                $image->move('public/images/biller', $formattedImageName);
            }
            $input['image'] = $imageName;
        }

        $lims_biller_data = Biller::findOrFail($id);
        $lims_biller_data->update($input);

        return redirect('biller')->with('message', 'Data updated successfully');
    }

    public function importBiller(Request $request)
    {
        $upload = $request->file('file');
        $ext = pathinfo($upload->getClientOriginalName(), PATHINFO_EXTENSION);
        if ($ext != 'csv') {
            return redirect()->back()->with('not_permitted', 'Please upload a CSV file');
        }
        $filename =  $upload->getClientOriginalName();
        $filePath = $upload->getRealPath();
        //open and read
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader = [];

        //validate
        foreach ($header as $key => $value) {
            $lheader = strtolower($value);
            $escapedItem = preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }

        //looping through othe columns
        while ($columns = fgetcsv($file)) {
            if ($columns[0] == "")
                continue;
            foreach ($columns as $key => $value) {
                $value = preg_replace('/\D/', '', $value);
            }
            $data = array_combine($escapedHeader, $columns);

            $biller = Biller::firstOrNew(['company_name' => $data['companyname']]);
            $biller->name = $data['name'];
            $biller->image = $data['image'];
            $biller->vat_number = $data['vatnumber'];
            $biller->email = $data['email'];
            $biller->phone_number = $data['phonenumber'];
            $biller->address = $data['address'];
            $biller->city = $data['city'];
            $biller->state = $data['state'];
            $biller->postal_code = $data['postalcode'];
            $biller->country = $data['country'];
            $biller->is_active = true;
            $biller->save();
            $message = 'Biller Imported successfully';
            if ($data['email']) {
                try {
                    Mail::to($data['email'])->send(new BillerCreate($data));
                } catch (\Exception $e) {
                    $message = 'Biller Imported successfully. Please setup your <a href="setting/mail_setting">mail setting</a> to send mail.';
                }
            }
        }
        return redirect('biller')->with('message', $message);
    }

    public function deleteBySelection(Request $request)
    {
        $biller_id = $request['billerIdArray'];
        foreach ($biller_id as $id) {
            $lims_biller_data = Biller::find($id);
            $lims_biller_data->is_active = false;
            $lims_biller_data->save();
        }
        return 'Biller deleted successfully!';
    }

    public function destroy($id)
    {
        $lims_biller_data = Biller::find($id);
        if ($lims_biller_data->image && !config('database.connections.saleprosaas_landlord')) {
            unlink('public/images/biller/' . $lims_biller_data->image);
        } elseif ($lims_biller_data->image) {
            unlink('images/biller/' . $lims_biller_data->image);
        }
        $lims_biller_data->is_active = false;
        $lims_biller_data->save();
        return redirect('biller')->with('not_permitted', 'Data deleted successfully');
    }
}
