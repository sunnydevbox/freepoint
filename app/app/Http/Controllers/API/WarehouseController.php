<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\WarehouseService;
use Illuminate\Http\Request;

class WarehouseController extends AbstractBreadController
{
    public function service(): object
    {
        return app(WarehouseService::class);
    }
}
