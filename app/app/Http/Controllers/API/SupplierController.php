<?php

namespace App\Http\Controllers\API;

use App\Services\SupplierService;

class SupplierController extends AbstractBreadController
{
    public array $resources = [
        'index' => '\App\Http\Resources\Tax\BrowseCollection',
        'show' => '\App\Http\Resources\Tax\ReadResource',
        'update' => '\App\Http\Resources\Tax\ReadResource',
        'store' => '\App\Http\Resources\Tax\ReadResource',
        'destroy' => '',
    ];

    public function service(): object
    {
        return app(SupplierService::class);
    }
}
