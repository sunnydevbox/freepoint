<?php

namespace App\Http\Controllers\API;

use App\Services\CategoryService;

class CategoryController extends AbstractBreadController
{
    public array $resources = [
        'index' => '\App\Http\Resources\Category\BrowseCollection',
        'show' => '\App\Http\Resources\Category\ReadResource',
        'update' => '\App\Http\Resources\Category\ReadResource',
        'store' => '\App\Http\Resources\Category\ReadResource',
        'destroy' => '',
    ];

    public function service(): object
    {
        return app(CategoryService::class);
    }
}
