<?php

namespace App\Http\Controllers\API;

use App\Services\BrandService;

class BrandController extends AbstractBreadController
{
    public array $resources = [
        'index' => '\App\Http\Resources\Brand\BrowseCollection',
        'show' => '\App\Http\Resources\Brand\ReadResource',
        'update' => '\App\Http\Resources\Brand\ReadResource',
        'store' => '\App\Http\Resources\Brand\ReadResource',
        'destroy' => '',
    ];

    public function service(): object
    {
        return app(BrandService::class);
    }
}
