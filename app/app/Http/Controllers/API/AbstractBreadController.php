<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseBrowseCollection as BrowseCollection;
use App\Http\Resources\BaseReadResource as ReadResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Services\ServiceInterface;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Http\Response;

abstract class AbstractBreadController extends Controller
{
    public array $resources = [
        'index' => '\App\Http\Resources\BaseBrowseCollection',
        'show' => '\App\Http\Resources\BaseReadResource',
        'update' => '\App\Http\Resources\BaseReadResource',
        'store' => '\App\Http\Resources\BaseReadResource',
        'destroy' => '',
    ];

    public ServiceInterface $service;


    protected function index(Request $request): ResourceCollection
    {
        $result = $this->service()->browse($request->all());
        return new $this->resources['index']($result);
    }

    public function show($id): JsonResource
    {
        $result = $this->service()->read($id);

        return new $this->resources['show']($result);
    }

    public function update(Request $request, $id): JsonResource
    {
        $result = $this->service()->edit($id, $request->all());

        return new $this->resources['update']($result);
    }


    public function store(Request $request): JsonResource
    {
        try {
            $result = $this->service()->add($request->all());
            return new $this->resources['store']($result);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function destroy($id): Response
    {
        $this->service()->destroy($id);

        return response()->noContent();
    }

    abstract public function service(): object;
}
