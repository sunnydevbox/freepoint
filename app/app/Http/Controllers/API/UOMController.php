<?php

namespace App\Http\Controllers\API;

use App\Services\UOMService;
use Illuminate\Http\Request;

class UOMController extends AbstractBreadController
{
    public array $resources = [
        'index' => '\App\Http\Resources\UOM\BrowseCollection',
        'show' => '\App\Http\Resources\UOM\ReadResource',
        'update' => '\App\Http\Resources\UOM\ReadResource',
        'store' => '\App\Http\Resources\UOM\ReadResource',
        'destroy' => '',
    ];

    public function limsUnitSearch(Request $request)
    {
        $result = $this->service()->search($request->get('q', null));

        return response()->json($result);
    }

    public function importUnit(Request $request)
    {
        //get file
        $filename =  $request->file->getClientOriginalName();
        $upload = $request->file('file');
        $filePath = $upload->getRealPath();
        //open and read
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader = [];
        //validate
        foreach ($header as $key => $value) {
            $lheader = strtolower($value);
            $escapedItem = preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        $lims_unit_data = [];
        while ($columns = fgetcsv($file)) {
            if ($columns[0] == "")
                continue;
            foreach ($columns as $key => $value) {
                $value = preg_replace('/\D/', '', $value);
            }
            $data = array_combine($escapedHeader, $columns);

            $unit = Unit::firstOrNew(['unit_code' => $data['code'], 'is_active' => true]);
            $unit->unit_code = $data['code'];
            $unit->unit_name = $data['name'];
            if ($data['baseunit'] == null)
                $unit->base_unit = null;
            else {
                $base_unit = Unit::where('unit_code', $data['baseunit'])->first();
                $unit->base_unit = $base_unit->id;
            }
            if ($data['operator'] == null)
                $unit->operator = '*';
            else
                $unit->operator = $data['operator'];
            if ($data['operationvalue'] == null)
                $unit->operation_value = 1;
            else
                $unit->operation_value = $data['operationvalue'];
            $unit->save();
        }
        return redirect('unit')->with('message', 'Unit imported successfully');
    }

    public function deleteBySelection(Request $request)
    {
        $unit_id = $request['unitIdArray'];
        foreach ($unit_id as $id) {
            $lims_unit_data = Unit::findOrFail($id);
            $lims_unit_data->is_active = false;
            $lims_unit_data->save();
        }
        return 'Unit deleted successfully!';
    }


    public function service(): object
    {
        return app(UOMService::class);
    }
}
