<?php

namespace App\Http\Controllers\API;

use App\Services\ProductService;

class ProductController extends AbstractBreadController
{
    public array $resources = [
        'index' => '\App\Http\Resources\Product\BrowseCollection',
        'show' => '\App\Http\Resources\Product\ReadResource',
        'update' => '\App\Http\Resources\Product\ReadResource',
        'store' => '\App\Http\Resources\Product\ReadResource',
        'destroy' => '',
    ];

    public function service(): object
    {
        return app(ProductService::class);
    }
}
