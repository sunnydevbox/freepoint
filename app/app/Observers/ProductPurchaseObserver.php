<?php

namespace App\Observers;

use App\Enums\TaxMethodEnum;
use App\Models\ProductPurchase;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ProductPurchase\ProductPurchaseRepository;

class ProductPurchaseObserver
{
    public function creating(ProductPurchase $productPurchase)
    {
        (new ProductPurchaseRepository)->recalculate($productPurchase);
    }

    public function saving(ProductPurchase $productPurchase)
    {
        (new ProductPurchaseRepository)->recalculate($productPurchase);
    }
}
