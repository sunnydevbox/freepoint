<?php

namespace App\Providers;

use App\Models\Product;
use App\Events\WarehouseCreated;
use App\Observers\ProductObserver;
use App\Events\Payment\PaymentAdded;
use Illuminate\Support\Facades\Event;
use App\Listeners\CreateDefaultWarehouseBin;
use App\Events\PurchaseOrderProductsChangedEvent;
use App\Listeners\RecalculatePurchaseOrderListener;
use App\Listeners\Payment\CalculateSaleOrderPayment;
use App\Listeners\Payment\AssignCashRegisterToPayment;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        \App\Events\PurchaseOrderProductsChangedEvent::class => [
            \App\Listeners\RecalculatePurchaseOrderListener::class,
        ],

        \App\Events\WarehouseCreated::class => [
            \App\Listeners\CreateDefaultWarehouseBin::class,
        ],

        PaymentAdded::class => [
            CalculateSaleOrderPayment::class,
            AssignCashRegisterToPayment::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Product::observe(ProductObserver::class);
    }
}
