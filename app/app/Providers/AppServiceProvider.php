<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Observers\CategoryObserver;
use App\Observers\ProductPurchaseObserver;
use App\Observers\PurchaseObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use DB;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Schema::defaultStringLength(191);

        Category::observe(CategoryObserver::class);
        
        ProductPurchase::observe(ProductPurchaseObserver::class);
        Purchase::observe(PurchaseObserver::class);
    }
}
