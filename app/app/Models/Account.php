<?php

namespace App\Models;

class Account extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['accounts'];
    public $cachePrefix = 'accounts_';
    /** End of Caching Settings */

    protected $fillable = [
        "account_no",
        "name",
        "initial_balance",
        "total_balance",
        "note",
        "is_default",
        "is_active"
    ];

    public function isAcive()
    {
        return !empty($this->attributes['is_active']);
    }

    public function isDefault()
    {
        return !empty($this->attributes['is_default']);
    }

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }

    public function scopeDefault($query)
    {
        $query->where('is_default', true);
    }
}
