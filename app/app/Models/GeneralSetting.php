<?php

namespace App\Models;

class GeneralSetting extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['generalSettings'];
    public $cachePrefix = 'generalSettings_';
    /** End of Caching Settings */

    protected $fillable = [

        'site_title',
        'site_logo',
        'is_rtl',
        'currency',
        'currency_position',
        'staff_access',
        'date_format',
        'theme',
        'developed_by',
        'invoice_format',
        'decimal',
        'state'
    ];
}
