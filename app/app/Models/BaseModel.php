<?php
namespace App\Models;

use App\Traits\ActivityLoggerTrait;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class BaseModel extends Model
{
    // use QueryCacheable;
    use ActivityLoggerTrait;

    /**
     * Invalidate the cache automatically
     * upon update in the database.
     *
     * Override this in resepective model 
     * classes as needed
     */
    protected static bool $flushCacheOnUpdate = true;
    
    public $cacheFor = 60;
}