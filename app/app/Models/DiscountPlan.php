<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class DiscountPlan extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */
    
    public $cacheTags = ['discountPlans'];
    public $cachePrefix = 'discountPlans_';
    /** End of Caching Settings */

    protected $fillable = ['name', 'is_active'];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'discount_plan_customers');
    }
}
