<?php

namespace App\Models;

class PurchaseProductReturn extends BaseModel
{
    protected $table = 'purchase_product_return';

    /** Startr of Caching Settings */
    
    public $cacheTags = ['purchasesProductReturn'];
    public $cachePrefix = 'purchasesProductReturn_';
    /** End of Caching Settings */

    protected $fillable = [
        "return_id",
        "product_id",
        "product_batch_id",
        "variant_id",
        "imei_number",
        "qty",
        "purchase_unit_id",
        "net_unit_cost",
        "discount",
        "tax_rate",
        "tax",
        "total"
    ];


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'purchase_unit_id');
    }

    public function return()
    {
        return $this->belongsTo(Returns::class, 'return_id');
    }
}
