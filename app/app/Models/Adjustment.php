<?php

namespace App\Models;

class Adjustment extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['adjustments'];
    public $cachePrefix = 'adjustments_';
    /** End of Caching Settings */

    protected $fillable = [
        "reference_no", "warehouse_id", "document", "total_qty", "item",
        "note"
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function items()
    {
        return $this->hasMany(ProductAdjustment::class, 'adjustment_id');
    }
}
