<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Brand extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */

    public $cacheTags = ['brands'];
    public $cachePrefix = 'brands_';
    /** End of Caching Settings */

    protected $fillable = [

        "title",
        "image",
        "is_active"
    ];

    public function product()
    {
        return $this->hasMany('App/Models/Product');
    }


    public function scopeActive($query)
    {
        $query->where('is_active', 1);
    }
}
