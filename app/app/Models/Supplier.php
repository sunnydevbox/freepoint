<?php

namespace App\Models;

use App\Traits\ActivityLoggerTrait;
use App\Traits\HasAttachment;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Supplier extends BaseModel
{
    use HasFactory, HasAttachment, ActivityLoggerTrait;
    
    /** Startr of Caching Settings */
    
    public $cacheTags = ['suppliers'];
    public $cachePrefix = 'suppliers_';
    /** End of Caching Settings */

    protected $fillable =[

        "name", "image", "company_name", "vat_number",
        "email", "phone_number", "address", "city",
        "state", "postal_code", "country", "is_active"
        
    ];

    public function product()
    {
    	return $this->hasMany('App/Models/Product');	
    }

    // public function getImageAttribute()
    // {
    //     return $this->attachments()->first();
    // }
}
