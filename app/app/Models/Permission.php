<?php
namespace App\Models;

use Spatie\Permission\Models\Permission as SpatiePermission;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Permission extends SpatiePermission
{
    // use QueryCacheable;

    /** Startr of Caching Settings */
    
    public $cacheTags = ['permissions'];
    public $cachePrefix = 'permissions_';
    /** End of Caching Settings */
}