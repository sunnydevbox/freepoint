<?php

namespace App\Models;

class StockCount extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['sotkcCounts'];
    public $cachePrefix = 'sotkcCounts_';
    /** End of Caching Settings */

    protected $table = 'stock_counts';
    protected $fillable = [
        "reference_no", "warehouse_id", "brand_id", "category_id", "user_id", "type", "initial_file", "final_file", "note", "is_adjusted"
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
