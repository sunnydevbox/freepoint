<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Purchase extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */
    public $cacheTags = ['purchases'];
    public $cachePrefix = 'purchases_';
    /** End of Caching Settings */

    protected $appends = [
        'due',
        'balance',
    ];

    protected $casts = [
        'is_reorder' => 'boolean',
    ];

    protected $fillable = [
        'reference_no',
        'user_id',
        'warehouse_id',
        'supplier_id',
        'item',
        'total_qty',
        'total_discount',
        'total_tax',
        'total_cost',
        'order_tax_rate',
        'order_tax',
        'order_discount',
        'shipping_cost',
        'grand_total',
        'paid_amount',
        'status',
        'payment_status',
        'document',
        'note',
        'created_at',
        'invoice_number',
        'is_reorder',
    ];

    public function supplier()
    {
        return $this->belongsTo(\App\Models\Supplier::class);
    }

    // @deprecated
    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Warehouse::class);
    }

    public function items()
    {
        return $this->hasMany(ProductPurchase::class, 'purchase_id');
    }

    public function returns()
    {
        return $this->hasMany(ReturnPurchase::class, 'purchase_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'purchase_id')->orderBy('created_at', 'DESC');
    }

    public function getDueAttribute()
    {
        return $this->grand_total - $this->returned_amount - $this->paid_amount;
    }
    
    // Alias of getDueAttribute
    public function getBalanceAttribute()
    {
        return $this->getDueAttribute();
    }
}
