<?php

namespace App\Models;


class Currency extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['currencies'];
    public $cachePrefix = 'currencies_';
    /** End of Caching Settings */

    protected $fillable = ["name", "code", "exchange_rate"];
}
