<?php

namespace App\Models;

use App\Enums\PaymentMethodEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    protected $fillable = [
        'purchase_id',
        'user_id',
        'sale_id',
        'cash_register_id',
        'account_id',
        'payment_reference',
        'amount',
        'used_points',
        'change',
        'paying_method', // @deprecated
        'payment_note',
        'payment_method_id',
    ];

    protected $casts = [
        'payment_method_id' => PaymentMethodEnum::class,
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sale(): BelongsTo
    {
        return $this->belongsTo(Sale::class, 'sale_id');
    }

    public function purchase(): BelongsTo
    {
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function getPaymentMethodAttribute()
    {
        return $this->payment_method_id;
    }
}
