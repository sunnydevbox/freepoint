<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Biller extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */

    public $cacheTags = ['billers'];
    public $cachePrefix = 'billers_';
    /** End of Caching Settings */

    protected $fillable = [
        "name",
        "image",
        "company_name",
        "vat_number",
        "email",
        "phone_number",
        "address",
        "city",
        "state",
        "postal_code",
        "country",
        "is_active",
        "warehouse_id"
    ];

    public function sale()
    {
        return $this->hasMany('App\Models\Sale');
    }

    public function scopeActive($query)
    {
        $query->where('is_active', 1);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
