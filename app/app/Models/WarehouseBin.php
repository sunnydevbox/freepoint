<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class WarehouseBin extends BaseModel
{
    use HasFactory;

    protected $table = 'warehouse_bins';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'warehouse_id',
        'aisle',
        'rack',
        'shelf',
        'bin',
        'is_default'
    ];

    protected $casts = [
        'is_default' => 'boolean'
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function stocks()
    {
        return $this->hasMany(Product_Warehouse::class, 'warehouse_bin_id');
    }
}
