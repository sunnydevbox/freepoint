<?php

namespace App\Models;

class ProductAdjustment extends BaseModel
{
    protected $table = 'product_adjustments';

    /** Startr of Caching Settings */
    
    public $cacheTags = ['product_adjustments'];
    public $cachePrefix = 'product_adjustments_';
    /** End of Caching Settings */

    protected $fillable = [
        "adjustment_id", "product_id", "variant_id", "qty", "action"
    ];

    public function adjustment()
    {
        return $this->belongsTo(Adjustment::class, 'adjustment_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function variant()
    {
        // TODO
        // Add this later
    }
}
