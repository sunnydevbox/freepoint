<?php

namespace App\Models;

class ReturnPurchase extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['returnPurchase'];
    public $cachePrefix = 'returnPurchase_';
    /** End of Caching Settings */

    protected $table = 'return_purchases';
    
    protected $fillable = [
        'reference_no',
        'purchase_id',
        'user_id',
        'supplier_id',
        'warehouse_id',
        'account_id',
        'item',
        'total_qty',
        'total_discount',
        'total_tax',
        'total_cost',
        'order_tax_rate',
        'order_tax',
        'grand_total',
        'document',
        'return_note',
        'staff_note'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function purchase()
    {
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }
}
