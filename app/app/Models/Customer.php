<?php

namespace App\Models;

class Customer extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['customers'];
    public $cachePrefix = 'customers_';
    /** End of Caching Settings */

    protected $fillable =[
        "customer_group_id", "user_id", "name", "company_name",
        "email", "phone_number", "tax_no", "address", "city",
        "state", "postal_code", "country", "points", "deposit", "expense", "is_active"
    ];

    public function customerGroup()
    {
        return $this->belongsTo('App\Models\CustomerGroup');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function discountPlans()
    {
        return $this->belongsToMany('App\Models\DiscountPlan', 'discount_plan_customers');
    }

    public function scopeActive($query)
    {
        $query->where('is_active', 1);
    }

}
