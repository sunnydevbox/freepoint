<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as SpatieRole;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Role extends SpatieRole
{
    use HasFactory;
    // use QueryCacheable;

    /** Startr of Caching Settings */
    
    public $cacheTags = ['roles'];
    public $cachePrefix = 'roles_';
    /** End of Caching Settings */
}