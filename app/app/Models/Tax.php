<?php

namespace App\Models;

class Tax extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['taxes'];
    public $cachePrefix = 'taxes_';
    /** End of Caching Settings */

    protected $fillable = [
        'sname',
        'rate',
        'is_active'
    ];

    public function product()
    {
        return $this->hasMany('App/Models/Product');
    }

    public function scopeActive($query)
    {
        $query->where('is_active', 1);
    }
}
