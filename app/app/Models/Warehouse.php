<?php

namespace App\Models;

use App\Events\WarehouseCreated;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Warehouse extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */

    public $cacheTags = ['warehouses'];
    public $cachePrefix = 'warehouses_';
    /** End of Caching Settings */


    protected $fillable = [
        "name",
        "phone",
        "email",
        "address",
        "is_active"
    ];

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'product_warehouse',
            'warehouse_id',
            'product_id'
        )
            ->withPivot((new Product_Warehouse)->getFillable());
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'user_warehouse_accesses',
            'warehouse_id',
            'user_id'
        );
    }

    public function bins()
    {
        return $this->hasMany(WarehouseBin::class, 'warehouse_id');
    }

    public function scopeActive($query)
    {
        $query->where('is_active', 1);
    }

    protected $dispatchesEvents = [
        'created' => WarehouseCreated::class,
    ];
}
