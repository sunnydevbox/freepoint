<?php

namespace App\Models;

class RewardPointSetting extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['customers'];
    public $cachePrefix = 'customers_';
    /** End of Caching Settings */

    protected $fillable = [
        "per_point_amount",
        "minimum_amount",
        "duration",
        "type",
        "is_active"
    ];
}
