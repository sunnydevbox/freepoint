<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductPurchase extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */
    public $cacheTags = ['productPurchases'];
    public $cachePrefix = 'productPurchases_';
    /** End of Caching Settings */

    protected $table = 'product_purchases';

    protected $appends = [
        'remaining',
        'received',
    ];

    protected $fillable = [
        "purchase_id",
        "product_id",
        "product_batch_id",
        "variant_id",
        "imei_number",
        "qty",
        "recieved",
        "purchase_unit_id",
        "net_unit_cost",
        "discount",
        "tax_rate",
        "tax",
        "total",
        "order_position",
        "warehouse_bin_id"
    ];

    public function purchase()
    {
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function batch()
    {
        return $this->belongsTo(ProductBatch::class, 'product_batch_id');   
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'purchase_unit_id');
    }

    public function variant()
    {
        return $this->belongsTo(ProductVariant::class, 'variant_id');
    }

    public function bin()
    {
        return $this->belongsTo(WarehouseBin::class, 'warehouse_bin_id');
    }

    public function stocks()
    {
        return $this->hasMany(Product_Warehouse::class, 'po_item_id');
    }

    public function getRemainingAttribute()
    {
        return $this->qty - $this->recieved;
    }

    public function getReceivedAttribute()
    {
        return $this->recieved;
    }
}
