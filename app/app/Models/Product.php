<?php

namespace App\Models;

use App\Enums\ProductTypeEnum;
use App\Traits\ActivityLoggerTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Bnb\Laravel\Attachments\HasAttachment;
use Givebutter\LaravelCustomFields\Traits\HasCustomFields;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends BaseModel
{
    use HasFactory, SoftDeletes, HasAttachment, ActivityLoggerTrait;
    use HasCustomFields;

    /** Startr of Caching Settings */

    public $cacheTags = ['products'];
    public $cachePrefix = 'products_';
    /** End of Caching Settings */

    protected $fillable =  [
        'name',
        'code',
        'type',
        'barcode_symbology',
        'brand_id',
        'category_id',
        'unit_id',
        'purchase_unit_id',
        'sale_unit_id',
        'cost',
        'price',
        'qty',
        'alert_quantity',
        'daily_sale_objective',
        'promotion',
        'promotion_price',
        'starting_date',
        'last_date',
        'tax_id',
        'tax_method',
        'image',
        'file',
        'is_embeded',
        'is_batch',
        'is_variant',
        'is_diffPrice',
        'is_imei',
        'featured',
        'product_list',
        'variant_list',
        'qty_list',
        'price_list',
        'product_details',
        'variant_option',
        'variant_value',
        'is_active',
        'model',

        // Additional fields
        'year',
        'version',
    ];

    protected $casts = [
        'type' => ProductTypeEnum::class,
        'is_active' => 'boolean',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function purchaseUnit()
    {
        return $this->belongsTo(Unit::class, 'purchase_unit_id');
    }

    public function saleUnit()
    {
        return $this->belongsTo(Unit::class, 'sale_unit_id');
    }

    public function variant()
    {
        return $this->belongsToMany(Variant::class, 'product_variants')->withPivot('id', 'item_code', 'additional_cost', 'additional_price');
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id');
    }

    public function warehouses()
    {
        return $this->belongsToMany(
            Warehouse::class,
            'product_warehouse',
            'product_id',
            'warehouse_id'
        )
            ->withPivot((new Product_Warehouse)->getFillable());
    }

    // @deprecated
    public function productInWarehouse()
    {
        // return $this->belongsToMany(Warehouse::class, 'product_warehouse', 'product_id');
        return $this->hasMany(Product_Warehouse::class, 'product_id');
    }


    public function inWarehouses()
    {
        return $this->hasMany(Product_Warehouse::class, 'product_id');
    }


    public function inPurchases()
    {
        return $this->hasMany(ProductPurchase::class, 'product_id');
    }

    public function inSales()
    {
        return $this->hasMany(Product_Sale::class, 'product_id');
    }

    public function getImageAttribute()
    {
        return $this->getImagesAttribute();
    }

    public function getImagesAttribute()
    {
        return null;
        return $this->attachmentsGroup('product');
    }

    // Sanitizer
    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if (is_string($value)) {
            $this->attributes[$key] = trim(strtoupper($value));
        }
    }

    public function scopeActiveStandard($query)
    {
        return $query->where([
            ['is_active', true],
            ['type', 'standard']
        ]);
    }

    public function scopeActiveFeatured($query)
    {
        return $query->where([
            ['is_active', true],
            ['featured', 1]
        ]);
    }

    public function isActive()
    {
        return !empty($this->is_active);
    }

    public function scopeActive($query)
    {
        $query->where('products.is_active', 1);
    }

    public function scopeNotActive($query)
    {
        $query->where('products.is_active', 0);
    }
}
