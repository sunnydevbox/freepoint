<?php

namespace App\Models;

class Transfer extends BaseModel
{
    /** Startr of Caching Settings */

    public $cacheTags = ['transfers'];
    public $cachePrefix = 'transfers_';
    /** End of Caching Settings */

    protected $fillable = [
        "reference_no",
        "user_id",
        "status",
        "from_warehouse_id",
        "from_warehouse_bin_id",
        "to_warehouse_id",
        "to_warehouse_bin_id",
        "item",
        "total_qty",
        "total_tax",
        "total_cost",
        "shipping_cost",
        "grand_total",
        "document",
        "note",
    ];

    public function fromWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'from_warehouse_id');
    }

    public function toWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'to_warehouse_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function items()
    {
        return $this->hasMany(ProductTransfer::class, 'transfer_id');
    }
}
