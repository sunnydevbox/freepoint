<?php
namespace App\Models\Traits;

use App\Models\Warehouse;

trait UserWarehouseAccessTrait
{
    /**
     * Relationship
     */
    public function warehouses()
    {
        return $this->belongsToMany(
            Warehouse::class,
            'user_warehouse_accesses',
            'user_id',
            'warehouse_id'
        );
    }

    public function getWarehouseIds()
    {
        dd($this);
    }

    public function hasAccessToWarehouse($warehouse)
    {
        dd($this);
    }
}