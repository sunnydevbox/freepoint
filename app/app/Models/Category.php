<?php

namespace App\Models;

use App\Traits\ActivityLoggerTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Bnb\Laravel\Attachments\HasAttachment;

class Category extends BaseModel
{
    use HasFactory, HasAttachment, ActivityLoggerTrait;

    /** Startr of Caching Settings */
    
    public $cacheTags = ['billers'];
    public $cachePrefix = 'billers_';
    /** End of Caching Settings */

    protected $fillable = [
        'name',
        'parent_id',
        'is_active'
    ];

    public function product()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    public function getImageAttribute()
    {
        return $this->attachments()->first();
    }

    public function scopeStatusActive($query)
    {
        $query->where('is_active', true);
    }
    
    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
