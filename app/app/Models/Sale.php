<?php

namespace App\Models;

use App\Enums\PaymentStatusEnum;
use App\Enums\SaleStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */
    public $cacheTags = ['sales'];
    public $cachePrefix = 'sales_';
    /** End of Caching Settings */

    protected $fillable = [
        'reference_no',
        'receipt_no',
        'user_id',
        'cash_register_id',
        'customer_id',
        'warehouse_id',
        'biller_id',
        'item',
        'total_qty',
        'total_discount',
        'total_tax',
        'total_price',
        'order_tax_rate',
        'order_tax',
        'order_discount_type',
        'order_discount_value',
        'order_discount',
        'coupon_id',
        'coupon_discount',
        'shipping_cost',
        'grand_total',
        'sale_status',
        'payment_status',
        'paid_amount',
        'document',
        'sale_note',
        'staff_note',
        'created_at',
    ];

    protected $appends = [
        'due',
    ];

    protected $casts = [
        // 'sale_status' => SaleStatusEnum::class . ':nullable',
        'payment_status' => PaymentStatusEnum::class . ':nullable',
    ];

    public function biller()
    {
        return $this->belongsTo(\App\Models\Biller::class);
    }

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Warehouse::class);
    }

    /**
     * @deprecated
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function items()
    {
        return $this->hasMany(Product_Sale::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'sale_id');
    }

    public function getDueAttribute()
    {
        return $this->grand_total /* - $this->returned_amount */ - $this->paid_amount;
    }

    public function setReceiptNoAttribute($value)
    {
        $this->attributes['receipt_no'] = trim($value);
    }

    public function setReferenceNoAttribute($value)
    {
        $this->attributes['reference_no'] = trim($value);
    }
}
