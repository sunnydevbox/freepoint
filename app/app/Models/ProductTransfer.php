<?php

namespace App\Models;

class ProductTransfer extends BaseModel
{
    protected $table = 'product_transfer';
 
    /** Startr of Caching Settings */
    
    public $cacheTags = ['productsTransfers'];
    public $cachePrefix = 'productsTransfers_';
    /** End of Caching Settings */
 
    protected $fillable = [
        "transfer_id", "product_id", "product_batch_id", "variant_id", "imei_number", "qty", "purchase_unit_id", "net_unit_cost", "tax_rate", "tax", "total"
    ];

    public function transfer()
    {
        return $this->belongsTo(Transfer::class, 'transfer_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'purchase_unit_id');
    }

    public function variant()
    {
        // TODO
    }

    public function productBatch()
    {
        // TODO
    }
}
