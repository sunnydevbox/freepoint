<?php

namespace App\Models;

class Product_Warehouse extends BaseModel
{
    protected $table = 'product_warehouse';

    /** Startr of Caching Settings */

    public $cacheTags = ['productWarehouses'];
    public $cachePrefix = 'productWarehouses_';
    /** End of Caching Settings */

    protected $fillable = [
        "product_id",
        "product_batch_id",
        "variant_id",
        "imei_number",
        "warehouse_id",
        "warehouse_bin_id",
        "qty",
        "price",
        'sale_item_id',
        'po_item_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @deprecated
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function purchaseItem()
    {
        return $this->belongsTo(ProductPurchase::class, 'po_item_id');
    }

    public function saleItem()
    {
        return $this->belongsTo(Product_Sale::class, 'sale_item_id');
    }

    public function bin()
    {
        return $this->belongsTo(WarehouseBin::class, 'warehouse_bin_id');
    }

    public function scopeFindProductWithVariant($query, $product_id, $variant_id, $warehouse_id)
    {
        return $query->where([
            ['product_id', $product_id],
            ['variant_id', $variant_id],
            ['warehouse_id', $warehouse_id]
        ]);
    }

    public function scopeFindProductWithoutVariant($query, $product_id, $warehouse_id)
    {
        return $query->where([
            ['product_id', $product_id],
            ['warehouse_id', $warehouse_id]
        ]);
    }

    // public function newQuery($excludeDeleted = true) {
    //     return parent::newQuery($excludeDeleted)
    //         ->where(status, '=', 1);
    // }
    // public function newQuery()
    // {
    //     return parent::newQuery()
    //         // This ensures that the entry is not of the SALE ORDER
    //         ->whereNull('sale_item_id')
    //         ;
    // }
}
