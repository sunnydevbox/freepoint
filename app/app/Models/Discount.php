<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Discount extends BaseModel
{
    use HasFactory;

    /** Startr of Caching Settings */
    
    public $cacheTags = ['discounts'];
    public $cachePrefix = 'discounts_';
    /** End of Caching Settings */

    protected $fillable = ['name', 'applicable_for', 'product_list', 'valid_from', 'valid_till', 'type', 'value', 'minimum_qty', 'maximum_qty', 'days', 'is_active'];

    public function discountPlans()
    {
        return $this->belongsToMany(DiscountPlan::class, 'discount_plan_discounts');
    }
}
