<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class CustomerGroup extends BaseModel
{
    /** Startr of Caching Settings */
    
    public $cacheTags = ['customer_groups'];
    public $cachePrefix = 'customer_groups_';
    /** End of Caching Settings */

    protected $fillable = [

        "name", "percentage", "is_active"
    ];

    public function scopeActive(Builder $query): void
    {
        $query->where('is_active', 1);
    }
}
