<?php

use App\Enums\PaymentStatusEnum;
use App\Enums\PurchaseStatusEnum;
use App\Models\GeneralSetting;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Database\Eloquent\Builder;

/**
 * The function will check the product stock count if it is below threshold
 * and return a bootstrap class for the status.
 */
if (!function_exists('productThresholdClassHightlighter')) {

    function productThresholdClassHightlighter(int $currentQty, int $alertQty): string
    {
        $thresholdLimit = 200;

        return match (true) {
            ($currentQty > ($alertQty + $thresholdLimit)) => 'text-success',
            ($currentQty < $alertQty) => 'text-danger',
            ($currentQty >= $alertQty && $currentQty <= ($alertQty + $thresholdLimit)) => 'text-warning',
            default => 'text-warning'
        };
    }

}

if (!function_exists('extractProductFieldValue')) {

    function extractProductFieldValue($productObject, string $key, callable $callback = null)
    {
        if (
            empty($productObject) ||
            !$productObject instanceof Product ||
            empty($key) ||
            !isset($productObject->{$key})
        ) {
            return null;
        }

        $value = $productObject->{$key};

        if (!empty($callback)) {
            $value = $callback($value);
        }

        return $value;
    }

}

if (!function_exists('dateFormat')) {

    function dateFormat($timestamp, $format = 'M. d, Y h:i A')
    {
        return $timestamp->format($format);
    }

}


if (!function_exists('getCurrentUserPermissions')) {

    function getCurrentUserPermissions()
    {
        if (auth()->check()) {
            return auth()->user()->getPermissionsViaRoles()->pluck('name')->toArray();
        }
        return [];
    }

}

if (!function_exists('purchaseStatusFormatter')) {

    function purchaseStatusFormatter($statusId)
    {
        try {
            $statusEnum = PurchaseStatusEnum::from($statusId);

            switch ($statusId) {
                case 1:
                    $classType = 'success';
                    break;
                case 2:
                    $classType = 'success';
                    break;
                case 3:
                    $classType = 'danger';
                    break;

                default:
                    $classType = 'danger';
                    break;
            }
            return [
                'label' => $statusEnum->label,
                'id' => $statusEnum->value,
                'class_type' => $classType,
            ];
        } catch (Exception $e) {
            return;
        }
    }

}

if (!function_exists('paymentStatusFormatter')) {

    function paymentStatusFormatter($statusId): array
    {
        try {
            $statusEnum = (!$statusId instanceof PaymentStatusEnum)
                ? PaymentStatusEnum::from($statusId)
                : $statusId;

            switch ($statusEnum->value) {
                case 1:
                    $classType = 'danger';
                    break;
                default:
                case 2:
                    $classType = 'success';
                    break;
            }

            return [
                'label' => $statusEnum->label,
                'id' => $statusEnum->value,
                'class_type' => $classType,
            ];
        } catch (Exception $e) {
            throw $e;
        }
    }

}

if (!function_exists('formatMoney')) {

    function formatMoney($amount, $includeCurrency = false)
    {
        $newAmount = number_format(
            $amount,
            2,
            '.',
            ','
        );

        if ($includeCurrency) {
            // TODO: Make this dynamic
            $currency = 'Php';
            return "{$currency} {$newAmount}";
        }

        return $newAmount;
    }

}


if (!function_exists('settings')) {

    function settings($key = null, $default = null)
    {
        return GeneralSetting::first()->toArray();
    }

}

if (!function_exists('hasJoinTable')) {

    function hasJoinTable(Builder $builder, string $tableName): bool
    {
        return collect($builder->getQuery()->joins)
            ->contains(function ($v, $k) use ($tableName) {
                if ($v->table == $tableName) {
                    return true;
                }
            });
    }

}

function canEditPurchaseOrderItem(Purchase $purchaseOrder)
{
    if (
        $purchaseOrder->status != PurchaseStatusEnum::received()->value
        || ($purchaseOrder->status == PurchaseStatusEnum::received()->value && auth()->user()->id != 70) // TODO: remove this because this is only temporary for this user
        || auth()->user()->hasRole('business-manager')
    ) {
        return true;
    }

    return false;
}

