<?php

namespace App\Rules;

use App\Models\Sale;
use Illuminate\Contracts\Validation\Rule;

class UniqueOfficialReceiptPerWarehouse implements Rule
{
    protected $warehouseId = null;

    protected $sale = null;

    protected $idToIgnore = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($warehouseId, $idToIgnore = null)
    {
        $this->warehouseId = $warehouseId;
        $this->idToIgnore = $idToIgnore;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if (empty($this->warehouseId)) {
            return false;
        }

        $value = trim($value);

        $query = Sale::with(['warehouse'])->where('warehouse_id', $this->warehouseId)->where('receipt_no', $value);

        if (!empty($this->idToIgnore)) {
            $query = $query->where('id', '!=', $this->idToIgnore);
        }
        
        $this->sale = $query ->first();

        return empty($this->sale);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (empty($this->warehouseId)) {
            return 'Please select a warehouse/location';
        }

        return 'This receipt # already exists for ' . $this->sale->warehouse->name;
    }
}
