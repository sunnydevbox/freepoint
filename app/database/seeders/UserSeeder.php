<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Roles;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Roles::all();

        User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role_id' => 1
        ])
        ->assignRole('admin');

        Roles::all()
            ->each(function($role) {
                User::factory(10)
                    ->create([
                        'role_id' => $role->id,
                    ])
                    ->each(function($user) {
                        $role = Roles::find($user->role_id);

                        $user->update([
                            'email' => $role->name .'-'. $user->email,
                        ]);
                    });
            });

       
           
    }
}
