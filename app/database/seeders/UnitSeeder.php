<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create([
            'unit_code' => 'pc', 
            'unit_name' => 'Piece',
            'base_unit' => null,
            'operator' => '*',
            'operation_value' => 1,
            'is_active' => 1
        ]);

        Unit::create([
            'unit_code' => 'dozen', 
            'unit_name' => 'dozen box',
            'base_unit' => 1,
            'operator' => '*',
            'operation_value' => 12,
            'is_active' => 1
        ]);

        Unit::create([
            'unit_code' => 'cartoon', 
            'unit_name' => 'cartoon box',
            'base_unit' => 1,
            'operator' => '*',
            'operation_value' => 24,
            'is_active' => 1
        ]);

        Unit::create([
            'unit_code' => 'm', 
            'unit_name' => 'meter',
            'base_unit' => null,
            'operator' => '*',
            'operation_value' => 1,
            'is_active' => 1
        ]);

        $kg = Unit::create([
            'unit_code' => 'kg', 
            'unit_name' => 'kilogram',
            'base_unit' => null,
            'operator' => '*',
            'operation_value' => 1,
            'is_active' => 1
        ]);

        Unit::create([
            'unit_code' => 'g', 
            'unit_name' => 'gram',
            'base_unit' => $kg->id,
            'operator' => '/',
            'operation_value' => 1000,
            'is_active' => 1
        ]);


        Unit::create([
            'unit_code' => 'inch', 
            'unit_name' => 'inch',
            'base_unit' => null,
            'operator' => '*',
            'operation_value' => 1,
            'is_active' => 1
        ]);


    }
}
