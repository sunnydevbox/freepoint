<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'name' => 'admin',
            'description' => '',
            'is_active' => true,
            'guard_name' => 'web',
        ]);

        Roles::create([
            'name' => 'owner',
            'description' => '',
            'is_active' => true,
            'guard_name' => 'web',
        ]);

        Roles::create([
            'name' => 'staff',
            'description' => '',
            'is_active' => true,
            'guard_name' => 'web',
        ]);

        Roles::create([
            'name' => 'customer',
            'description' => '',
            'is_active' => true,
            'guard_name' => 'web',
        ]);

        Roles::create([
            'name' => 'Inventory Manager',
            'description' => '',
            'is_active' => true,
            'guard_name' => 'web',
        ]);
    }
}
