<?php

namespace Database\Seeders;

use App\Models\GeneralSetting;
use Illuminate\Database\Seeder;

class GeneralSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GeneralSetting::create([
            'site_title' => 'OfficeTul',
            'site_logo' => '',
            'is_rtl' => 0,
            'currency' => 1,
            'currency_position' => 2,
            'staff_access' => 'own',
            'date_format' => 'Y-m-d',
            'theme' => 'default.css',
            // 'developed_by' => '',
            'invoice_format' => 'standard',
            // 'decimal' => 2,
            'state' => 1,

        ]);

        // CREATE TABLE `general_settings` (
        //     `site_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        //     `is_rtl` tinyint(1) DEFAULT NULL,
        //     `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     `staff_access` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     `date_format` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     `developed_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        //     `invoice_format` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        //     `state` int(11) DEFAULT NULL,
        //     `theme` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     `created_at` timestamp NULL DEFAULT NULL,
        //     `updated_at` timestamp NULL DEFAULT NULL,
        //     `currency_position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
        //     PRIMARY KEY (`id`)
        //   ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
          
        //   INSERT INTO general_settings VALUES("SalePro","20210530062516.png","0","1","own","d/m/Y","LionCoders","standard","1","default.css","2018-07-06 12:13:11","2021-11-14 13:22:32","prefix");
          
    }
}
