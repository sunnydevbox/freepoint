<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = [
            'Chassis',
            'Motorcycle Engine',
            'Motorcycle Transmissions',
            'Final Drive',
            'Wheels and Tires',
            'Body Panels',
            'Mudguards',
            'Lights',
            'Handlebars',
            'Footpegs',
            'Motorcycle Brakes',
            'Exhausts'
        ];

        $faker = app(Faker::class);
        $service = app(CategoryService::class);

        collect($cats)->each(function($cat) use ($service) {
            Category::factory(1)
                ->create([
                    'name' => $cat,
                ])
                ->each(function($category) use ($service) {
                    $service->attachImage($category->id, base_path('public/product/files/1.jpg'));
                });
        });
    }
}
