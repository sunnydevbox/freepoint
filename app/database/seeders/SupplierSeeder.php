<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*

                AG													
        AGP													
        G.C		8367-3537/8367-3440				27 Makaturing St., Manresa, Quezon City							
        Good Year Autocare													
        HPI	HPI	(032) 233-0944 233-0939	(032) 412-9387	sp_order@hondaph.com	www.hondaph.com	Salinas Drive, Lahug		Cebu	Cebu	Philippines	6000		
        Infiniteserv Int'l Corp.													
        IZUMI													
        Kawasaki	Cheryl Eludo	0998-9647906	(032) 3446082	Cheryl_E@kmp.com.ph		B1 BF Centerpoint Bldg. Hernan Cortes St. Brgy.	Subangdaku	Mandaue	Cebu	Philippines	6014		Commercial
        Leo Tire	Leo Tire Manufacturing Corp	(02) 3434692				1186 APC Bldg., Quezon Ave		Quezon	M.M.		1113		
        REPLACEMENT													
        RSV													
        SGC	Philippine SGC Corporation	(02) 332-1332 364-1480	(02) 364-3865			159 9th street, 9th Avenue Grace Park		Caloocan 	M.M.	Philippines			
        SMDI-TALIBON													
        SPH	SPH	(02) 462-5000 (049) 502-1458 LOC 216	(02) 462-5040 -41	allan.llave@suuki.com.ph	www.suzuki.com.ph	126 Progress Avenue, Camelray Industrial Park I	Canlubang	Calamba	Laguna	Philippines	4028		
        TRI-MARK DISTRIBUTORS, INC.	Lemeuel Gilamon	0(9217349667)  (09262303781) (09178943135	(372-5853;374-4743)	Trimark99@yahoo.com		99 Roosevelt Ave., Sta, Cruz,		Quezon City	Metro Manila	Philippines	1105		Commercial
        Yamaha	Clan Yongco	+639175550266	(043) 981-2459	clan_yongco@yamaha-motor.com.ph	www.yamaha-motor.com.ph	8th floor FPN Epic Center, AS Foruna Street, Bakilid		Mandaue	Cebu	Philippines	6014		Commercial
        YSS													
        */
        $suppliers = [
            [
                'name' => 'AG',
                'company_name' => 'AG',
            ],
            [
                'name' => 'AGP',
                'company_name' => 'AGP',
            ],
            [
                'name' => 'Good Year Autocare',
                'company_name' => 'Good Year Autocare',
            ],
            [
                'name' => 'Infiniteserv Intl Corp.',
                'company_name' => 'Infiniteserv Intl Corp.',
            ],
            [
                'name' => 'IZUMI',
                'company_name' => 'IZUMI',
            ],
            [
                'name' => 'SMDI-TALIBON',
                'company_name' => 'SMDI-TALIBON',
            ],
            [
                'name' => 'YSS',
                'company_name' => 'YSS',
            ],
            [
                'name' => 'G.C.',
                'company_name' => 'G.C.',
                'phone_number' => '8367-3537/8367-3440',
                'address' => '27 Makaturing St., Manresa',
                'city' => 'Quezon City', 
                'state' => 'Manila',
            ],
            [
                'name'  => 'HPI',
                'company_name' => 'HPI',
                'phone_number' => '(032) 233-0944 / 233-0939 / 412-9387',
                'email' => 'sp_order@hondaph.com',
                'website' => 'www.hondaph.com',
                'address' => 'Salinas Drive, Lahug',
                'city' => 'Cebu City', 
                'state' => 'Cebu',
                'postal_code' => '6000',
            ],
            [
                'name'  => 'Cheryl Eludo',
                'company_name' => 'Kawasaki',
                'phone_number' => '(032) 3446082 / 0998-9647906',
                'email' => 'cheryl_e@kmp.com.ph',
                'address' => 'B1 BF Centerpoint Bldg. Hernan Cortes St. Brgy. Subangdaku',
                'city' => 'Mandaue City', 
                'state' => 'Cebu',
                'postal_code' => '6014',
            ],
            [
                'name'  => 'Leo Tire',
                'company_name' => 'Leo Tire Manufacturing Corp',
                'phone_number' => '(02) 3434692',
                'address' => '1186 APC Bldg., Quezon Ave',
                'city' => 'Quezon City', 
                'state' => 'Manila',
                'postal_code' => '1113',
            ],
            [
                'name'  => 'SGC',
                'company_name' => 'Philippine SGC Corporation',
                'phone_number' => '(02) 332-1332 / 364-1480	/ 364-3865',
                'address' => '159 9th street, 9th Avenue Grace Park',
                'city' => 'Caloocan  City', 
                'state' => 'Manila',
            ],
            [
                'name'  => 'SPH',
                'company_name' => 'SPH',
                'phone_number' => '(02) 462-5000 / 462-5040 -41 / (049) 502-1458 Loc 216',
                'email' => 'allan.llave@suzuki.com.ph',
                'website' => 'https://www1.suzuki.com.ph/',
                'address' => '126 Progress Avenue, Camelray Industrial Park I, Canlubang',
                'city' => 'Calamba City', 
                'state' => 'Laguna',
                'postal_code' => '4028',
            ],
            [
                'name'  => 'Lemeuel Gilamon',
                'company_name' => 'TRI-MARK DISTRIBUTORS, INC.',
                'phone_number' => '09217349667 / 09262303781 / 09178943135 / (02) 372-5853 / 374-4743',
                'email' => 'trimark99@yahoo.com',
                'address' => '99 Roosevelt Ave., Sta, Cruz,',
                'city' => 'Quezon City', 
                'state' => 'Manila',
                'postal_code' => '1105',
            ],
            [
                'name'  => 'Clan Yongco',
                'company_name' => 'Yamaha',
                'phone_number' => '+639175550266 / (043) 981-2459',
                'email' => 'clan_yongco@yamaha-motor.com.ph',
                'website' => 'https://www.yamaha-motor.com.ph/',
                'address' => '8th floor FPN Epic Center, AS Foruna Street, Bakilid',
                'city' => 'Mandaue City', 
                'state' => 'Cebu',
                'postal_code' => '6014',
            ],
        ];

        collect($suppliers)->each(function($supplier) {
            $supplier['is_active'] = 1;
            $supplier['country'] = 'PH';
            Supplier::create($supplier);
        });

    }
}
