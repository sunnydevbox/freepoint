<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use App\Services\ProductService;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = Brand::where('is_active', 1)->pluck('id');
        $categories = Category::where('is_active', 1)->pluck('id');
        $units = Unit::where('is_active', 1)->pluck('id');

        $productService = app(ProductService::class);

        Product::factory(100)->create([
            'brand_id' => $brands[rand(0, count($brands) - 1)],
            'category_id' => $categories[rand(0, count($categories) - 1)],
            'unit_id' =>  $units[rand(0, count($units) - 1)],
            'purchase_unit_id' => $units[rand(0, count($units) - 1)],
            'sale_unit_id' => $units[rand(0, count($units) - 1)],
        ])
        ->each(function($product) use ($productService) {
            $productService->attachImage($product->id, base_path('public/product/files/1.jpg'));
        });
    }
}
