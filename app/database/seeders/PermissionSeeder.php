<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'products-edit',
            'products-delete',
            'products-add',
            'products-index',
            'purchases-index',
            'purchases-add',
            'purchases-edit',
            'purchases-delete',
            'sales-index',
            'sales-add',
            'sales-edit',
            'sales-delete',
            'quotes-index',
            'quotes-add',
            'quotes-edit',
            'quotes-delete',
            'transfers-index',
            'transfers-add',
            'transfers-edit',
            'transfers-delete',
            'returns-index',
            'returns-add',
            'returns-edit',
            'returns-delete',
            'customers-index',
            'customers-add',
            'customers-edit',
            'customers-delete',
            'suppliers-index',
            'suppliers-add',
            'suppliers-edit',
            'suppliers-delete',
            'product-report',
            'purchase-report',
            'sale-report',
            'customer-report',
            'due-report',
            'users-index',
            'users-add',
            'users-edit',
            'users-delete',
            'profit-loss',
            'best-seller',
            'daily-sale',
            'monthly-sale',
            'daily-purchase',
            'monthly-purchase',
            'payment-report',
            'warehouse-stock-report',
            'product-qty-alert',
            'supplier-report',
            'supplier-due-report',
            'expenses-index',
            'expenses-add',
            'expenses-edit',
            'expenses-delete',
            'general_setting',
            'mail_setting',
            'pos_setting',
            'hrm_setting',
            'purchase-return-index',
            'purchase-return-add',
            'purchase-return-edit',
            'purchase-return-delete',
            'account-index',
            'balance-sheet',
            'account-statement',
            'department',
            'attendance',
            'payroll',
            'employees-index',
            'employees-add',
            'employees-edit',
            'employees-delete',
            'user-report',
            'stock_count',
            'adjustment',
            'sms_setting',
            'create_sms',
            'print_barcode',
            'empty_database',
            'customer_group',
            'unit',
            'tax',
            'gift_card',
            'coupon',
            'holiday',
            'warehouse-report',
            'warehouse',
            'brand',
            'billers-index',
            'billers-add',
            'billers-edit',
            'billers-delete',
            'money-transfer',
            'category',
            'delivery',
            'send_notification',
            'today_sale',
            'today_profit',
            'currency',
            'backup_database',
            'reward_point_setting',
            'revenue_profit_summary',
            'cash_flow',
            'monthly_summary',
            'yearly_report',
            'discount_plan',
            'discount',
            'product-expiry-report',
            'purchase-payment-index',
            'purchase-payment-add',
            'purchase-payment-edit',
            'purchase-payment-delete',
            'sale-payment-index',
            'sale-payment-add',
            'sale-payment-edit',
            'sale-payment-delete',
            'all_notification',
            'sale-report-chart',
            'dso-report',
            'product_history',
        ];
        

        collect($permissions)->each(function($permission) {
            $p = Permission::firstOrCreate([
                'name' => $permission,
                'guard_name' => 'web'
            ]);

            Role::findByName('admin')->givePermissionTo($p);
            
        });

    }
}
