<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSuppliersAddWebsite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('suppliers', function($table) {
            $table->string('website')->nullable();
            $table->string('email')->nullable()->change();

            $table->string('email')->nullable()->change();
            $table->string('phone_number')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('city')->nullable()->change();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function($table) {
            $table->dropColumn('website');

            $table->string('email')->change();
            $table->string('phone_number')->change();
            $table->string('address')->change();
            $table->string('city')->change();
        });
    }
}
