<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBinToAndIsBinTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function ($table) {
            $table->unsignedBigInteger('from_warehouse_bin_id')->nullable()->after('from_warehouse_id');
            $table->foreign('from_warehouse_bin_id')->references('id')->on('warehouse_bins')->onDelete('cascade');
            $table->unsignedBigInteger('to_warehouse_bin_id')->nullable()->after('to_warehouse_id');
            $table->foreign('to_warehouse_bin_id')->references('id')->on('warehouse_bins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function ($table) {
            $table->dropForeign(['from_warehouse_bin_id']); // Drop foreign key for 'from'
        $table->dropForeign(['to_warehouse_bin_id']);   // Drop foreign key for 'to'
        $table->dropColumn(['from_warehouse_bin_id', 'to_warehouse_bin_id']); // Drop the columns
        });
    }
}
