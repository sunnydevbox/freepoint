<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPoItemIdInProductWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('product_warehouse', function (Blueprint $table) {
             $table->integer('po_item_id')->unsigned()->nullable()->after('warehouse_bin_id');
             $table->foreign('po_item_id')
                 ->references('id')
                 ->on('product_purchases')
                 ->onDelete('cascade');
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('product_warehouse', function (Blueprint $table) {
             $table->dropForeign(['po_item_id']);
             $table->dropColumn('po_item_id');
         });
     }
}
