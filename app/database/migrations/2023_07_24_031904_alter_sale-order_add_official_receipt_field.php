<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaleOrderAddOfficialReceiptField extends Migration
{
    public function up()
    {
        Schema::table('sales',  function($table) {
            $table->string('receipt_no')->nullable()->after('reference_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales',  function($table) {
            $table->dropColumn('receipt_no');
        });
    }   
}
