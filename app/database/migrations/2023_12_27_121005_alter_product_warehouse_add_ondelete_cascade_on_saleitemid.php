<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductWarehouseAddOndeleteCascadeOnSaleitemid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->dropForeign('product_warehouse_sale_item_id_foreign');
        });

        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->foreign('sale_item_id')
                ->references('id')
                ->on('product_sales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
