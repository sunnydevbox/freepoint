<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdjustmentsAddWarehouseFK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adjustments', function ($table) {
            $table->integer('warehouse_id')->unsigned()->change();
            $table->integer('item')->comment('number of items in this adjustment record')->change();
            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses');
        });


        Schema::table('product_adjustments', function ($table) {
            $table->integer('adjustment_id')->unsigned()->change();
            $table->integer('product_id')->unsigned()->change();
            $table->foreign('adjustment_id')
                ->references('id')
                ->on('adjustments');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adjustments', function ($table) {
            $table->dropForeign('adjustments_warehouse_id_foreign');
        });

        Schema::table('adjustments', function ($table) {
            $table->integer('warehouse_id')->change();
            $table->integer('item')->change();
        });




        Schema::table('product_adjustments', function ($table) {
            $table->dropForeign('product_adjustments_adjustment_id_foreign');
            $table->dropForeign('product_adjustments_product_id_foreign');
        });

        Schema::table('product_adjustments', function ($table) {
            $table->integer('adjustment_id')->change();
            $table->integer('product_id')->change();
        });
    }
}
