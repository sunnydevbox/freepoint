<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WarehouseBins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_bins', function($table) {
            $table->id();
            $table->integer('warehouse_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('aisle');
            $table->string('rack');
            $table->string('shelf');
            $table->string('bin');

            $table->unique(['warehouse_id', 'aisle', 'rack', 'shelf', 'bin']);
            $table->unique(['name', 'warehouse_id']);

            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_bins');
    }
}
