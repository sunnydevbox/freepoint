<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWarehouseIdOnBillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billers', function (Blueprint $table) {
            $table->unsignedInteger('warehouse_id')->after('id')->nullable();
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
            $table->string('email')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('billers', function (Blueprint $table) {
            $table->dropForeign(['warehouse_id']);
            $table->dropColumn('warehouse_id');
            $table->string('email')->nullable(false)->change();
        });
    }
}
