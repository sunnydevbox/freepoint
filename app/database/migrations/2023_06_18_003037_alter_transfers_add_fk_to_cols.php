<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTransfersAddFkToCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function ($table) {
            $table->integer('user_id')->unsigned()->change();
            $table->integer('from_warehouse_id')->unsigned()->change();
            $table->integer('to_warehouse_id')->unsigned()->change();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('from_warehouse_id')
                ->references('id')
                ->on('warehouses');

            $table->foreign('to_warehouse_id')
                ->references('id')
                ->on('warehouses');
        });


        Schema::table('product_transfer', function ($table) {
            $table->integer('product_id')->unsigned()->change();
            $table->integer('transfer_id')->unsigned()->change();

            $table->foreign('product_id')
                ->references('id')
                ->on('products');

            $table->foreign('transfer_id')
                ->references('id')
                ->on('transfers');
        });


        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function ($table) {
            $table->dropForeign('transfers_user_id_foreign');
            $table->dropForeign('transfers_from_warehouse_id_foreign');
            $table->dropForeign('transfers_to_warehouse_id_foreign');
        });

        Schema::table('transfers', function ($table) {
            $table->integer('user_id')->change();
            $table->integer('from_warehouse_id')->change();
            $table->integer('to_warehouse_id')->change();
        });


        Schema::table('product_transfer', function ($table) {
            $table->dropForeign('product_transfer_product_id_foreign');
            $table->dropForeign('product_transfer_transfer_id_foreign');
        });

        Schema::table('product_transfer', function ($table) {
            $table->integer('product_id')->change();
            $table->integer('transfer_id')->change();
        });
    }
}
