<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSalesSetDefaultValuesToColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->integer('item')->default(0)->change();
            $table->decimal('total_qty', 11, 2)->default(0)->change();
            $table->decimal('total_discount', 11, 2)->default(0)->change();
            $table->decimal('total_tax', 11, 2)->default(0)->change();
            $table->decimal('total_price', 11, 2)->default(0)->change();
            $table->decimal('grand_total', 11, 2)->default(0)->change();
            $table->decimal('total_tax', 11, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->integer('item')->change();
            $table->double('total_qty')->change();
            $table->double('total_discount')->change();
            $table->double('total_tax')->change();
            $table->double('total_price')->change();
            $table->double('grand_total')->change();
            $table->double('total_tax')->change();
        });
    }
}
