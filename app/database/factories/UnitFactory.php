<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UnitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'unit_code' => $this->faker->name(),
            'unit_name' => $this->faker->name(),
            'operator' => '*',
            'operation_value' => rand(5, 50),    
        ];
    }
}
