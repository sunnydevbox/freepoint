<?php

namespace Database\Factories;

use App\Models\Purchase;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Factories\Factory;

class PurchaseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference_no' => $this->faker->randomNumber,
            'item' => 0,
            // 'user_id',
            // 'warehouse_id',
            // 'supplier_id',
            // 'item',
            'total_qty' => 0,
            'total_discount' => 0,
            'total_tax' => 0,
            'total_cost' => 0,
            'order_tax_rate' => 0,
            'order_tax' => 0,
            'order_discount' => 0,
            // 'shipping_cost',
            'grand_total' => 0,
            'paid_amount' => 0,
            'status' => 1,
            // 'payment_status',
            // 'document',
            // 'note',
            // 'created_at',
            
            'invoice_number' => $this->faker->randomNumber,
        ];
    }

    public function configure()
    {
        return $this->afterMaking(function (Purchase $purchase) {
            $purchase->warehouse()->associate(Warehouse::factory()->create());
            $purchase->author()->associate(User::factory()->create());
        });
    }
}
