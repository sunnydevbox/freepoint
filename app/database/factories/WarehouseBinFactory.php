<?php

namespace Database\Factories;

use App\Models\Warehouse;
use App\Models\WarehouseBin;
use Illuminate\Database\Eloquent\Factories\Factory;

class WarehouseBinFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'aisle' => $this->faker->word(),
            'rack' => $this->faker->word(),
            'shelf' => $this->faker->word(),
            'bin' => $this->faker->word(),
        ];
    }

    public function configure()
    {
        return $this
            ->afterMaking(function (WarehouseBin $warehouseBin) {
                $warehouseBin->warehouse()->associate(Warehouse::factory()->create());
            });
    }
}
