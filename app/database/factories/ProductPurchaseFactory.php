<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\WarehouseBin;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductPurchaseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'qty' => 0,
            'recieved' => 0,
            'discount' => 0,
            'tax_rate' => 0,
            'tax' => 0,
        ];
    }

    public function configure()
    {
        return $this
            ->afterMaking(function (ProductPurchase $productPurchase) {
                $product = Product::factory()->create();
 
                $productPurchase->product()->associate($product);
                $productPurchase->unit()->associate($product->unit);    
                $productPurchase->net_unit_cost = $product->cost;                    
                $productPurchase->bin()->associate(WarehouseBin::factory()->create());

               

                // compute ->total
            });
    }
}
