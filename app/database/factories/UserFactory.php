<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            // 'email_verified_at' => now(),
            'password' => bcrypt('123qwe'),
            'remember_token' => Str::random(10),
            'phone' => '',
            'company_name' => $this->faker->name(),
            // 'role_id' => '',
            // 'biller_id' => '',
            // 'warehouse_id' => '',
            'is_active' => 1,
            'is_deleted' => false,
        ];
    }
    
    public function configure()
    {
        $role = Role::factory()->create();
        return $this
            ->afterMaking(function (User $user) use ($role) {
                $user->role_id = $role->id;
            })
            ->afterCreating(function (User $user) use ($role) {
                $user->roles()->sync($role);
            });
    }
}
