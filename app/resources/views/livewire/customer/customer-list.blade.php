<div wire:ignore>
    <select class="selectpicker form-control"
        data-live-search="true"
        id="customerId"
        name="customer_id"
        title="Select customer..."
        wire:model="customer_id">
        @foreach ($customers as $customer)
            <option {{ $customer->id == $customer_id ? 'selected' : '' }}
                value="{{ $customer->id }}">
                {{ $customer->name . ' (' . $customer->company_name . ')' }}
            </option>
        @endforeach
    </select>
</div>
