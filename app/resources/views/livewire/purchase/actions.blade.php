<div class="flex space-x-1 justify-around">
    <!--
        view,
        edit,
        view paymnt,
        add payment,
        delete
     -->
    <a
        href="javascript:void"
        onclick="Livewire.emit('openModal', 'purchase.modals.view-details', {{ json_encode(['purchaseId' => $id]) }})"
        class="p-1 text-black-500 hover:bg-black hover:text-white rounded .view"
        title="View"
    >
        <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-6 h-6"
        >
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
            />
            <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
            />
        </svg>

    </a>

    <a
        href="{{ route('purchases.edit', [$id]) }}"
        class="p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded"
        title="Edit"
    >
        <svg
            class=" w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"
            >
            </path>
        </svg>
    </a>
    <a
        href="javascript:void()"
        onclick="Livewire.emit(
                'openModal', 
                'purchase.modals.view-payments', 
                {{ json_encode(['purchase_id' => $id]) }})"
        class="add-payment p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded"
        title="View Payments"
    >
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            class="bi bi-binoculars-fill"
            viewBox="0 0 16 16"
        >
            <path
                d="M4.5 1A1.5 1.5 0 0 0 3 2.5V3h4v-.5A1.5 1.5 0 0 0 5.5 1h-1zM7 4v1h2V4h4v.882a.5.5 0 0 0 .276.447l.895.447A1.5 1.5 0 0 1 15 7.118V13H9v-1.5a.5.5 0 0 1 .146-.354l.854-.853V9.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v.793l.854.853A.5.5 0 0 1 7 11.5V13H1V7.118a1.5 1.5 0 0 1 .83-1.342l.894-.447A.5.5 0 0 0 3 4.882V4h4zM1 14v.5A1.5 1.5 0 0 0 2.5 16h3A1.5 1.5 0 0 0 7 14.5V14H1zm8 0v.5a1.5 1.5 0 0 0 1.5 1.5h3a1.5 1.5 0 0 0 1.5-1.5V14H9zm4-11H9v-.5A1.5 1.5 0 0 1 10.5 1h1A1.5 1.5 0 0 1 13 2.5V3z"
            />
        </svg>
    </a>

    @if (!empty($balance))
        <a
            href="javascript:void"
            onclick="Livewire.emit('openModal', 'purchase.modals.add-payment', {{ json_encode(['purchase_id' => $id]) }})"
            class="add-payment p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded"
            title="Edit"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                class="bi bi-cash w-5 h-5"
                viewBox="0 0 16 16"
            >
                <path d="M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
                <path
                    d="M0 4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4zm3 0a2 2 0 0 1-2 2v4a2 2 0 0 1 2 2h10a2 2 0 0 1 2-2V6a2 2 0 0 1-2-2H3z"
                />
            </svg>
        </a>
    @endif
    {{-- 
    <a href="{{ route('products.history', ['product_id' => $id]) }}" class="p-1 text-black-500 hover:bg-black hover:text-white rounded" title="History">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z" />
        </svg>
    </a> --}}


    @include('datatables::delete', [
        'value' => $label,
        'open' => false,
    ])
</div>


@push('scripts')
    <script></script>
@endpush
