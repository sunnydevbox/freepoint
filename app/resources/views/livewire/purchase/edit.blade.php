<div>
    <p class="italic">
        <small>{{ trans('file.The field labels marked with * are required input fields') }}.</small>
    </p>

    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>{{ trans('file.Update Purchase') }} <strong
                            class="text-uppercase">{{ $purchaseOrder->reference_no }}</strong></h4>
                </div>
                <div class="card-body">
                    <livewire:purchase.sections.form.basic :generalSettings="$generalSettings"
                        :purchaseOrder="$purchaseOrder"
                        :suppliers="$suppliers"
                        :warehouses="$warehouses" />
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Others </h4>
                </div>
                <div class="card-body">
                    <livewire:purchase.sections.form.extra :purchaseOrder="$purchaseOrder"
                        :taxes="$taxes" />
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Summary </h4>
                </div>
                <div class="card-body">
                    <livewire:purchase.sections.p-o-summary :purchaseOrder="$purchaseOrder" />
                </div>
            </div>
        </div>

        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <livewire:purchase.form.attach-product :purchaseOrder="$purchaseOrder" />
                </div>
            </div>
        </div>
    </div>
</div>
