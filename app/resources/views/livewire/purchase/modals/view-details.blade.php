<?php
$status = purchaseStatusFormatter($purchaseOrder->status);
$paymentStatus = paymentStatusFormatter($purchaseOrder->payment_status);
?>
<div class="container-fluid visible-print-block"
    id="purchase-details">
    <div class=" mt-3 pb-2 border-bottom">
        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-default btn-sm"
                    href="{{ route('purchase.print', $purchaseOrder->id) }}"
                    target="_blank"><i class="dripicons-print"></i> Print</a>
            </div>
            <div class="col-md-6 d-print-none">
                <button aria-label="Close"
                    class="close"
                    data-dismiss="modal"
                    id="close-btn"
                    type="button"
                    wire:click="$emit('closeModal')"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <div class="col-md-12">
                <h3 class="modal-title text-center container-fluid"
                    id="exampleModalLabel">Freepoint</h3>
            </div>
            <div class="col-md-12 text-center">
                <i style="font-size: 15px;">Purchase Details</i>
            </div>
        </div>
    </div>

    <div id="purchase-content">
        <div class="mb-5">
            <div class="d-block w-100">
                <strong>Date:</strong> {{ dateFormat($purchaseOrder->created_at) }}
            </div>
            <div class="d-block w-100">
                <strong>Reference: </strong>{{ strtoupper($purchaseOrder->reference_no) }}
            </div>
            <div class="d-block w-100">
                <strong>Purchase Status: </strong>
                <span class="badge badge-{{ $status['class_type'] }}">{{ $status['label'] }}</span>
            </div>
            <div class="d-block w-100">
                <strong>Payment Status: </strong>
                <span class="badge badge-{{ $paymentStatus['class_type'] }}">{{ $paymentStatus['label'] }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <strong>From:</strong>
            <br>
            {{ $purchaseOrder->warehouse->name }}
            <br>
            {{ $purchaseOrder->warehouse->phone }}
            <br>
            {{ $purchaseOrder->warehouse->address }}
        </div>
        <div class="col-md-6">
            <div class="float-right">
                <strong>To:</strong>
                <br>
                {{ $purchaseOrder->supplier->name }}
                <br>
                {{ $purchaseOrder->supplier->company_name }}
                <br>
                {{ $purchaseOrder->supplier->email }}
                <br>
                {{ $purchaseOrder->supplier->phone_number }}
                <br>
                {{ $purchaseOrder->supplier->address }}
                <br>
                {{ $purchaseOrder->supplier->city }}
            </div>
        </div>
    </div>

    <br>
    <table class="table table-bordered product-purchase-list">
        <thead>
            <tr>
                {{-- <th>#</th> --}}
                <th>Product</th>
                <th>Part Number</th>
                {{-- <th>Batch No</th> --}}
                <th>Qty</th>
                <th>Received</th>
                <th>Unit Cost</th>
                <th>Tax</th>
                <th>Discount</th>
                <th>SubTotal</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($purchaseOrder->items as $key => $item)
                <tr>
                    {{-- <th>{{ $item->id }}</th> --}}
                    <th>{{ $item->product->name }}</th>
                    <th>{{ $item->product->code }}</th>
                    {{-- <th>{{  $item->product->product_batch_id }}</th> --}}
                    <th>{{ $item->qty }}</th>
                    <th>{{ $item->recieved }}</th>
                    <th>{{ $item->net_unit_cost }}</th>
                    <th>{{ $item->tax_rate ?? 0.0 }}</th>
                    <th>{{ $item->discsount ?? 0.0 }}</th>
                    <th>{{ $item->total ?? 0.0 }}</th>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="modal-body"
        id="purchase-footer">
        <p>
            <strong>Note:</strong>
            {{ $purchaseOrder->note }}
        </p>
        <strong>Created By:</strong>
        <br>{{ $purchaseOrder->author->name }}
        <br>{{ $purchaseOrder->author->email }}
    </div>
</div>
