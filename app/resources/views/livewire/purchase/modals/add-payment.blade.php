<div id="add-payment">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">{{ trans('file.Add Payment') }} for </h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"
                    wire:click="$emit('closeModal')"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <div class="modal-body">
                Balance: {{ formatMoney($due, true) }}
                <hr />
                <form wire:submit.prevent="doAddPayment" wire:loading.attr="disabled">
                    <div class="row">
                        <input type="hidden" name="balance">
                        <div class="form-group col-md-6">
                            <label>{{ trans('file.Recieved Amount') }} *</label>
                            <input name="paying_amount" class="form-control numkey"
                                wire:model.debounce.500ms="formattedPayingAmount" wire:loading.attr="disabled" step="any" required>
                            <small class="form-text text-muted">Money handed over to cashier</small>
                            @error('paying_amount')
                                <span class="validation-message small text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label>{{ trans('file.Paying Amount') }} *</label>
                            <input id="amount" name="amount" class="form-control"
                                wire:model.debounce.500ms="formattedAmount" step="any"
                                wire:loading.attr="disabled"
                                required>
                            @error('amount')
                                <span class="validation-message small text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 mt-1 card">
                            <label>{{ trans('file.Change') }} : </label>
                            <p class="change ml-2">{{ formatMoney($change, true) }}</p>
                        </div>
                        <div class="form-group col-md-6 mt-1">
                            <label>Mode of Payment</label>
                            <select name="paid_by_id" class="form-control" wire:model.lazy="paid_by_id">
                                @foreach ($paymentMethods as $methodId => $methodName)
                                    <option value="{{ $methodId }}">{{ $methodName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group mt-2">
                        <div class="card-element" class="form-control">
                        </div>
                        <div class="card-errors" role="alert"></div>
                    </div>
                    <div class="form-group">
                        <label> {{ trans('file.Account') }}</label>

                        <select class="form-control" name="account_id" wire:model="account_id" required>
                            @foreach ($accounts as $account)
                                @if ($account->is_default)
                                    <option selected value="{{ $account->id }}">{{ $account->name }}
                                        [#{{ $account->account_no }}]
                                    </option>
                                @else
                                    <option value="{{ $account->id }}">{{ $account->name }}
                                        [{{ $account->account_no }}]
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        @error('account_id')
                            <span class="validation-message small text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ trans('file.Payment Note') }}</label>
                        <textarea rows="3" class="form-control" name="payment_note" wire:model.defer="payment_note"></textarea>
                    </div>

                    <input type="hidden" name="purchase_id">

                    <button type="submit" class="btn btn-primary">{{ trans('file.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

{{--
 * This script formats numeric input fields in real-time as the user types.
 * - As the user types, it removes any existing commas and non-numeric characters (except the decimal point).
 * - It automatically adds commas to the integer part of the number (e.g., "1000" becomes "1,000").
 * - The script handles both integer and decimal numbers and ensures that the value remains valid numeric input.
 * 
  --}}
<script>
    document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('input.numkey').forEach(function (input) {
        input.addEventListener('input', function (event) {
            let value = this.value.replace(/,/g, ''); // Remove existing commas
            if (!/^\d*\.?\d*$/.test(value)) {
                value = value.replace(/[^0-9.]/g, ''); // Remove non-numeric characters
            }

            const parts = value.split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ','); // Add commas to the integer part

            this.value = parts.join('.');
        });
    });
});
</script>
