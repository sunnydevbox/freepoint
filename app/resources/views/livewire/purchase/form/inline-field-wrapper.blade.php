<div>
    @if ($canEdit)
        @php
            $randomKey = time() . $productPurchaseId;
        @endphp

        <livewire:purchase.form.inline-field :field="$field"
            :productPurchaseId="$productPurchaseId"
            :type="$type"
            :validation="$validation ?? ''"
            :value="$value"
            :wire:key="$randomKey" />
    @else
        {{ $value }}
    @endif
</div>
