<div>
    <input class="form-control"
        type="{{ $type }}"
        wire:model.debounce.500ms="{{ $field }}" />
    @error($field)
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>
