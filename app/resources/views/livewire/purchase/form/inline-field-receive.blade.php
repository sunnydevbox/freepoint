<div>
    @if ($remaining == 0)
        <span class="text-success">COMPLETED</span>
    @else
        <div class="form-group">
            <input
                type="number"
                wire:model.debounce.500ms="value"
                class="form-control text-right"
                min="0"
                max="{{ $maxValue ?? 0 }}"
                wire:loading.attr="disabled"
            />
            <span class="form-text text-muted small">
                Received {{ $received }} w/ {{ $remaining }} remaining
            </span>
        </div>

        @foreach ($messages as $error)
            <div class="small text-danger">{{ $error }}</div>
        @endforeach
    @endif
</div>
