<div wire:ignore>
    @if ($canEdit)
        <select {{ $isDisabled ? 'disabled' : '' }}
            class="form-control {{ $isDisabled ? 'disabled' : '' }}"
            data-size="4"
            wire:model.lazy="bin">
            @if (count($warehouseBins) == 0)
                <option disabled>No Warehouse Bin</option>
            @elseif(!$bin)
                <option hidden
                    selected>Select Bin</option>
            @endif
            @foreach ($warehouseBins as $binKey => $binName)
                <option value="{{ $binKey }}">{{ $binName }}</option>
            @endforeach
        </select>
    @else
        as {{-- {{ $warehouseBins->where('id', $bin)?->first()?->name }} --}}
    @endif
</div>
