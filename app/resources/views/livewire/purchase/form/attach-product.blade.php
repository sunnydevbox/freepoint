<div>

    <form>
        <div class="relative">
            <input class="form-control"
                placeholder="Search by product name or code"
                type="text"
                wire:model.debounce.500ms="term" />
            <div class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group results"
                wire:loading>
                <div class="item p-1">Searching...</div>
            </div>

            @if (!empty($term))
                <div class="fixed top-0 bottom-0 left-0 right-0"
                    wire:click="_reset"></div>
                <div class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group results small border">
                    @if (!empty($results))
                        @foreach ($results as $i => $product)
                            {{-- <a href="{{ route('show-contact', $contact['id']) }}"
                                class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }}">
                                 --}}
                            <div class="item p-1 cursor-pointer"
                                wire:click="addProduct({{ $product['id'] }})"
                                wire:key="{{ $product['id'] }}">
                                {{ $product['name'] }} [{{ $product['code'] }}]
                            </div>

                            {{-- </a> --}}
                        @endforeach
                    @else
                        <div class="list-item">No results!</div>
                    @endif
                </div>
            @endif
        </div>

    </form>
    <br />
    <livewire:purchase.form.attached-products :purchaseOrderId="$purchaseOrder->id"
        :purchaseOrder="$purchaseOrder" />
</div>

@push('scripts')
    <script>
        let warehouseBins = @json($warehouseBins);
        document.addEventListener('DOMContentLoaded', function() {
            // warehouseBins ;

            // Function to populate options
            function populateOptions(selectElement, options) {
                Object.values(options).forEach((label, value) => {
                    const opt = document.createElement('option');
                    opt.value = value;
                    opt.textContent = label;
                    selectElement.appendChild(opt);
                });
            }

            // Get all select elements with class 'bin-select'
            // const selectElements = document.querySelectorAll('.bin-select');
            // Populate each select element with options

            // setTimeout(() => {
            const selectElements = document.querySelectorAll('.bin-select');
            selectElements.forEach((selectElement, index) => {
                console.log(index)
                // populateOptions(selectElement, warehouseBins);

            });
            // }, 10000);
            // Array of options

        });
    </script>
@endpush

@push('styles')
    <style>
        .results .item:hover {
            background-color: rgb(237, 219, 174);
        }
    </style>
@endpush
