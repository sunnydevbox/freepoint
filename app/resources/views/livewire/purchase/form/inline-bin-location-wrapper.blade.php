<div>
    @php
        $randomKey = time() . $productPurchaseId;
    @endphp
    <livewire:purchase.form.inline-bin-location :bins="$bins"
        :canEdit="$canEdit"
        :field="$field"
        :productPurchaseId="$productPurchaseId"
        :productPurchase="$productPurchase"
        :validation="$validation ?? ''"
        :value="$value"
        :wire:key="$randomKey" />
</div>
