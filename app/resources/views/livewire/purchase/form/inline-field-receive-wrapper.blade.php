<div>
    @php
        $randomKey = time() . $productPurchaseId;
    @endphp
    <livewire:purchase.form.inline-field-receive
        :productPurchaseId="$productPurchaseId"
        :validation="$validation ?? ''"
        :wire:key="$randomKey"
    />
</div>
