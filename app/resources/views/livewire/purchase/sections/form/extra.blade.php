<div class="row">
    <div class="col-md-4">
        <div class="form-group" wire:ignore>
            <label>{{ trans('file.Order Tax') }}</label>
            <input type="hidden" name="order_tax_rate_hidden" value="{{ $purchaseOrder->order_tax_rate }}">
            <select class="form-control" name="order_tax_rate" wire:model="order_tax_rate" wire:loading.attr="disabled">
                @foreach ($taxes as $tax)
                    <option value="{{ $tax->rate }}">{{ $tax->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>
                <strong>{{ trans('file.Discount') }}</strong>
            </label>
            <input type="number" name="order_discount" class="form-control"
                wire:model="order_discount" wire:loading.attr="disabled" step="any" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>
                <strong>{{ trans('file.Shipping Cost') }}</strong>
            </label>
            <input type="number" name="shipping_cost" class="form-control" wire:model="shipping_cost" wire:loading.attr="disabled"
                step="any" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>{{ trans('file.Note') }}</label>
            <textarea rows="5" class="form-control" name="note" wire:model.debounce.500ms="note" wire:loading.attr="disabled"></textarea>
        </div>
    </div>
</div>
