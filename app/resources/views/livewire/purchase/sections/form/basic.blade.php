<div>
    <form wire:submit.prevent="save">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group"
                    wire:ignore>
                    <label>{{ trans('file.Date') }}</label>
                    <input class="form-control date"
                        name="created_at"
                        type="text"
                        wire:loading.attr="disabled"
                        wire:model="created_at" />

                </div>
                @error('created_at')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-4">
                <div class="form-group"
                    wire:ignore>
                    <label>{{ trans('file.Warehouse') }} *</label>
                    <select {{ empty($purchaseOrder) ?? 'disabled' }}
                        class="selectpicker form-control"
                        data-live-search="true"
                        name="warehouse_id"
                        required
                        title="Select warehouse..."
                        wire:loading.attr="disabled"
                        wire:model="warehouse_id">
                        @foreach ($warehouses as $warehouse)
                            <option {{ $purchaseOrder->warehouse_id == $warehouse->id ? 'selected' : '' }}
                                value="{{ $warehouse->id }}">
                                {{ $warehouse->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @error('warehouse_id')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-md-4">
                <div class="form-group"
                    wire:ignore>
                    <label>{{ trans('file.Supplier') }}</label>
                    <select class="selectpicker form-control"
                        data-live-search="true"
                        id="supplier-id"
                        name="supplier_id"
                        title="Select supplier..."
                        wire:loading.attr="disabled"
                        wire:model="supplier_id">
                        @foreach ($suppliers as $supplier)
                            <option {{ $purchaseOrder->supplier_id == $supplier->id ? 'selected' : '' }}
                                value="{{ $supplier->id }}">
                                {{ $supplier->name . ' (' . $supplier->company_name . ')' }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @error('supplier_id')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>{{ trans('file.Purchase Status') }}</label>
                    {{-- <div wire:ignore> --}}

                    <select @if (!$this->canChangeStatus()) disabled="disabled" @endif
                        class="form-control"
                        name="status"
                        wire:loading.attr="disabled"
                        wire:model.defer="status">
                        <option value="1">{{ trans('file.Recieved') }}</option>
                        <option value="2">{{ trans('file.Partial') }}</option>
                        <option value="3">{{ trans('file.Pending') }}</option>
                        <option value="4">{{ trans('file.Ordered') }}</option>
                    </select>

                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Invoice</label> <i class="dripicons-question"
                        data-toggle="tooltip"
                        title="Invoice number provided by the purchaser"></i>
                    <input class="form-control"
                        name="invoice_number"
                        type="text"
                        wire:loading.attr="disabled"
                        wire:model.debounce.500ms="invoice_number">
                    @error('invoice_number')
                        <span class="text-danger small">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-12">
                <button class="btn btn-primary"
                    type="submit">
                    <span wire:loading.remove>Save</span>
                    <span wire:loading>
                        Saving
                        <svg class="inline-block animate-spin -mr-1 ml-3 h-5 w-5 text-white"
                            fill="none"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <circle class="opacity-25"
                                cx="12"
                                cy="12"
                                r="10"
                                stroke-width="4"
                                stroke="currentColor"></circle>
                            <path class="opacity-75"
                                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                                fill="currentColor">
                            </path>
                        </svg>
                    </span>
                </button>
            </div>

        </div>
    </form>
</div>
<?php

use Carbon\Carbon;
?>
@push('scripts')
    <script type="text/javascript">
        document.addEventListener("livewire:load", function() {
            Livewire.on('selectpicker:initialize', function() {
                $('.selectpicker').selectpicker({
                    style: 'btn-link',
                });
                $('.date').datepicker({
                    format: "yyyy-mm-dd",
                    startDate: "<?php echo Carbon::now()->format('Y-m-d'); ?>",
                    autoclose: true,
                    todayHighlight: true
                });
            });
        });
    </script>
@endpush
