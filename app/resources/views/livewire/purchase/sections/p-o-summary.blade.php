<div>
    <table class="table table-bordered table-condensed totals mb-0">
        <tr>
            <td><strong>{{ trans('file.Items') }}</strong></td>
            <td><span class="pull-right" id="item">{{ number_format($items, $decimal, '.', '') }}</span>
            </td>
        </tr>

        <tr>
            <td><strong>Subtotal</strong></td>
            <td><span class="pull-right" id="subtotal">{{ formatMoney($total, true) }}</span>
            </td>
        </tr>

        <tr>
            <td><strong>{{ trans('file.Order Tax') }}</strong></td>
            <td><span class="pull-right" id="order_tax">{{ formatMoney($orderTax, true) }}</span>
            </td>
        </tr>

        <tr>
            <td><strong>{{ trans('file.Order Discount') }}</strong></td>
            <td><span class="pull-right" id="order_discount">{{ formatMoney($orderDiscount, true) }}</span>
            </td>
        </tr>

        <tr>
            <td><strong>{{ trans('file.Shipping Cost') }}</strong></td>
            <td><span class="pull-right" id="shipping_cost">{{ formatMoney($shippingCost, true) }}</span>
            </td>
        </tr>

        <tr>
            <td><strong>{{ trans('file.grand total') }}</strong></td>
            <td><span class="pull-right" id="grand_total">{{ formatMoney($grandTotal, true) }}</span>
            </td>
        </tr>
    </table>
</div>
