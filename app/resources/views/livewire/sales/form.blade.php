<div>
    <p class="italic">
        <small>{{ trans('file.The field labels marked with * are required input fields') }}.</small>
    </p>

    <div class="row">
        <div class="
            {{ $isNew ? 'col-lg-12' : 'col-lg-8' }}
        ">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h4>{{ $isNew ? 'Create' : 'Update' }} Sale <strong
                                    class="text-uppercase">{!! $reference_no !!}</strong></h4>
                        </div>
                        <div class="card-body">
                            <livewire:sales.sections.form.basic :generalSettings="$generalSettings"
                                :saleOrder="$saleOrder" />
                        </div>
                    </div>
                </div>

                @if ($isNew)
                    <div class="col-md-12 mt-4">
                        <div class="jumbotron w-100 text-center">
                            <div class="container">
                                <p class="lead">Create this sales order first before adding products to it
                                </p>
                            </div>
                        </div>
                    </div>
                @endif

                @if (!$isNew)
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h4>Items</h4>
                            </div>
                            <div class="card-body">
                                <livewire:sales.form.attach-product :saleOrder="$saleOrder" />
                            </div>
                        </div>
                    </div>
                @endif

                @if (!$isNew)
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h4>Payment</h4>
                            </div>
                            <div class="card-body">
                                <livewire:sales.sections.form.payment :saleOrder="$saleOrder" />
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @if (!$isNew)
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h4>Summary </h4>
                            </div>
                            <div class="card-body">
                                <livewire:sales.form.summary :saleOrder="$saleOrder" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h4>Others </h4>
                            </div>
                            <div class="card-body">
                                <livewire:sales.sections.form.extra :saleOrder="$saleOrder" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h4>Notes</h4>
                            </div>
                            <div class="card-body">
                                <livewire:sales.sections.form.notes :saleOrder="$saleOrder" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif
    </div>
</div>
