<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Staff Note</label>
            <textarea rows="5" class="form-control" name="staff_note" wire:model.debounce.500ms="staff_note" wire:loading.attr="disabled"></textarea>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Sale Note</label>
            <textarea rows="5" class="form-control" name="sale_note" wire:model.debounce.500ms="sale_note" wire:loading.attr="disabled"></textarea>
        </div>
    </div>
</div>
