<div class="w-100 d-flex gap-2" wire:ignore>
    <select
        wire:model="payment_status"
        class="form-control "
    >
        @foreach ($this->paymentStatuses as $paymentStatusId => $paymentStatus)
            <option value="{{ $paymentStatusId }}">{{ $paymentStatus }}</option>
        @endforeach
    </select>
    <button
        class=" btn btn-primary btn-sm float-end"
        onclick="Livewire.emit('openModal', 'sales.modals.add-payment', {{ json_encode(['saleOrder' => $this->saleOrder->id]) }})"
    >Add Payment</button>
</div>
