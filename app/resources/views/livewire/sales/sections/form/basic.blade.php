<div>
    <form wire:submit.prevent="save">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label>{{ trans('file.Date') }}</label>
                    <input class="form-control date"
                        name="created_at"
                        type="text"
                        wire:loading.attr="disabled"
                        wire:model="created_at" />
                    @error('created_at')
                        <span class="text-danger small">{{ $message }}</span>
                    @enderror
                </div>

            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Reference No.</label>
                    <input {{ $isNew ?: 'disabled' }}
                        class="form-control"
                        name="reference_no"
                        type="text"
                        wire:loading.attr="disabled"
                        wire:model.debounce.500ms="reference_no" />

                    @if ($isNew)
                        <small class="form-text text-muted"
                            id="passwordHelpBlock">
                            Leave this field blank to auto-generate a reference number
                        </small>
                    @endif
                    @error('reference_no')
                        <span class="text-danger small">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group mb-0"
                    wire:ignore>
                    <label>Customer</label>
                    <select class="selectpicker form-control"
                        data-live-search="true"
                        id="customer-id"
                        name="customer_id"
                        title="Select customer..."
                        wire:loading.attr="disabled"
                        wire:model="customer_id">
                        @foreach ($customers as $customer)
                            <option {{ $customer->id == $customer_id ? 'selected' : '' }}
                                value="{{ $customer->id }}">
                                {{ $customer->name . ' (' . $customer->company_name . ')' }}
                            </option>
                        @endforeach
                    </select>
                    {{-- Note: temporarily disable due to select tag on change problem --}}
                    {{-- <livewire:customer.customer-list /> --}}
                </div>
                @error('customer_id')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-4">
                <div class="form-group mb-0"
                    wire:ignore>
                    <label>{{ trans('file.Warehouse') }} *</label>
                    <select {{ !$isNew || !empty($saleOrder->id) ? ' disabled ' : '' }}
                        class="selectpicker form-control"
                        data-live-search="true"
                        name="warehouse_id"
                        title="Select warehouse..."
                        wire:loading.attr="disabled"
                        wire:model="warehouse_id">
                        @foreach ($warehouses as $warehouse)
                            <option {{ $warehouse->id == $warehouse_id ? 'selected' : '' }}
                                value="{{ $warehouse->id }}">
                                {{ $warehouse->name }}</option>
                        @endforeach
                    </select>
                </div>
                @error('warehouse_id')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-4">
                <div class="form-group mb-0"
                    wire:ignore>
                    <label>Biller</label>
                    <select class="selectpicker form-control"
                        id="biller-id"
                        name="biller_id"
                        title="Select biller..."
                        wire:model="biller_id">
                        @foreach ($billers as $biller)
                            <option {{ $biller->id == $biller_id ? 'selected' : '' }}
                                value="{{ $biller->id }}">
                                {{ $biller->name . ' (' . $biller->name . ')' }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @error('biller_id')
                    <span class="text-danger small">{{ $message }}</span>
                @enderror

            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Official Receipt</label> <i class="dripicons-question"
                        data-toggle="tooltip"
                        title="Official Receipt "></i>
                    <input class="form-control"
                        name="receipt_no"
                        type="text"
                        wire:loading.attr="disabled"
                        wire:model.debounce.500ms="receipt_no">
                    @error('receipt_no')
                        <span class="text-danger small">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-12">
                <div class="form-group"
                    wire:ignore>
                    <label>Status</label>
                    <select class="selectpicker form-control"
                        class="form-control"
                        name="sale_status"
                        wire:model="sale_status">
                        @foreach ($saleStatuses as $key => $saleStatus)
                            <option {{ $key == $sale_status ? ' selected ' : '' }}
                                value="{{ $key }}">{{ $saleStatus }}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-warning"
                        id="emailHelp">Marking this sale as COMPLETED will proceed to adjust the stock count of the
                        items.</small>
                </div>
            </div>
            {{--
            @if ($isNew)
                <div class="col-12 mt-2">
                    <button
                        type="submit"
                        class="btn btn-primary"
                    >Create Sales Order</button>
                </div>
            @endif
 --}}

            <div class="col-12">
                <button class="btn btn-primary"
                    type="submit">
                    <span wire:loading.remove>{{ $btnSaveLabel }} Sales Order</span>
                    <span wire:loading>
                        Saving
                        <svg class="inline-block animate-spin -mr-1 ml-3 h-5 w-5 text-white"
                            fill="none"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg">
                            <circle class="opacity-25"
                                cx="12"
                                cy="12"
                                r="10"
                                stroke-width="4"
                                stroke="currentColor"></circle>
                            <path class="opacity-75"
                                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                                fill="currentColor">
                            </path>
                        </svg>
                    </span>
                </button>
            </div>

        </div>
    </form>
</div>
<?php

use Carbon\Carbon;
?>
@push('scripts')
    <script type="text/javascript">
        window.addEventListener('contentChanged', event => {
            // Due to broken select tag after update, we have to reload the page for customer list.
            setTimeout(function() {
                window.location.reload();
            }, 1000);
        });

        $(document).ready(function() {
            $('.selectpicker').selectpicker();
            $('.selectpicker').on('change', function(e) {

                var selectedValue = $(e.target)[0].value;
                var field = $(e.target)[0].name;
                // var data = $('#select2').select2("val");
                // @this.set(field, selectedValue);
            });
            $('.date').datepicker();

            $('.date').on('change', function(e) {
                var selectedValue = $(e.target)[0].value;
                var field = $(e.target)[0].name;
                @this.set(field, selectedValue);
            })
        });
    </script>
@endpush
