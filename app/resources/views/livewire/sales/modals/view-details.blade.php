<div class="container-fluid visible-print-block"
    id="print-details">
    <div class=" mt-3 pb-2 border-bottom">
        <div class="row">
            <div class="col-md-6">

                <a class="btn btn-default btn-sm"
                    href="{{ route('sale.print', $sale->id) }}"
                    target="_blank"><i class="dripicons-print"></i> Print</a>

            </div>
            <div class="col-md-6 d-print-none">
                <button aria-label="Close"
                    class="close"
                    data-dismiss="modal"
                    id="close-btn"
                    type="button"
                    wire:click="$emit('closeModal')"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <div class="col-md-12">
                <h3 class="modal-title text-center container-fluid"
                    id="exampleModalLabel">Freepoint</h3>
            </div>
            <div class="col-md-12 text-center">
                <i style="font-size: 15px;">Sale Details</i>
            </div>
        </div>
    </div>

    <div id="purchase-content">
        <div class="mb-5">
            <div class="d-block w-100">
                <strong>Date:</strong> {{ dateFormat($sale->created_at) }}
            </div>
            <div class="d-block w-100">
                <strong>Reference: </strong>{{ strtoupper($sale->reference_no) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <strong>From:</strong>
            <br>
            {{ $sale->warehouse->name }}
            <br>
            {{ $sale->warehouse->phone }}
            <br>
            {{ $sale->warehouse->address }}
        </div>
        <div class="col-md-6">
            <div class="float-right">
                <strong>To:</strong>
                <br>
                {{ $sale->customer->name }}
                <br>
                {{ $sale->customer->company_name }}
                <br>
                {{ $sale->customer->email }}
                <br>
                {{ $sale->customer->phone_number }}
                <br>
                {{ $sale->customer->address }}
                <br>
                {{ $sale->customer->city }}
            </div>
        </div>
    </div>

    <br>
    <table class="table table-bordered product-purchase-list">
        <thead>
            <tr>
                <th>{{ trans('file.Product') }}</th>
                <th>{{ trans('file.Qty') }}</th>
                <th>{{ trans('file.Unit Cost') }}</th>
                <th>{{ trans('file.Tax') }}</th>
                <th>{{ trans('file.Discount') }}</th>
                <th>{{ trans('file.Subtotal') }}</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    @foreach ($sale->items as $value)
                        {{ $value->product->name }}
                        <br>
                    @endforeach
                </td>
                <td>{{ $sale->total_qty }}</td>
                <td>{{ $sale->total_price }}</td>
                <td>{{ $sale->total_tax }}</td>
                <td>{{ $sale->total_discount }}</td>
                <td>{{ $sale->grand_total }}</td>
            </tr>
        </tbody>
    </table>
</div>
