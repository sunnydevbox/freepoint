<div id="add-payment">
    <div class="modal-dialog"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLabel">{{ trans('file.Add Payment') }} </h5>
                <button aria-label="Close"
                    class="close"
                    data-dismiss="modal"
                    type="button"
                    wire:click="$emit('closeModal')"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <div class="modal-body">
                <strong>Balance:</strong> {{ formatMoney($due, true) }}
                <hr />
                <form wire:loading.attr="disabled"
                    wire:submit.prevent="doAddPayment">
                    <div class="row">
                        <input name="balance"
                            type="hidden">
                        <div class="form-group col-md-6">
                            <label>{{ trans('file.Recieved Amount') }} *</label>
                            <input class="form-control numkey"
                                name="paying_amount"
                                required
                                step="0.01"
                                type="number"
                                wire:loading.attr="disabled"
                                wire:model.debounce.500ms="paying_amount">
                            <small class="form-text text-muted">Money handed over to cashier</small>
                            @error('paying_amount')
                                <span class="validation-message small text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label>{{ trans('file.Paying Amount') }} *</label>
                            [{{ $amount }}]
                            <input {{-- value="{{ number_format($amount, 2, '.', ',') }}" --}}
                                class="form-control"
                                id="amount"
                                name="amount"
                                required
                                step="0.01"
                                type="number"
                                wire:loading.attr="disabled"
                                wire:model.debounce.500ms="amount">
                            @error('amount')
                                <span class="validation-message small text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 mt-1 card">
                            <label>{{ trans('file.Change') }} : </label>
                            <p class="change ml-2">{{ formatMoney($change, true) }}</p>
                        </div>
                        <div class="form-group col-md-6 mt-1">
                            <label>Mode of Payment</label>
                            <select class="form-control"
                                name="paid_by_id"
                                wire:model="selectedPaymentMethod">
                                @foreach ($paymentMethods as $methodId => $methodName)
                                    <option value="{{ $methodId }}">{{ $methodName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group mt-2">
                        <div class="card-element"
                            class="form-control">
                        </div>
                        <div class="card-errors"
                            role="alert"></div>
                    </div>

                    <!-- CHEQUE -->
                    @if ($this->showChequeField)
                        <div id="cheque">
                            <div class="form-group">
                                <label>{{ trans('file.Cheque Number') }} *</label>
                                <input class="form-control"
                                    name="cheque_no"
                                    type="text"
                                    wire:model="cheque_no">
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label> {{ trans('file.Account') }}</label>

                        <select class="form-control"
                            name="account_id"
                            required
                            wire:model="account_id">
                            @foreach ($accounts as $account)
                                @if ($account->is_default)
                                    <option selected
                                        value="{{ $account->id }}">{{ $account->name }}
                                        [#{{ $account->account_no }}]
                                    </option>
                                @else
                                    <option value="{{ $account->id }}">{{ $account->name }}
                                        [{{ $account->account_no }}]
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        @error('account_id')
                            <span class="validation-message small text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{ trans('file.Payment Note') }}</label>
                        <textarea class="form-control"
                            name="payment_note"
                            rows="3"
                            wire:model.defer="payment_note"></textarea>
                    </div>

                    <input name="purchase_id"
                        type="hidden">

                    <button class="btn btn-primary"
                        type="submit"
                        wire:loading.attr="disabled">{{ trans('file.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
