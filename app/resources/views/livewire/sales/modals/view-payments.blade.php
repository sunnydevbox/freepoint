<div>
    <h5 id="exampleModalLabel" class="modal-title">All Payment</h5>
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Reference No</th>
                <th>Account Amount</th>
                <th>Paid By Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($payments AS $payment)
            <tr>
                <td>{{ $payment->created_at }}</td>
                <td>{{ $payment->payment_reference }}</td>
                <td>{{ formatMoney($payment->amount) }}</td>
                <td>{{ $payment->user->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
