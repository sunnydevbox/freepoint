<div class="container py-8">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header align-items-center">
                    <h4>{{trans('file.Add Customer')}}</h4>
                </div>
                <div class="card-body">
                    <form wire:submit.prevent="store">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Customer Group')}} *</strong> </label>
                                        <div wire:ignore>
                                        <select wire:ignore class="form-control selectpicker" 
                                        wire:model="customer_group_id"
                                        id="customer-group-id" name="customer_group_id">
                                            @foreach($customerGroup as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.name')}} *</strong> </label>
                                    <input type="text" id="name" name="customer_name" wire:model="name" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Company Name')}} <span class="asterisk">*</span></label>
                                    <input type="text" name="company_name"  wire:model="company_name" class="form-control">
                                    @error('company_name') <span class="error text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Email')}} <span class="asterisk">*</span></label>
                                    <input type="email" name="email" wire:model="email" placeholder="example@example.com" class="form-control">
                                    @error('email') <span class="error text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Phone Number')}} *</label>
                                    <input type="text" name="phone_number" wire:model="phone_number" required class="form-control">
                                    @error('phone_no') <span class="error text-danger">{{ $message }}</span> @enderror

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Tax Number')}}</label>
                                    <input type="text" name="tax_no" wire:model="tax_no" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Address')}} *</label>
                                    <input type="text" name="address" wire:model="address" required class="form-control">
                                    @error('address') <span class="error text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.City')}} *</label>
                                    <input type="text" name="city" wire:model="city" class="form-control">
                                    @error('city') <span class="error text-danger">{{ $message }}</span> @enderror

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.State')}}</label>
                                    <input type="text" name="state" wire:model="state"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Postal Code')}}</label>
                                    <input type="text" name="postal_code" wire:model="postal_code"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{trans('file.Country')}}</label>
                                    <input type="text" name="country" wire:model="country"  class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="pos" value="0">
                            <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>