<div>
    <select
        class="form-control"
        wire:model="tax_rate"
    >
        @foreach ($taxes as $tax)
            <option
                value="{{ $tax->rate }}"
                {{ $tax->rate == $tax_rate ? ' selected ' : '' }}
            >{{ $tax->name }}</option>
        @endforeach
    </select>

    <div>
        Amount: {{ formatMoney($taxAmount) }}
    </div>
    
</div>
