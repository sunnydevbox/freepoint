<div wire:ignore>

    @if ($canEdit)

        @php
            $disabled = $isDisabled ? 'disabled' : '';
        @endphp
        <select $disabled
            class="form-control $disabled"
            data-size="4"
            wire:model.lazy="bin">
            @if ($warehouseBins->isEmpty())
                <option disabled>No Warehouse Bin</option>
            @elseif(!$bin)
                <option hidden
                    selected>Select Bin</option>
            @endif
            @foreach ($warehouseBins as $bin)
                <option value="{{ $bin->id }}">{{ $bin->name }}</option>
            @endforeach
        </select>
    @else
        {{ $warehouseBins->where('id', $bin)?->first()?->name }}
    @endif
</div>
