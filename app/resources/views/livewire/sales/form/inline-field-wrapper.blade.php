<div>
    @if ($canEdit)
        @php
            $randomKey = time() . $saleItem->id;
        @endphp

        <livewire:sales.form.inline-field :field="$field"
            :saleItem="$saleItem"
            :type="$type"
            :validation="$validation ?? ''"
            :value="$value"
            :wire:key="$randomKey" />
    @else
        {{ $value }}
    @endif
</div>
