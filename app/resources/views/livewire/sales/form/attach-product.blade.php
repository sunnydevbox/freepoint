<div>

    <form>
        <div class="relative">
            <input
                type="text"
                class="form-control"
                placeholder="Search by product name or code"
                wire:model.debounce.500ms="term"
            />
            <div
                wire:loading
                class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group results"
            >
                <div class="item p-1">Searching...</div>
            </div>

            @if (!empty($term))
                {{-- <div
                    class="fixed top-0 bottom-0 left-0 right-0"
                    wire:click="_reset"
                >asd</div> --}}
                <div class="absolute z-10 w-full bg-white rounded-t-none shadow-lg list-group results small border">
                    @if ($results->count())
                        @foreach ($results as $i => $product)
                            {{-- <a href="{{ route('show-contact', $contact['id']) }}"
                                class="list-item {{ $highlightIndex === $i ? 'highlight' : '' }}">
                                 --}}
                            <div
                                class="item p-2 cursor-pointer border-1"
                                wire:click="addProduct({{ $product->id }}, {{ $product->warehouse_bin_id }})"
                                wire:key="{{ $product->id .'-'.$product->warehouse_bin_id }}"
                            >
                                {{ $product->name }}
                                <br />
                                <div class="small">
                                    <strong>Code:</strong> {{ $product->code }} | 
                                    <strong>Stock Count:</strong> {{ $product->stock_qty }}
                                    <strong>Location:</strong> {{ $product->warehouse_name }}/{!! $product->bin_name ?? '<em><strong>Not Set</strong></em>' !!}]
                                </div>
                            </div>

                            {{-- </a> --}}
                        @endforeach
                    @else
                        <div class="item p-1 text-center h1">No results!</div>
                    @endif
                </div>
            @endif
        </div>


    </form>
    <br />
    <livewire:sales.form.attached-products :saleOrder="$saleOrder" />
</div>
@push('styles')
    <style>
        .item:hover {
            background-color: rgb(145, 145, 145);
            color: white;
        }
    </style>
@endpush
