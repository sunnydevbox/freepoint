<div class="flex space-x-1 justify-around">
    {{-- <a href="{{ route('purchases.edit', [$id]) }}"
        class="p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded" title="Edit">
        <svg class=" w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z">
            </path>
        </svg>
    </a>

    <a href="javascript:void"
        onclick="Livewire.emit('openModal', 'purchase.modals.add-payment', {{ json_encode(['purchase_id' => $id]) }})"
        class="add-payment p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded" title="Edit">
        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-cash w-5 h-5" viewBox="0 0 16 16">
            <path d="M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
            <path
                d="M0 4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4zm3 0a2 2 0 0 1-2 2v4a2 2 0 0 1 2 2h10a2 2 0 0 1 2-2V6a2 2 0 0 1-2-2H3z" />
        </svg>
    </a> --}}

    <a href="javascript:vod()"
    onclick="Livewire.emit('openModal', 'purchase.modals.deelte', {{ json_encode(['purchase_id' => $id]) }})"
        class="p-1 text-red-500 hover:text-red rounded" title="History">
        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi w-5 h-5 bi-trash"
            viewBox="0 0 16 16">
            <path
                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z" />
            <path
                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z" />
        </svg>
    </a>



</div>


@push('scripts')
    <script></script>
@endpush
