<div>
    <livewire:sales.form.inline-field 
        :saleItem="$saleItem" 
        :field="$field" 
        :value="$value" 
        :type="$type" 
        :wire:key="'inline-one'.$saleItem->id"
    />
</div>