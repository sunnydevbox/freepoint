<div>
    @php
        $randomKey = time() . $saleItem->id;
    @endphp
    <livewire:sales.form.inline-field-tax
        :saleItem="$saleItem"
        :field="$field"
        :value="$value"
        :type="$type"
        :wire:key="$randomKey"
    />
</div>
