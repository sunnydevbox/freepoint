<div>
    <input type="{{ $type }}" wire:model.debounce.500ms="{{ $field }}" class="form-control" />
    @error($field) <span class="text-danger">{{ $message }}</span> @enderror
</div>