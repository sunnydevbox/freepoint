<div>
    @php
        $randomKey = time() . $productSale->id;
    @endphp
    <livewire:sales.form.inline-bin-location :canEdit="$canEdit"
        :field="$field"
        :productSale="$productSale"
        :validation="$validation ?? ''"
        :value="$value"
        :wire:key="$randomKey" />
</div>
