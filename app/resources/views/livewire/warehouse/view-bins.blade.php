<div role="document" class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-title font-weight-bold">View Bin</h5>
        </div>
        <div class="modal-body">
            <div class="mb-2">
                Bin Name: {{$bins->name ?? 'N/A'}}
            </div>

            <div class="mb-2">
                Location: {{$bins->warehouse->name}}
            </div>

            <div class="mb-2">
                Aisle: {{$bins->aisle ?? 'N/A'}}
            </div>

            <div class="mb-2">
                Rack: {{$bins->rack ?? 'N/A'}}
            </div>
            
            <div class="mb-2">
                Shelf: {{$bins->shelf ?? 'N/A'}}
            </div>
            <div class="mb-2">
                Bin: {{$bins->bin ?? 'N/A'}}
            </div>
        </div>
    </div>
</div>


