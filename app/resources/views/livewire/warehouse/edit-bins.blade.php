<section>
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Edit Warehouse Bin</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <form wire:submit.prevent="submit">
            <div class="modal-body">
                <p class="italic">
                    <small>{{ trans('file.The field labels marked with * are required input fields') }}.</small>
                </p>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>{{ trans('file.name') }}</label>
                                <input type="text" placeholder="Descriptive name for this bin"
                                    required="required" wire:model.defer="name" class="form-control" maxlength="100">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>{{ trans('file.Warehouse') }} *</label>
                                <select required wire:model="warehouse" class="form-control"
                                    data-live-search="true">
                                    @foreach ($warehouses as $warehouseName)
                                        <option value="{{ $warehouseName->id }}">{{ $warehouseName->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3><strong>Bin's Physical Position</strong></h3>
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label>Aisle</label>
                                <input type="text"  wire:model.defer="aisle"  class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Rack</label>
                                <input type="text"  wire:model.defer="rack"  class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Shelf</label>
                                <input type="text"  wire:model.defer="shelf"  class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Bin</label>
                                <input type="text" wire:model.defer="bin"  class="form-control"
                                    maxlength="100">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary mt-2">
                    Submit
                </button>
            </div>
            </form>
        </div>
</section>