<div class="flex space-x-1">
   
    <a href="javascript:void" onclick="Livewire.emit('openModal', 'warehouse.bins.view-bins', {{ json_encode(['warehouse_bin_id' => $id]) }})"
        class="p-1 text-black-500 hover:bg-black hover:text-white rounded .view" title="View">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
            class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
            <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
        </svg>

    </a>

    <a href="{{ route('warehouse.bins.edit', [$id]) }}"
        class="p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded" title="Edit">
        <svg class=" w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z">
            </path>
        </svg>
    </a>

</div>


@push('scripts')
    <script></script>
@endpush
