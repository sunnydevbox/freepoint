<div class="row">
    <div class="form-group col-lg-12">
        <label>{{trans('file.Product Name')}} *</strong> </label>
        <input type="text" name="name" class="form-control" id="name" wire:model.lazy="name" aria-describedby="name" required>
        @error('name') <span class="validation-message text-danger small">{{ $message }}</span> @enderror
    </div>

    {{--
    <div class="form-group col-lg-3">
        <label>{{trans('file.Barcode Symbology')}} *</strong> </label>
        <div class="input-group">
            <select required class="form-control" wire:model.lazy="barcode_symbology">
                <option value="C128">Code 128</option>
                <option value="C39">Code 39</option>
                <option value="UPCA">UPC-A</option>
                <option value="UPCE">UPC-E</option>
                <option value="EAN8">EAN-8</option>
                <option value="EAN13">EAN-13</option>
            </select>
        </div>
        @error('barcode_symbology') <span class="validation-message small text-danger small">{{ $message }}</span> @enderror
    </div>
    --}}

    <div class="form-group col-lg-6">
        <label>{{trans('file.Code')}} *</strong> </label>
        <div class="input-group">
            <input type="text" class="form-control" id="code" wire:model.debounce.500ms="code" required>
            <!-- <div class="input-group-append">
                <button id="genbutton" type="button" class="btn btn-sm btn-default" title="{{trans('file.Generate')}}"><i class="fa fa-refresh"></i></button>
            </div> -->
        </div>
        @error('code') <span class="validation-message text-danger small">{{ $message }}</span> @enderror
    </div>


    <div class="form-group col-lg-6">
        <label>Model *</strong> </label>
        <div class="input-group">
            <input type="text" class="form-control" id="model" wire:model.lazy="model" required>
            <!-- <div class="input-group-append">
                <button id="genbutton" type="button" class="btn btn-sm btn-default" title="{{trans('file.Generate')}}"><i class="fa fa-refresh"></i></button>
            </div> -->
        </div>
        @error('model') <span class="validation-message text-danger small">{{ $message }}</span> @enderror
    </div>

    {{--
    <div class="form-group col-lg-12">
        <label>{{trans('file.Product Details')}}</label>
        <textarea name="product_details" class="form-control" wire:model.lazy="product_details" rows="3"></textarea>
    </div>
    --}}
</div>