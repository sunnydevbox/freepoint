<div class="row">
    <div class="form-group col-lg-4">
        <label>{{trans('file.Product Cost')}} *</strong> </label>
        <input type="number" name="cost" wire:model.lazy="cost" required class="form-control" step="any">
        @error('cost') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>


    <div class="form-group col-lg-4">
        <label>{{trans('file.Product Price')}} *</strong> </label>
        <input type="number" name="price" wire:model.lazy="price" required class="form-control" step="any">
        @error('price') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group col-lg-4">
        <label>Alert QTY</strong> </label>
        <input type="number" name="alert_quantity" class="form-control" step="any">
    </div>

</div>