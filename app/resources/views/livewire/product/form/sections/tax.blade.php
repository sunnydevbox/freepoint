<div class="row">
    <div class="form-group col-md-6">
        <label>{{trans('file.Product Tax')}}</strong> </label>
        <select name="tax_id" class="form-control" wire:model.lazy="tax_id">
            @foreach($taxes as $tax)
            <option value="{{$tax->id}}">{{$tax->name}}</option>
            @endforeach
        </select>
        @error('tax_id') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group col-md-6">
        <label>{{trans('file.Tax Method')}}</strong> </label> <i class="dripicons-question" data-toggle="tooltip" title="{{trans('file.Exclusive: Poduct price = Actual product price + Tax. Inclusive: Actual product price = Product price - Tax')}}"></i>
        <select name="tax_method" class="form-control selectpicker" wire:model.lazy="tax_method">
            <option value="1">{{trans('file.Exclusive')}}</option>
            <option value="2">{{trans('file.Inclusive')}}</option>
        </select>
        @error('tax_method') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>
</div>