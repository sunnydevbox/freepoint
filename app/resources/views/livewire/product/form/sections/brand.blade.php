<div class="form-group">
    <label>{{ trans('file.Brand') }}</strong> </label>
    <div class="input-group">
        <select class="form-control"
            data-live-search-style="begins"
            data-live-search="true"
            name="brand_id"
            title="Select Brand..."
            wire:model.lazy="brand_id">
            <option value="">Select Brand</option>
            @foreach ($brands as $brand)
                <option value="{{ $brand->id }}">{{ $brand->title }}</option>
            @endforeach
        </select>
    </div>
    @error('brand_id')
        <span class="validation-message small text-danger">{{ $message }}</span>
    @enderror

</div>

<div class="form-group">
    <label>{{ trans('file.category') }}</strong> </label>
    <div class="input-group">
        <select class="form-control"
            data-live-search-style="begins"
            data-live-search="true"
            name="category_id"
            title="Select Category..."
            wire:model.lazy="category_id">
            <option value="">Select Category</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
    @error('category_id')
        <span class="validation-message small text-danger">{{ $message }}</span>
    @enderror

</div>
