<div class="form-group">
    <label>{{trans('file.Product Unit')}} *</strong> </label>
    <div class="input-group">
        <select class="form-control" wire:model="unit_id">
            <option value="" selected>Select Product Unit...</option>
            @foreach($units as $unit)
            <option value="{{$unit->id}}">{{$unit->unit_name}} ({{$unit->unit_code}})</option>
            @endforeach
        </select>
    </div>
    @error('unit_id') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
</div>
<div class="form-group">
    <label>{{trans('file.Sale Unit')}}</strong> </label>
    <div class="input-group">
        <select class="form-control" wire:model.defer="sale_unit_id" {{ empty($purchaseUnits) ? "disabled" : "" }}>
            <option value="" selected>Select Sale Unit...</option>
            @foreach($saleUnits as $saleUnit)
            <option value="{{$saleUnit->id}}">{{$saleUnit->unit_name}} ({{$saleUnit->unit_code}})</option>
            @endforeach
        </select>
    </div>
    @error('sale_unit_id') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
</div>
<div class="form-group">
    <div class="form-group">
        <label>{{trans('file.Purchase Unit')}}</strong> </label>
        <div class="input-group">
            <select class="form-control" wire:model.defer="purchase_unit_id" 
                {{ empty($purchaseUnits) ? "disabled" : "" }}
            >
                <option value="" selected>Select Purchase Unit...</option>
                @foreach($purchaseUnits as $purchaseUnit)
                <option value="{{$purchaseUnit->id}}">{{$purchaseUnit->unit_name}} ({{$purchaseUnit->unit_code}})</option>
                @endforeach
            </select>
        </div>
        @error('purchase_unit_id') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>
</div>