<div class="row">
    <div class="form-group col-lg-4">

        <div class="form-floating mb-3">
            <input type="text" name="extra_year" wire:model.lazy="extra_year" class="form-control">
            <label for="floatingInput">Year</label>
        </div>
        @error('extra_year') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group col-lg-4">
        <label>Year</strong> </label>
        <input type="number" name="extra_year" wire:model.lazy="extra_year" required class="form-control" step="any">
        @error('extra_year') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>


</div>