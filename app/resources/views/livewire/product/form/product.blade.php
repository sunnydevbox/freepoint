<div id="product-form">
    <form wire:submit.prevent="submit">
        {{--
      <div class="form-group">
         <label>{{trans('file.Product Type')}} *</strong> </label>
      <div class="input-group">
         <select name="type" required class="form-control" id="type" wire:model.lazy="type">
            @foreach ($types as $typeKey => $typeValue)
            <option value="{{$typeKey}}">{{$typeValue}}</option>
            @endforeach
         </select>
         @error('type') <span class="text-danger">{{ $message }}</span> @enderror
      </div>
</div>
--}}

        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        @include('livewire.product.form.sections.basic')
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">

                        <div class="card">
                            <div class="card-body">
                                @include('livewire.product.form.sections.cost')
                            </div>
                        </div>
                    </div>

                    <div class="col-6">

                        <div class="card">
                            <div class="card-body">
                                @include('livewire.product.form.sections.tax')
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header text-center">Additional Info</div>
                    <div class="card-body">
                        @include('livewire.product.form.sections.extra-fields')
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                {{-- TODO: this should only appear when user has rights --}}
                <div class="card">
                    <div class="card-header">
                        Admin
                    </div>
                    <div class="card-body">
                        @include('livewire.product.form.sections.admin')
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        @include('livewire.product.form.sections.brand')
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        @include('livewire.product.form.sections.units')
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" wire:loading.attr="disabled">Save Product</button>
    </form>
</div>


@push('scripts')
    <script type="text/javascript">
        // tinymce.init({
        //    selector: 'textarea',
        //    height: 130,
        //    plugins: [
        //       'advlist autolink lists link image charmap print preview anchor textcolor',
        //       'searchreplace visualblocks code fullscreen',
        //       'insertdatetime media table contextmenu paste code wordcount'
        //    ],
        //    toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
        //    branding: false
        // });
    </script>
@endpush

@push('styles')
    <style>
        #product-form * {
            text-transform: uppercase;
        }
    </style>
@endpush
