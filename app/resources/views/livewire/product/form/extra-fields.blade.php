<div class="row">
    <div class="form-group col-lg-4">
        <label>Year</strong> </label>
        <input type="text" wire:model.lazy="year" class="form-control" step="any">
        @error('year') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group col-lg-4">
        <label>Version</strong> </label>
        <input type="text" wire:model.lazy="version" class="form-control" step="any">
        @error('version') <span class="validation-message small text-danger">{{ $message }}</span> @enderror
    </div>


</div>