<div wire:ignore>
    <select name="is_active" wire:model="is_active" class="form-control">
        <option value="1">Publish</option>
        <option value="0">Unpublish</option>
    </select>
    <div class="small form-text">
        <strong>Publish</strong>: this product is usable site-wide
        <br />
        <strong>Unpublish</strong>: this product disabled
    </div>
</div>