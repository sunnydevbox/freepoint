<div class="flex space-x-1 justify-around">
    <!--
    view: ?
    * EDIT http://localhost/products/103/edit
    * GET history http://localhost/products/history ?product_id=ID
    GET http://localhost/products/print_barcode  ex /products/print_barcode?data=06430-KT0-305+%2806430-KT0-305%29
    POST delete http://localhost/products/103
     -->

    <a class="p-1 text-black-500 hover:bg-black hover:text-white rounded .view"
        href="{{ route('products.show', [$id]) }}"
        title="View">
        <svg class="w-6 h-6"
            fill="none"
            stroke-width="1.5"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                stroke-linecap="round"
                stroke-linejoin="round" />
            <path d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                stroke-linecap="round"
                stroke-linejoin="round" />
        </svg>

    </a>

    <a class="p-1 text-yellow-500 hover:bg-yellow-500 hover:text-white rounded"
        href="{{ route('products.edit', [$id]) }}"
        title="Edit">
        <svg class=" w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z">
            </path>
        </svg>
    </a>

    <a class="p-1 text-black-500 hover:bg-black hover:text-white rounded"
        href="{{ route('products.history', ['product_id' => $id]) }}"
        title="History">
        <svg class="w-6 h-6"
            fill="none"
            stroke-width="1.5"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z"
                stroke-linecap="round"
                stroke-linejoin="round" />
        </svg>
    </a>

    <a class="p-1 text-black-500 hover:bg-black hover:text-white rounded"
        href="{{ route('product.printBarcode', ['data' => $code]) }}"
        title="Print Barcode">
        <svg class="w-6 h-6"
            fill="none"
            stroke-width="1.5"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0110.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0l.229 2.523a1.125 1.125 0 01-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0021 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 00-1.913-.247M6.34 18H5.25A2.25 2.25 0 013 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 011.913-.247m10.5 0a48.536 48.536 0 00-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5zm-3 0h.008v.008H15V10.5z"
                stroke-linecap="round"
                stroke-linejoin="round" />
        </svg>
    </a>

    @if (
        $this->currentUser->hasPermissionTo('products-delete') &&
            ($product->inWarehouses->sum('qty') == 0 &&
                $product->inPurchases->count() == 0 &&
                $product->inSales->count() == 0))
        @include('datatables::delete', ['value' => $id])
    @endif

</div>

@push('scripts')
    <script></script>
@endpush
