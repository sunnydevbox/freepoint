<div>
    <button class="btn btn-default btn-sm"
        id="print-purchase-btn"
        onClick="printData('print-purchase-details')"
        type="button">
        <i class="dripicons-print"></i>
        Print
    </button>
    <div class="container-fluid visible-print-block"
        id="print-purchase-details">
        <div class="row d-none">
            <div class=" mt-3 pb-2 border-bottom">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="modal-title text-center container-fluid"
                            id="exampleModalLabel">Freepoint</h3>
                    </div>
                    <div class="col-md-12 text-center">
                        <i style="font-size: 15px;">Product Purchase History</i>
                    </div>
                </div>
            </div>
            <table class="table table-bordered product-purchase-list ">
                <thead>
                    <tr>
                        <th>{{ trans('file.date') }}</th>
                        <th>{{ trans('file.reference') }}</th>
                        <th>{{ trans('file.Invoice Number') }}</th>
                        <th>{{ trans('file.Supplier') }}</th>
                        <th>{{ trans('file.Location') }}</th>
                        <th>{{ trans('file.qty') }}</th>
                        <th>{{ trans('file.On-hand') }}</th>
                        <th>{{ trans('file.Unit Price') }}</th>
                        <th>{{ trans('file.Subtotal') }}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($data as $key => $value)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($value->purchase->created_at)->toFormattedDateString() }}</td>
                            <td>{{ $value->purchase->reference_no }}</td>
                            <td>{{ $value->purchase->invoice_number }}</td>
                            <td>{{ $value->purchase->supplier?->name }}</td>
                            <td>
                                {{ $value->purchase->warehouse->name }}
                                @if (!empty($value->bin->name))
                                    : <strong>{{ $value->bin->name }}</strong>
                                @else
                                    : <em class="text-warning">No BIN assigned</em>
                                @endif
                            </td>
                            <td>{{ $value->qty }}</td>
                            <td>{{ $value->received }}</td>
                            <td>{{ $value->net_unit_cost }}</td>
                            <td>{{ formatMoney($value->total, true) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
