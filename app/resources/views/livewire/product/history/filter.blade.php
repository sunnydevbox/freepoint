<div>
    <div class="card-header mt-2">
        <h3 class="text-center">{{ trans('file.Product History') }}</h3>
    </div>
    <form>
        <div class="row ml-1">
            <input name="product_id"
                type="hidden">
            <div class="col-md-4">
                <div class="form-group">
                    <h4 class="mt-4">{{ $product->name . ' [' . $product->code . ']' }}</h4>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label><strong>{{ trans('file.Date') }}</strong></label>
                    <input class="daterangepicker-field form-control"
                        required
                        type="text"
                        value="{{ $startingDate }} To {{ $endingDate }}" />
                    <input name="starting_date"
                        type="hidden"
                        wire:change="set"
                        wire:model="startingDate" />
                    <input name="ending_date"
                        type="hidden"
                        wire:model="endingDate" />
                </div>
            </div>
            <div class="col-md-3"
                wire:ignore>
                <div class="form-group">
                    <label><strong>{{ trans('file.Warehouse') }}</strong></label>
                    <select class="selectpicker form-control"
                        data-live-search-style="begins"
                        data-live-search="true"
                        id="warehouse_id"
                        name="warehouse_id"
                        wire:model ="warehouseId">
                        <option value="0">{{ trans('file.All Warehouse') }}</option>
                        @foreach ($warehouses as $warehouse)
                            <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- <div class="col-md-2 mt-4">
                <div class="form-group">
                    <button
                        class="btn btn-primary"
                        id="filter-btn"
                        type="submit"
                    >{{ trans('file.submit') }}</button>
                </div>
            </div> --}}
        </div>
    </form>

</div>
<script>
    document.addEventListener('livewire:load', function() {
        // Your JS here.
        $(".daterangepicker-field").daterangepicker({
            callback: function(startDate, endDate, period) {


                var starting_date = startDate.format('YYYY-MM-DD 0:0:0');
                var ending_date = endDate.format('YYYY-MM-DD 23:59:59');
                var title = starting_date + ' To ' + ending_date;
                $(this).val(title);

                $('input[name="starting_date"]').val(starting_date);
                $('input[name="ending_date"]').val(ending_date);

                @this.startingDate = starting_date
                @this.endingDate = ending_date
            }

        });
    })
</script>
