<div>

    <section>
        <div class="container-fluid">
            <div class="card">
                <livewire:product.history.filter :product="$product" />
            </div>
        </div>
        <ul class="nav nav-tabs ml-4 mt-3"
            role="tablist">
            <li class="nav-item">
                <a class="nav-link active"
                    data-toggle="tab"
                    href="#product-sale"
                    role="tab">{{ trans('file.Sale') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                    data-toggle="tab"
                    href="#product-purchase"
                    role="tab">{{ trans('file.Purchase') }}</a>
            </li>
            {{-- <li class="nav-item">
                <a
                    class="nav-link"
                    href="#product-sale-return"
                    role="tab"
                    data-toggle="tab"
                >{{ trans('file.Sale Return') }}</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    href="#product-purchase-return"
                    role="tab"
                    data-toggle="tab"
                >{{ trans('file.Purchase Return') }}</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    href="#product-stock-adjustments"
                    role="tab"
                    data-toggle="tab"
                >Stock Adjustments</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    href="#product-stock-transfer"
                    role="tab"
                    data-toggle="tab"
                >Stock Transfer</a>
            </li> --}}
        </ul>
        <div class="tab-content">
            <!-- sale table -->
            <div class="tab-pane fade show active"
                id="product-sale"
                role="tabpanel">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <livewire:product.history.print.print-sale-history :productId="$productId" />
                                    <livewire:product.history.tabs.sale :productId="$productId"
                                        theme="bootstrap-4" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- sale table -->
            <div class="tab-pane fade show"
                id="product-purchase"
                role="tabpanel">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <livewire:product.history.print.print-purchase-history :productId="$productId" />
                                    <livewire:product.history.tabs.purchase-order :productId="$productId"
                                        theme="bootstrap-4" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            function printData(type) {
                var divContents = document.getElementById(type).innerHTML;
                var page = window.open("");
                page.document.write("<html>");
                page.document.write(
                    "<body><style>body{font-family: sans-serif;line-height: 1.15;-webkit-text-size-adjust: 100%;}.d-print-none{display:none}.text-center{text-align:center}.row{width:100%;margin-right: -15px;margin-left: -15px;}.col-md-12{width:100%;display:block;padding: 5px 15px;}.col-md-6{width: 50%;float:left;padding: 5px 15px;}table{width:100%;margin-top:30px;}th{text-aligh:left;}td{padding:10px}table, th, td{border: 1px solid black; border-collapse: collapse;}</style><style>@media print {.modal-dialog { max-width: 1000px;} }</style>"
                );
                page.document.write(divContents);
                page.document.write("</body></html>");
                page.document.close();
                setTimeout(function() {
                    page.close();
                }, 10);
                page.print();
            }
        </script>
    @endpush
</div>
