<div>
    <table
        id="sale-table"
        class="table table-hover"
        style="width: 100%"
    >
        <thead>
            <tr>
                <th class="not-exported-sale"></th>
                <th>{{ trans('file.Date') }}</th>
                <th>{{ trans('file.reference') }}</th>
                <th>{{ trans('file.Warehouse') }}</th>
                <th>{{ trans('file.customer') }}</th>
                <th>{{ trans('file.qty') }}</th>
                <th>{{ trans('file.Unit Price') }}</th>
                <th>{{ trans('file.Subtotal') }}</th>
            </tr>
        </thead>
    </table>
</div>
