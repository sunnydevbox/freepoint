<div>
    <button class="btn btn-default btn-sm"
        id="print-sale-btn"
        onClick="printData('print-sale-details')"
        type="button">
        <i class="dripicons-print"></i>
        Print
    </button>
    <div class="container-fluid visible-print-block"
        id="print-sale-details">
        <div class="row d-none">
            <div class=" mt-3 pb-2 border-bottom">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="modal-title text-center container-fluid"
                            id="exampleModalLabel">Freepoint</h3>
                    </div>
                    <div class="col-md-12 text-center">
                        <i style="font-size: 15px;">Product Sale History</i>
                    </div>
                </div>
            </div>
            <table class="table table-bordered product-sale-list">
                <thead>
                    <tr>
                        <th>{{ trans('file.date') }}</th>
                        <th>{{ trans('file.Reference No') }}</th>
                        <th>{{ trans('file.Receipt No') }}</th>
                        <th>{{ trans('file.Location') }}</th>
                        <th>{{ trans('file.customer') }}</th>
                        <th>{{ trans('file.qty') }}</th>
                        <th>{{ trans('file.Unit Price') }}</th>
                        <th>{{ trans('file.Subtotal') }}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($data as $key => $value)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($value->sale->created_at)->toFormattedDateString() }}</td>
                            <td>{{ $value->sale->reference_no }}</td>
                            <td>{{ $value->sale->receipt_no }}</td>
                            <td>
                                {{ $value->sale->warehouse->name }}
                                @if (!empty($value->bin->name))
                                    : <strong>{{ $value->bin->name }}</strong>
                                @else
                                    : <em class="text-warning">No BIN assigned</em>
                                @endif
                            </td>
                            <td>{{ $value->sale->customer->name }}</td>
                            <td>{{ $value->qty }}</td>
                            <td>{{ $value->net_unit_price }}</td>
                            <td>{{ formatMoney($value->total, true) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
