<table class="table-border table">    
    @foreach ($productInWarehouse as $product)
        <tr>
            <td>
                {{ $product['warehouse_name'] }} : {{ $product['bin_name'] ?: 'Not specified' }}
            </td>
            <td>
                {{  $product['sum'] }} 
            </td>
        </tr>
    @endforeach
</table>
