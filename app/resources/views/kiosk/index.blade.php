@extends('kiosk.layout.main')


@section('content')
    <div class="container">
        <h1 class="mb-10 text-center">Kiosk</h1>
        <livewire:kiosk.searchbar />
    </div>
@endsection
