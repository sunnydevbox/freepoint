@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Hmmm... something went wrong</h1>
            <p class="lead">There was a problem processing your request.</p>
            <hr class="my-4">
            <p class="lead">
                Let's try <a href="{{ url('/')}}">again</a>
            </p>
        </div>
    </div>
@endsection
