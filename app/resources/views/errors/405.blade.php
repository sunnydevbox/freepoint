@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Oops... <br />we don't know how to process your request</h1>
            <p class="lead">Method not allowed</p>
            <hr class="my-4">
            <p class="lead">
                Let's try <a href="{{ url()->previous() }}">again</a>
            </p>
        </div>
    </div>
@endsection
