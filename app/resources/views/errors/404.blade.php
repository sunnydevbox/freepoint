@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Oops... we can't find that page</h1>
            <p class="lead">The page your are trying to access does not exist.</p>
            <hr class="my-4">
            <p class="lead">
                Let's try <a href="{{ url()->previous() }}">again</a>
            </p>
        </div>
    </div>
@endsection
