<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1.0"
            name="viewport">
        <title>Sale Details</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 40px;
            }

            h1 {
                text-align: right;
            }

            strong {
                font-size: 14px
            }

            .row {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .billing-info {
                margin-top: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th {
                border-bottom: 1px solid black;
                padding: 10px;
                text-align: left;
                font-weight: 100;
                color: rgb(117, 115, 115);
                font-size: 12px
            }

            th {
                background-color: lightgray;
            }

            .text-right {
                text-align: right;
                margin-top: 20px;
            }

            img {
                width: 200px;
            }

            td {
                font-size: 10px;
            }
        </style>
    </head>

    <body onload="window.print()">
        <div class="row">
            <div>
                <img alt=""
                    alt="Logo"
                    src="{{ asset('images/print/fp-logo.png') }}">

            </div>
            <div class="text-right">
                <h1>Invoice</h1>
                <h4 style="color: red">Sale Details</h4>
            </div>
        </div>
        <div class="row">
            <div>
                <p>Freepoint Motorcycles & Parts Inc.</p>
                <p>A.S. Fortuna, Mandaue, Cebu Philippines 6014</p>
            </div>
            <div class="text-right">
                <strong>INVOICE</strong> <br>
                <strong>Invoice #:</strong> {{ $sale->receipt_no }} <br>
                <strong>Date:</strong> {{ dateFormat($sale->created_at, 'M. d, Y') }}
            </div>
        </div>
        <div class="billing-info">
            <strong>Billing Address:</strong> {{ $sale->biller->name ?? 'No Name' }} <br>
            <br>
            <strong>Contact:</strong> {{ $sale->customer->phone_number ?? 'No Contact' }}
        </div>

        <table>
            <thead>
                <tr>
                    <th>ITEM</th>
                    <th>DESCRIPTION</th>
                    <th>QUANTITY</th>
                    <th>UNIT PRICE</th>
                    <th>DISCOUNT</th>
                    <th>SUB-TOTAL</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sale->items as $item)
                    <tr>
                        <td>{{ $item->product->code ?? 'No Item' }}</td>
                        <td>{{ $item->product->name ?? 'No Product' }}</td>
                        <td>{{ $item->qty }}</td>
                        <td>Php {{ formatMoney($item->net_unit_price) }}
                        </td>
                        <td>{{ $item->product->discount ?? 0 }}%</td>
                        <td>
                            Php {{ formatMoney($item->total) }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
            <strong>SUB-TOTAL:</strong>
            {{ formatMoney($sale->grand_total) }}
            <br>
            <br>
            <strong>TOTAL:</strong>
            {{ formatMoney($sale->grand_total) }}
        </div>
    </body>

</html>
