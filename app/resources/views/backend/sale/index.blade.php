@extends('backend.layout.main') @section('content')
    @if (session()->has('message'))
        <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close"
                data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{!! session()->get('message') !!}
        </div>
    @endif
    @if (session()->has('not_permitted'))
        <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert"
                aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}
        </div>
    @endif

    <section>
        <div class="container-fluid">
            @if (in_array('sales-add', $all_permission))
                <a href="{{ route('sales.create') }}" class="btn btn-info"><i class="dripicons-plus"></i>
                    {{ trans('file.Add Sale') }}
                </a>&nbsp;
            @endif
            <div class="row mt-5" style="overflow:auto">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <livewire:sales.browse theme="bootstrap-4" />
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </section>


@endsection

@push('scripts')
    <script src="{{asset('js/print.js')}}"></script>
@endpush