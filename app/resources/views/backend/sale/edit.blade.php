@extends('backend.layout.main') @section('content')
    @if (session()->has('not_permitted'))
        <div class="alert alert-danger alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
    @endif
    <section class="forms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <livewire:sales.form :saleOrder="$lims_sale_data" />
                </div>
            </div>
        </div>
    </section>
@endsection