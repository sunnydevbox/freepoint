@extends('backend.layout.main') @section('content')
    @if (session()->has('create_message'))
        <div class="alert alert-success alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('create_message') }}</div>
    @endif
    @if (session()->has('edit_message'))
        <div class="alert alert-success alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('edit_message') }}</div>
    @endif
    @if (session()->has('import_message'))
        <div class="alert alert-success alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('import_message') }}</div>
    @endif
    @if (session()->has('not_permitted'))
        <div class="alert alert-danger alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
    @endif
    @if (session()->has('message'))
        <div class="alert alert-danger alert-dismissible text-center"><button
                type="button"
                class="close"
                data-dismiss="alert"
                aria-label="Close"
            ><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div>
    @endif

    <section>
        <div class="container-fluid">
            @if (in_array('products-add', $all_permission))
                <a
                    href="{{ route('products.create') }}"
                    class="btn btn-info"
                ><i class="dripicons-plus"></i> {{ __('file.add_product') }}</a>
                <a
                    href="#"
                    data-toggle="modal"
                    data-target="#importProduct"
                    class="btn btn-primary"
                ><i class="dripicons-copy"></i> {{ __('file.import_product') }}</a>
            @endif
        </div>
        <div class="table-responsiv">
            <div
                id="product-data-table_wrapper"
                class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
            >
                <div class="card">
                    <div class="card-body">
                        <livewire:product.browse />
                    </div>
                </div>
                {{-- <livewire:product.browse /> --}}
            </div>
        </div>
    </section>


    @push('styles')
        <style>
            #product-data-table_wrapper .table-row:first-child div:first-child div {
                display: none;
            }

            #product-data-table_wrapper .table-row:first-child div button {
                padding: 2px !important;
                /* height: auto; */
                text-align: center;
            }

            #product-data-table_wrapper .table-row:nth-child(2) div:first-child {
                padding: 5px !important;
            }

            #product-data-table_wrapper .datatable-footer {}
        </style>
    @endpush
@endsection
