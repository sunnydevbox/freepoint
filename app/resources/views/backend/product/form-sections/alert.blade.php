<div class="row">
    {{--
    <div class="col-md-7">
        <div class="form-group">
            <label>{{trans('file.Daily Sale Objective')}}</strong></label> <i class="dripicons-question" data-toggle="tooltip" title="{{trans('file.Minimum qty which must be sold in a day. If not, you will be notified on dashboard. But you have to set up the cron job properly for that. Follow the documentation in that regard.')}}"></i>
            <input type="number" name="daily_sale_objective" class="form-control" step="any">
        </div>
    </div>
    --}}
    <div id="alert-qty" class="col-md-6">
        <div class="form-group">
            <label>{{trans('file.Alert Quantity')}}</strong> </label>
            <input type="number" name="alert_quantity" class="form-control" step="any">
        </div>
    </div>
</div>