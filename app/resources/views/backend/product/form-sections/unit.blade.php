<div class="row ">
    <div class="col-md-12 form-group">
        <label>{{trans('file.Product Unit')}} *</strong> </label>
        <div class="input-group">
            <select required class="form-control selectpicker" name="unit_id">
                <option value="" disabled selected>Select Product Unit...</option>
                @foreach($lims_unit_list as $unit)
                    @if($unit->base_unit==null)
                        <option value="{{$unit->id}}">{{$unit->unit_name}}</option>
                    @endif
                @endforeach
            </select>
            <input type="hidden" name="unit" value="{{ extractProductFieldValue($lims_product_data, 'unit_id') }}">
        </div>
        <span class="validation-msg"></span>
    </div>
    <div class="col-md-6">
        <label>{{trans('file.Sale Unit')}}</strong> </label>
        <div class="input-group">
            <select class="form-control selectpicker" name="sale_unit_id">
            </select>
            <input type="hidden" name="sale_unit" value="{{ extractProductFieldValue($lims_product_data, 'sale_unit_id') }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{trans('file.Purchase Unit')}}</strong> </label>
            <div class="input-group">
                <select class="form-control selectpicker" name="purchase_unit_id">
                <input type="hidden" name="purchase_unit" value="{{ extractProductFieldValue($lims_product_data, 'purchase_unit_id') }}">
                </select>
            </div>
        </div>
    </div>
</div>