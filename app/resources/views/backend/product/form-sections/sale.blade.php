<div class="row ">
    <div id="cost" class="col-md-6">
        <div class="form-group">
            <label>{{trans('file.Product Cost')}} *</strong> </label>
            <input type="number" name="cost" required class="form-control" value="{{ extractProductFieldValue($lims_product_data, 'cost') }}" step="any">
            <span class="validation-msg"></span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{trans('file.Product Price')}} *</strong> </label>
            <input type="number" name="price" required class="form-control" value="{{ extractProductFieldValue($lims_product_data, 'price') }}" step="any">
            <span class="validation-msg"></span>
        </div>
        <div class="form-group">
            <input type="hidden" name="qty" value="{{number_format(0, $general_setting->decimal, '.', '')}}">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>{{trans('file.Product Tax')}}</strong> </label>
            <input type="hidden" name="tax" value="{{ extractProductFieldValue($lims_product_data, 'tax_id') }}">
            <select name="tax_id" class="form-control selectpicker">
                <option value="">No Tax</option>
                @foreach($lims_tax_list as $tax)
                <option value="{{$tax->id}}">{{$tax->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="hidden" name="tax_method_id" value="{{ extractProductFieldValue($lims_product_data, 'tax_method') }}">
            <label>{{trans('file.Tax Method')}}</strong> </label> <i class="dripicons-question" data-toggle="tooltip" title="{{trans('file.Exclusive: Poduct price = Actual product price + Tax. Inclusive: Actual product price = Product price - Tax')}}"></i>
            <select name="tax_method" class="form-control selectpicker">
                <option value="1">{{trans('file.Exclusive')}}</option>
                <option value="2">{{trans('file.Inclusive')}}</option>
            </select>
        </div>
    </div>
</div>