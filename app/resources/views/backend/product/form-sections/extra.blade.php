<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="checkbox" name="featured" value="1">&nbsp;
            <label>{{trans('file.Featured')}}</label>
            <p class="italic">{{trans('file.Featured product will be displayed in POS')}}</p>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="checkbox" name="is_embeded" value="1">&nbsp;
            <label>{{trans('file.Embedded Barcode')}} <i class="dripicons-question" data-toggle="tooltip" title="{{trans('file.Check this if this product will be used in weight scale machine.')}}"></i></label>
        </div>
    </div>




    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4" id="promotion_price">
                <label>{{trans('file.Promotional Price')}}</label>
                <input type="number" name="promotion_price" class="form-control" step="any" />
            </div>
            <div class="col-md-4" id="start_date">
                <div class="form-group">
                    <label>{{trans('file.Promotion Starts')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="dripicons-calendar"></i></div>
                        </div>
                        <input type="text" name="starting_date" id="starting_date" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="last_date">
                <div class="form-group">
                    <label>{{trans('file.Promotion Ends')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="dripicons-calendar"></i></div>
                        </div>
                        <input type="text" name="last_date" id="ending_date" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>