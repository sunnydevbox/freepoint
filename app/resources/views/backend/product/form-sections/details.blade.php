<div class="row">
    {{--<div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Product Type')}} *</strong> </label>
            <div class="input-group">
                <select name="type" required class="form-control selectpicker" id="type">
                    <option value="standard">Standard</option>
                    <!-- <option value="combo">Combo</option>
                    <option value="digital">Digital</option>
                    <option value="service">Service</option> -->
                </select>
                <input type="hidden" name="type_hidden" value="{{ extractProductFieldValue($lims_product_data, 'type') }}">
            </div>
        </div>
    </div>
    --}}
    <div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Product Name')}} *</strong> </label>
            <input type="text" name="name" class="form-control" id="name" value="{{ extractProductFieldValue($lims_product_data, 'name') }}" aria-describedby="name" required>
            <span class="validation-msg" id="name-error"></span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Product Code')}} *</strong> </label>
            <div class="input-group">
                <input type="text" name="code" class="form-control" id="code"  value="{{extractProductFieldValue($lims_product_data, 'code')}}" aria-describedby="code" required>
                <div class="input-group-append">
                    <button id="genbutton" type="button" class="btn btn-sm btn-default" title="{{trans('file.Generate')}}"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
            <span class="validation-msg" id="code-error"></span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Model')}} *</strong> </label>
            <input type="text" name="model" class="form-control" id="model" aria-describedby="model" value="{{extractProductFieldValue($lims_product_data, 'model')}}">
            <span class="validation-msg" id="name-error"></span>


            <div style="display: none">
                <label>{{trans('file.Barcode Symbology')}} *</strong> </label>
                <div class="input-group">
                    <select name="barcode_symbology" required class="form-control selectpicker">
                        <option value="C128">Code 128</option>
                        <option value="C39">Code 39</option>
                        <option value="UPCA">UPC-A</option>
                        <option value="UPCE">UPC-E</option>
                        <option value="EAN8">EAN-8</option>
                        <option value="EAN13">EAN-13</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div id="digital" class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Attach File')}} *</strong> </label>
            <div class="input-group">
                <input type="file" name="file" class="form-control">
            </div>
            <span class="validation-msg"></span>
        </div>
    </div>
    <div id="combo" class="col-md-9 mb-1">
        <label>{{trans('file.add_product')}}</label>
        <div class="search-box input-group mb-3">
            <button class="btn btn-secondary"><i class="fa fa-barcode"></i></button>
            <input type="text" name="product_code_name" id="lims_productcodeSearch" placeholder="Please type product code and select..." class="form-control" />
        </div>
        <label>{{trans('file.Combo Products')}}</label>
        <div class="table-responsive">
            <table id="myTable" class="table table-hover order-list">
                <thead>
                    <tr>
                        <th>{{trans('file.product')}}</th>
                        <th>{{trans('file.Quantity')}}</th>
                        <th>{{trans('file.Unit Price')}}</th>
                        <th><i class="dripicons-trash"></i></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Brand')}}</strong> </label>
            <div class="input-group">
                <input type="hidden" name="brand" value="{{ extractProductFieldValue($lims_product_data, 'brand_id')}}">
                <select name="brand_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Select Brand...">
                    @foreach($lims_brand_list as $brand)
                    <option value="{{$brand->id}}">{{$brand->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.category')}} *</strong> </label>
            <div class="input-group">
                <input type="hidden" name="category" value="{{ extractProductFieldValue($lims_product_data, 'category_id') }}">
                <select name="category_id" required class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Select Category...">
                    @foreach($lims_category_list as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <span class="validation-msg"></span>
        </div>
    </div>

    <div id="alert-qty" class="col-md-4">
        <div class="form-group">
            <label>{{trans('file.Alert Quantity')}}</strong> </label>
            <input type="number" name="alert_quantity" class="form-control" step="any">
        </div>
    </div>

    {{--
    <div class="col-md-12">
        <div class="form-group">
            <label>{{trans('file.Product Details')}}</label>
            <textarea name="product_details" class="form-control" rows="3">
                @if ($lims_product_data)
                    {{str_replace('@', '"', $lims_product_data->product_details)}}
                @endif
            </textarea>
        </div>
    </div>
    --}}
</div>