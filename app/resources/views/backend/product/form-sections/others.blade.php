    <div class="col-md-12 mt-3" id="variant-option">
        <span><input name="is_variant" type="checkbox" id="is-variant" value="1">&nbsp; {{trans('file.This product has variant')}}</span>
    </div>
    <div class="col-md-12" id="variant-section">
        <div class="row" id="variant-input-section">
            <div class="col-md-4 form-group mt-2">
                <label>{{trans('file.Option')}} *</label>
                <input type="text" name="variant_option[]" class="form-control variant-field" placeholder="Size, Color etc...">
            </div>
            <div class="col-md-6 form-group mt-2">
                <label>{{trans('file.Value')}} *</label>
                <input type="text" name="variant_value[]" class="type-variant form-control variant-field">
            </div>
        </div>
        <div class="col-md-12 form-group">
            <button type="button" class="btn btn-info add-more-variant"><i class="dripicons-plus"></i> {{trans('file.Add More Variant')}}</button>
        </div>
        <div class="table-responsive ml-2">
            <table id="variant-table" class="table table-hover variant-list">
                <thead>
                    <tr>
                        <th>{{trans('file.name')}}</th>
                        <th>{{trans('file.Item Code')}}</th>
                        <th>{{trans('file.Additional Cost')}}</th>
                        <th>{{trans('file.Additional Price')}}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 mt-2" id="diffPrice-option">
        <span><input name="is_diffPrice" type="checkbox" id="is-diffPrice" value="1">&nbsp; {{trans('file.This product has different price for different warehouse')}}</span>
    </div>
    <div class="col-md-12" id="diffPrice-section">
        <div class="table-responsive ml-2">
            <table id="diffPrice-table" class="table table-hover">
                <thead>
                    <tr>
                        <th>{{trans('file.Warehouse')}}</th>
                        <th>{{trans('file.Price')}}</th>
                    </tr>
                    @foreach($lims_warehouse_list as $warehouse)
                    <tr>
                        <td>
                            <input type="hidden" name="warehouse_id[]" value="{{$warehouse->id}}">
                            {{$warehouse->name}}
                        </td>
                        <td><input type="number" name="diff_price[]" class="form-control"></td>
                    </tr>
                    @endforeach
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 mt-3" id="batch-option">
        <span><input name="is_batch" type="checkbox" id="is-batch" value="1">&nbsp; {{trans('file.This product has batch and expired date')}}</span>
    </div>
    {{--
        <div class="col-md-12 mt-3" id="imei-option">
            <span><input name="is_imei" type="checkbox" id="is-imei" value="1">&nbsp; {{trans('file.This product has IMEI or Serial numbers')}}</span>
        </div>
    --}}
    {{--
    <div class="col-md-12 mt-3">
        <input name="promotion" type="checkbox" id="promotion" value="1">&nbsp;
        <label><span> {{trans('file.Add Promotional Price')}}</span></label>
    </div>
    --}}