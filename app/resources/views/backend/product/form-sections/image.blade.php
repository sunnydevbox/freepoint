<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>{{trans('file.Product Image')}}</strong> </label> <i class="dripicons-question" data-toggle="tooltip" title="{{trans('file.You can upload multiple image. Only .jpeg, .jpg, .png, .gif file can be uploaded. First image will be base image.')}}"></i>
            <div id="imageUpload" class="dropzone"></div>
            <span class="validation-msg" id="image-error"></span>
        </div>
    </div>

</div>