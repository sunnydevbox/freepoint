@extends('backend.layout.main')
@section('content')

    <section>
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route' => 'warehouse.bins.store', 'method' => 'post']) !!}



            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">{{ trans('file.Add Warehouse') }}</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i
                            class="dripicons-cross"></i></span></button>
            </div>
            <div class="modal-body">
                <p class="italic">
                    <small>{{ trans('file.The field labels marked with * are required input fields') }}.</small>
                </p>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>{{ trans('file.name') }}</label>
                                <input type="text" placeholder="Descriptive name for this bin" name="name"
                                    required="required" value="{{ old('name') }}" class="form-control" maxlength="100">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>{{ trans('file.Warehouse') }} *</label>
                                <select required name="warehouse_id" class="selectpicker form-control"
                                    data-live-search="true" title="Select warehouse...">
                                    @foreach ($warehouses as $warehouseId => $warehouseName)
                                        <option value="{{ $warehouseId }}"
                                            {{ $warehouseId == old('warehouse_id') ? 'selected' : '' }}>
                                            {{ $warehouseName }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3><strong>Bin's Physical Position</strong></h3>
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label>Aisle</label>
                                <input type="text" name="aisle" value="{{ old('aisle') }}" class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Rack</label>
                                <input type="text" name="rack" value="{{ old('rack') }}" class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Shelf</label>
                                <input type="text" name="shelf" value="{{ old('shelf') }}" class="form-control"
                                    maxlength="100">
                            </div>

                            <div class="form-group col-sm-3">
                                <label>Bin</label>
                                <input type="text" name="bin" value="{{ old('bin') }}" class="form-control"
                                    maxlength="100">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" value="{{ trans('file.submit') }}" class="btn btn-primary">
                </div>
            </div>
            {{ Form::close() }}
        </div>
        </div>
        </div>
    @endsection

    @push('scripts')
        <script type="text/javascript">
            $("ul#setting").siblings('a').attr('aria-expanded', 'true');
            $("ul#setting").addClass("show");
            $("ul#setting #warehouse-menu").addClass("active");

            var warehouse_id = [];
            var user_verified = <?php echo json_encode(env('USER_VERIFIED')); ?>;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function confirmDelete() {
                if (confirm("Are you sure want to delete?")) {
                    return true;
                }
                return false;
            }

            $(document).ready(function() {

                $(document).on('click', '.open-EditWarehouseDialog', function() {
                    var url = "warehouse/"
                    var id = $(this).data('id').toString();
                    url = url.concat(id).concat("/edit");

                    $.get(url, function(data) {
                        $("#editModal input[name='name']").val(data['name']);
                        $("#editModal input[name='phone']").val(data['phone']);
                        $("#editModal input[name='email']").val(data['email']);
                        $("#editModal textarea[name='address']").val(data['address']);
                        $("#editModal input[name='warehouse_id']").val(data['id']);

                    });
                });
            });

            $('#warehouse-table').DataTable({
                "order": [],
                'language': {
                    'lengthMenu': '_MENU_ {{ trans('file.records per page') }}',
                    "info": '<small>{{ trans('file.Showing') }} _START_ - _END_ (_TOTAL_)</small>',
                    "search": '{{ trans('file.Search') }}',
                    'paginate': {
                        'previous': '<i class="dripicons-chevron-left"></i>',
                        'next': '<i class="dripicons-chevron-right"></i>'
                    }
                },
                'columnDefs': [{
                        "orderable": false,
                        'targets': [0, 5, 6, 7]
                    },
                    {
                        'render': function(data, type, row, meta) {
                            if (type === 'display') {
                                data =
                                    '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                            }

                            return data;
                        },
                        'checkboxes': {
                            'selectRow': true,
                            'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                        },
                        'targets': [0]
                    }
                ],
                'select': {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'lengthMenu': [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                dom: '<"row"lfB>rtip',
                buttons: [{
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible:Not(.not-exported)',
                            rows: ':visible'
                        },
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible:Not(.not-exported)',
                            rows: ':visible'
                        },
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible:Not(.not-exported)',
                            rows: ':visible'
                        },
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible:Not(.not-exported)',
                            rows: ':visible'
                        },
                    },
                    {
                        text: '<i title="delete" class="dripicons-cross"></i>',
                        className: 'buttons-delete',
                        action: function(e, dt, node, config) {
                            if (user_verified == '1') {
                                warehouse_id.length = 0;
                                $(':checkbox:checked').each(function(i) {
                                    if (i) {
                                        warehouse_id[i - 1] = $(this).closest('tr').data('id');
                                    }
                                });
                                if (warehouse_id.length && confirm("Are you sure want to delete?")) {
                                    $.ajax({
                                        type: 'POST',
                                        url: 'warehouse/deletebyselection',
                                        data: {
                                            warehouseIdArray: warehouse_id
                                        },
                                        success: function(data) {
                                            alert(data);
                                        }
                                    });
                                    dt.rows({
                                        page: 'current',
                                        selected: true
                                    }).remove().draw(false);
                                } else if (!warehouse_id.length)
                                    alert('No warehouse is selected!');
                            } else
                                alert('This feature is disable for demo!');
                        }
                    },
                    {
                        extend: 'colvis',
                        columns: ':gt(0)'
                    },
                ],
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#select_all").on("change", function() {
                if ($(this).is(':checked')) {
                    $("tbody input[type='checkbox']").prop('checked', true);
                } else {
                    $("tbody input[type='checkbox']").prop('checked', false);
                }
            });

            $("#export").on("click", function(e) {
                e.preventDefault();
                var warehouse = [];
                $(':checkbox:checked').each(function(i) {
                    warehouse[i] = $(this).val();
                });
                $.ajax({
                    type: 'POST',
                    url: '/exportwarehouse',
                    data: {

                        warehouseArray: warehouse
                    },
                    success: function(data) {
                        alert('Exported to CSV file successfully! Click Ok to download file');
                        window.location.href = data;
                    }
                });
            });
        </script>
    @endpush
