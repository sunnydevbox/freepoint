@extends('backend.layout.main')
@section('content')
    @if ($errors->has('name'))
        <div class="alert alert-danger alert-dismissible text-center">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>{{ $errors->first('name') }}
        </div>
    @endif
    @if (session()->has('message'))
        <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close"
                data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div>
    @endif
    @if (session()->has('not_permitted'))
        <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close"
                data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
    @endif

    <section>

        <div class="container-fluid mb-5">
            <a href="{{ route('warehouse.bins.create') }}"  class="btn btn-info"><i
                    class="dripicons-plus"></i> Add Bin Location</a>
        </div>

        <div class="container-fluid">
            <livewire:warehouse.bins.browse theme="bootstrap-4"/>
        </div>
    </section>
@endsection
