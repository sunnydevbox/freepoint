@extends('backend.layout.main') @section('content')

    <section class="forms">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header mt-2">
                    <h3 class="text-center">{{ trans('file.Sale Report') }}</h3>
                </div>
                <div class="row sale-report-filter">
                    <div class="col-md-12">
                        <div class="row p-4">
                            <div class="col-md-4">
                                <div class="form-group text-center">
                                    <label class="d-tc mt-2"><strong>{{ trans('file.Choose Your Date') }}</strong> &nbsp;</label>
                                    <div class="d-tc">
                                        <div class="input-group">
                                            <input class="daterangepicker-field form-control"
                                                required
                                                type="text"
                                                value="{{ $startDate }} To {{ $endDate }}" />
                                            <input name="start_date"
                                                type="hidden"
                                                value="{{ $startDate }}" />
                                            <input name="end_date"
                                                type="hidden"
                                                value="{{ $endDate }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group text-center">
                                    <label class="d-tc mt-2"><strong>{{ trans('file.Choose Warehouse') }}</strong> &nbsp;</label>
                                    <div class="d-tc">
                                        <input name="warehouse_id_hidden"
                                            type="hidden"
                                            value="{{ $warehouseId }}" />
                                        <select class="selectpicker form-control"
                                            data-live-search-style="begins"
                                            data-live-search="true"
                                            id="warehouse_id"
                                            name="warehouse_id">
                                            <option value="0">{{ trans('file.All Warehouse') }}</option>
                                            @foreach ($warehouses as $warehouse)
                                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group text-center">
                                    <label class="d-tc mt-2"><strong>Choose Brand</strong> &nbsp;</label>
                                    <div class="d-tc">
                                        <select class="selectpicker form-control"
                                            data-live-search-style="begins"
                                            data-live-search="true"
                                            id="brand_id"
                                            name="brand_id">
                                            <option value="0">All Brand</option>
                                            @foreach ($brands as $brand)
                                                <option value="{{ $brand->id }}" {{ request('brand_id') == $brand->id ? 'selected' : '' }}>
                                                    {{ $brand->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped dataTable w-100" 
                id="report-table">
                <thead>
                    <tr>
                        <th>SO</th>
                        <th>Name</th>
                        <th>Warehouse</th>
                        <th>Sale Item</th>
                        <th>Sold amount</th>
                        <th>Sold Qty</th>
                        <th>Stock in bin</th>
                    </tr>
                </thead>
            </table>
        </div>
    </section>

@endsection

@push('scripts')
   <script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("ul#report").siblings('a').attr('aria-expanded', 'true');
        $("ul#report").addClass("show");
        $("ul#report #purchase-report-menu").addClass("active");

        $('#warehouse_id').val($('input[name="warehouse_id_hidden"]').val());
        $('.selectpicker').selectpicker('refresh');
        var table = $('#report-table').DataTable({
            processing: true,
            serverSide: true, 
            "ajax": {
                url: "{{ route('report.sale-data') }}",
                type: "post",
                data: function (param) {
                    param.start_date = $('input[name="start_date"]').val();
                    param.end_date = $('input[name="end_date"]').val();
                    param.warehouse_id = $('#warehouse_id').val();
                    param.brand_id = $('#brand_id').val();
                },
                dataType: "json"
            },
            'language': {
                'lengthMenu': '_MENU_ {{ trans('file.records per page') }}',
                "info": '<small>{{ trans('file.Showing') }} _START_ - _END_ (_TOTAL_)</small>',
                "search": '{{ trans('file.Search') }}',
                'paginate': {
                    'previous': '<i class="dripicons-chevron-left"></i>',
                    'next': '<i class="dripicons-chevron-right"></i>'
                }
            },
            columns: [
                { "data": "SO" },
                { "data": "productName" },
                { "data": "warehouse" },
                { "data": "saleItem" },
                { "data": "soldAmount" },
                { "data": "soldQty" },
                { "data": "stockInBins" }
            ],
            'lengthMenu': [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: '<"row"lfB>rtip',
            buttons: [
                {
                    extend: 'pdf',
                    text: '<i title="export to pdf" class="fa fa-file-pdf-o"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function (e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'excel',
                    text: '<i title="export to excel" class="dripicons-document-new"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function (e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'csv',
                    text: '<i title="export to csv" class="fa fa-file-text-o"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function (e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'print',
                    text: '<i title="print" class="fa fa-print"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function (e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.print.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'colvis',
                    text: '<i title="column visibility" class="fa fa-eye"></i>',
                    columns: ':gt(0)'
                }
            ]
        });

        function updateReportTable() {
            table.ajax.reload(null, false); 
        }


        $(".sale-report-filter select, .sale-report-filter input").on('change', function () {
            updateReportTable();
        });

        $(".daterangepicker-field").daterangepicker({
            callback: function (startDate, endDate, period) {
                var start_date = startDate.format('YYYY-MM-DD');
                var end_date = endDate.format('YYYY-MM-DD');
                var title = start_date + ' To ' + end_date;
                $(this).val(title);
                $('input[name="start_date"]').val(start_date);
                $('input[name="end_date"]').val(end_date);

             
                updateReportTable();
            }
        });

        function datatable_sum(dt_selector, is_calling_first) {
            if (dt_selector.rows('.selected').any() && is_calling_first) {
                var rows = dt_selector.rows('.selected').indexes();

                $(dt_selector.column(2).footer()).html(
                    dt_selector.cells(rows, 2, { page: 'current' }).data().sum().toFixed({{ $general_setting->decimal }})
                );
                $(dt_selector.column(3).footer()).html(
                    dt_selector.cells(rows, 3, { page: 'current' }).data().sum()
                );
                $(dt_selector.column(4).footer()).html(
                    dt_selector.cells(rows, 4, { page: 'current' }).data().sum().toFixed({{ $general_setting->decimal }})
                );
            } else {
                $(dt_selector.column(2).footer()).html(
                    dt_selector.column(2, { page: 'current' }).data().sum().toFixed({{ $general_setting->decimal }})
                );
                $(dt_selector.column(3).footer()).html(
                    dt_selector.column(3, { page: 'current' }).data().sum()
                );
                $(dt_selector.column(4).footer()).html(
                    dt_selector.column(4, { page: 'current' }).data().sum().toFixed({{ $general_setting->decimal }})
                );
            }
        }
    });
</script>

@endpush
