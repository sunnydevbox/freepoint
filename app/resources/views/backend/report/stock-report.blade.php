@extends('backend.layout.main') @section('content')
<section class="forms">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header mt-2">
                <h3 class="text-center">Stock Report</h3>
                <livewire:reports.stock-report />
            </div>
        </div>
    </div>  
</section>



@endsection
