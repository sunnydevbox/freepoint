@extends('backend.layout.main') @section('content')

    @if (empty($products))
        <div class="alert alert-danger alert-dismissible text-center"><button aria-label="Close"
                class="close"
                data-dismiss="alert"
                type="button"><span aria-hidden="true">&times;</span></button>{{ 'No Data exist between this date range!' }}
        </div>
    @endif

    <section class="forms">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header mt-2">
                    <h3 class="text-center">{{ trans('file.Purchase Report') }}</h3>
                </div>
                {!! Form::open(['route' => 'report.purchase', 'method' => 'post']) !!}
                <div class="row mb-3">
                    <div class="col-md-4 offset-md-2 mt-3">
                        <div class="form-group row">
                            <label class="d-tc mt-2"><strong>{{ trans('file.Choose Your Date') }}</strong> &nbsp;</label>
                            <div class="d-tc">
                                <div class="input-group">
                                    <input class="daterangepicker-field form-control"
                                        required
                                        type="text"
                                        value="{{ $startDate }} To {{ $endDate }}" />
                                    <input name="start_date"
                                        type="hidden"
                                        value="{{ $startDate }}" />
                                    <input name="end_date"
                                        type="hidden"
                                        value="{{ $endDate }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-3">
                        <div class="form-group row">
                            <label class="d-tc mt-2"><strong>{{ trans('file.Choose Warehouse') }}</strong> &nbsp;</label>
                            <div class="d-tc">
                                <input name="warehouse_id_hidden"
                                    type="hidden"
                                    value="{{ $warehouseId }}" />
                                <select class="selectpicker form-control"
                                    data-live-search-style="begins"
                                    data-live-search="true"
                                    id="warehouse_id"
                                    name="warehouse_id">
                                    <option value="0">{{ trans('file.All Warehouse') }}</option>
                                    @foreach ($warehouses as $warehouse)
                                        <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 mt-3">
                        <div class="form-group">
                            <button class="btn btn-primary"
                                type="submit">{{ trans('file.submit') }}</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="table-responsive mb-4">
            <table class="table table-hover"
                id="report-table">
                <thead>
                    <tr>
                        <th class="not-exported"></th>
                        <th>PO</th>
                        <th>{{ trans('file.Product Name') }}</th>
                        <th>Warehouse</th>
                        <th>{{ trans('file.Purchase Amount') }}</th>
                        <th>{{ trans('file.Purchased Qty') }}</th>
                        <th>Stock in bin</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($products))
                        @foreach ($products as $key => $warehosueProduct)
                            <tr>
                                <td>{{ $warehosueProduct->id }}</td>
                                <td>
                                    <strong>
                                        {{ $warehosueProduct->purchaseItem->purchase->reference_no }}
                                        <br />
                                        {{ $warehosueProduct->purchaseItem->purchase->id }}
                                    </strong>
                                    <br />
                                    {{ $warehosueProduct->purchaseItem->purchase->created_at->format('M d, Y h:i A') }}
                                </td>
                                <td>
                                    {{-- {{ $warehosueProduct->purchaseItem->id }}<br />
                                    {{ $warehosueProduct->product->id }} - --}}

                                    {{ $warehosueProduct->product->name }}</td>
                                <td>
                                    {{-- {{ $warehosueProduct->warehouse?->id }} - {{ $warehosueProduct->bin?->id }} / --}}

                                    {{ $warehosueProduct->warehouse?->name }} - {{ $warehosueProduct->bin?->name }}
                                </td>
                                @php

                                @endphp
                                <td>{{ number_format((float) $warehosueProduct->purchaseItem->net_unit_cost, 2, '.', '') }}
                                </td>
                                <td>{{ $warehosueProduct->purchaseItem->qty }}</td>
                                <td>
                                    Bin/Warehouse: {!! $warehosueProduct->warehouse_bin_count ?? '<em class="small text-danger">Not Set</em>' !!} /
                                    {{ $warehosueProduct->warehouse_count }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                {{-- <tfoot>
                    <th></th>
                    <th>Total</th>
                    <th>{{ number_format(0, $general_setting->decimal, '.', '') }}</th>
                    <th>0</th>
                    <th>0</th>
                    <th></th>
                </tfoot> --}}
            </table>
        </div>
    </section>

@endsection

@push('scripts')
    <script type="text/javascript">
        $("ul#report").siblings('a').attr('aria-expanded', 'true');
        $("ul#report").addClass("show");
        $("ul#report #purchase-report-menu").addClass("active");

        $('#warehouse_id').val($('input[name="warehouse_id_hidden"]').val());
        $('.selectpicker').selectpicker('refresh');

        $('#warehouse_id').on('change', function() {
            console.log($('#report-table').DataTable().ajax.params())
            // $('#report-table').DataTable().ajax.reload();

        })

        $('#report-table').DataTable({
            {{-- // ajax: '{{ route('report.purchase.api') }}?warehouse_id=' + $('#warehouse_id').value,
            // processing: true,
            // serverSide: true, --}} "order": [],
            'language': {
                'lengthMenu': '_MENU_ {{ trans('file.records per page') }}',
                "info": '<small>{{ trans('file.Showing') }} _START_ - _END_ (_TOTAL_)</small>',
                "search": '{{ trans('file.Search') }}',
                'paginate': {
                    'previous': '<i class="dripicons-chevron-left"></i>',
                    'next': '<i class="dripicons-chevron-right"></i>'
                }
            },
            'columnDefs': [{
                    "orderable": false,
                    'targets': 0
                },
                {
                    'render': function(data, type, row, meta) {
                        if (type === 'display') {
                            data =
                                '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                        }

                        return data;
                    },
                    'checkboxes': {
                        'selectRow': true,
                        'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                    },
                    'targets': [0]
                }
            ],
            'select': {
                style: 'multi',
                selector: 'td:first-child'
            },
            'lengthMenu': [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: '<"row"lfB>rtip',
            buttons: [{
                    extend: 'pdf',
                    text: '<i title="export to pdf" class="fa fa-file-pdf-o"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function(e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'excel',
                    text: '<i title="export to excel" class="dripicons-document-new"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function(e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'csv',
                    text: '<i title="export to csv" class="fa fa-file-text-o"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function(e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'print',
                    text: '<i title="print" class="fa fa-print"></i>',
                    exportOptions: {
                        columns: ':visible:not(.not-exported)',
                        rows: ':visible'
                    },
                    action: function(e, dt, button, config) {
                        datatable_sum(dt, true);
                        $.fn.dataTable.ext.buttons.print.action.call(this, e, dt, button, config);
                        datatable_sum(dt, false);
                    },
                    footer: true
                },
                {
                    extend: 'colvis',
                    text: '<i title="column visibility" class="fa fa-eye"></i>',
                    columns: ':gt(0)'
                }
            ],
            drawCallback: function() {
                var api = this.api();
                datatable_sum(api, false);
            }
        });

        function datatable_sum(dt_selector, is_calling_first) {
            if (dt_selector.rows('.selected').any() && is_calling_first) {
                var rows = dt_selector.rows('.selected').indexes();

                $(dt_selector.column(2).footer()).html(dt_selector.cells(rows, 2, {
                    page: 'current'
                }).data().sum().toFixed({{ $general_setting->decimal }}));
                $(dt_selector.column(3).footer()).html(dt_selector.cells(rows, 3, {
                    page: 'current'
                }).data().sum());
                $(dt_selector.column(4).footer()).html(dt_selector.cells(rows, 4, {
                    page: 'current'
                }).data().sum().toFixed({{ $general_setting->decimal }}));
            } else {
                $(dt_selector.column(2).footer()).html(dt_selector.column(2, {
                    page: 'current'
                }).data().sum().toFixed({{ $general_setting->decimal }}));
                $(dt_selector.column(3).footer()).html(dt_selector.column(3, {
                    page: 'current'
                }).data().sum());
                $(dt_selector.column(4).footer()).html(dt_selector.column(4, {
                    page: 'current'
                }).data().sum().toFixed({{ $general_setting->decimal }}));
            }
        }

        $(".daterangepicker-field").daterangepicker({
            callback: function(startDate, endDate, period) {
                var start_date = startDate.format('YYYY-MM-DD');
                var end_date = endDate.format('YYYY-MM-DD');
                var title = start_date + ' To ' + end_date;
                $(this).val(title);
                $('input[name="start_date"]').val(start_date);
                $('input[name="end_date"]').val(end_date);
            }
        });
    </script>
@endpush
