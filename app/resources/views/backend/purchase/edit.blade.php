@extends('backend.layout.main') @section('content')
    @if (session()->has('not_permitted'))
        <div class="alert alert-danger alert-dismissible text-center"><button aria-label="Close"
                class="close"
                data-dismiss="alert"
                type="button"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
    @endif

    <section class="forms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <livewire:purchase.edit :generalSettings="$general_setting"
                        :purchaseOrder="$lims_purchase_data"
                        :suppliers="$lims_supplier_list"
                        :taxes="$lims_tax_list"
                        :warehouses="$lims_warehouse_list" />

                </div>
            </div>
        </div>

    </section>
@endsection
