<div id="purchase-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="container mt-3 pb-2 border-bottom">
                <div class="row">
                    <div class="col-md-6 d-print-none">
                        <button id="print-btn" type="button" class="btn btn-default btn-sm"><i class="dripicons-print"></i> {{trans('file.Print')}}</button>
                    </div>
                    <div class="col-md-6 d-print-none">
                        <button type="button" id="close-btn" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i class="dripicons-cross"></i></span></button>
                    </div>
                    <div class="col-md-12">
                        <h3 id="exampleModalLabel" class="modal-title text-center container-fluid">{{$general_setting->site_title}}</h3>
                    </div>
                    <div class="col-md-12 text-center">
                        <i style="font-size: 15px;">{{trans('file.Purchase Details')}}</i>
                    </div>
                </div>
            </div>
            <div id="purchase-content" class="modal-body"></div>
            <br>
            <table class="table table-bordered product-purchase-list">
                <thead>
                    <th>#</th>
                    <th>{{trans('file.product')}}</th>
                    <th>{{trans('file.Batch No')}}</th>
                    <th>Qty</th>
                    <th>Received</th>
                    <th>{{trans('file.Unit Cost')}}</th>
                    <th>{{trans('file.Tax')}}</th>
                    <th>{{trans('file.Discount')}}</th>
                    <th>{{trans('file.Subtotal')}}</th>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="purchase-footer" class="modal-body"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function purchaseDetails(purchase) {
        var htmltext = '<strong>{{trans("file.Date")}}: </strong>' + purchase[0] + '<br><strong>{{trans("file.reference")}}: </strong>' + purchase[1] + '<br><strong>{{trans("file.Purchase Status")}}: </strong>' + purchase[2] + '<br>';
        if (purchase[25])
            htmltext += '<strong>{{trans("file.Attach Document")}}: </strong><a href="documents/purchase/' + purchase[25] + '">Download</a><br>';
        htmltext += '<br><div class="row"><div class="col-md-6"><strong>{{trans("file.From")}}:</strong><br>' + purchase[4] + '<br>' + purchase[5] + '<br>' + purchase[6] + '</div><div class="col-md-6"><div class="float-right"><strong>{{trans("file.To")}}:</strong><br>' + purchase[7] + '<br>' + purchase[8] + '<br>' + purchase[9] + '<br>' + purchase[10] + '<br>' + purchase[11] + '<br>' + purchase[12] + '</div></div></div>';
        $(".product-purchase-list tbody").remove();
        $.get('purchases/product_purchase/' + purchase[3], function(data) {
            if (data == 'Something is wrong!') {
                var newBody = $("<tbody>");
                var newRow = $("<tr>");
                cols = '<td colspan="8">Something is wrong!</td>';
                newRow.append(cols);
                newBody.append(newRow);
            } else {
                console.log(data)
                var name_code = data[0];
                var qty = data[1];
                var unit_code = data[2];
                var tax = data[3];
                var tax_rate = data[4];
                var discount = data[5];
                var subtotal = data[6];
                var batch_no = data[7];
                var received = data[8];
                var newBody = $("<tbody>");
                $.each(name_code, function(index) {
                    var newRow = $("<tr>");
                    var cols = '';
                    cols += '<td><strong>' + (index + 1) + '</strong></td>';
                    cols += '<td>' + name_code[index] + '</td>';
                    cols += '<td>' + batch_no[index] + '</td>';
                    cols += '<td>' + qty[index] + ' ' + unit_code[index] + '</td>';
                    cols += '<td>' + received[index] + ' ' + unit_code[index] + '</td>';
                    cols += '<td>' + (subtotal[index] / qty[index]) + '</td>';
                    cols += '<td>' + tax[index] + '(' + tax_rate[index] + '%)' + '</td>';
                    cols += '<td>' + discount[index] + '</td>';
                    cols += '<td>' + subtotal[index] + '</td>';
                    newRow.append(cols);
                    newBody.append(newRow);
                });

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=5><strong>{{trans("file.Total")}}:</strong></td>';
                cols += '<td>' + purchase[13] + '</td>';
                cols += '<td>' + purchase[14] + '</td>';
                cols += '<td>' + purchase[15] + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.Order Tax")}}:</strong></td>';
                cols += '<td>' + purchase[16] + '(' + purchase[17] + '%)' + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.Order Discount")}}:</strong></td>';
                cols += '<td>' + purchase[18] + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.Shipping Cost")}}:</strong></td>';
                cols += '<td>' + purchase[19] + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.grand total")}}:</strong></td>';
                cols += '<td>' + purchase[20] + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.Paid Amount")}}:</strong></td>';
                cols += '<td>' + purchase[21] + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                var newRow = $("<tr>");
                cols = '';
                cols += '<td colspan=7><strong>{{trans("file.Due")}}:</strong></td>';
                cols += '<td>' + (purchase[20] - purchase[21]) + '</td>';
                newRow.append(cols);
                newBody.append(newRow);

                $("table.product-purchase-list").append(newBody);
            }
        });

        var htmlfooter = '<p><strong>{{trans("file.Note")}}:</strong> ' + purchase[22] + '</p><strong>{{trans("file.Created By")}}:</strong><br>' + purchase[23] + '<br>' + purchase[24];

        $('#purchase-content').html(htmltext);
        $('#purchase-footer').html(htmlfooter);
        $('#purchase-details').modal('show');
    }
</script>