@extends('backend.layout.main')


@section('content')
<section>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2>{{trans('Product Details')}}</h2>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="label">{{trans('file.Product Name')}}</td>
                            <td class="value">{{ $product->name }}</td>
                        </tr>
                        <tr>
                            <td class="label">{{trans('file.Product Code')}}</td>
                            <td class="value">{{ $product->code }}</td>
                        </tr>
                        <tr>
                            <td class="label">{{trans('file.Quantity')}}</td>
                            <td class="value">{{ $product->warehouses->sum(fn($warehouse) => $warehouse->pivot->qty, 0) }}</td>
                        </tr>

                        <tr>
                            <td class="label">{{trans('file.Product Unit')}}</td>
                            <td class="value">{{ $product->unit->unit_code }}</td>
                        </tr>

                        <tr>
                            <td class="label">{{trans('file.Product Price')}}</td>
                            <td class="value">{{ $product->price }}</td>
                        </tr>

                        <tr>
                            <td class="label">{{trans('file.Product Details')}}</td>
                            <td class="value">{!! $product->product_details !!}</td>
                        </tr>

                        <tr>
                            <td class="label">{{trans('file.Alert Quantity')}}</td>
                            <td class="value">{{ $product->alert_quantity }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>




        <!-- <button id="print-btn" type="button" class="btn btn-default btn-sm ml-3"><i class="dripicons-print"></i> {{trans('file.Print')}}</button>
                <button type="button" id="close-btn" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i class="dripicons-cross"></i></span></button> -->


        <div class="card">
            <div class="card-header">
                <h2>{{trans('file.Warehouse Quantity')}}</h2>
            </div>
            <div class="card-body">
            <table class="table table-bordered table-hover product-warehouse-list">
                    <thead>
                        <tr>
                            <th>Warehouse</th>
                            {{-- <th>Batch No</th> --}}
                            {{-- <th>Expired Date</th> --}}
                            <th>Quantity</th>
                            {{-- <th>IMEI or Serial Numbers</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($product->warehouses AS $warehouse)
                        <tr>    
                            <td>{{ $warehouse->name }}</td>
                            {{-- <td>Batch No</td> --}}
                            {{-- <td>Expired Date</td> --}}
                            <td>{{ $warehouse->pivot->qty }} {{ $product->unit->unit_code }}</td>
                            {{-- <td>IMEI or Serial Numbers</td> --}}
                        </tr>
                        @endforeach


                        @if (empty($product->warehouses->count()))
                        <tr>
                            <td colspan="2" class="text-center">
                                <em>No entries yet</em>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection

@push('styles')
<style>
    .card-body table .label {
        font-weight: bold;
        width: 20%;
    }

    .card-body table .value {}
</style>
@endpush