<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1.0"
            name="viewport">
        <title>Purchase Details</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 40px;
            }

            h1 {
                text-align: right;
            }

            strong {
                font-size: 14px
            }

            .row {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .billing-info {
                margin-top: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th {
                border-bottom: 1px solid black;
                padding: 10px;
                text-align: left;
                font-weight: 100;
                color: rgb(117, 115, 115);
                font-size: 12px
            }

            th {
                background-color: lightgray;
            }

            .text-right {
                text-align: right;
                margin-top: 20px;
            }

            img {
                width: 200px;
            }

            td {
                font-size: 10px;
            }
        </style>
    </head>

    <body onload="window.print()">
        <div class="row">
            <div>
                <img alt=""
                    alt="Logo"
                    src="{{ asset('images/print/fp-logo.png') }}">

            </div>
            <div class="text-right">
                <h1>Invoice</h1>
                <h4 style="color: red">Purchase Details</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div>
                <p>Freepoint Motorcycles & Parts Inc.</p>
                <p>A.S. Fortuna, Mandaue, Cebu Philippines 6014</p>
            </div>
            <div class="text-right">
                <strong>INVOICE</strong> <br>
                <strong>Reference #:</strong> {{ $purchaseOrder->reference_no }} <br>
                <strong>Date:</strong> {{ dateFormat($purchaseOrder->created_at) }} <br>
                <strong>Author:</strong> {{ $purchaseOrder->author->name }}
            </div>
        </div>
        <hr>
        <div class="billing-info">
            <strong>Supplier :</strong> {{ $purchaseOrder->supplier->company_name ?? 'N/A' }}<br>
            <br>
            <strong>Contact:</strong> {{ $purchaseOrder->supplier->phone_number ?? 'No Phone number' }}
        </div>

        <table>
            <thead>
                <tr>

                    <th>Product</th>
                    <th>Part Number</th>
                    <th>Qty</th>
                    <th>Received</th>
                    <th>Unit Cost</th>
                    <th>Tax</th>
                    <th>Discount</th>
                    <th>SubTotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($purchaseOrder->items as $key => $item)
                    <tr>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->product->code }}</td>
                        <td>{{ $item->qty }}</td>
                        <td>{{ $item->recieved }}</td>
                        <td>{{ formatMoney($item->net_unit_cost) }}</td>
                        <td>{{ $item->tax_rate }}</td>
                        <td>% {{ $item->discsount ?? 0 }}</td>
                        <td>{{ formatMoney($item->total) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
            <strong>SUB-TOTAL:</strong>
            {{ formatMoney($purchaseOrder->grand_total) }}
            <br>
            <br>
            <strong>TOTAL:</strong>
            {{ formatMoney($purchaseOrder->grand_total) }}
        </div>
    </body>

</html>
