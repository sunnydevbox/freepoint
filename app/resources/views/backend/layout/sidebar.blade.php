<span class="brand-big">
    @if ($general_setting->site_logo)
        <a href="{{ url('/') }}"><img src="{{ url('logo', $general_setting->site_logo) }}"
                width="115"></a>
    @else
        <a href="{{ url('/') }}">
            <h1 class="d-inline">{{ $general_setting->site_title }}</h1>
        </a>
    @endif
</span>
<ul class="side-menu list-unstyled"
    id="side-main-menu">
    <li><a href="{{ url('/') }}"> <i class="dripicons-meter"></i><span>{{ __('file.dashboard') }}</span></a></li>

    <?php
    $category_permission_active = auth()->user()->can('category');
    $index_permission_active = auth()->user()->can('products-index');
    $print_barcode_active = auth()->user()->can('print_barcode');
    $stock_count_active = auth()->user()->can('stock_count');
    $adjustment_active = auth()->user()->can('adjustment');
    ?>
    @if (
        $category_permission_active ||
            $index_permission_active ||
            $print_barcode_active ||
            $stock_count_active ||
            $adjustment_active)
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#product"> <i class="dripicons-list"></i><span>{{ __('file.product') }}</span><span></a>
            <ul class="collapse list-unstyled "
                id="product">
                @if ($category_permission_active)
                    <li id="category-menu"><a href="{{ route('category.index') }}">{{ __('file.category') }}</a></li>
                @endif
                @if ($index_permission_active)
                    <li id="product-list-menu"><a
                            href="{{ route('products.index') }}">{{ __('file.product_list') }}</a></li>
                    <?php
                    $add_permission_active = auth()->user()->can('products-add');
                    ?>
                    @if ($add_permission_active)
                        <li id="product-create-menu"><a
                                href="{{ route('products.create') }}">{{ __('file.add_product') }}</a></li>
                    @endif
                @endif
                {{-- @if ($print_barcode_active)
                    <li id="printBarcode-menu"><a
                            href="{{ route('product.printBarcode') }}">{{ __('file.print_barcode') }}</a></li>
                @endif --}}
                @if ($adjustment_active)
                    <li id="adjustment-list-menu"><a
                            href="{{ route('qty_adjustment.index') }}">{{ trans('file.Adjustment List') }}</a></li>
                    <li id="adjustment-create-menu"><a
                            href="{{ route('qty_adjustment.create') }}">{{ trans('file.Add Adjustment') }}</a></li>
                @endif
                {{-- @if ($stock_count_active)
                    <li id="stock-count-menu"><a
                            href="{{ route('stock-count.index') }}">{{ trans('file.Stock Count') }}</a></li>
                @endif --}}
            </ul>
        </li>
    @endif
    <?php
    $index_permission_active = auth()->user()->can('purchases-index');
    ?>
    @if ($index_permission_active)
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#purchase"> <i class="dripicons-card"></i><span>{{ trans('file.Purchase') }}</span></a>
            <ul class="collapse list-unstyled "
                id="purchase">
                <li id="purchase-list-menu"><a
                        href="{{ route('purchases.index') }}">{{ trans('file.Purchase List') }}</a></li>
                <?php
                $add_permission_active = auth()->user()->can('purchases-add');
                ?>
                @if ($add_permission_active)
                    <li id="purchase-create-menu"><a
                            href="{{ route('purchases.create') }}">{{ trans('file.Add Purchase') }}</a></li>
                    {{-- <li id="purchase-import-menu"><a
                            href="{{ url('purchases/purchase_by_csv') }}">{{ trans('file.Import Purchase By CSV') }}</a>
                    </li> --}}
                @endif
            </ul>
        </li>
    @endif
    <?php
    $sale_index_permission_active = auth()->user()->can('sales-index');
    
    $gift_card_permission_active = auth()->user()->can('gift_card');
    
    $coupon_permission_active = auth()->user()->can('coupon');
    
    $delivery_permission_active = auth()->user()->can('delivery');
    
    $sale_add_permission_active = auth()->user()->can('sales-add');
    ?>
    @if (
        $sale_index_permission_active ||
            $gift_card_permission_active ||
            $coupon_permission_active ||
            $delivery_permission_active ||
            $sale_add_permission_active)
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#sale"> <i class="dripicons-cart"></i><span>{{ trans('file.Sale') }}</span></a>
            <ul class="collapse list-unstyled "
                id="sale">
                @if ($sale_index_permission_active)
                    <li id="sale-list-menu"><a href="{{ route('sales.index') }}">{{ trans('file.Sale List') }}</a>
                    </li>
                    @if ($sale_add_permission_active)
                        {{-- <li><a href="{{route('sale.pos')}}">POS</a></li> --}}
                        <li id="sale-create-menu"><a
                                href="{{ route('sales.create') }}">{{ trans('file.Add Sale') }}</a></li>
                        {{-- <li id="sale-import-menu"><a
                                href="{{ url('sales/sale_by_csv') }}">{{ trans('file.Import Sale By CSV') }}</a></li> --}}
                    @endif
                @endif

                {{-- @if ($gift_card_permission_active)
                    <li id="gift-card-menu"><a
                            href="{{ route('gift_cards.index') }}">{{ trans('file.Gift Card List') }}</a> </li>
                @endif
                @if ($coupon_permission_active)
                    <li id="coupon-menu"><a href="{{ route('coupons.index') }}">{{ trans('file.Coupon List') }}</a>
                    </li>
                @endif --}}
                {{-- @if ($delivery_permission_active)
                    <li id="delivery-menu"><a
                            href="{{ route('delivery.index') }}">{{ trans('file.Delivery List') }}</a></li>
                @endif --}}
            </ul>
        </li>
    @endif

    <?php
    $index_permission_active = auth()->user()->can('expenses-index');
    ?>
    @if (false)
        {{-- @if ($index_permission_active) --}}
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#expense"> <i class="dripicons-wallet"></i><span>{{ trans('file.Expense') }}</span></a>
            <ul class="collapse list-unstyled "
                id="expense">
                <li id="exp-cat-menu"><a
                        href="{{ route('expense_categories.index') }}">{{ trans('file.Expense Category') }}</a></li>
                <li id="exp-list-menu"><a href="{{ route('expenses.index') }}">{{ trans('file.Expense List') }}</a>
                </li>
                <?php
                $add_permission_active = auth()->user()->can('expenses-add');
                ?>
                @if ($add_permission_active)
                    <li><a href=""
                            id="add-expense"> {{ trans('file.Add Expense') }}</a></li>
                @endif
            </ul>
        </li>
    @endif
    <?php
    $index_permission_active = auth()->user()->can('quotes-index');
    ?>
    @if (false) {{-- $index_permission_active) --}}
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#quotation"> <i
                    class="dripicons-document"></i><span>{{ trans('file.Quotation') }}</span><span></a>
            <ul class="collapse list-unstyled "
                id="quotation">
                <li id="quotation-list-menu"><a
                        href="{{ route('quotations.index') }}">{{ trans('file.Quotation List') }}</a></li>
                <?php
                $add_permission_active = auth()->user()->can('quotes-add');
                ?>
                @if ($add_permission_active)
                    <li id="quotation-create-menu"><a
                            href="{{ route('quotations.create') }}">{{ trans('file.Add Quotation') }}</a></li>
                @endif
            </ul>
        </li>
    @endif

    @can('transfers-index')
        <li><a aria-expanded="false"
                data-toggle="collapse"
                href="#transfer"> <i class="dripicons-export"></i><span>{{ trans('file.Transfer') }}</span></a>
            <ul class="collapse list-unstyled "
                id="transfer">
                <li id="transfer-list-menu"><a href="{{ route('transfers.index') }}">{{ trans('file.Transfer List') }}</a>
                </li>

                @can('transfers-add')
                    <li id="transfer-create-menu"><a
                            href="{{ route('transfers.create') }}">{{ trans('file.Add Transfer') }}</a></li>
                    <li id="transfer-import-menu"><a
                            href="{{ url('transfers/transfer_by_csv') }}">{{ trans('file.Import Transfer By CSV') }}</a>
                    </li>
                @endcan
            </ul>
        </li>
    @endcan

    @canany(['purchase-return-index', 'returns-index'])
        <li>
            <a aria-expanded="false"
                data-toggle="collapse"
                href="#return">
                <i class="dripicons-return"></i>
                <span>{{ trans('file.return') }}</span>
            </a>
            <ul class="collapse list-unstyled"
                id="return">
                @can('returns-index')
                    <li id="sale-return-menu">
                        <a href="{{ route('return-sale.index') }}">{{ trans('file.Sale') }}</a>
                    </li>
                @endcan
                @can('purchase-return-index')
                    <li id="purchase-return-menu">
                        <a href="{{ route('return-purchase.index') }}">{{ trans('file.Purchase') }}</a>
                    </li>
                @endcan
            </ul>
        </li>
    @endcanany

    @role('admin')
        <?php
        $index_permission_active = auth()->user()->can('account-index');
        
        $money_transfer_permission_active = auth()->user()->can('money-transfer');
        
        $balance_sheet_permission_active = auth()->user()->can('balance-sheet');
        
        $account_statement_permission_active = auth()->user()->can('account-statement');
        
        ?>
        @if (
            $index_permission_active ||
                $balance_sheet_permission_active ||
                $account_statement_permission_active ||
                $money_transfer_permission_active)
            <li class=""><a aria-expanded="false"
                    data-toggle="collapse"
                    href="#account"> <i class="dripicons-briefcase"></i><span>{{ trans('file.Accounting') }}</span></a>
                <ul class="collapse list-unstyled "
                    id="account">
                    @if ($index_permission_active)
                        <li id="account-list-menu"><a
                                href="{{ route('accounts.index') }}">{{ trans('file.Account List') }}</a></li>
                        <li><a href=""
                                id="add-account">{{ trans('file.Add Account') }}</a></li>
                    @endif
                    @if ($money_transfer_permission_active)
                        <li id="money-transfer-menu"><a
                                href="{{ route('money-transfers.index') }}">{{ trans('file.Money Transfer') }}</a></li>
                    @endif
                    @if ($balance_sheet_permission_active)
                        <li id="balance-sheet-menu"><a
                                href="{{ route('accounts.balancesheet') }}">{{ trans('file.Balance Sheet') }}</a></li>
                    @endif
                    @if ($account_statement_permission_active)
                        <li id="account-statement-menu"><a href=""
                                id="account-statement">{{ trans('file.Account Statement') }}</a></li>
                    @endif
                </ul>
            </li>
        @endif

    @endrole
    <?php
    $department_active = auth()->user()->can('department');
    
    $index_employee_active = auth()->user()->can('employees-index');
    
    $attendance_active = auth()->user()->can('attendance');
    
    $payroll_active = auth()->user()->can('payroll');
    ?>

    @if (false) {{-- in_array(auth()->user()->role_id, [1, 2])) --}}
        <li class=""><a aria-expanded="false"
                data-toggle="collapse"
                href="#hrm"> <i class="dripicons-user-group"></i><span>HRM</span></a>
            <ul class="collapse list-unstyled "
                id="hrm">
                @if ($department_active)
                    <li id="dept-menu"><a href="{{ route('departments.index') }}">{{ trans('file.Department') }}</a>
                    </li>
                @endif
                @if ($index_employee_active)
                    <li id="employee-menu"><a href="{{ route('employees.index') }}">{{ trans('file.Employee') }}</a>
                    </li>
                @endif
                @if ($attendance_active)
                    <li id="attendance-menu"><a
                            href="{{ route('attendance.index') }}">{{ trans('file.Attendance') }}</a></li>
                @endif
                @if ($payroll_active)
                    <li id="payroll-menu"><a href="{{ route('payroll.index') }}">{{ trans('file.Payroll') }}</a>
                    </li>
                @endif
                <li id="holiday-menu"><a href="{{ route('holidays.index') }}">{{ trans('file.Holiday') }}</a></li>
            </ul>
        </li>
    @endif

    @role('admin|business-manager|Branch Manager')
        <?php
        $user_index_permission_active = auth()->user()->can('users-index');
        
        $customer_index_permission_active = auth()->user()->can('customers-index');
        
        $biller_index_permission_active = auth()->user()->can('billers-index');
        
        $supplier_index_permission_active = auth()->user()->can('suppliers-index');
        ?>
        @if (
            $user_index_permission_active ||
                $customer_index_permission_active ||
                $biller_index_permission_active ||
                $supplier_index_permission_active)
            <li><a aria-expanded="false"
                    data-toggle="collapse"
                    href="#people"> <i class="dripicons-user"></i><span>{{ trans('file.People') }}</span></a>
                <ul class="collapse list-unstyled "
                    id="people">

                    @if ($user_index_permission_active)
                        <li id="user-list-menu"><a href="{{ route('user.index') }}">{{ trans('file.User List') }}</a>
                        </li>
                        <?php
                        $user_add_permission_active = auth()->user()->can('users-add');
                        ?>
                        @if ($user_add_permission_active)
                            <li id="user-create-menu"><a
                                    href="{{ route('user.create') }}">{{ trans('file.Add User') }}</a></li>
                        @endif
                    @endif

                    @if ($customer_index_permission_active)
                        <li id="customer-list-menu"><a
                                href="{{ route('customer.index') }}">{{ trans('file.Customer List') }}</a></li>
                        <?php
                        $customer_add_permission_active = auth()->user()->can('customers-add');
                        ?>
                        @if ($customer_add_permission_active)
                            <li id="customer-create-menu"><a
                                    href="{{ route('customer.create') }}">{{ trans('file.Add Customer') }}</a></li>
                        @endif
                    @endif

                    @if ($biller_index_permission_active)
                        <li id="biller-list-menu"><a
                                href="{{ route('biller.index') }}">{{ trans('file.Biller List') }}</a></li>
                        <?php
                        $biller_add_permission_active = auth()->user()->can('billers-add');
                        ?>
                        @if ($biller_add_permission_active)
                            <li id="biller-create-menu"><a
                                    href="{{ route('biller.create') }}">{{ trans('file.Add Biller') }}</a></li>
                        @endif
                    @endif

                    @if ($supplier_index_permission_active)
                        <li id="supplier-list-menu"><a
                                href="{{ route('supplier.index') }}">{{ trans('file.Supplier List') }}</a></li>
                        <?php
                        $supplier_add_permission_active = auth()->user()->can('suppliers-add');
                        ?>
                        @if ($supplier_add_permission_active)
                            <li id="supplier-create-menu"><a
                                    href="{{ route('supplier.create') }}">{{ trans('file.Add Supplier') }}</a></li>
                        @endif
                    @endif
                </ul>
            </li>
        @endif

        <?php
        $profit_loss_active = auth()->user()->can('profit-loss');
        $best_seller_active = auth()->user()->can('best-seller');
        $warehouse_report_active = auth()->user()->can('warehouse-report');
        $warehouse_stock_report_active = auth()->user()->can('warehouse-stock-report');
        $product_report_active = auth()->user()->can('product-report');
        $daily_sale_active = auth()->user()->can('daily-sale');
        $monthly_sale_active = auth()->user()->can('monthly-sale');
        $daily_purchase_active = auth()->user()->can('daily-purchase');
        $monthly_purchase_active = auth()->user()->can('monthly-purchase');
        $purchase_report_active = auth()->user()->can('purchase-report');
        $sale_report_active = auth()->user()->can('sale-report');
        $sale_report_chart_active = auth()->user()->can('sale-report-chart');
        $payment_report_active = auth()->user()->can('payment-report');
        $product_expiry_report_active = auth()->user()->can('product-expiry-report');
        $product_qty_alert_active = auth()->user()->can('product-qty-alert');
        $dso_report_active = auth()->user()->can('dso-report');
        $user_report_active = auth()->user()->can('user-report');
        $customer_report_active = auth()->user()->can('customer-report');
        $supplier_report_active = auth()->user()->can('supplier-report');
        $due_report_active = auth()->user()->can('due-report');
        $supplier_due_report_active = auth()->user()->can('supplier-due-report');
        ?>
        @if (
            $profit_loss_active ||
                $best_seller_active ||
                $warehouse_report_active ||
                $warehouse_stock_report_active ||
                $product_report_active ||
                $daily_sale_active ||
                $monthly_sale_active ||
                $daily_purchase_active ||
                $monthly_purchase_active ||
                $purchase_report_active ||
                $sale_report_active ||
                $sale_report_chart_active ||
                $payment_report_active ||
                $product_expiry_report_active ||
                $product_qty_alert_active ||
                $dso_report_active ||
                $user_report_active ||
                $customer_report_active ||
                $supplier_report_active ||
                $due_report_active ||
                $supplier_due_report_active)
            <li><a aria-expanded="false"
                    data-toggle="collapse"
                    href="#report"> <i class="dripicons-document-remove"></i><span>{{ trans('file.Reports') }}</span></a>
                <ul class="collapse list-unstyled "
                    id="report">
                    <li><a href="{{ route('report.stock-report') }}">Stock Report</a></li>
                    {{-- @if ($profit_loss_active)
                    <li id="profit-loss-report-menu">
                        {!! Form::open(['route' => 'report.profitLoss', 'method' => 'post', 'id' => 'profitLoss-report-form']) !!}
                        <input
                            type="hidden"
                            name="start_date"
                            value="{{ date('Y-m') . '-' . '01' }}"
                        />
                        <input
                            type="hidden"
                            name="end_date"
                            value="{{ date('Y-m-d') }}"
                        />
                        <a
                            id="profitLoss-link"
                            href=""
                        >{{ trans('file.Summary Report') }}</a>
                        {!! Form::close() !!}
                    </li>
                @endif
                @if ($best_seller_active)
                    <li id="best-seller-report-menu">
                        <a href="{{ url('report/best_seller') }}">{{ trans('file.Best Seller') }}</a>
                    </li>
                @endif
                --}}
                    @if ($product_report_active)
                        <li id="product-report-menu">
                            {!! Form::open(['route' => 'report.product', 'method' => 'get', 'id' => 'product-report-form']) !!}
                            <input name="start_date"
                                type="hidden"
                                value="{{ date('Y-m') . '-' . '01' }}" />
                            <input name="end_date"
                                type="hidden"
                                value="{{ date('Y-m-d') }}" />
                            <input name="warehouse_id"
                                type="hidden"
                                value="0" />
                            <a href=""
                                id="report-link">{{ trans('file.Product Report') }}</a>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    {{--
                @if ($daily_sale_active)
                    <li id="daily-sale-report-menu">
                        <a
                            href="{{ url('report/daily_sale/' . date('Y') . '/' . date('m')) }}">{{ trans('file.Daily Sale') }}</a>
                    </li>
                @endif
                @if ($monthly_sale_active)
                    <li id="monthly-sale-report-menu">
                        <a href="{{ url('report/monthly_sale/' . date('Y')) }}">{{ trans('file.Monthly Sale') }}</a>
                    </li>
                @endif
                @if ($daily_purchase_active)
                    <li id="daily-purchase-report-menu">
                        <a
                            href="{{ url('report/daily_purchase/' . date('Y') . '/' . date('m')) }}">{{ trans('file.Daily Purchase') }}</a>
                    </li>
                @endif
                @if ($monthly_purchase_active)
                    <li id="monthly-purchase-report-menu">
                        <a
                            href="{{ url('report/monthly_purchase/' . date('Y')) }}">{{ trans('file.Monthly Purchase') }}</a>
                    </li>
                @endif --}}
                    @if ($sale_report_active)
                        <li id="sale-report-menu">
                            {!! Form::open(['route' => 'report.sale', 'method' => 'post', 'id' => 'sale-report-form']) !!}
                            <input name="start_date"
                                type="hidden"
                                value="{{ date('Y-m') . '-' . '01' }}" />
                            <input name="end_date"
                                type="hidden"
                                value="{{ date('Y-m-d') }}" />
                            <input name="warehouse_id"
                                type="hidden"
                                value="0" />
                            <a href=""
                                id="sale-report-link">{{ trans('file.Sale Report') }}</a>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    {{-- @if ($sale_report_chart_active)
                    <li id="sale-report-chart-menu">
                        {!! Form::open(['route' => 'report.saleChart', 'method' => 'post', 'id' => 'sale-report-chart-form']) !!}
                        <input name="start_date"
                            type="hidden"
                            value="{{ date('Y-m') . '-' . '01' }}" />
                        <input name="end_date"
                            type="hidden"
                            value="{{ date('Y-m-d') }}" />
                        <input name="warehouse_id"
                            type="hidden"
                            value="0" />
                        <input name="time_period"
                            type="hidden"
                            value="weekly" />
                        <a href=""
                            id="sale-report-chart-link">{{ trans('file.Sale Report Chart') }}</a>
                        {!! Form::close() !!}
                    </li>
                @endif --}}
                    {{-- @if ($payment_report_active)
                    <li id="payment-report-menu">
                        {!! Form::open(['route' => 'report.paymentByDate', 'method' => 'post', 'id' => 'payment-report-form']) !!}
                        <input name="start_date"
                            type="hidden"
                            value="{{ date('Y-m') . '-' . '01' }}" />
                        <input name="end_date"
                            type="hidden"
                            value="{{ date('Y-m-d') }}" />
                        <a href=""
                            id="payment-report-link">{{ trans('file.Payment Report') }}</a>
                        {!! Form::close() !!}
                    </li>
                @endif --}}
                    @if ($purchase_report_active)
                        <li id="purchase-report-menu">
                            {!! Form::open(['route' => 'report.purchase', 'method' => 'post', 'id' => 'purchase-report-form']) !!}
                            <input name="start_date"
                                type="hidden"
                                value="{{ date('Y-m') . '-' . '01' }}" />
                            <input name="end_date"
                                type="hidden"
                                value="{{ date('Y-m-d') }}" />
                            <input name="warehouse_id"
                                type="hidden"
                                value="0" />
                            <a href=""
                                id="purchase-report-link">{{ trans('file.Purchase Report') }}</a>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    {{-- @if ($customer_report_active)
                    <li id="customer-report-menu">
                        <a href=""
                            id="customer-report-link">{{ trans('file.Customer Report') }}</a>
                    </li>
                @endif
                @if ($customer_report_active)
                    <li id="customer-report-menu">
                        <a href=""
                            id="customer-group-report-link">{{ trans('file.Customer Group Report') }}</a>
                    </li>
                @endif
                @if ($due_report_active)
                    <li id="due-report-menu">
                        {!! Form::open(['route' => 'report.customerDueByDate', 'method' => 'post', 'id' => 'customer-due-report-form']) !!}
                        <input name="start_date"
                            type="hidden"
                            value="{{ date('Y-m-d', strtotime('-1 year')) }}" />
                        <input name="end_date"
                            type="hidden"
                            value="{{ date('Y-m-d') }}" />
                        <a href=""
                            id="due-report-link">{{ trans('file.Customer Due Report') }}</a>
                        {!! Form::close() !!}
                    </li>
                @endif
                @if ($supplier_report_active)
                    <li id="supplier-report-menu">
                        <a href=""
                            id="supplier-report-link">{{ trans('file.Supplier Report') }}</a>
                    </li>
                @endif
                @if ($supplier_due_report_active)
                    <li id="supplier-due-report-menu">
                        {!! Form::open(['route' => 'report.supplierDueByDate', 'method' => 'post', 'id' => 'supplier-due-report-form']) !!}
                        <input name="start_date"
                            type="hidden"
                            value="{{ date('Y-m-d', strtotime('-1 year')) }}" />
                        <input name="end_date"
                            type="hidden"
                            value="{{ date('Y-m-d') }}" />
                        <a href=""
                            id="supplier-due-report-link">{{ trans('file.Supplier Due Report') }}</a>
                        {!! Form::close() !!}
                    </li>
                @endif
                @if ($warehouse_report_active)
                    <li id="warehouse-report-menu">
                        <a href=""
                            id="warehouse-report-link">{{ trans('file.Warehouse Report') }}</a>
                    </li>
                @endif
                @if ($warehouse_stock_report_active)
                    <li id="warehouse-stock-report-menu">
                        <a
                            href="{{ route('report.warehouseStock') }}">{{ trans('file.Warehouse Stock Chart') }}</a>
                    </li>
                @endif
                @if ($product_expiry_report_active)
                    <li id="productExpiry-report-menu">
                        <a href="{{ route('report.productExpiry') }}">{{ trans('file.Product Expiry Report') }}</a>
                    </li>
                @endif
                @if ($product_qty_alert_active)
                    <li id="qtyAlert-report-menu">
                        <a href="{{ route('report.qtyAlert') }}">{{ trans('file.Product Quantity Alert') }}</a>
                    </li>
                @endif
                @if ($dso_report_active)
                    <li id="daily-sale-objective-menu">
                        <a
                            href="{{ route('report.dailySaleObjective') }}">{{ trans('file.Daily Sale Objective Report') }}</a>
                    </li>
                @endif
                @if ($user_report_active)
                    <li id="user-report-menu">
                        <a href=""
                            id="user-report-link">{{ trans('file.User Report') }}</a>
                    </li>
                @endif --}}
                </ul>
            </li>
        @endif
    @endrole
    @role('admin|business-manager')
        <li>
            <a aria-expanded="false"
                data-toggle="collapse"
                href="#setting">
                <i class="dripicons-gear"></i><span>{{ trans('file.settings') }}</span>
            </a>
            <ul class="collapse list-unstyled "
                id="setting">
                <?php
                $all_notification_permission_active = auth()->user()->can('all_notification');
                $send_notification_permission_active = auth()->user()->can('send_notification');
                $warehouse_permission_active = auth()->user()->can('warehouse');
                $customer_group_permission_active = auth()->user()->can('customer_group');
                $brand_permission_active = auth()->user()->can('brand');
                $unit_permission_active = auth()->user()->can('unit');
                $currency_permission_active = auth()->user()->can('currency');
                $tax_permission_active = auth()->user()->can('tax');
                $general_setting_permission_active = auth()->user()->can('general_setting');
                $backup_database_permission_active = auth()->user()->can('backup_database');
                $mail_setting_permission_active = auth()->user()->can('mail_setting');
                $sms_setting_permission_active = auth()->user()->can('sms_setting');
                $create_sms_permission_active = auth()->user()->can('create_sms');
                $pos_setting_permission_active = auth()->user()->can('pos_setting');
                $hrm_setting_permission_active = auth()->user()->can('hrm_setting');
                $reward_point_setting_permission_active = auth()->user()->can('reward_point_setting');
                $discount_plan_permission_active = auth()->user()->can('discount_plan');
                $discount_permission_active = auth()->user()->can('discount');
                ?>
                @if (
                    (auth()->user()->roles->first() && auth()->user()->roles->first()->id <= 2) ||
                        auth()->user()->role('business-manager'))
                    <li id="role-menu">
                        <a href="{{ route('role.index') }}">{{ trans('file.Role Permission') }}</a>
                    </li>
                @endif
                @if ($discount_plan_permission_active)
                    <li id="discount-plan-list-menu"><a
                            href="{{ route('discount-plans.index') }}">{{ trans('file.Discount Plan') }}</a></li>
                @endif
                @if ($discount_permission_active)
                    <li id="discount-list-menu"><a
                            href="{{ route('discounts.index') }}">{{ trans('file.Discount') }}</a></li>
                @endif
                @if ($all_notification_permission_active)
                    <li id="notification-list-menu">
                        <a href="{{ route('notifications.index') }}">{{ trans('file.All Notification') }}</a>
                    </li>
                @endif
                @if ($send_notification_permission_active)
                    <li id="notification-menu">
                        <a href=""
                            id="send-notification">{{ trans('file.Send Notification') }}</a>
                    </li>
                @endif
                @if ($warehouse_permission_active)
                    <li id="warehouse-menu"><a href="{{ route('warehouse.index') }}">{{ trans('file.Warehouse') }}</a>
                    </li>
                @endif

                {{-- @if ($warehouse_permission_active) --}}
                <li id="bin-location-menu"><a href="{{ route('warehouse.bins.index') }}">Bin Locations</a></li>
                {{-- @endif --}}

                @if ($customer_group_permission_active)
                    <li id="customer-group-menu"><a
                            href="{{ route('customer_group.index') }}">{{ trans('file.Customer Group') }}</a></li>
                @endif
                @if ($brand_permission_active)
                    <li id="brand-menu"><a href="{{ route('brand.index') }}">{{ trans('file.Brand') }}</a></li>
                @endif
                @if ($unit_permission_active)
                    <li id="unit-menu"><a href="{{ route('unit.index') }}">{{ trans('file.Unit') }}</a></li>
                @endif
                @if ($currency_permission_active)
                    <li id="currency-menu"><a href="{{ route('currency.index') }}">{{ trans('file.Currency') }}</a>
                    </li>
                @endif
                @if ($tax_permission_active)
                    <li id="tax-menu"><a href="{{ route('tax.index') }}">{{ trans('file.Tax') }}</a></li>
                @endif
                <li id="user-menu"><a
                        href="{{ route('user.profile', ['id' => Auth::id()]) }}">{{ trans('file.User Profile') }}</a>
                </li>
                @if ($create_sms_permission_active)
                    <li id="create-sms-menu"><a
                            href="{{ route('setting.createSms') }}">{{ trans('file.Create SMS') }}</a></li>
                @endif
                @if ($backup_database_permission_active)
                    <li><a href="{{ route('setting.backup') }}">{{ trans('file.Backup Database') }}</a></li>
                @endif
                @if ($general_setting_permission_active)
                    <li id="general-setting-menu"><a
                            href="{{ route('setting.general') }}">{{ trans('file.General Setting') }}</a></li>
                @endif
                @if ($mail_setting_permission_active)
                    <li id="mail-setting-menu"><a
                            href="{{ route('setting.mail') }}">{{ trans('file.Mail Setting') }}</a></li>
                @endif
                @if ($reward_point_setting_permission_active)
                    <li id="reward-point-setting-menu"><a
                            href="{{ route('setting.rewardPoint') }}">{{ trans('file.Reward Point Setting') }}</a></li>
                @endif
                @if ($sms_setting_permission_active)
                    <li id="sms-setting-menu"><a href="{{ route('setting.sms') }}">{{ trans('file.SMS Setting') }}</a>
                    </li>
                @endif
                @if ($pos_setting_permission_active)
                    <li id="pos-setting-menu"><a href="{{ route('setting.pos') }}">POS
                            {{ trans('file.settings') }}</a></li>
                @endif
                @if ($hrm_setting_permission_active)
                    <li id="hrm-setting-menu"><a href="{{ route('setting.hrm') }}">
                            {{ trans('file.HRM Setting') }}</a></li>
                @endif
            </ul>
        </li>
        @if (in_array(auth()->user()->role_id, [1, 2]))
            <li><a href="{{ url('public/read_me') }}"
                    target="_blank"> <i
                        class="dripicons-information"></i><span>{{ trans('file.Documentation') }}</span></a></li>
        @endif
    @endrole
</ul>
