<?php

namespace Tests;

use App\Models\User;
use App\Services\AuthService;
use Closure;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\SQLiteBuilder;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Fluent;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    use DatabaseMigrations;
    
    protected static bool $setUpHasRunOnce = false;

    // protected function setUp(): void
    // {
    //     parent::setUp();
    //     dd(1);
    //     // $this->artisan('db:seed');
    //     if (!static::$setUpHasRunOnce) {
    //         // Artisan::call('db:wipe');
    //         // Artisan::call('migrate');
    //         // Artisan::call('db:seed', ['--class' => 'DatabaseSeeder']);
    //         static::$setUpHasRunOnce = true;
    //     }

    //     $this->seed();
    // }

    public function _getToken()
    {
        $password = '123qwe';
        $user = User::inRandomOrder()->first();

        $service = app(AuthService::class);
        $token = $service->login($user->name, $password, 'name');

        return $token;
    }


    public function hotfixSqlite()
    {
        \Illuminate\Database\Connection::resolverFor('sqlite', function ($connection, $database, $prefix, $config) {
            return new class($connection, $database, $prefix, $config) extends SQLiteConnection {
                public function getSchemaBuilder()
                {
                    if ($this->schemaGrammar === null) {
                        $this->useDefaultSchemaGrammar();
                    }
                    return new class($this) extends SQLiteBuilder {
                        protected function createBlueprint($table, Closure $callback = null)
                        {
                            return new class($table, $callback) extends Blueprint {
                                public function dropForeign($index)
                                {
                                    return new Fluent();
                                }
                            };
                        }
                    };
                }
            };
        });
    }

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->hotfixSqlite();
    }
}
