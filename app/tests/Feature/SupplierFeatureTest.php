<?php

namespace Tests\Feature;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Tests\Interfaces\BREADResourceInterface;
use Tests\TestCase;
use Tests\Traits\BaseBREADResourceFeatureTestTrait;

class SupplierFeatureTest extends TestCase implements BREADResourceInterface
{
    use BaseBREADResourceFeatureTestTrait;

    public function resourceModel(): Model
    {
        return app(Supplier::class);
    }

    public function resourceEndpoint(): string
    {
        return '/api/supplier';
    }

    public function test_add_success()
    {
        $this->_getToken();
        $params = [
            'name' => 'Vat@some12',
            'company_name' => 12,
            'email' => 'some@email.com',
            'phone_number' => '123',
            'address' => 'Some Address In This Country',
            'city' => 'Ciudad'
        ];

        $response = $this->postJson($this->resourceEndpoint(), $params);
        $response->assertStatus(201);
        $response->assertJsonPath('data.name', $params['name']);
    }

    public function test_edit_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $params = [
            'name' => 'Vat@some12',
            'company_name' => 12,
            'email' => 'some@email.com',
            'phone_number' => '123',
            'address' => 'Some Address In This Country',
            'city' => 'Ciudad'
        ];

        $response = $this->putJson("{$this->resourceEndpoint()}/{$obj->id}", $params);
        $response->assertStatus(200);
        $response->assertJsonPath('data.name', $params['name']);
    }
}