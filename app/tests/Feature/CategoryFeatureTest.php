<?php

namespace Tests\Feature;

use App\Models\Category;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Tests\Interfaces\BREADResourceInterface;
use Tests\TestCase;
use Tests\Traits\BaseBREADResourceFeatureTestTrait;

class CategoryFeatureTest extends TestCase implements BREADResourceInterface
{
    use BaseBREADResourceFeatureTestTrait;

    public function resourceModel(): Model
    {
        return app(Category::class);
    }

    public function resourceEndpoint(): string
    {
        return '/api/category';
    }

    public function test_add_success()
    {
        $this->_getToken();
        $params = [
            'name' => 'SomeCategory',
        ];

        $response = $this->postJson($this->resourceEndpoint(), $params);
        $response->assertStatus(201);
        $response->assertJsonPath('data.name', $params['name']);
    }

    public function test_edit_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $params = [
            'name' => 'SomeCategory',
        ];

        $response = $this->putJson("{$this->resourceEndpoint()}/{$obj->id}", $params);
        $response->assertStatus(200);
        $response->assertJsonPath('data.name', $params['name']);
    }
}
