<?php

namespace Tests\Feature;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use Tests\Interfaces\BREADResourceInterface;
use Tests\TestCase;
use Tests\Traits\BaseBREADResourceFeatureTestTrait;

class BrandFeatureTest extends TestCase implements BREADResourceInterface
{
    use BaseBREADResourceFeatureTestTrait;

    public function resourceModel(): Model
    {
        return app(Brand::class);
    }

    public function resourceEndpoint(): string
    {
        return '/api/brand';
    }
}
