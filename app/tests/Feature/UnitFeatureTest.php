<?php

namespace Tests\Feature;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Model;
use Tests\Interfaces\BREADResourceInterface;
use Tests\TestCase;
use Tests\Traits\BaseBREADResourceFeatureTestTrait;

class UnitFeatureTest extends TestCase implements BREADResourceInterface
{
    use BaseBREADResourceFeatureTestTrait;

    public function resourceModel(): Model
    {
        return app(Unit::class);
    }

    public function resourceEndpoint(): string
    {
        return '/api/uom';
    }

    public function test_add_success()
    {
        $this->_getToken();
        $params = [
            'unit_name' => 'ThisIsUnitName',
            'unit_code' => 'ShrtCode',
        ];


        $response = $this->postJson($this->resourceEndpoint(), $params);
        $response->assertStatus(201);
        $response->assertJsonPath('data.unit_name', $params['unit_name']);
    }

    public function test_edit_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $params = [
            'unit_name' => 'ThisIsUnitName',
            'unit_code' => 'ShrtCode',
        ];

        $response = $this->putJson("{$this->resourceEndpoint()}/{$obj->id}", $params);
        $response->assertStatus(200);
        $response->assertJsonPath('data.unit_name', $params['unit_name']);
    }
}