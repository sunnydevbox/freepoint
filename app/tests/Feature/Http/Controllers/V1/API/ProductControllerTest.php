<?php

namespace Tests\Feature\Http\Controllers\V1\API;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PDO;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;
    /** @test **/
    public function test_login_expect_success()
    {
        $response = $this
            ->get('/products/search/auth/login');
        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token']);
    }

    
}
