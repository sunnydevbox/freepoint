<?php

namespace Tests\Feature;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ProductFeatureTest extends TestCase
{
    public function test_browse()
    {
        $this->_getToken();

        $response = $this->getJson('/api/products?page=1');

        // dd($response->json());
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'current_page',
                'total',
                'per_page',
            ]
        ]);
    }
}
