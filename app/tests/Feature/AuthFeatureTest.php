<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PDO;
use Tests\TestCase;

class AuthFeatureTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_expect_success()
    {
        $password = '123qwe';
        $user = User::inRandomOrder()->first();

        $response = $this
            ->postJson('/api/auth/login', [
                'name' => $user->name,
                'password' => $password,
            ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token']);
    }

    public function test_logout_expect_success()
    {
        $this->_getToken();

        $response = $this
            // ->withheaders($headers)
            ->postJson('/api/auth/logout');

        $response->assertStatus(200);
    }

    public function test_me_expect_success()
    {
        $user = User::inRandomOrder()->first();

        $responseLogin = $this->postJson('/api/auth/login', [
            'name' => $user->email,
            'password' => '123qwe',
        ]);

        $responseLogin->assertStatus(200);
        $responseLogin->assertJsonStructure(['access_token']);

        $response = $this
            // ->withHeader('Authorization', 'asd')
            ->postJson('/api/auth/me');
        // dd($response->json());

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name'
            ]
        ]);
    }

    public function test_refresh_token()
    {
        $this->_getToken();

        $responseRefresh = $this->postJson('/api/auth/refresh');
        // dd($responseRefresh);
        $responseRefresh->assertStatus(200);
        $responseRefresh->assertJsonStructure([
            'access_token',
        ]);
    }
}
