<?php

namespace Tests\Feature;

use App\Models\Warehouse;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Tests\Interfaces\BREADResourceInterface;
use Tests\TestCase;
use Tests\Traits\BaseBREADResourceFeatureTestTrait;

class WarehouseFeatureTest extends TestCase implements BREADResourceInterface
{
    use BaseBREADResourceFeatureTestTrait;

    public function resourceModel(): Model
    {
        return app(Warehouse::class);
    }

    public function resourceEndpoint(): string
    {
        return '/api/warehouse';
    }

    public function test_add_success()
    {
        $this->_getToken();
        $params = [
            'name' => 'ThisIsUnitName',
            'address' => 'ShrtCode',
        ];


        $response = $this->postJson($this->resourceEndpoint(), $params);
        $response->assertStatus(201);
        $response->assertJsonPath('data.name', $params['name']);
    }

    public function test_edit_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $params = [
            'name' => 'ThisIsUnitName',
            'address' => 'ShrtCode',
        ];

        $response = $this->putJson("{$this->resourceEndpoint()}/{$obj->id}", $params);
        $response->assertStatus(200);
        $response->assertJsonPath('data.name', $params['name']);
    }
}
