<?php
namespace Tests\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface BREADResourceInterface {
    public function resourceEndpoint(): string;
    public function resourceModel(): Model;
}