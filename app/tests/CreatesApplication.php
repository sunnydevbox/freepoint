<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $this->withHeaders([
            'content-type' => 'application/json',
            'Accept' => 'application/json',
            'x-tenant' => 123,
            'Authorization' => '1233'
        ]);

        return $app;
    }
}
