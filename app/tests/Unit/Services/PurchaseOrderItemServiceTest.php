<?php

namespace Tests\Unit\Services;

use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\Purchase;
use App\Models\Warehouse;
use App\Services\PurchaseOrderService;
use Exception;
use Tests\TestCase;

class PurchaseOrderItemServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_received_successful()
    {
        // Create PO record
        $purchase = Purchase::factory()
            ->create(
                [
                    'payment_status' => 1
                ]
            );

        $purchaseOrderItem = $purchase
            ->items()
            ->save(
                ProductPurchase::factory()
                    ->create([
                        'purchase_id' => $purchase->id,
                        'qty' => 10,
                    ])
            );


        $service = (new PurchaseOrderService);
        $receivedQty = 1;
        $originalPurchaseOrderItemQty = $purchaseOrderItem->qty;
        $originalProductQty = $purchaseOrderItem->product->qty;
        $originalPurchaseOrderReceivedQty = $purchaseOrderItem->recieved;

        $purchaseOrderItem = $service->received($purchaseOrderItem, $receivedQty);

        $newPurchaseOrderItemQty = $purchaseOrderItem->qty;
        $newProductQty = $purchaseOrderItem->product->qty;

        // dd($purchaseOrderItem, $newPurchaseOrderItemQty, $originalPurchaseOrderItemQty, $receivedQty);
        // $originalPurchaseOrderReceivedQty + $howmuch
        // // Asserts that the product_purchase record was persisted and with correct increment
        // $this->assertTrue($newPurchaseOrderItemQty == ($originalPurchaseOrderItemQty + $receivedQty));

        // Asserts that the global product record was updated and with correct increment
        $this->assertTrue($newProductQty == $originalProductQty + $receivedQty);


        // dd($purchaseOrderItem->toArray());
        // dd($originalQty,$receivedQty, $purchaseOrderItem);
        // dd($originalPurchaseOrderItemQty, $originalProductQty, $receivedQty, $purchaseOrderItem->product->qty);


        // expect ProductWarehouse record to exist;
        // expect purchaseOPrderItem->received to increment
        // expect current product qty = current product
        // expect product qty increments

        $this->assertTrue(true);
    }

    public function test_expect_exception_from_received_qty_that_exceeds()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessageMatches('/exceeds/');

        $purchase = Purchase::factory()
            ->create(
                [
                    'payment_status' => 1
                ]
            );

        $purchaseOrderItem = $purchase
            ->items()
            ->save(
                ProductPurchase::factory()
                    ->create([
                        'purchase_id' => $purchase->id,
                        'qty' => 11,
                    ])
            );


        $service = (new PurchaseOrderService);
        $receivedQty = $purchaseOrderItem->qty + 999;

        $purchaseOrderItem = $service->received($purchaseOrderItem, $receivedQty);
    }

    public function test_expect_exception_from_passing_non_numeric_qty_to_receive_method()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessageMatches('/type/');

        $purchase = Purchase::factory()
            ->create(
                [
                    'payment_status' => 1
                ]
            );

        $purchaseOrderItem = $purchase
            ->items()
            ->save(
                ProductPurchase::factory()
                    ->create([
                        'purchase_id' => $purchase->id,
                        'qty' => 11,
                    ])
            );


        $service = (new PurchaseOrderService);
        $receivedQty = 'ASD';


        $purchaseOrderItem = $service->received($purchaseOrderItem, $receivedQty);
    }

    public function test_expect_exception_from_passing_invalid_warehouse_bin_id_to_receive_method()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessageMatches('/warehouse bin/');
        $purchase = Purchase::factory()
            ->create(
                [
                    'payment_status' => 1
                ]
            );

        $purchaseOrderItem = $purchase
            ->items()
            ->save(
                ProductPurchase::factory()
                    ->create([
                        'purchase_id' => $purchase->id,
                        'qty' => 11,
                    ])
            );


        $service = (new PurchaseOrderService);
        $receivedQty = 1;

        // Invalidated the warehouse_bin_id
        $purchaseOrderItem->warehouse_bin_id = null;
        $purchaseOrderItem->save();

        $purchaseOrderItem = $service->received($purchaseOrderItem, $receivedQty);
    }

    // public function test_validate_item_recalculation_of_item_total()
    // {
    //     $purchase = Purchase::factory()
    //         ->create(
    //             [
    //                 'payment_status' => 1
    //             ]
    //         );

    //     $purchaseOrderItem1 = $purchase
    //         ->items()
    //         ->save(
    //             ProductPurchase::factory()
    //                 // ->count(10)
    //                 ->create([
    //                     'purchase_id' => $purchase->id,
    //                     'qty' => 11,
    //                 ])
    //         );

    
    //     $currentTotalCost = $purchaseOrderItem1->total;
    //     $newQty = $purchaseOrderItem1->qty + rand(9, 25);
        
    //     // $expectedNewTotal = 
    //     $purchaseOrderItem2 = (new PurchaseOrderService)->itemQuantityChange($purchaseOrderItem1, $newQty);
    //     // dd ($purchaseOrderItem1);
    //     // $newTotalCost = $purchaseOrderItem2->total;
    //     dd($purchaseOrderItem1->qty, 
    //         $purchaseOrderItem2->qty, 
    //         $newQty, 
    //         $currentTotalCost,
    //         $purchaseOrderItem2->total
    //     );


    //     // dd($purchaseOrderItems->first()->product);

    // }
}
