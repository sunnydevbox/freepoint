<?php
namespace Tests\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

trait BaseBREADResourceFeatureTestTrait
{
    public function test_browse()
    {
        $this->_getToken();
        $response = $this->getJson("{$this->resourceEndpoint()}?page=1");

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'current_page',
                'total',
                'per_page',
            ]
        ]);
    }

    public function test_browse_invalid_paramters()
    {
        $this->_getToken();

        $response = $this->getJson("{$this->resourceEndpoint()}?page=asd&limit=lkjlkj");

        $response->assertStatus(422);
    }

    public function test_browse_next_page()
    {
        $this->_getToken();
        $page = 2;

        $response = $this->getJson("{$this->resourceEndpoint()}?page={$page}");

        // dd($response->json());
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'current_page',
                'total',
                'per_page',
            ]
        ]);

        $response->assertJsonPath('meta.current_page', $page);
    }


    public function test_read_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $response = $this->getJson("{$this->resourceEndpoint()}/{$obj->id}");

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
            ]
        ]);

        $response->assertJsonPath('data.id', $obj->id);
    }

    public function test_edit_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();
        $params = [
            'title' => 'ThisIsNewBrand',
        ];

        $response = $this->putJson("{$this->resourceEndpoint()}/{$obj->id}", $params);
        $response->assertStatus(200);
        $response->assertJsonPath('data.title', $params['title']);
    }


    public function test_add_success()
    {
        $this->_getToken();
        $params = [
            'title' => 'BrandName',
        ];

        $response = $this->postJson($this->resourceEndpoint(), $params);
        $response->assertStatus(201);
        $response->assertJsonPath('data.title', $params['title']);
    }

    public function test_destory_success()
    {
        $this->_getToken();
        $obj = $this->resourceModel()::inRandomOrder()->first();

        $response = $this->deleteJson("{$this->resourceEndpoint()}/{$obj->id}");
        $response->assertStatus(204);
    }


    public function test_add_with_invalid_name()
    {
        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);
        $this->_getToken();
        $params = [
            'titleee' => 'BrandName',
        ];

        $response = $this->postJson($this->resourceEndpoint(), $params);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => ['title'],
        ]);
    }

    public function test_read_not_found()
    {
        $this->withoutExceptionHandling();
        $this->expectException(ModelNotFoundException::class);
        $this->_getToken();
        $id = 999999;
        $response = $this->getJson("{$this->resourceEndpoint()}/{$id}}");
        $response->assertStatus(404);
    }
}
